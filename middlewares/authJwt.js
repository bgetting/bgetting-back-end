const debug = require('debug')('express:login');
const knex = require('../db');
const jwt = require('jsonwebtoken');
const { secretKey } = require('../config/auth.config');
const { accessToken } = require('../middlewares/repository');
const {
  INTERNAL_SERVER_ERROR,
  NO_TOKEN_PRIVIDED,
  UNAUTHORIZED,
} = require('../constants/authJwtConstants');

// verifyToken = (req, res, next) => {
async function verifyToken(req, res, next) {
  const authHeader = req.headers['authorization'];
  let token = authHeader && authHeader.split(' ')[1];

  if (!token) {
    return res.status(403).send({
      message: NO_TOKEN_PRIVIDED,
    });
  }

  try {
    getToken = await accessToken(token);
    if(!getToken){
      return res.status(403).send({
        message: UNAUTHORIZED,
      });
    }
  } catch (error) {
    return res.status(403).send({
      message: NO_TOKEN_PRIVIDED,
    });
  }

  jwt.verify(token, secretKey.secret, (err, decoded) => {
    if (err) {
      return res.status(401).send({
        message: UNAUTHORIZED,
      });
    }
    req.userId = decoded.id;
    next();
  });
};

const verifySecretHeader = (req, res, next) => {
  const {oauthid, oauthsecret }= req.headers;
  if (oauthid === secretKey.oauthId && oauthsecret === secretKey.oauthSecret) {
    next();
    return;
  } else {
    const messages = {
      errors: {
        invalidOauth: INTERNAL_SERVER_ERROR,
      },
    };
    return res.status(403).send({ success: false, messages });
  }
};

const verifySecret = (req, res, next) => {
  if (req.body.oauthId === secretKey.oauthId && req.body.oauthSecret === secretKey.oauthSecret) {
    next();
    return;
  } else {
    const messages = {
      errors: {
        invalidOauth: INTERNAL_SERVER_ERROR,
      },
    };
    return res.status(403).send({ success: false, messages });
  }
};


const authJwt = {
  verifyToken: verifyToken,
  verifySecret: verifySecret,
  verifySecretHeader: verifySecretHeader,
};
module.exports = authJwt;
