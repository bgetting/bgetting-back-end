const knex = require('../db');
const debug = require('debug')('express:login');
async function accessToken(token) {
  // debug(token)
  const [user] = await knex('log_users')
    .where('access_token', token)
    .select('*');
    // debug(user)
  return user;
}


module.exports = {
  accessToken,
};
