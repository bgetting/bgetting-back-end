/*
PostgreSQL Backup
Database: bgetting/public
Backup Time: 2022-07-08 09:57:50
*/

DROP SEQUENCE IF EXISTS "public"."feedbacks_id_seq";
DROP SEQUENCE IF EXISTS "public"."log_users_id_seq";
DROP SEQUENCE IF EXISTS "public"."migrations_id_seq";
DROP SEQUENCE IF EXISTS "public"."migrations_lock_index_seq";
DROP SEQUENCE IF EXISTS "public"."mt_lov_id_seq";
DROP SEQUENCE IF EXISTS "public"."register_verify_id_seq";
DROP SEQUENCE IF EXISTS "public"."users_id_seq";
DROP SEQUENCE IF EXISTS "public"."video_playlist_id_seq";
DROP SEQUENCE IF EXISTS "public"."videos_action_id_seq";
DROP SEQUENCE IF EXISTS "public"."videos_channel_id_seq";
DROP SEQUENCE IF EXISTS "public"."videos_history_id_seq";
DROP SEQUENCE IF EXISTS "public"."videos_id_seq";
DROP SEQUENCE IF EXISTS "public"."videos_playlist_add_id_seq";
DROP SEQUENCE IF EXISTS "public"."videos_tags_id_seq";
DROP SEQUENCE IF EXISTS "public"."videos_views_id_seq";
DROP SEQUENCE IF EXISTS "public"."videos_watch_tags_id_seq";
DROP TABLE IF EXISTS "public"."auth_provider";
DROP TABLE IF EXISTS "public"."auth_user_provider";
DROP TABLE IF EXISTS "public"."device";
DROP TABLE IF EXISTS "public"."feedbacks";
DROP TABLE IF EXISTS "public"."log_users";
DROP TABLE IF EXISTS "public"."migrations";
DROP TABLE IF EXISTS "public"."migrations_lock";
DROP TABLE IF EXISTS "public"."mt_lov";
DROP TABLE IF EXISTS "public"."register_verify";
DROP TABLE IF EXISTS "public"."users";
DROP TABLE IF EXISTS "public"."videos";
DROP TABLE IF EXISTS "public"."videos_action";
DROP TABLE IF EXISTS "public"."videos_channel";
DROP TABLE IF EXISTS "public"."videos_history";
DROP TABLE IF EXISTS "public"."videos_playlist";
DROP TABLE IF EXISTS "public"."videos_playlist_add";
DROP TABLE IF EXISTS "public"."videos_tags";
DROP TABLE IF EXISTS "public"."videos_views";
DROP TABLE IF EXISTS "public"."videos_watch_tags";
DROP VIEW IF EXISTS "public"."vw_channel_videos_info";
DROP VIEW IF EXISTS "public"."vw_like_unlike";
DROP VIEW IF EXISTS "public"."vw_like_unlike_not_login";
DROP VIEW IF EXISTS "public"."vw_search_title";
DROP VIEW IF EXISTS "public"."vw_users";
DROP VIEW IF EXISTS "public"."vw_videos_history";
DROP VIEW IF EXISTS "public"."vw_videos_master";
DROP VIEW IF EXISTS "public"."vw_videos_master_list";
DROP VIEW IF EXISTS "public"."vw_videos_master_playlist";
DROP VIEW IF EXISTS "public"."vw_videos_no_watch_tags";
DROP VIEW IF EXISTS "public"."vw_videos_playlist";
DROP VIEW IF EXISTS "public"."vw_videos_playlist_watch";
DROP VIEW IF EXISTS "public"."vw_videos_watch_tags";
DROP FUNCTION IF EXISTS "public"."fn_decimal_million(value_number int4)";
DROP FUNCTION IF EXISTS "public"."fn_decimal_million_subscribe(value_number int4)";
DROP FUNCTION IF EXISTS "public"."fn_time_ago(value_time timestamp)";
CREATE SEQUENCE "feedbacks_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE SEQUENCE "log_users_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE SEQUENCE "migrations_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE SEQUENCE "migrations_lock_index_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE SEQUENCE "mt_lov_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE SEQUENCE "register_verify_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE SEQUENCE "users_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE SEQUENCE "video_playlist_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE SEQUENCE "videos_action_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE SEQUENCE "videos_channel_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE SEQUENCE "videos_history_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE SEQUENCE "videos_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE SEQUENCE "videos_playlist_add_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE SEQUENCE "videos_tags_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE SEQUENCE "videos_views_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE SEQUENCE "videos_watch_tags_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE TABLE "auth_provider" (
  "auth_provider_id" int8 NOT NULL,
  "provider_type" varchar(255) COLLATE "pg_catalog"."default",
  "created_by" varchar(150) COLLATE "pg_catalog"."default",
  "created_at" timestamptz(6) DEFAULT CURRENT_TIMESTAMP,
  "updated_by" varchar(150) COLLATE "pg_catalog"."default",
  "updated_at" timestamptz(6) DEFAULT CURRENT_TIMESTAMP,
  "status" bool DEFAULT true
)
;
ALTER TABLE "auth_provider" OWNER TO "postgres";
CREATE TABLE "auth_user_provider" (
  "auth_user_provider_id" int8 NOT NULL DEFAULT nextval('users_id_seq'::regclass),
  "created_by" varchar(150) COLLATE "pg_catalog"."default",
  "created_at" timestamp(6),
  "updated_by" varchar(150) COLLATE "pg_catalog"."default",
  "updated_at" timestamp(6),
  "auth_provider_id" int8,
  "auth_user_id" int8
)
;
ALTER TABLE "auth_user_provider" OWNER TO "postgres";
CREATE TABLE "device" (
  "device_id" int8 NOT NULL,
  "browser" varchar(150) COLLATE "pg_catalog"."default",
  "ip_address" varchar(150) COLLATE "pg_catalog"."default",
  "model" varchar(150) COLLATE "pg_catalog"."default",
  "name" varchar(150) COLLATE "pg_catalog"."default",
  "version" varchar(150) COLLATE "pg_catalog"."default",
  "created_by" varchar(150) COLLATE "pg_catalog"."default",
  "created_at" timestamptz(6),
  "updated_by" varchar(150) COLLATE "pg_catalog"."default",
  "updated_at" timestamptz(6)
)
;
ALTER TABLE "device" OWNER TO "postgres";
CREATE TABLE "feedbacks" (
  "id" int4 NOT NULL GENERATED ALWAYS AS IDENTITY (
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
),
  "feedback" varchar(255) COLLATE "pg_catalog"."default",
  "image" text COLLATE "pg_catalog"."default",
  "created_at" timestamptz(6),
  "updated_at" timestamptz(6)
)
;
ALTER TABLE "feedbacks" OWNER TO "postgres";
CREATE TABLE "log_users" (
  "id" int4 NOT NULL GENERATED ALWAYS AS IDENTITY (
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
),
  "users_id" int4,
  "access_token" text COLLATE "pg_catalog"."default",
  "expires" varchar(255) COLLATE "pg_catalog"."default",
  "created_at" timestamptz(6),
  "updated_at" timestamptz(6)
)
;
ALTER TABLE "log_users" OWNER TO "postgres";
CREATE TABLE "migrations" (
  "id" int4 NOT NULL DEFAULT nextval('migrations_id_seq'::regclass),
  "name" varchar(255) COLLATE "pg_catalog"."default",
  "batch" int4,
  "migration_time" timestamptz(6)
)
;
ALTER TABLE "migrations" OWNER TO "postgres";
CREATE TABLE "migrations_lock" (
  "index" int4 NOT NULL DEFAULT nextval('migrations_lock_index_seq'::regclass),
  "is_locked" int4
)
;
ALTER TABLE "migrations_lock" OWNER TO "postgres";
CREATE TABLE "mt_lov" (
  "id" int4 NOT NULL GENERATED ALWAYS AS IDENTITY (
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
),
  "lov_code" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "name_kh" varchar(255) COLLATE "pg_catalog"."default",
  "name_en" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "type" varchar(255) COLLATE "pg_catalog"."default",
  "status" bool,
  "created_at" timestamptz(6),
  "updated_at" timestamptz(6),
  "icon" varchar(255) COLLATE "pg_catalog"."default",
  "image" varchar(500) COLLATE "pg_catalog"."default",
  "tags" varchar(500) COLLATE "pg_catalog"."default",
  "description" varchar(500) COLLATE "pg_catalog"."default",
  "order_by" varchar(255) COLLATE "pg_catalog"."default"
)
;
ALTER TABLE "mt_lov" OWNER TO "postgres";
CREATE TABLE "register_verify" (
  "id" int4 NOT NULL GENERATED BY DEFAULT AS IDENTITY (
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
),
  "phone_number" varchar(50) COLLATE "pg_catalog"."default",
  "code" varchar(255) COLLATE "pg_catalog"."default",
  "resend" int4 DEFAULT 1,
  "status" bool,
  "created_at" timestamptz(6),
  "updated_at" timestamptz(6)
)
;
ALTER TABLE "register_verify" OWNER TO "postgres";
CREATE TABLE "users" (
  "id" int8 NOT NULL DEFAULT nextval('users_id_seq'::regclass),
  "email_address" varchar(150) COLLATE "pg_catalog"."default",
  "username" varchar(150) COLLATE "pg_catalog"."default",
  "password" varchar(150) COLLATE "pg_catalog"."default" NOT NULL,
  "email_verified_at" timestamptz(6) DEFAULT CURRENT_TIMESTAMP,
  "created_at" timestamptz(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "updated_at" timestamptz(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "created_by" varchar(150) COLLATE "pg_catalog"."C",
  "updated_by" varchar(150) COLLATE "pg_catalog"."POSIX",
  "phone_number" varchar(30) COLLATE "pg_catalog"."default",
  "status" bool DEFAULT true,
  "image" text COLLATE "pg_catalog"."default",
  "verify_code" varchar(255) COLLATE "pg_catalog"."default",
  "verify_status" bool,
  "image_cover" text COLLATE "pg_catalog"."default",
  "image_path" varchar(255) COLLATE "pg_catalog"."default",
  "image_cover_path" varchar(255) COLLATE "pg_catalog"."default"
)
;
ALTER TABLE "users" OWNER TO "postgres";
CREATE TABLE "videos" (
  "id" int4 NOT NULL DEFAULT nextval('videos_id_seq'::regclass),
  "userid" int8 NOT NULL,
  "patch" text COLLATE "pg_catalog"."default",
  "status" bool DEFAULT true,
  "created_at" timestamptz(6),
  "updated_at" timestamptz(6),
  "created_by" varchar(255) COLLATE "pg_catalog"."default",
  "updated_by" varchar(255) COLLATE "pg_catalog"."default",
  "category" varchar(255) COLLATE "pg_catalog"."default",
  "title" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "filename" text COLLATE "pg_catalog"."default",
  "destination" text COLLATE "pg_catalog"."default",
  "originalname" text COLLATE "pg_catalog"."default",
  "size" varchar(255) COLLATE "pg_catalog"."default",
  "mimetype" varchar(150) COLLATE "pg_catalog"."default",
  "encoding" varchar(50) COLLATE "pg_catalog"."default",
  "duration" varchar(150) COLLATE "pg_catalog"."default",
  "video_info" json,
  "provider_name" varchar(255) COLLATE "pg_catalog"."default",
  "thumbnail_url" text COLLATE "pg_catalog"."default",
  "description" text COLLATE "pg_catalog"."default"
)
;
ALTER TABLE "videos" OWNER TO "postgres";
CREATE TABLE "videos_action" (
  "id" int4 NOT NULL DEFAULT nextval('videos_action_id_seq'::regclass),
  "userid" int8,
  "likes" int8 DEFAULT 0,
  "unlikes" int8 DEFAULT 0,
  "status" bool DEFAULT true,
  "created_at" timestamptz(6),
  "updated_at" timestamptz(6),
  "created_by" varchar(255) COLLATE "pg_catalog"."default",
  "updated_by" varchar(255) COLLATE "pg_catalog"."default",
  "videoid" int8
)
;
ALTER TABLE "videos_action" OWNER TO "postgres";
CREATE TABLE "videos_channel" (
  "id" int4 NOT NULL GENERATED ALWAYS AS IDENTITY (
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
),
  "userid_channel" int4,
  "subscribe_id" int4,
  "created_at" timestamptz(6),
  "updated_at" timestamptz(6)
)
;
ALTER TABLE "videos_channel" OWNER TO "postgres";
CREATE TABLE "videos_history" (
  "id" int4 NOT NULL GENERATED ALWAYS AS IDENTITY (
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
),
  "video_id" int4,
  "user_watch_id" int4,
  "created_at" timestamptz(6),
  "updated_at" timestamptz(6)
)
;
ALTER TABLE "videos_history" OWNER TO "postgres";
CREATE TABLE "videos_playlist" (
  "id" int4 NOT NULL GENERATED ALWAYS AS IDENTITY (
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
),
  "user_id" int4 NOT NULL,
  "title" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "description" varchar(255) COLLATE "pg_catalog"."default",
  "created_at" timestamptz(6),
  "updated_at" timestamptz(6)
)
;
ALTER TABLE "videos_playlist" OWNER TO "postgres";
CREATE TABLE "videos_playlist_add" (
  "id" int4 NOT NULL GENERATED ALWAYS AS IDENTITY (
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
),
  "playlist_id" int4 NOT NULL,
  "video_id" int4 NOT NULL,
  "order_by" varchar(255) COLLATE "pg_catalog"."default",
  "created_at" timestamptz(6),
  "updated_at" timestamptz(6)
)
;
ALTER TABLE "videos_playlist_add" OWNER TO "postgres";
CREATE TABLE "videos_tags" (
  "id" int4 NOT NULL GENERATED ALWAYS AS IDENTITY (
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
),
  "title_tags" varchar(255) COLLATE "pg_catalog"."default",
  "video_id" int4,
  "created_by" varchar(255) COLLATE "pg_catalog"."default",
  "created_at" timestamptz(6),
  "updated_by" varchar(255) COLLATE "pg_catalog"."default",
  "updated_at" timestamptz(6)
)
;
ALTER TABLE "videos_tags" OWNER TO "postgres";
CREATE TABLE "videos_views" (
  "id" int4 NOT NULL GENERATED ALWAYS AS IDENTITY (
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
),
  "video_id" int4,
  "views" int4
)
;
ALTER TABLE "videos_views" OWNER TO "postgres";
CREATE TABLE "videos_watch_tags" (
  "id" int4 NOT NULL GENERATED ALWAYS AS IDENTITY (
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
),
  "title_tags" varchar(255) COLLATE "pg_catalog"."default",
  "video_watch_id" int4,
  "user_watch_id" int4,
  "created_by" varchar(255) COLLATE "pg_catalog"."default",
  "created_at" timestamptz(6),
  "updated_by" varchar(255) COLLATE "pg_catalog"."default",
  "updated_at" timestamptz(6)
)
;
ALTER TABLE "videos_watch_tags" OWNER TO "postgres";
CREATE VIEW "vw_channel_videos_info" AS  SELECT a.id AS user_id,
    fn_decimal_million_subscribe(bb.videos_number::integer) AS videos_number,
    fn_decimal_million_subscribe(cc.subscribe_number::integer) AS subscribe_number,
    fn_decimal_million_subscribe(dd.liked::integer) AS liked,
    fn_decimal_million_subscribe(ee.unliked::integer) AS unliked
   FROM users a
     LEFT JOIN ( SELECT b.userid,
            count(b.userid) AS videos_number
           FROM videos b
          GROUP BY b.userid) bb ON a.id = bb.userid
     LEFT JOIN ( SELECT c.userid_channel,
            count(c.userid_channel) AS subscribe_number
           FROM videos_channel c
          GROUP BY c.userid_channel) cc ON a.id = cc.userid_channel
     LEFT JOIN ( SELECT d.userid,
            count(d.userid) AS liked
           FROM videos_action d
          WHERE d.likes = 1
          GROUP BY d.userid) dd ON a.id = dd.userid
     LEFT JOIN ( SELECT e.userid,
            count(e.userid) AS unliked
           FROM videos_action e
          WHERE e.unlikes = 1
          GROUP BY e.userid) ee ON a.id = ee.userid;
ALTER TABLE "vw_channel_videos_info" OWNER TO "postgres";
CREATE VIEW "vw_like_unlike" AS  SELECT a.id AS video_id,
    b.userid,
    sum(b.likes) AS likes,
    COALESCE(c.likes_view::bigint, 0::bigint) AS likes_view,
    sum(b.unlikes) AS unlikes,
    COALESCE(d.unlikes_view::bigint, 0::bigint) AS unlikes_view
   FROM videos a
     LEFT JOIN videos_action b ON a.id = b.videoid
     LEFT JOIN ( SELECT aa.videoid,
            sum(aa.likes) AS likes_view
           FROM videos_action aa
          WHERE aa.likes = 1
          GROUP BY aa.videoid) c ON a.id = c.videoid
     LEFT JOIN ( SELECT bb.videoid,
            sum(bb.unlikes) AS unlikes_view
           FROM videos_action bb
          WHERE bb.unlikes = 1
          GROUP BY bb.videoid) d ON a.id = d.videoid
  GROUP BY a.id, b.userid, c.likes_view, d.unlikes_view;
ALTER TABLE "vw_like_unlike" OWNER TO "postgres";
CREATE VIEW "vw_like_unlike_not_login" AS  SELECT a.id AS video_id,
    COALESCE(c.likes_view::bigint, 0::bigint) AS likes_view,
    COALESCE(d.unlikes_view::bigint, 0::bigint) AS unlikes_view
   FROM videos a
     LEFT JOIN videos_action b ON a.id = b.videoid
     LEFT JOIN ( SELECT aa.videoid,
            sum(aa.likes) AS likes_view
           FROM videos_action aa
          WHERE aa.likes = 1
          GROUP BY aa.videoid) c ON a.id = c.videoid
     LEFT JOIN ( SELECT bb.videoid,
            sum(bb.unlikes) AS unlikes_view
           FROM videos_action bb
          WHERE bb.unlikes = 1
          GROUP BY bb.videoid) d ON a.id = d.videoid
  GROUP BY a.id, c.likes_view, d.unlikes_view;
ALTER TABLE "vw_like_unlike_not_login" OWNER TO "postgres";
CREATE VIEW "vw_search_title" AS  SELECT DISTINCT row_number() OVER () AS id,
    c.video_id,
    c.title,
    c.type
   FROM ( SELECT DISTINCT a.title,
            a.id AS video_id,
            'title'::text AS type
           FROM videos a
        UNION ALL
         SELECT DISTINCT b.title_tags AS title,
            b.video_id,
            'tags'::text AS type
           FROM videos_tags b) c;
ALTER TABLE "vw_search_title" OWNER TO "postgres";
CREATE VIEW "vw_users" AS  SELECT a.id,
    a.username,
    a.phone_number,
    a.status,
    a.image,
    a.created_at,
    a.updated_at,
    b.auth_provider_id,
    c.provider_type
   FROM users a
     LEFT JOIN auth_user_provider b ON a.id = b.auth_user_id
     LEFT JOIN auth_provider c ON b.auth_provider_id = c.auth_provider_id;
ALTER TABLE "vw_users" OWNER TO "postgres";
CREATE VIEW "vw_videos_history" AS  SELECT b.rownumvideo,
    a.rownum,
    a.id,
    a.email_address,
    a.username,
    a.phone_number,
    a.email_verified_at,
    a.user_status,
    a.user_created_at,
    a.img_profile,
    a.image_cover,
    a.image_path,
    a.image_cover_path,
    a.video_id,
    a.video_patch,
    a.thumbnail,
    a.video_status,
    a.title,
    a.filename,
    a.destination,
    a.originalname,
    a.size,
    a.video_created_at,
    a.duration,
    a.time_agos,
    a.provider_name,
    a.views,
    a.subscribe,
    b.user_watch_id,
    a.description,
    b.created_at
   FROM ( SELECT row_number() OVER (ORDER BY bb.video_id) AS rownumvideo,
            bb.video_id,
            bb.user_watch_id,
            bb.created_at
           FROM videos_history bb
          GROUP BY bb.video_id, bb.user_watch_id, bb.created_at) b
     LEFT JOIN vw_videos_master a ON b.video_id = a.video_id
  ORDER BY b.user_watch_id, b.created_at DESC;
ALTER TABLE "vw_videos_history" OWNER TO "postgres";
CREATE VIEW "vw_videos_master" AS  SELECT a.rownum,
    a.id,
    a.email_address,
    a.username,
    a.phone_number,
    a.email_verified_at,
    a.user_status,
    a.user_created_at,
    a.img_profile,
    a.image_cover,
    a.image_path,
    a.image_cover_path,
    a.video_id,
    a.video_patch,
    a.thumbnail,
    a.video_status,
    a.title,
    a.filename,
    a.destination,
    a.originalname,
    a.size,
    a.video_created_at,
    a.duration,
    a.time_agos,
    a.provider_name,
    fn_decimal_million(a.views::integer) AS views,
    a.subscribe,
    a.description
   FROM ( SELECT row_number() OVER (ORDER BY b.id DESC) AS rownum,
            a_1.id,
            a_1.email_address,
            a_1.username,
            a_1.phone_number,
            a_1.email_verified_at,
            a_1.status AS user_status,
            a_1.created_at AS user_created_at,
            a_1.image AS img_profile,
            a_1.image_cover,
            a_1.image_path,
            a_1.image_cover_path,
            b.provider_name,
            b.id AS video_id,
            (b.destination || '/'::text) || b.filename AS video_patch,
            ((b.destination || '/thumbnail/'::text) || b.filename) || '_thumbnail.png'::text AS thumbnail,
            b.status AS video_status,
            b.title,
            b.filename,
            b.destination,
            b.originalname,
            b.size,
            b.created_at AS video_created_at,
            b.duration,
            fn_time_ago(b.created_at::timestamp without time zone) AS time_agos,
            COALESCE(d.views::bigint, 0::bigint) AS views,
            fn_decimal_million_subscribe(ab.subscribe::integer) AS subscribe,
            b.description
           FROM users a_1
             JOIN videos b ON a_1.id = b.userid
             LEFT JOIN videos_views d ON b.id = d.video_id
             LEFT JOIN ( SELECT aa.userid_channel,
                    count(aa.userid_channel) AS subscribe
                   FROM videos_channel aa
                  GROUP BY aa.userid_channel) ab ON a_1.id = ab.userid_channel) a;
ALTER TABLE "vw_videos_master" OWNER TO "postgres";
CREATE VIEW "vw_videos_master_list" AS  SELECT a.id AS playlist_id,
    a.title AS title_playlist,
    a.description AS desc_playlist,
    bb.video_number,
    v.rownum,
    v.id,
    v.email_address,
    v.username,
    v.phone_number,
    v.email_verified_at,
    v.user_status,
    v.user_created_at,
    v.img_profile,
    v.image_cover,
    v.image_path,
    v.image_cover_path,
    v.video_id,
    v.video_patch,
    v.thumbnail,
    v.video_status,
    v.title,
    v.filename,
    v.destination,
    v.originalname,
    v.size,
    v.video_created_at,
    v.duration,
    v.time_agos,
    v.provider_name,
    v.views,
    v.subscribe,
    v.description
   FROM videos_playlist a
     LEFT JOIN ( SELECT count(b.playlist_id) AS video_number,
            b.playlist_id
           FROM videos_playlist_add b
          GROUP BY b.playlist_id) bb ON a.id = bb.playlist_id
     LEFT JOIN ( SELECT h.playlist_id,
            h.video_id
           FROM ( SELECT y.playlist_id,
                    y.video_id,
                    rank() OVER (PARTITION BY y.playlist_id ORDER BY y.video_id DESC) AS rank_number
                   FROM videos_playlist_add y) h
          WHERE h.rank_number = 1) f ON a.id = f.playlist_id
     RIGHT JOIN vw_videos_master v ON f.video_id = v.video_id;
ALTER TABLE "vw_videos_master_list" OWNER TO "postgres";
CREATE VIEW "vw_videos_master_playlist" AS  SELECT l.playlist_id,
    y.title_playlist,
    y.desc_playlist,
    y.video_number,
    y.rownum,
    y.id,
    y.email_address,
    y.username,
    y.phone_number,
    y.email_verified_at,
    y.user_status,
    y.user_created_at,
    y.img_profile,
    y.image_cover,
    y.image_path,
    y.image_cover_path,
    y.video_id,
    y.video_patch,
    y.thumbnail,
    y.video_status,
    y.title,
    y.filename,
    y.destination,
    y.originalname,
    y.size,
    y.video_created_at,
    y.duration,
    y.time_agos,
    y.provider_name,
    y.views,
    y.subscribe,
    y.description
   FROM videos_playlist_add l
     LEFT JOIN ( SELECT a.title AS title_playlist,
            a.description AS desc_playlist,
            ''::text AS video_number,
            v.rownum,
            v.id,
            v.email_address,
            v.username,
            v.phone_number,
            v.email_verified_at,
            v.user_status,
            v.user_created_at,
            v.img_profile,
            v.image_cover,
            v.image_path,
            v.image_cover_path,
            v.video_id,
            v.video_patch,
            v.thumbnail,
            v.video_status,
            v.title,
            v.filename,
            v.destination,
            v.originalname,
            v.size,
            v.video_created_at,
            v.duration,
            v.time_agos,
            v.provider_name,
            v.views,
            v.subscribe,
            v.description
           FROM videos_playlist a
             LEFT JOIN ( SELECT h.playlist_id,
                    h.video_id
                   FROM ( SELECT y_1.playlist_id,
                            y_1.video_id,
                            rank() OVER (PARTITION BY y_1.playlist_id ORDER BY y_1.video_id DESC) AS rank_number
                           FROM videos_playlist_add y_1) h
                  WHERE h.rank_number = 1) f ON a.id = f.playlist_id
             RIGHT JOIN vw_videos_master v ON f.video_id = v.video_id) y ON l.video_id = y.video_id;
ALTER TABLE "vw_videos_master_playlist" OWNER TO "postgres";
CREATE VIEW "vw_videos_no_watch_tags" AS  SELECT row_number() OVER () AS rownum,
    c.title_tags
   FROM ( SELECT b.title_tags
           FROM ( SELECT a.title_tags,
                    count(a.title_tags) AS count_tags
                   FROM videos_watch_tags a
                  GROUP BY a.title_tags, a.video_watch_id) b
          ORDER BY b.count_tags DESC) c
  GROUP BY c.title_tags;
ALTER TABLE "vw_videos_no_watch_tags" OWNER TO "postgres";
CREATE VIEW "vw_videos_playlist" AS  SELECT a.id AS playlist_id,
    a.title AS title_playlist,
    a.description AS desc_playlist,
    bb.video_number,
    v.rownum,
    v.id,
    v.email_address,
    v.username,
    v.phone_number,
    v.email_verified_at,
    v.user_status,
    v.user_created_at,
    v.img_profile,
    v.image_cover,
    v.image_path,
    v.image_cover_path,
    v.video_id,
    v.video_patch,
    v.thumbnail,
    v.video_status,
    v.title,
    v.filename,
    v.destination,
    v.originalname,
    v.size,
    v.video_created_at,
    v.duration,
    v.time_agos,
    v.provider_name,
    v.views,
    v.subscribe,
    v.description
   FROM videos_playlist a
     LEFT JOIN ( SELECT count(b.playlist_id) AS video_number,
            b.playlist_id
           FROM videos_playlist_add b
          GROUP BY b.playlist_id) bb ON a.id = bb.playlist_id
     LEFT JOIN ( SELECT h.playlist_id,
            h.video_id
           FROM ( SELECT y.playlist_id,
                    y.video_id,
                    rank() OVER (PARTITION BY y.playlist_id ORDER BY y.video_id DESC) AS rank_number
                   FROM videos_playlist_add y) h
          WHERE h.rank_number = 1) f ON a.id = f.playlist_id
     LEFT JOIN vw_videos_master v ON f.video_id = v.video_id;
ALTER TABLE "vw_videos_playlist" OWNER TO "postgres";
CREATE VIEW "vw_videos_playlist_watch" AS  SELECT a.id AS playlist_id,
    a.title AS title_playlist,
    a.description AS desc_playlist,
    v.rownum,
    v.id,
    v.email_address,
    v.username,
    v.phone_number,
    v.email_verified_at,
    v.user_status,
    v.user_created_at,
    v.img_profile,
    v.image_cover,
    v.image_path,
    v.image_cover_path,
    v.video_id,
    v.video_patch,
    v.thumbnail,
    v.video_status,
    v.title,
    v.filename,
    v.destination,
    v.originalname,
    v.size,
    v.video_created_at,
    v.duration,
    v.time_agos,
    v.provider_name,
    v.views,
    v.subscribe,
    v.description
   FROM videos_playlist a
     LEFT JOIN videos_playlist_add b ON a.id = b.playlist_id
     LEFT JOIN vw_videos_master v ON b.video_id = v.video_id;
ALTER TABLE "vw_videos_playlist_watch" OWNER TO "postgres";
CREATE VIEW "vw_videos_watch_tags" AS  SELECT row_number() OVER () AS rownum,
    c.title_tags,
    c.user_watch_id
   FROM ( SELECT b.title_tags,
            b.user_watch_id,
            b.count_tags
           FROM ( SELECT a.title_tags,
                    a.user_watch_id,
                    count(a.title_tags) AS count_tags
                   FROM videos_watch_tags a
                  GROUP BY a.title_tags, a.user_watch_id, a.video_watch_id) b
          ORDER BY b.count_tags DESC) c
  GROUP BY c.title_tags, c.user_watch_id;
ALTER TABLE "vw_videos_watch_tags" OWNER TO "postgres";
CREATE OR REPLACE FUNCTION "fn_decimal_million"("value_number" int4)
  RETURNS "pg_catalog"."bpchar" AS $BODY$
DECLARE decimal_number varchar;
BEGIN
decimal_number := (CASE
	WHEN value_number < 1000 THEN CONCAT(value_number::FLOAT, ' ', 'Views')
	WHEN 1000 <= value_number AND value_number < 1000000 THEN CONCAT(value_number::FLOAT / 1000 , 'K')
	WHEN 1000000 <= value_number THEN CONCAT(value_number::FLOAT / 1000000, 'M')
	END);
RETURN decimal_number;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION "fn_decimal_million"("value_number" int4) OWNER TO "postgres";
CREATE OR REPLACE FUNCTION "fn_decimal_million_subscribe"("value_number" int4)
  RETURNS "pg_catalog"."bpchar" AS $BODY$
DECLARE decimal_number varchar;
BEGIN
decimal_number := (CASE
	WHEN value_number < 1000 THEN CONCAT(value_number::FLOAT)
	WHEN 1000 <= value_number AND value_number < 1000000 THEN CONCAT(value_number::FLOAT / 1000 , 'K')
	WHEN 1000000 <= value_number THEN CONCAT(value_number::FLOAT / 1000000, 'M')
	END);
RETURN decimal_number;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION "fn_decimal_million_subscribe"("value_number" int4) OWNER TO "postgres";
CREATE OR REPLACE FUNCTION "fn_time_ago"("value_time" timestamp)
  RETURNS "pg_catalog"."bpchar" AS $BODY$
	DECLARE ago varchar;
BEGIN	

ago := (CASE
					WHEN ROUND(EXTRACT(EPOCH FROM (now() - value_time))) <= 60 THEN '0 just now'
					WHEN ROUND(EXTRACT(EPOCH FROM (now() - value_time) / 60)) <= 60 THEN (CASE WHEN ROUND(EXTRACT(EPOCH FROM (now() - value_time) / 60)) = 1 THEN '1 minute ago' ELSE (CONCAT(ROUND(EXTRACT(EPOCH FROM (now() - value_time) / 60)),' ','minutes ago')) END)
					WHEN ROUND(EXTRACT(EPOCH FROM (now() - value_time) / 3600)) <= 24 THEN (CASE WHEN ROUND(EXTRACT(EPOCH FROM (now() - value_time) / 3600)) = 1 THEN '1 hour ago' ELSE (CONCAT(ROUND(EXTRACT(EPOCH FROM (now() - value_time) / 3600)),' ','hours ago')) END)
					WHEN ROUND(EXTRACT(EPOCH FROM (now() - value_time) / 86400)) <= 7 THEN (CASE WHEN ROUND(EXTRACT(EPOCH FROM (now() - value_time) / 86400)) = 1 THEN 'Yesterday' ELSE (CONCAT(ROUND(EXTRACT(EPOCH FROM (now() - value_time) / 86400)),' ','days ago')) END)
					WHEN ROUND(EXTRACT(EPOCH FROM (now() - value_time) / 604800)) <= 4.3 THEN (CASE WHEN ROUND(EXTRACT(EPOCH FROM (now() - value_time) / 604800)) = 1 THEN '1 week ago' ELSE (CONCAT(ROUND(EXTRACT(EPOCH FROM (now() - value_time) / 604800)),' ','weeks ago')) END)
					WHEN ROUND(EXTRACT(EPOCH FROM (now() - value_time) / 2600640)) <= 12 THEN (CASE WHEN ROUND(EXTRACT(EPOCH FROM (now() - value_time) / 2600640)) = 1 THEN '1 month ago' ELSE (CONCAT(ROUND(EXTRACT(EPOCH FROM (now() - value_time) / 2600640)),' ','months ago')) END)
					ELSE (CASE WHEN ROUND(EXTRACT(EPOCH FROM (now() - value_time) / 31207680)) = 1 THEN '1 year ago' ELSE (CONCAT(ROUND(EXTRACT(EPOCH FROM (now() - value_time) / 31207680)),' ','years ago')) END) END 
				  );
RETURN ago;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION "fn_time_ago"("value_time" timestamp) OWNER TO "postgres";
BEGIN;
LOCK TABLE "public"."auth_provider" IN SHARE MODE;
DELETE FROM "public"."auth_provider";
INSERT INTO "public"."auth_provider" ("auth_provider_id","provider_type","created_by","created_at","updated_by","updated_at","status") VALUES (1, 'PHONE_NUMBER', 'admin', NULL, NULL, NULL, 't'),(2, 'EMAIL', 'admin', NULL, NULL, NULL, 't'),(3, 'FACEBOOK', 'admin', NULL, NULL, NULL, 't');
COMMIT;
BEGIN;
LOCK TABLE "public"."auth_user_provider" IN SHARE MODE;
DELETE FROM "public"."auth_user_provider";
INSERT INTO "public"."auth_user_provider" ("auth_user_provider_id","created_by","created_at","updated_by","updated_at","auth_provider_id","auth_user_id") VALUES (70, 'yunsariem@gmail.com', '2022-07-07 12:00:42.732', NULL, '2022-07-07 12:00:42.732', 2, 69);
COMMIT;
BEGIN;
LOCK TABLE "public"."device" IN SHARE MODE;
DELETE FROM "public"."device";
COMMIT;
BEGIN;
LOCK TABLE "public"."feedbacks" IN SHARE MODE;
DELETE FROM "public"."feedbacks";
COMMIT;
BEGIN;
LOCK TABLE "public"."log_users" IN SHARE MODE;
DELETE FROM "public"."log_users";
INSERT INTO "public"."log_users" ("id","users_id","access_token","expires","created_at","updated_at") VALUES (285, 69, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmVzIjoxNjg4NzA2MDQyLCJ1c2VyIjp7ImlkIjoiNjkiLCJ1c2VybmFtZSI6Inl1bnNhcmllbUBnbWFpbC5jb20iLCJjcmVhdGVkX2F0IjoiMjAyMi0wNy0wN1QwNTowMDo0Mi43MjFaIn19.UsS7tK-yM8xCOQp0YcfnY2XPbadYQBbZDaxkXFDa05E', '1688706042', '2022-07-07 12:00:42.826+07', '2022-07-07 12:00:42.826+07');
COMMIT;
BEGIN;
LOCK TABLE "public"."migrations" IN SHARE MODE;
DELETE FROM "public"."migrations";
COMMIT;
BEGIN;
LOCK TABLE "public"."migrations_lock" IN SHARE MODE;
DELETE FROM "public"."migrations_lock";
COMMIT;
BEGIN;
LOCK TABLE "public"."mt_lov" IN SHARE MODE;
DELETE FROM "public"."mt_lov";
INSERT INTO "public"."mt_lov" ("id","lov_code","name_kh","name_en","type","status","created_at","updated_at","icon","image","tags","description","order_by") VALUES (20, 'PHP', NULL, 'PHP', 'Topic', 't', '2021-09-02 15:45:54+07', '2021-09-02 15:45:54+07', NULL, NULL, 'Music', NULL, NULL),(3, 'Web', NULL, 'Web Development', 'Explore', 't', '2021-09-02 15:45:54+07', '2021-09-02 15:45:54+07', 'web.png', 'cover2.png', 'Music', NULL, NULL),(5, 'MOBILE', NULL, 'Mobile Development', 'Explore', 't', '2021-09-02 15:45:54+07', '2021-09-02 15:45:54+07', 'mobile.png', 'cover4.png', 'Mobile Development', NULL, NULL),(1, 'LANGUAGE', NULL, 'Language', 'Explore', 't', '2021-09-02 15:45:54+07', '2021-09-02 15:45:54+07', 'language.png', 'cover.png', 'Language', NULL, NULL),(6, 'GENERAL_KNOWLEDGE', NULL, 'General Knowledge', 'Explore', 't', '2021-09-02 15:45:54+07', '2021-09-02 15:45:54+07', 'general.png', 'cover5.png', 'General Knowledge', NULL, NULL),(4, 'NETWORK', NULL, 'Network & Security', 'Explore', 't', '2021-09-02 15:45:54+07', '2021-09-02 15:45:54+07', 'network.png', 'cover3.png', 'Network & Security', NULL, NULL),(9, 'SLIDE03', NULL, 'Slide 03', 'HomeSlide', 't', '2021-09-02 15:45:54+07', '2021-09-02 15:45:54+07', NULL, 'slide3.png', NULL, 'slide description 3', NULL),(7, 'SLIDE01', NULL, 'Slide 01', 'HomeSlide', 't', '2021-09-02 15:45:54+07', '2021-09-02 15:45:54+07', NULL, 'slide1.png', NULL, 'Slide description 1', NULL),(11, 'SLIDE05', NULL, 'Slide 05', 'HomeSlide', 't', '2021-09-02 15:45:54+07', '2021-09-02 15:45:54+07', NULL, 'slide5.png', NULL, 'slide description 5', NULL),(8, 'SLIDE02', NULL, 'Slide 02', 'HomeSlide', 't', '2021-09-02 15:45:54+07', '2021-09-02 15:45:54+07', NULL, 'slide2.png', NULL, 'slide description 2', NULL),(10, 'SLIDE04', NULL, 'Slide 04', 'HomeSlide', 't', '2021-09-02 15:45:54+07', '2021-09-02 15:45:54+07', NULL, 'slide4.png', NULL, 'slide description 4', NULL),(12, 'Web', NULL, 'Web Development', 'Topic', 't', '2021-09-02 15:45:54+07', '2021-09-02 15:45:54+07', NULL, NULL, 'Web Development', NULL, NULL),(17, 'ReactJS', NULL, 'React JS', 'Topic', 't', '2021-09-02 15:45:54+07', '2021-09-02 15:45:54+07', NULL, NULL, 'React JS', NULL, NULL),(19, 'NodeJS', NULL, 'Node JS', 'Topic', 't', '2021-09-02 15:45:54+07', '2021-09-02 15:45:54+07', NULL, NULL, 'Node JS', NULL, NULL),(21, 'React Native', NULL, 'React Native', 'Topic', 't', '2021-09-02 15:45:54+07', '2021-09-02 15:45:54+07', NULL, NULL, 'React Native', NULL, NULL),(2, 'BUSINESS', NULL, 'Business', 'Explore', 't', '2021-09-02 15:45:54+07', '2021-09-02 15:45:54+07', 'business.png', 'cover1.png', 'Business', NULL, NULL),(22, 'All', NULL, 'All', 'Topic', 't', '2021-09-02 15:45:54+07', '2021-09-02 15:45:54+07', NULL, NULL, NULL, NULL, '1');
COMMIT;
BEGIN;
LOCK TABLE "public"."register_verify" IN SHARE MODE;
DELETE FROM "public"."register_verify";
COMMIT;
BEGIN;
LOCK TABLE "public"."users" IN SHARE MODE;
DELETE FROM "public"."users";
INSERT INTO "public"."users" ("id","email_address","username","password","email_verified_at","created_at","updated_at","created_by","updated_by","phone_number","status","image","verify_code","verify_status","image_cover","image_path","image_cover_path") VALUES (69, 'yunsariem@gmail.com', NULL, '$2b$05$LIqhyb5ErL0wtVTLCXwVyulNFG92UcJ6hbZw.ycl38plxrZLmNTzO', '2022-07-07 12:00:42.721+07', '2022-07-07 12:00:42.721+07', '2022-07-07 12:00:42.721+07', 'yunsariem@gmail.com', NULL, NULL, 't', NULL, NULL, NULL, NULL, NULL, NULL);
COMMIT;
BEGIN;
LOCK TABLE "public"."videos" IN SHARE MODE;
DELETE FROM "public"."videos";
INSERT INTO "public"."videos" ("id","userid","patch","status","created_at","updated_at","created_by","updated_by","category","title","filename","destination","originalname","size","mimetype","encoding","duration","video_info","provider_name","thumbnail_url","description") VALUES (204, 69, '..\resources\video\uploads\20220707\1657174004039_xe15ebkl5amm53c-videoplayback.mp4', 't', '2022-07-07 13:06:49.07+07', '2022-07-07 13:06:49.07+07', '69', NULL, NULL, '10 - WSO2 EI Tutorial - Configure HTTP Endpoint', '1657174004039_xe15ebkl5amm53c-videoplayback.mp4', '20220707', 'videoplayback.mp4', '26856715', 'video/mp4', '7bit', '12:15', '{"streams":[{"index":0,"codec_name":"h264","codec_long_name":"H.264 / AVC / MPEG-4 AVC / MPEG-4 part 10","profile":"High","codec_type":"video","codec_time_base":"1/60","codec_tag_string":"avc1","codec_tag":"0x31637661","width":1920,"height":1000,"coded_width":1920,"coded_height":1008,"has_b_frames":1,"sample_aspect_ratio":"1:1","display_aspect_ratio":"48:25","pix_fmt":"yuv420p","level":40,"color_range":"tv","color_space":"smpte170m","color_transfer":"bt709","color_primaries":"bt709","chroma_location":"left","refs":3,"is_avc":"1","nal_length_size":"4","r_frame_rate":"30/1","avg_frame_rate":"30/1","time_base":"1/15360","start_pts":0,"start_time":"0.000000","duration_ts":11295232,"duration":"735.366667","bit_rate":"289115","bits_per_raw_sample":"8","disposition":{"default":1,"dub":0,"original":0,"comment":0,"lyrics":0,"karaoke":0,"forced":0,"hearing_impaired":0,"visual_impaired":0,"clean_effects":0,"attached_pic":0},"tags":{"creation_time":"2020-05-19 13:52:50","language":"und","handler_name":"ISO Media file produced by Google Inc."}}],"format":{"filename":"..\\resources\\video\\uploads\\20220707\\1657174004039_xe15ebkl5amm53c-videoplayback.mp4","nb_streams":1,"nb_programs":0,"format_name":"mov,mp4,m4a,3gp,3g2,mj2","format_long_name":"QuickTime / MOV","start_time":"0.000000","duration":"735.366667","size":"26856715","bit_rate":"292172","probe_score":100,"tags":{"major_brand":"dash","minor_version":"0","compatible_brands":"iso6avc1mp41","creation_time":"2020-05-19 13:52:50"}}}', 'BGetting', NULL, NULL),(205, 69, '..\resources\video\uploads\20220707\1657174243869_xe15ebkl5amra59-1.OBIEE12CTutorialPart01DWConcepts.mp4', 't', '2022-07-07 13:11:15.43+07', '2022-07-07 13:11:15.43+07', '69', NULL, NULL, '1. OBIEE 12C Tutorial Part 01 DW Concepts', '1657174243869_xe15ebkl5amra59-1.OBIEE12CTutorialPart01DWConcepts.mp4', '20220707', '1. OBIEE 12C Tutorial Part 01 DW Concepts.mp4', '173082910', 'video/mp4', '7bit', '1:08:33', '{"streams":[{"index":0,"codec_name":"h264","codec_long_name":"H.264 / AVC / MPEG-4 AVC / MPEG-4 part 10","profile":"High","codec_type":"video","codec_time_base":"1/60","codec_tag_string":"avc1","codec_tag":"0x31637661","width":1280,"height":720,"coded_width":1280,"coded_height":720,"has_b_frames":2,"sample_aspect_ratio":"1:1","display_aspect_ratio":"16:9","pix_fmt":"yuv420p","level":30,"chroma_location":"left","refs":4,"is_avc":"1","nal_length_size":"4","r_frame_rate":"30/1","avg_frame_rate":"30/1","time_base":"1/30","start_pts":0,"start_time":"0.000000","duration_ts":123396,"duration":"4113.200000","bit_rate":"143994","bits_per_raw_sample":"8","nb_frames":"123396","disposition":{"default":1,"dub":0,"original":0,"comment":0,"lyrics":0,"karaoke":0,"forced":0,"hearing_impaired":0,"visual_impaired":0,"clean_effects":0,"attached_pic":0},"tags":{"language":"und","handler_name":"VideoHandler"}},{"index":1,"codec_name":"aac","codec_long_name":"AAC (Advanced Audio Coding)","profile":"LC","codec_type":"audio","codec_time_base":"1/48000","codec_tag_string":"mp4a","codec_tag":"0x6134706d","sample_fmt":"fltp","sample_rate":"48000","channels":2,"channel_layout":"stereo","bits_per_sample":0,"r_frame_rate":"0/0","avg_frame_rate":"0/0","time_base":"1/48000","start_pts":0,"start_time":"0.000000","duration_ts":197450752,"duration":"4113.557333","bit_rate":"189375","max_bit_rate":"192000","nb_frames":"192823","disposition":{"default":1,"dub":0,"original":0,"comment":0,"lyrics":0,"karaoke":0,"forced":0,"hearing_impaired":0,"visual_impaired":0,"clean_effects":0,"attached_pic":0},"tags":{"language":"und","handler_name":"SoundHandler"}}],"format":{"filename":"..\\resources\\video\\uploads\\20220707\\1657174243869_xe15ebkl5amra59-1.OBIEE12CTutorialPart01DWConcepts.mp4","nb_streams":2,"nb_programs":0,"format_name":"mov,mp4,m4a,3gp,3g2,mj2","format_long_name":"QuickTime / MOV","start_time":"0.000000","duration":"4113.558000","size":"173082910","bit_rate":"336609","probe_score":100,"tags":{"major_brand":"mp42","minor_version":"0","compatible_brands":"isomiso2avc1mp41mp42","encoder":"Lavf54.29.104"}}}', 'BGetting', NULL, NULL),(206, 69, '..\resources\video\uploads\20220707\1657174293157_xe15ebkl5amsc6d-2.OBIEE12CTutorialPart02OBIEE12CInstallation.mp4', 't', '2022-07-07 13:12:10.529+07', '2022-07-07 13:12:10.529+07', '69', NULL, NULL, '2. OBIEE 12C Tutorial Part 02 OBIEE 12C Installation', '1657174293157_xe15ebkl5amsc6d-2.OBIEE12CTutorialPart02OBIEE12CInstallation.mp4', '20220707', '2. OBIEE 12C Tutorial Part 02 OBIEE 12C Installation.mp4', '155152085', 'video/mp4', '7bit', '1:11:41', '{"streams":[{"index":0,"codec_name":"h264","codec_long_name":"H.264 / AVC / MPEG-4 AVC / MPEG-4 part 10","profile":"High","codec_type":"video","codec_time_base":"1/20","codec_tag_string":"avc1","codec_tag":"0x31637661","width":1280,"height":720,"coded_width":1280,"coded_height":720,"has_b_frames":2,"sample_aspect_ratio":"1:1","display_aspect_ratio":"16:9","pix_fmt":"yuv420p","level":30,"chroma_location":"left","refs":4,"is_avc":"1","nal_length_size":"4","r_frame_rate":"10/1","avg_frame_rate":"10/1","time_base":"1/10","start_pts":0,"start_time":"0.000000","duration_ts":43011,"duration":"4301.100000","bit_rate":"97006","bits_per_raw_sample":"8","nb_frames":"43011","disposition":{"default":1,"dub":0,"original":0,"comment":0,"lyrics":0,"karaoke":0,"forced":0,"hearing_impaired":0,"visual_impaired":0,"clean_effects":0,"attached_pic":0},"tags":{"language":"und","handler_name":"VideoHandler"}},{"index":1,"codec_name":"aac","codec_long_name":"AAC (Advanced Audio Coding)","profile":"LC","codec_type":"audio","codec_time_base":"1/48000","codec_tag_string":"mp4a","codec_tag":"0x6134706d","sample_fmt":"fltp","sample_rate":"48000","channels":2,"channel_layout":"stereo","bits_per_sample":0,"r_frame_rate":"0/0","avg_frame_rate":"0/0","time_base":"1/48000","start_pts":0,"start_time":"0.000000","duration_ts":206467072,"duration":"4301.397333","bit_rate":"189375","max_bit_rate":"192000","nb_frames":"201628","disposition":{"default":1,"dub":0,"original":0,"comment":0,"lyrics":0,"karaoke":0,"forced":0,"hearing_impaired":0,"visual_impaired":0,"clean_effects":0,"attached_pic":0},"tags":{"language":"und","handler_name":"SoundHandler"}}],"format":{"filename":"..\\resources\\video\\uploads\\20220707\\1657174293157_xe15ebkl5amsc6d-2.OBIEE12CTutorialPart02OBIEE12CInstallation.mp4","nb_streams":2,"nb_programs":0,"format_name":"mov,mp4,m4a,3gp,3g2,mj2","format_long_name":"QuickTime / MOV","start_time":"0.000000","duration":"4301.398000","size":"155152085","bit_rate":"288561","probe_score":100,"tags":{"major_brand":"mp42","minor_version":"0","compatible_brands":"isomiso2avc1mp41mp42","encoder":"Lavf54.29.104"}}}', 'BGetting', NULL, NULL),(207, 69, '..\resources\video\uploads\20220707\1657174341373_xe15ebkl5amtddp-3.OBIEE12CTutorialPart03StarSchemaVsSnowFlakeSchema.mp4', 't', '2022-07-07 13:12:51.02+07', '2022-07-07 13:12:51.02+07', '69', NULL, NULL, '3. OBIEE 12C Tutorial Part 03 Star Schema Vs Snow Flake Schema', '1657174341373_xe15ebkl5amtddp-3.OBIEE12CTutorialPart03StarSchemaVsSnowFlakeSchema.mp4', '20220707', '3. OBIEE 12C Tutorial Part 03 Star Schema Vs Snow Flake Schema.mp4', '147763402', 'video/mp4', '7bit', '1:04:58', '{"streams":[{"index":0,"codec_name":"h264","codec_long_name":"H.264 / AVC / MPEG-4 AVC / MPEG-4 part 10","profile":"High","codec_type":"video","codec_time_base":"1/40","codec_tag_string":"avc1","codec_tag":"0x31637661","width":1280,"height":720,"coded_width":1280,"coded_height":720,"has_b_frames":2,"sample_aspect_ratio":"1:1","display_aspect_ratio":"16:9","pix_fmt":"yuv420p","level":30,"chroma_location":"left","refs":4,"is_avc":"1","nal_length_size":"4","r_frame_rate":"20/1","avg_frame_rate":"20/1","time_base":"1/20","start_pts":0,"start_time":"0.000000","duration_ts":77971,"duration":"3898.550000","bit_rate":"111095","bits_per_raw_sample":"8","nb_frames":"77971","disposition":{"default":1,"dub":0,"original":0,"comment":0,"lyrics":0,"karaoke":0,"forced":0,"hearing_impaired":0,"visual_impaired":0,"clean_effects":0,"attached_pic":0},"tags":{"language":"und","handler_name":"VideoHandler"}},{"index":1,"codec_name":"aac","codec_long_name":"AAC (Advanced Audio Coding)","profile":"LC","codec_type":"audio","codec_time_base":"1/48000","codec_tag_string":"mp4a","codec_tag":"0x6134706d","sample_fmt":"fltp","sample_rate":"48000","channels":2,"channel_layout":"stereo","bits_per_sample":0,"r_frame_rate":"0/0","avg_frame_rate":"0/0","time_base":"1/48000","start_pts":0,"start_time":"0.000000","duration_ts":187147264,"duration":"3898.901333","bit_rate":"189375","max_bit_rate":"192000","nb_frames":"182761","disposition":{"default":1,"dub":0,"original":0,"comment":0,"lyrics":0,"karaoke":0,"forced":0,"hearing_impaired":0,"visual_impaired":0,"clean_effects":0,"attached_pic":0},"tags":{"language":"und","handler_name":"SoundHandler"}}],"format":{"filename":"..\\resources\\video\\uploads\\20220707\\1657174341373_xe15ebkl5amtddp-3.OBIEE12CTutorialPart03StarSchemaVsSnowFlakeSchema.mp4","nb_streams":2,"nb_programs":0,"format_name":"mov,mp4,m4a,3gp,3g2,mj2","format_long_name":"QuickTime / MOV","start_time":"0.000000","duration":"3898.902000","size":"147763402","bit_rate":"303189","probe_score":100,"tags":{"major_brand":"mp42","minor_version":"0","compatible_brands":"isomiso2avc1mp41mp42","encoder":"Lavf54.29.104"}}}', 'BGetting', NULL, NULL),(208, 69, '..\resources\video\uploads\20220707\1657174383220_xe15ebkl5amu9o4-4.OBIEE12CTutorialPart04PhysicalLayerPart01.mp4', 't', '2022-07-07 13:13:32.424+07', '2022-07-07 13:13:32.424+07', '69', NULL, NULL, '4. OBIEE 12C Tutorial Part 04 Physical Layer Part 01', '1657174383220_xe15ebkl5amu9o4-4.OBIEE12CTutorialPart04PhysicalLayerPart01.mp4', '20220707', '4. OBIEE 12C Tutorial Part 04 Physical Layer Part 01.mp4', '141874848', 'video/mp4', '7bit', '1:06:41', '{"streams":[{"index":0,"codec_name":"h264","codec_long_name":"H.264 / AVC / MPEG-4 AVC / MPEG-4 part 10","profile":"High","codec_type":"video","codec_time_base":"1/60","codec_tag_string":"avc1","codec_tag":"0x31637661","width":1280,"height":720,"coded_width":1280,"coded_height":720,"has_b_frames":2,"sample_aspect_ratio":"1:1","display_aspect_ratio":"16:9","pix_fmt":"yuv420p","level":30,"chroma_location":"left","refs":4,"is_avc":"1","nal_length_size":"4","r_frame_rate":"30/1","avg_frame_rate":"30/1","time_base":"1/30","start_pts":0,"start_time":"0.000000","duration_ts":120043,"duration":"4001.433333","bit_rate":"91029","bits_per_raw_sample":"8","nb_frames":"120043","disposition":{"default":1,"dub":0,"original":0,"comment":0,"lyrics":0,"karaoke":0,"forced":0,"hearing_impaired":0,"visual_impaired":0,"clean_effects":0,"attached_pic":0},"tags":{"language":"und","handler_name":"VideoHandler"}},{"index":1,"codec_name":"aac","codec_long_name":"AAC (Advanced Audio Coding)","profile":"LC","codec_type":"audio","codec_time_base":"1/48000","codec_tag_string":"mp4a","codec_tag":"0x6134706d","sample_fmt":"fltp","sample_rate":"48000","channels":2,"channel_layout":"stereo","bits_per_sample":0,"r_frame_rate":"0/0","avg_frame_rate":"0/0","time_base":"1/48000","start_pts":0,"start_time":"0.000000","duration_ts":192084992,"duration":"4001.770667","bit_rate":"189375","max_bit_rate":"192000","nb_frames":"187583","disposition":{"default":1,"dub":0,"original":0,"comment":0,"lyrics":0,"karaoke":0,"forced":0,"hearing_impaired":0,"visual_impaired":0,"clean_effects":0,"attached_pic":0},"tags":{"language":"und","handler_name":"SoundHandler"}}],"format":{"filename":"..\\resources\\video\\uploads\\20220707\\1657174383220_xe15ebkl5amu9o4-4.OBIEE12CTutorialPart04PhysicalLayerPart01.mp4","nb_streams":2,"nb_programs":0,"format_name":"mov,mp4,m4a,3gp,3g2,mj2","format_long_name":"QuickTime / MOV","start_time":"0.000000","duration":"4001.771000","size":"141874848","bit_rate":"283624","probe_score":100,"tags":{"major_brand":"mp42","minor_version":"0","compatible_brands":"isomiso2avc1mp41mp42","encoder":"Lavf54.29.104"}}}', 'BGetting', NULL, NULL),(209, 69, '..\resources\video\uploads\20220707\1657174423870_xe15ebkl5amv51a-5.OBIEE12CTutorialPart05PhysicalLayerPart02.mp4', 't', '2022-07-07 13:14:06.74+07', '2022-07-07 13:14:06.74+07', '69', NULL, NULL, '5. OBIEE 12C Tutorial Part 05 Physical Layer Part 02', '1657174423870_xe15ebkl5amv51a-5.OBIEE12CTutorialPart05PhysicalLayerPart02.mp4', '20220707', '5. OBIEE 12C Tutorial Part 05 Physical Layer Part 02.mp4', '113257984', 'video/mp4', '7bit', '56:44', '{"streams":[{"index":0,"codec_name":"h264","codec_long_name":"H.264 / AVC / MPEG-4 AVC / MPEG-4 part 10","profile":"High","codec_type":"video","codec_time_base":"1/40","codec_tag_string":"avc1","codec_tag":"0x31637661","width":1280,"height":720,"coded_width":1280,"coded_height":720,"has_b_frames":2,"sample_aspect_ratio":"1:1","display_aspect_ratio":"16:9","pix_fmt":"yuv420p","level":30,"chroma_location":"left","refs":4,"is_avc":"1","nal_length_size":"4","r_frame_rate":"20/1","avg_frame_rate":"20/1","time_base":"1/20","start_pts":0,"start_time":"0.000000","duration_ts":68087,"duration":"3404.350000","bit_rate":"74025","bits_per_raw_sample":"8","nb_frames":"68087","disposition":{"default":1,"dub":0,"original":0,"comment":0,"lyrics":0,"karaoke":0,"forced":0,"hearing_impaired":0,"visual_impaired":0,"clean_effects":0,"attached_pic":0},"tags":{"language":"und","handler_name":"VideoHandler"}},{"index":1,"codec_name":"aac","codec_long_name":"AAC (Advanced Audio Coding)","profile":"LC","codec_type":"audio","codec_time_base":"1/48000","codec_tag_string":"mp4a","codec_tag":"0x6134706d","sample_fmt":"fltp","sample_rate":"48000","channels":2,"channel_layout":"stereo","bits_per_sample":0,"r_frame_rate":"0/0","avg_frame_rate":"0/0","time_base":"1/48000","start_pts":0,"start_time":"0.000000","duration_ts":163425280,"duration":"3404.693333","bit_rate":"189375","max_bit_rate":"192000","nb_frames":"159595","disposition":{"default":1,"dub":0,"original":0,"comment":0,"lyrics":0,"karaoke":0,"forced":0,"hearing_impaired":0,"visual_impaired":0,"clean_effects":0,"attached_pic":0},"tags":{"language":"und","handler_name":"SoundHandler"}}],"format":{"filename":"..\\resources\\video\\uploads\\20220707\\1657174423870_xe15ebkl5amv51a-5.OBIEE12CTutorialPart05PhysicalLayerPart02.mp4","nb_streams":2,"nb_programs":0,"format_name":"mov,mp4,m4a,3gp,3g2,mj2","format_long_name":"QuickTime / MOV","start_time":"0.000000","duration":"3404.694000","size":"113257984","bit_rate":"266121","probe_score":100,"tags":{"major_brand":"mp42","minor_version":"0","compatible_brands":"isomiso2avc1mp41mp42","encoder":"Lavf54.29.104"}}}', 'BGetting', NULL, NULL),(210, 69, '..\resources\video\uploads\20220707\1657174456006_xe15ebkl5amvtty-6.OBIEE12CTutorialPart06PhysicalLayerPart03.mp4', 't', '2022-07-07 13:14:41.641+07', '2022-07-07 13:14:41.641+07', '69', NULL, NULL, '6. OBIEE 12C Tutorial Part 06 Physical Layer Part 03', '1657174456006_xe15ebkl5amvtty-6.OBIEE12CTutorialPart06PhysicalLayerPart03.mp4', '20220707', '6. OBIEE 12C Tutorial Part 06 Physical Layer Part 03.mp4', '126566718', 'video/mp4', '7bit', '1:00:59', '{"streams":[{"index":0,"codec_name":"h264","codec_long_name":"H.264 / AVC / MPEG-4 AVC / MPEG-4 part 10","profile":"High","codec_type":"video","codec_time_base":"1/20","codec_tag_string":"avc1","codec_tag":"0x31637661","width":1280,"height":720,"coded_width":1280,"coded_height":720,"has_b_frames":2,"sample_aspect_ratio":"1:1","display_aspect_ratio":"16:9","pix_fmt":"yuv420p","level":30,"chroma_location":"left","refs":4,"is_avc":"1","nal_length_size":"4","r_frame_rate":"10/1","avg_frame_rate":"10/1","time_base":"1/10","start_pts":0,"start_time":"0.000000","duration_ts":36592,"duration":"3659.200000","bit_rate":"85114","bits_per_raw_sample":"8","nb_frames":"36592","disposition":{"default":1,"dub":0,"original":0,"comment":0,"lyrics":0,"karaoke":0,"forced":0,"hearing_impaired":0,"visual_impaired":0,"clean_effects":0,"attached_pic":0},"tags":{"language":"und","handler_name":"VideoHandler"}},{"index":1,"codec_name":"aac","codec_long_name":"AAC (Advanced Audio Coding)","profile":"LC","codec_type":"audio","codec_time_base":"1/48000","codec_tag_string":"mp4a","codec_tag":"0x6134706d","sample_fmt":"fltp","sample_rate":"48000","channels":2,"channel_layout":"stereo","bits_per_sample":0,"r_frame_rate":"0/0","avg_frame_rate":"0/0","time_base":"1/48000","start_pts":0,"start_time":"0.000000","duration_ts":175655936,"duration":"3659.498667","bit_rate":"189375","max_bit_rate":"192000","nb_frames":"171539","disposition":{"default":1,"dub":0,"original":0,"comment":0,"lyrics":0,"karaoke":0,"forced":0,"hearing_impaired":0,"visual_impaired":0,"clean_effects":0,"attached_pic":0},"tags":{"language":"und","handler_name":"SoundHandler"}}],"format":{"filename":"..\\resources\\video\\uploads\\20220707\\1657174456006_xe15ebkl5amvtty-6.OBIEE12CTutorialPart06PhysicalLayerPart03.mp4","nb_streams":2,"nb_programs":0,"format_name":"mov,mp4,m4a,3gp,3g2,mj2","format_long_name":"QuickTime / MOV","start_time":"0.000000","duration":"3659.499000","size":"126566718","bit_rate":"276686","probe_score":100,"tags":{"major_brand":"mp42","minor_version":"0","compatible_brands":"isomiso2avc1mp41mp42","encoder":"Lavf54.29.104"}}}', 'BGetting', NULL, NULL),(211, 69, '..\resources\video\uploads\20220707\1657174491195_xe15ebkl5amwkzf-7.OBIEE12CTutorialPart07PhysicalLayerPart04.mp4', 't', '2022-07-07 13:15:13.048+07', '2022-07-07 13:15:13.048+07', '69', NULL, NULL, '7. OBIEE 12C Tutorial Part 07 Physical Layer Part 04', '1657174491195_xe15ebkl5amwkzf-7.OBIEE12CTutorialPart07PhysicalLayerPart04.mp4', '20220707', '7. OBIEE 12C Tutorial Part 07 Physical Layer Part 04.mp4', '107784241', 'video/mp4', '7bit', '53:42', '{"streams":[{"index":0,"codec_name":"h264","codec_long_name":"H.264 / AVC / MPEG-4 AVC / MPEG-4 part 10","profile":"High","codec_type":"video","codec_time_base":"1/20","codec_tag_string":"avc1","codec_tag":"0x31637661","width":1280,"height":720,"coded_width":1280,"coded_height":720,"has_b_frames":2,"sample_aspect_ratio":"1:1","display_aspect_ratio":"16:9","pix_fmt":"yuv420p","level":30,"chroma_location":"left","refs":4,"is_avc":"1","nal_length_size":"4","r_frame_rate":"10/1","avg_frame_rate":"10/1","time_base":"1/10","start_pts":0,"start_time":"0.000000","duration_ts":32217,"duration":"3221.700000","bit_rate":"76068","bits_per_raw_sample":"8","nb_frames":"32217","disposition":{"default":1,"dub":0,"original":0,"comment":0,"lyrics":0,"karaoke":0,"forced":0,"hearing_impaired":0,"visual_impaired":0,"clean_effects":0,"attached_pic":0},"tags":{"language":"und","handler_name":"VideoHandler"}},{"index":1,"codec_name":"aac","codec_long_name":"AAC (Advanced Audio Coding)","profile":"LC","codec_type":"audio","codec_time_base":"1/48000","codec_tag_string":"mp4a","codec_tag":"0x6134706d","sample_fmt":"fltp","sample_rate":"48000","channels":2,"channel_layout":"stereo","bits_per_sample":0,"r_frame_rate":"0/0","avg_frame_rate":"0/0","time_base":"1/48000","start_pts":0,"start_time":"0.000000","duration_ts":154657792,"duration":"3222.037333","bit_rate":"189375","max_bit_rate":"192000","nb_frames":"151033","disposition":{"default":1,"dub":0,"original":0,"comment":0,"lyrics":0,"karaoke":0,"forced":0,"hearing_impaired":0,"visual_impaired":0,"clean_effects":0,"attached_pic":0},"tags":{"language":"und","handler_name":"SoundHandler"}}],"format":{"filename":"..\\resources\\video\\uploads\\20220707\\1657174491195_xe15ebkl5amwkzf-7.OBIEE12CTutorialPart07PhysicalLayerPart04.mp4","nb_streams":2,"nb_programs":0,"format_name":"mov,mp4,m4a,3gp,3g2,mj2","format_long_name":"QuickTime / MOV","start_time":"0.000000","duration":"3222.038000","size":"107784241","bit_rate":"267617","probe_score":100,"tags":{"major_brand":"mp42","minor_version":"0","compatible_brands":"isomiso2avc1mp41mp42","encoder":"Lavf54.29.104"}}}', 'BGetting', NULL, NULL),(212, 69, '..\resources\video\uploads\20220707\1657174523250_xe15ebkl5amx9pu-8.OBIEE12CTutorialPart08BMMLayer.mp4', 't', '2022-07-07 13:15:48.534+07', '2022-07-07 13:15:48.534+07', '69', NULL, NULL, '8. OBIEE 12C Tutorial Part 08 BMM Layer', '1657174523250_xe15ebkl5amx9pu-8.OBIEE12CTutorialPart08BMMLayer.mp4', '20220707', '8. OBIEE 12C Tutorial Part 08 BMM Layer.mp4', '141453787', 'video/mp4', '7bit', '1:07:55', '{"streams":[{"index":0,"codec_name":"h264","codec_long_name":"H.264 / AVC / MPEG-4 AVC / MPEG-4 part 10","profile":"High","codec_type":"video","codec_time_base":"1/20","codec_tag_string":"avc1","codec_tag":"0x31637661","width":1280,"height":720,"coded_width":1280,"coded_height":720,"has_b_frames":2,"sample_aspect_ratio":"1:1","display_aspect_ratio":"16:9","pix_fmt":"yuv420p","level":30,"chroma_location":"left","refs":4,"is_avc":"1","nal_length_size":"4","r_frame_rate":"10/1","avg_frame_rate":"10/1","time_base":"1/10","start_pts":0,"start_time":"0.000000","duration_ts":40755,"duration":"4075.500000","bit_rate":"86096","bits_per_raw_sample":"8","nb_frames":"40755","disposition":{"default":1,"dub":0,"original":0,"comment":0,"lyrics":0,"karaoke":0,"forced":0,"hearing_impaired":0,"visual_impaired":0,"clean_effects":0,"attached_pic":0},"tags":{"language":"und","handler_name":"VideoHandler"}},{"index":1,"codec_name":"aac","codec_long_name":"AAC (Advanced Audio Coding)","profile":"LC","codec_type":"audio","codec_time_base":"1/48000","codec_tag_string":"mp4a","codec_tag":"0x6134706d","sample_fmt":"fltp","sample_rate":"48000","channels":2,"channel_layout":"stereo","bits_per_sample":0,"r_frame_rate":"0/0","avg_frame_rate":"0/0","time_base":"1/48000","start_pts":0,"start_time":"0.000000","duration_ts":195638272,"duration":"4075.797333","bit_rate":"189375","max_bit_rate":"192000","nb_frames":"191053","disposition":{"default":1,"dub":0,"original":0,"comment":0,"lyrics":0,"karaoke":0,"forced":0,"hearing_impaired":0,"visual_impaired":0,"clean_effects":0,"attached_pic":0},"tags":{"language":"und","handler_name":"SoundHandler"}}],"format":{"filename":"..\\resources\\video\\uploads\\20220707\\1657174523250_xe15ebkl5amx9pu-8.OBIEE12CTutorialPart08BMMLayer.mp4","nb_streams":2,"nb_programs":0,"format_name":"mov,mp4,m4a,3gp,3g2,mj2","format_long_name":"QuickTime / MOV","start_time":"0.000000","duration":"4075.798000","size":"141453787","bit_rate":"277646","probe_score":100,"tags":{"major_brand":"mp42","minor_version":"0","compatible_brands":"isomiso2avc1mp41mp42","encoder":"Lavf54.29.104"}}}', 'BGetting', NULL, NULL),(213, 69, '..\resources\video\uploads\20220707\1657174556560_xe15ebkl5amxzf4-9.OBIEE12CTutorialPart09PresentationLayer.mp4', 't', '2022-07-07 13:16:15.264+07', '2022-07-07 13:16:15.264+07', '69', NULL, NULL, '9. OBIEE 12C Tutorial Part 09 Presentation Layer', '1657174556560_xe15ebkl5amxzf4-9.OBIEE12CTutorialPart09PresentationLayer.mp4', '20220707', '9. OBIEE 12C Tutorial Part 09 Presentation Layer.mp4', '117877074', 'video/mp4', '7bit', '57:45', '{"streams":[{"index":0,"codec_name":"h264","codec_long_name":"H.264 / AVC / MPEG-4 AVC / MPEG-4 part 10","profile":"High","codec_type":"video","codec_time_base":"1/40","codec_tag_string":"avc1","codec_tag":"0x31637661","width":1280,"height":720,"coded_width":1280,"coded_height":720,"has_b_frames":2,"sample_aspect_ratio":"1:1","display_aspect_ratio":"16:9","pix_fmt":"yuv420p","level":30,"chroma_location":"left","refs":4,"is_avc":"1","nal_length_size":"4","r_frame_rate":"20/1","avg_frame_rate":"20/1","time_base":"1/20","start_pts":0,"start_time":"0.000000","duration_ts":69297,"duration":"3464.850000","bit_rate":"80045","bits_per_raw_sample":"8","nb_frames":"69297","disposition":{"default":1,"dub":0,"original":0,"comment":0,"lyrics":0,"karaoke":0,"forced":0,"hearing_impaired":0,"visual_impaired":0,"clean_effects":0,"attached_pic":0},"tags":{"language":"und","handler_name":"VideoHandler"}},{"index":1,"codec_name":"aac","codec_long_name":"AAC (Advanced Audio Coding)","profile":"LC","codec_type":"audio","codec_time_base":"1/48000","codec_tag_string":"mp4a","codec_tag":"0x6134706d","sample_fmt":"fltp","sample_rate":"48000","channels":2,"channel_layout":"stereo","bits_per_sample":0,"r_frame_rate":"0/0","avg_frame_rate":"0/0","time_base":"1/48000","start_pts":0,"start_time":"0.000000","duration_ts":166327296,"duration":"3465.152000","bit_rate":"189375","max_bit_rate":"192000","nb_frames":"162429","disposition":{"default":1,"dub":0,"original":0,"comment":0,"lyrics":0,"karaoke":0,"forced":0,"hearing_impaired":0,"visual_impaired":0,"clean_effects":0,"attached_pic":0},"tags":{"language":"und","handler_name":"SoundHandler"}}],"format":{"filename":"..\\resources\\video\\uploads\\20220707\\1657174556560_xe15ebkl5amxzf4-9.OBIEE12CTutorialPart09PresentationLayer.mp4","nb_streams":2,"nb_programs":0,"format_name":"mov,mp4,m4a,3gp,3g2,mj2","format_long_name":"QuickTime / MOV","start_time":"0.000000","duration":"3465.152000","size":"117877074","bit_rate":"272142","probe_score":100,"tags":{"major_brand":"mp42","minor_version":"0","compatible_brands":"isomiso2avc1mp41mp42","encoder":"Lavf54.29.104"}}}', 'BGetting', NULL, NULL);
COMMIT;
BEGIN;
LOCK TABLE "public"."videos_action" IN SHARE MODE;
DELETE FROM "public"."videos_action";
COMMIT;
BEGIN;
LOCK TABLE "public"."videos_channel" IN SHARE MODE;
DELETE FROM "public"."videos_channel";
COMMIT;
BEGIN;
LOCK TABLE "public"."videos_history" IN SHARE MODE;
DELETE FROM "public"."videos_history";
INSERT INTO "public"."videos_history" ("id","video_id","user_watch_id","created_at","updated_at") VALUES (564, 208, 69, '2022-07-07 13:19:01.257+07', '2022-07-07 13:19:01.257+07'),(565, 213, 69, '2022-07-07 13:19:53.399+07', '2022-07-07 13:19:53.399+07'),(566, 204, 69, '2022-07-07 14:53:45.355+07', '2022-07-07 14:53:45.355+07'),(567, 213, 69, '2022-07-07 16:34:39.392+07', '2022-07-07 16:34:39.392+07'),(568, 213, 69, '2022-07-07 16:38:49.095+07', '2022-07-07 16:38:49.095+07'),(569, 213, 69, '2022-07-07 16:45:34.331+07', '2022-07-07 16:45:34.331+07'),(570, 213, 69, '2022-07-07 16:45:42.419+07', '2022-07-07 16:45:42.419+07'),(571, 204, 69, '2022-07-07 16:48:31.599+07', '2022-07-07 16:48:31.599+07'),(572, 205, 69, '2022-07-07 16:48:43.948+07', '2022-07-07 16:48:43.948+07');
COMMIT;
BEGIN;
LOCK TABLE "public"."videos_playlist" IN SHARE MODE;
DELETE FROM "public"."videos_playlist";
INSERT INTO "public"."videos_playlist" ("id","user_id","title","description","created_at","updated_at") VALUES (43, 69, 'OBIEE 12C', NULL, '2022-07-07 13:18:17.545+07', '2022-07-07 13:18:17.545+07');
COMMIT;
BEGIN;
LOCK TABLE "public"."videos_playlist_add" IN SHARE MODE;
DELETE FROM "public"."videos_playlist_add";
INSERT INTO "public"."videos_playlist_add" ("id","playlist_id","video_id","order_by","created_at","updated_at") VALUES (62, 43, 213, NULL, '2022-07-07 13:18:17.553+07', '2022-07-07 13:18:17.553+07'),(63, 43, 212, NULL, '2022-07-07 13:18:17.553+07', '2022-07-07 13:18:17.553+07'),(64, 43, 211, NULL, '2022-07-07 13:18:17.553+07', '2022-07-07 13:18:17.553+07'),(65, 43, 210, NULL, '2022-07-07 13:18:17.553+07', '2022-07-07 13:18:17.553+07'),(66, 43, 209, NULL, '2022-07-07 13:18:17.553+07', '2022-07-07 13:18:17.553+07'),(67, 43, 208, NULL, '2022-07-07 13:18:17.553+07', '2022-07-07 13:18:17.553+07'),(68, 43, 207, NULL, '2022-07-07 13:18:17.553+07', '2022-07-07 13:18:17.553+07'),(69, 43, 206, NULL, '2022-07-07 13:18:17.553+07', '2022-07-07 13:18:17.553+07'),(70, 43, 205, NULL, '2022-07-07 13:18:17.553+07', '2022-07-07 13:18:17.553+07');
COMMIT;
BEGIN;
LOCK TABLE "public"."videos_tags" IN SHARE MODE;
DELETE FROM "public"."videos_tags";
INSERT INTO "public"."videos_tags" ("id","title_tags","video_id","created_by","created_at","updated_by","updated_at") VALUES (89, 'WSO2', 204, '69', '2022-07-07 13:06:49.975+07', NULL, '2022-07-07 13:06:49.975+07'),(90, 'Programe', 204, '69', '2022-07-07 13:06:49.975+07', NULL, '2022-07-07 13:06:49.975+07'),(91, 'API', 204, '69', '2022-07-07 13:06:49.975+07', NULL, '2022-07-07 13:06:49.975+07'),(92, 'OBIEE', 205, '69', '2022-07-07 13:11:15.45+07', NULL, '2022-07-07 13:11:15.45+07'),(93, 'Oracle', 205, '69', '2022-07-07 13:11:15.45+07', NULL, '2022-07-07 13:11:15.45+07'),(94, 'Report', 205, '69', '2022-07-07 13:11:15.45+07', NULL, '2022-07-07 13:11:15.45+07'),(95, 'OBIEE', 206, '69', '2022-07-07 13:12:10.549+07', NULL, '2022-07-07 13:12:10.549+07'),(96, 'Oracle', 206, '69', '2022-07-07 13:12:10.549+07', NULL, '2022-07-07 13:12:10.549+07'),(97, 'Report', 206, '69', '2022-07-07 13:12:10.549+07', NULL, '2022-07-07 13:12:10.549+07'),(98, 'OBIEE', 207, '69', '2022-07-07 13:12:51.039+07', NULL, '2022-07-07 13:12:51.039+07'),(99, 'Oracle', 207, '69', '2022-07-07 13:12:51.039+07', NULL, '2022-07-07 13:12:51.039+07'),(100, 'Report', 207, '69', '2022-07-07 13:12:51.039+07', NULL, '2022-07-07 13:12:51.039+07'),(101, 'OBIEE', 208, '69', '2022-07-07 13:13:32.445+07', NULL, '2022-07-07 13:13:32.445+07'),(102, 'Oracle', 208, '69', '2022-07-07 13:13:32.445+07', NULL, '2022-07-07 13:13:32.445+07'),(103, 'Report', 208, '69', '2022-07-07 13:13:32.445+07', NULL, '2022-07-07 13:13:32.445+07'),(104, 'OBIEE', 209, '69', '2022-07-07 13:14:06.76+07', NULL, '2022-07-07 13:14:06.76+07'),(105, 'Oracle', 209, '69', '2022-07-07 13:14:06.76+07', NULL, '2022-07-07 13:14:06.76+07'),(106, 'Report', 209, '69', '2022-07-07 13:14:06.76+07', NULL, '2022-07-07 13:14:06.76+07'),(108, 'Oracle', 210, '69', '2022-07-07 13:14:41.661+07', NULL, '2022-07-07 13:14:41.661+07'),(107, 'OBIEE', 210, '69', '2022-07-07 13:14:41.661+07', NULL, '2022-07-07 13:14:41.661+07'),(109, 'Report', 210, '69', '2022-07-07 13:14:41.661+07', NULL, '2022-07-07 13:14:41.661+07'),(110, 'OBIEE', 211, '69', '2022-07-07 13:15:13.065+07', NULL, '2022-07-07 13:15:13.065+07'),(111, 'Oracle', 211, '69', '2022-07-07 13:15:13.065+07', NULL, '2022-07-07 13:15:13.065+07'),(112, 'Report', 211, '69', '2022-07-07 13:15:13.065+07', NULL, '2022-07-07 13:15:13.065+07'),(114, 'Oracle', 212, '69', '2022-07-07 13:15:48.554+07', NULL, '2022-07-07 13:15:48.554+07'),(113, 'OBIEE', 212, '69', '2022-07-07 13:15:48.554+07', NULL, '2022-07-07 13:15:48.554+07'),(115, 'Report', 212, '69', '2022-07-07 13:15:48.554+07', NULL, '2022-07-07 13:15:48.554+07'),(116, 'OBIEE', 213, '69', '2022-07-07 13:16:15.284+07', NULL, '2022-07-07 13:16:15.284+07'),(117, 'Oracle', 213, '69', '2022-07-07 13:16:15.284+07', NULL, '2022-07-07 13:16:15.284+07'),(118, 'Report', 213, '69', '2022-07-07 13:16:15.284+07', NULL, '2022-07-07 13:16:15.284+07');
COMMIT;
BEGIN;
LOCK TABLE "public"."videos_views" IN SHARE MODE;
DELETE FROM "public"."videos_views";
INSERT INTO "public"."videos_views" ("id","video_id","views") VALUES (40, 208, 1),(41, 213, 5),(42, 204, 2),(43, 205, 1);
COMMIT;
BEGIN;
LOCK TABLE "public"."videos_watch_tags" IN SHARE MODE;
DELETE FROM "public"."videos_watch_tags";
INSERT INTO "public"."videos_watch_tags" ("id","title_tags","video_watch_id","user_watch_id","created_by","created_at","updated_by","updated_at") VALUES (3589, 'Oracle', 208, 69, '69', '2022-07-07 13:19:01.3+07', NULL, '2022-07-07 13:19:01.3+07'),(3588, 'Report', 208, 69, '69', '2022-07-07 13:19:01.3+07', NULL, '2022-07-07 13:19:01.3+07'),(3590, 'OBIEE', 208, 69, '69', '2022-07-07 13:19:01.3+07', NULL, '2022-07-07 13:19:01.3+07'),(3591, 'API', 204, 69, '69', '2022-07-07 14:53:45.274+07', NULL, '2022-07-07 14:53:45.274+07'),(3592, 'Programe', 204, 69, '69', '2022-07-07 14:53:45.274+07', NULL, '2022-07-07 14:53:45.274+07'),(3593, 'WSO2', 204, 69, '69', '2022-07-07 14:53:45.274+07', NULL, '2022-07-07 14:53:45.274+07'),(3594, 'Report', 213, 69, '69', '2022-07-07 16:34:39.325+07', NULL, '2022-07-07 16:34:39.325+07'),(3595, 'Oracle', 213, 69, '69', '2022-07-07 16:34:39.325+07', NULL, '2022-07-07 16:34:39.325+07'),(3596, 'OBIEE', 213, 69, '69', '2022-07-07 16:34:39.325+07', NULL, '2022-07-07 16:34:39.325+07'),(3597, 'Report', 213, 69, '69', '2022-07-07 16:38:49.087+07', NULL, '2022-07-07 16:38:49.087+07'),(3598, 'Oracle', 213, 69, '69', '2022-07-07 16:38:49.087+07', NULL, '2022-07-07 16:38:49.087+07'),(3599, 'OBIEE', 213, 69, '69', '2022-07-07 16:38:49.087+07', NULL, '2022-07-07 16:38:49.087+07'),(3600, 'Report', 213, 69, '69', '2022-07-07 16:39:00.361+07', NULL, '2022-07-07 16:39:00.361+07'),(3601, 'Oracle', 213, 69, '69', '2022-07-07 16:39:00.361+07', NULL, '2022-07-07 16:39:00.361+07'),(3602, 'OBIEE', 213, 69, '69', '2022-07-07 16:39:00.361+07', NULL, '2022-07-07 16:39:00.361+07'),(3603, 'Report', 213, 69, '69', '2022-07-07 16:39:01.835+07', NULL, '2022-07-07 16:39:01.835+07'),(3604, 'Oracle', 213, 69, '69', '2022-07-07 16:39:01.835+07', NULL, '2022-07-07 16:39:01.835+07'),(3605, 'OBIEE', 213, 69, '69', '2022-07-07 16:39:01.835+07', NULL, '2022-07-07 16:39:01.835+07'),(3606, 'Report', 213, 69, '69', '2022-07-07 16:39:04.204+07', NULL, '2022-07-07 16:39:04.204+07'),(3607, 'Oracle', 213, 69, '69', '2022-07-07 16:39:04.204+07', NULL, '2022-07-07 16:39:04.204+07'),(3608, 'OBIEE', 213, 69, '69', '2022-07-07 16:39:04.204+07', NULL, '2022-07-07 16:39:04.204+07'),(3609, 'Report', 213, 69, '69', '2022-07-07 16:39:05.921+07', NULL, '2022-07-07 16:39:05.921+07'),(3610, 'Oracle', 213, 69, '69', '2022-07-07 16:39:05.921+07', NULL, '2022-07-07 16:39:05.921+07'),(3611, 'OBIEE', 213, 69, '69', '2022-07-07 16:39:05.921+07', NULL, '2022-07-07 16:39:05.921+07'),(3612, 'Report', 213, 69, '69', '2022-07-07 16:39:19.851+07', NULL, '2022-07-07 16:39:19.851+07'),(3613, 'Oracle', 213, 69, '69', '2022-07-07 16:39:19.851+07', NULL, '2022-07-07 16:39:19.851+07'),(3614, 'OBIEE', 213, 69, '69', '2022-07-07 16:39:19.851+07', NULL, '2022-07-07 16:39:19.851+07'),(3615, 'Report', 213, 69, '69', '2022-07-07 16:39:23.033+07', NULL, '2022-07-07 16:39:23.033+07'),(3616, 'Oracle', 213, 69, '69', '2022-07-07 16:39:23.033+07', NULL, '2022-07-07 16:39:23.033+07'),(3617, 'OBIEE', 213, 69, '69', '2022-07-07 16:39:23.033+07', NULL, '2022-07-07 16:39:23.033+07'),(3618, 'Report', 213, 69, '69', '2022-07-07 16:39:24.034+07', NULL, '2022-07-07 16:39:24.034+07'),(3619, 'Oracle', 213, 69, '69', '2022-07-07 16:39:24.035+07', NULL, '2022-07-07 16:39:24.035+07'),(3620, 'OBIEE', 213, 69, '69', '2022-07-07 16:39:24.035+07', NULL, '2022-07-07 16:39:24.035+07'),(3621, 'Report', 213, 69, '69', '2022-07-07 16:39:25.617+07', NULL, '2022-07-07 16:39:25.617+07'),(3622, 'Oracle', 213, 69, '69', '2022-07-07 16:39:25.617+07', NULL, '2022-07-07 16:39:25.617+07'),(3623, 'OBIEE', 213, 69, '69', '2022-07-07 16:39:25.617+07', NULL, '2022-07-07 16:39:25.617+07'),(3624, 'Report', 213, 69, '69', '2022-07-07 16:39:27.519+07', NULL, '2022-07-07 16:39:27.519+07'),(3625, 'Oracle', 213, 69, '69', '2022-07-07 16:39:27.519+07', NULL, '2022-07-07 16:39:27.519+07'),(3626, 'OBIEE', 213, 69, '69', '2022-07-07 16:39:27.519+07', NULL, '2022-07-07 16:39:27.519+07'),(3627, 'Report', 213, 69, '69', '2022-07-07 16:39:36.703+07', NULL, '2022-07-07 16:39:36.703+07'),(3628, 'Oracle', 213, 69, '69', '2022-07-07 16:39:36.703+07', NULL, '2022-07-07 16:39:36.703+07'),(3629, 'OBIEE', 213, 69, '69', '2022-07-07 16:39:36.703+07', NULL, '2022-07-07 16:39:36.703+07'),(3630, 'Report', 213, 69, '69', '2022-07-07 16:39:37.529+07', NULL, '2022-07-07 16:39:37.529+07'),(3631, 'Oracle', 213, 69, '69', '2022-07-07 16:39:37.529+07', NULL, '2022-07-07 16:39:37.529+07'),(3632, 'OBIEE', 213, 69, '69', '2022-07-07 16:39:37.529+07', NULL, '2022-07-07 16:39:37.529+07'),(3633, 'Report', 213, 69, '69', '2022-07-07 16:39:39.912+07', NULL, '2022-07-07 16:39:39.912+07'),(3634, 'OBIEE', 213, 69, '69', '2022-07-07 16:39:39.912+07', NULL, '2022-07-07 16:39:39.912+07'),(3635, 'Oracle', 213, 69, '69', '2022-07-07 16:39:39.912+07', NULL, '2022-07-07 16:39:39.912+07'),(3636, 'Report', 213, 69, '69', '2022-07-07 16:39:42.083+07', NULL, '2022-07-07 16:39:42.083+07'),(3637, 'Oracle', 213, 69, '69', '2022-07-07 16:39:42.083+07', NULL, '2022-07-07 16:39:42.083+07'),(3638, 'OBIEE', 213, 69, '69', '2022-07-07 16:39:42.083+07', NULL, '2022-07-07 16:39:42.083+07'),(3639, 'Report', 213, 69, '69', '2022-07-07 16:39:43.175+07', NULL, '2022-07-07 16:39:43.175+07'),(3640, 'Oracle', 213, 69, '69', '2022-07-07 16:39:43.175+07', NULL, '2022-07-07 16:39:43.175+07'),(3641, 'OBIEE', 213, 69, '69', '2022-07-07 16:39:43.175+07', NULL, '2022-07-07 16:39:43.175+07'),(3642, 'Report', 213, 69, '69', '2022-07-07 16:39:44.618+07', NULL, '2022-07-07 16:39:44.618+07'),(3643, 'Oracle', 213, 69, '69', '2022-07-07 16:39:44.618+07', NULL, '2022-07-07 16:39:44.618+07'),(3644, 'OBIEE', 213, 69, '69', '2022-07-07 16:39:44.618+07', NULL, '2022-07-07 16:39:44.618+07'),(3645, 'Oracle', 213, 69, '69', '2022-07-07 16:39:46.572+07', NULL, '2022-07-07 16:39:46.572+07'),(3646, 'Report', 213, 69, '69', '2022-07-07 16:39:46.572+07', NULL, '2022-07-07 16:39:46.572+07'),(3647, 'OBIEE', 213, 69, '69', '2022-07-07 16:39:46.572+07', NULL, '2022-07-07 16:39:46.572+07'),(3648, 'Report', 213, 69, '69', '2022-07-07 16:45:42.424+07', NULL, '2022-07-07 16:45:42.424+07'),(3649, 'Oracle', 213, 69, '69', '2022-07-07 16:45:42.424+07', NULL, '2022-07-07 16:45:42.424+07'),(3650, 'OBIEE', 213, 69, '69', '2022-07-07 16:45:42.424+07', NULL, '2022-07-07 16:45:42.424+07'),(3651, 'Report', 213, 69, '69', '2022-07-07 16:45:59.253+07', NULL, '2022-07-07 16:45:59.253+07'),(3652, 'Oracle', 213, 69, '69', '2022-07-07 16:45:59.253+07', NULL, '2022-07-07 16:45:59.253+07'),(3653, 'OBIEE', 213, 69, '69', '2022-07-07 16:45:59.253+07', NULL, '2022-07-07 16:45:59.253+07'),(3654, 'Report', 213, 69, '69', '2022-07-07 16:46:01.14+07', NULL, '2022-07-07 16:46:01.14+07'),(3655, 'Oracle', 213, 69, '69', '2022-07-07 16:46:01.14+07', NULL, '2022-07-07 16:46:01.14+07'),(3656, 'OBIEE', 213, 69, '69', '2022-07-07 16:46:01.14+07', NULL, '2022-07-07 16:46:01.14+07'),(3657, 'Report', 213, 69, '69', '2022-07-07 16:46:06.353+07', NULL, '2022-07-07 16:46:06.353+07'),(3658, 'Oracle', 213, 69, '69', '2022-07-07 16:46:06.353+07', NULL, '2022-07-07 16:46:06.353+07'),(3659, 'OBIEE', 213, 69, '69', '2022-07-07 16:46:06.353+07', NULL, '2022-07-07 16:46:06.353+07'),(3661, 'Oracle', 213, 69, '69', '2022-07-07 16:46:08.563+07', NULL, '2022-07-07 16:46:08.563+07'),(3660, 'Report', 213, 69, '69', '2022-07-07 16:46:08.563+07', NULL, '2022-07-07 16:46:08.563+07'),(3662, 'OBIEE', 213, 69, '69', '2022-07-07 16:46:08.563+07', NULL, '2022-07-07 16:46:08.563+07'),(3663, 'Report', 213, 69, '69', '2022-07-07 16:46:09.256+07', NULL, '2022-07-07 16:46:09.256+07'),(3664, 'Oracle', 213, 69, '69', '2022-07-07 16:46:09.256+07', NULL, '2022-07-07 16:46:09.256+07'),(3665, 'OBIEE', 213, 69, '69', '2022-07-07 16:46:09.256+07', NULL, '2022-07-07 16:46:09.256+07'),(3666, 'Report', 213, 69, '69', '2022-07-07 16:46:12.551+07', NULL, '2022-07-07 16:46:12.551+07'),(3667, 'Oracle', 213, 69, '69', '2022-07-07 16:46:12.551+07', NULL, '2022-07-07 16:46:12.551+07'),(3668, 'OBIEE', 213, 69, '69', '2022-07-07 16:46:12.551+07', NULL, '2022-07-07 16:46:12.551+07'),(3669, 'Report', 213, 69, '69', '2022-07-07 16:46:14.459+07', NULL, '2022-07-07 16:46:14.459+07'),(3670, 'Oracle', 213, 69, '69', '2022-07-07 16:46:14.459+07', NULL, '2022-07-07 16:46:14.459+07'),(3671, 'OBIEE', 213, 69, '69', '2022-07-07 16:46:14.459+07', NULL, '2022-07-07 16:46:14.459+07'),(3672, 'Report', 213, 69, '69', '2022-07-07 16:46:15.454+07', NULL, '2022-07-07 16:46:15.454+07'),(3673, 'Oracle', 213, 69, '69', '2022-07-07 16:46:15.454+07', NULL, '2022-07-07 16:46:15.454+07'),(3674, 'OBIEE', 213, 69, '69', '2022-07-07 16:46:15.454+07', NULL, '2022-07-07 16:46:15.454+07'),(3675, 'Report', 213, 69, '69', '2022-07-07 16:46:16.877+07', NULL, '2022-07-07 16:46:16.877+07'),(3676, 'Oracle', 213, 69, '69', '2022-07-07 16:46:16.877+07', NULL, '2022-07-07 16:46:16.877+07'),(3677, 'OBIEE', 213, 69, '69', '2022-07-07 16:46:16.877+07', NULL, '2022-07-07 16:46:16.877+07'),(3678, 'Report', 213, 69, '69', '2022-07-07 16:46:18.707+07', NULL, '2022-07-07 16:46:18.707+07'),(3679, 'Oracle', 213, 69, '69', '2022-07-07 16:46:18.707+07', NULL, '2022-07-07 16:46:18.707+07'),(3680, 'OBIEE', 213, 69, '69', '2022-07-07 16:46:18.707+07', NULL, '2022-07-07 16:46:18.707+07'),(3681, 'Report', 213, 69, '69', '2022-07-07 16:46:27.411+07', NULL, '2022-07-07 16:46:27.411+07'),(3682, 'Oracle', 213, 69, '69', '2022-07-07 16:46:27.411+07', NULL, '2022-07-07 16:46:27.411+07'),(3683, 'OBIEE', 213, 69, '69', '2022-07-07 16:46:27.411+07', NULL, '2022-07-07 16:46:27.411+07'),(3684, 'Report', 213, 69, '69', '2022-07-07 16:46:29.084+07', NULL, '2022-07-07 16:46:29.084+07'),(3685, 'Oracle', 213, 69, '69', '2022-07-07 16:46:29.084+07', NULL, '2022-07-07 16:46:29.084+07'),(3686, 'OBIEE', 213, 69, '69', '2022-07-07 16:46:29.084+07', NULL, '2022-07-07 16:46:29.084+07'),(3687, 'Report', 213, 69, '69', '2022-07-07 16:46:33.43+07', NULL, '2022-07-07 16:46:33.43+07'),(3688, 'Oracle', 213, 69, '69', '2022-07-07 16:46:33.43+07', NULL, '2022-07-07 16:46:33.43+07'),(3689, 'OBIEE', 213, 69, '69', '2022-07-07 16:46:33.43+07', NULL, '2022-07-07 16:46:33.43+07'),(3690, 'Report', 213, 69, '69', '2022-07-07 16:46:34.402+07', NULL, '2022-07-07 16:46:34.402+07'),(3691, 'Oracle', 213, 69, '69', '2022-07-07 16:46:34.402+07', NULL, '2022-07-07 16:46:34.402+07'),(3692, 'OBIEE', 213, 69, '69', '2022-07-07 16:46:34.402+07', NULL, '2022-07-07 16:46:34.402+07'),(3693, 'Report', 213, 69, '69', '2022-07-07 16:46:36.683+07', NULL, '2022-07-07 16:46:36.683+07'),(3694, 'Oracle', 213, 69, '69', '2022-07-07 16:46:36.683+07', NULL, '2022-07-07 16:46:36.683+07'),(3695, 'OBIEE', 213, 69, '69', '2022-07-07 16:46:36.683+07', NULL, '2022-07-07 16:46:36.683+07'),(3696, 'Report', 213, 69, '69', '2022-07-07 16:46:38.815+07', NULL, '2022-07-07 16:46:38.815+07'),(3697, 'Oracle', 213, 69, '69', '2022-07-07 16:46:38.815+07', NULL, '2022-07-07 16:46:38.815+07'),(3698, 'OBIEE', 213, 69, '69', '2022-07-07 16:46:38.815+07', NULL, '2022-07-07 16:46:38.815+07'),(3699, 'Report', 213, 69, '69', '2022-07-07 16:46:39.782+07', NULL, '2022-07-07 16:46:39.782+07'),(3700, 'Oracle', 213, 69, '69', '2022-07-07 16:46:39.782+07', NULL, '2022-07-07 16:46:39.782+07'),(3701, 'OBIEE', 213, 69, '69', '2022-07-07 16:46:39.782+07', NULL, '2022-07-07 16:46:39.782+07'),(3702, 'Report', 213, 69, '69', '2022-07-07 16:46:41.259+07', NULL, '2022-07-07 16:46:41.259+07'),(3703, 'Oracle', 213, 69, '69', '2022-07-07 16:46:41.259+07', NULL, '2022-07-07 16:46:41.259+07'),(3704, 'OBIEE', 213, 69, '69', '2022-07-07 16:46:41.259+07', NULL, '2022-07-07 16:46:41.259+07'),(3705, 'Report', 213, 69, '69', '2022-07-07 16:46:43.056+07', NULL, '2022-07-07 16:46:43.056+07'),(3706, 'Oracle', 213, 69, '69', '2022-07-07 16:46:43.056+07', NULL, '2022-07-07 16:46:43.056+07'),(3707, 'OBIEE', 213, 69, '69', '2022-07-07 16:46:43.056+07', NULL, '2022-07-07 16:46:43.056+07'),(3708, 'API', 204, 69, '69', '2022-07-07 16:48:31.601+07', NULL, '2022-07-07 16:48:31.601+07'),(3710, 'WSO2', 204, 69, '69', '2022-07-07 16:48:31.601+07', NULL, '2022-07-07 16:48:31.601+07'),(3709, 'Programe', 204, 69, '69', '2022-07-07 16:48:31.601+07', NULL, '2022-07-07 16:48:31.601+07'),(3711, 'Report', 205, 69, '69', '2022-07-07 16:48:43.939+07', NULL, '2022-07-07 16:48:43.939+07'),(3712, 'Oracle', 205, 69, '69', '2022-07-07 16:48:43.939+07', NULL, '2022-07-07 16:48:43.939+07'),(3713, 'OBIEE', 205, 69, '69', '2022-07-07 16:48:43.939+07', NULL, '2022-07-07 16:48:43.939+07'),(3714, 'Report', 205, 69, '69', '2022-07-07 16:48:50.188+07', NULL, '2022-07-07 16:48:50.188+07'),(3715, 'Oracle', 205, 69, '69', '2022-07-07 16:48:50.188+07', NULL, '2022-07-07 16:48:50.188+07'),(3716, 'OBIEE', 205, 69, '69', '2022-07-07 16:48:50.188+07', NULL, '2022-07-07 16:48:50.188+07'),(3717, 'Oracle', 205, 69, '69', '2022-07-07 16:48:51.771+07', NULL, '2022-07-07 16:48:51.771+07'),(3718, 'Report', 205, 69, '69', '2022-07-07 16:48:51.771+07', NULL, '2022-07-07 16:48:51.771+07'),(3719, 'OBIEE', 205, 69, '69', '2022-07-07 16:48:51.771+07', NULL, '2022-07-07 16:48:51.771+07'),(3720, 'Report', 205, 69, '69', '2022-07-07 16:48:52.674+07', NULL, '2022-07-07 16:48:52.674+07'),(3721, 'Oracle', 205, 69, '69', '2022-07-07 16:48:52.674+07', NULL, '2022-07-07 16:48:52.674+07'),(3722, 'OBIEE', 205, 69, '69', '2022-07-07 16:48:52.674+07', NULL, '2022-07-07 16:48:52.674+07'),(3723, 'Report', 205, 69, '69', '2022-07-07 16:48:53.999+07', NULL, '2022-07-07 16:48:53.999+07'),(3724, 'Oracle', 205, 69, '69', '2022-07-07 16:48:53.999+07', NULL, '2022-07-07 16:48:53.999+07'),(3725, 'OBIEE', 205, 69, '69', '2022-07-07 16:48:53.999+07', NULL, '2022-07-07 16:48:53.999+07'),(3726, 'Report', 205, 69, '69', '2022-07-07 16:48:55.667+07', NULL, '2022-07-07 16:48:55.667+07'),(3727, 'Oracle', 205, 69, '69', '2022-07-07 16:48:55.667+07', NULL, '2022-07-07 16:48:55.667+07'),(3728, 'OBIEE', 205, 69, '69', '2022-07-07 16:48:55.667+07', NULL, '2022-07-07 16:48:55.667+07'),(3729, 'Report', 205, 69, '69', '2022-07-07 16:49:08.559+07', NULL, '2022-07-07 16:49:08.559+07'),(3730, 'Oracle', 205, 69, '69', '2022-07-07 16:49:08.559+07', NULL, '2022-07-07 16:49:08.559+07'),(3731, 'OBIEE', 205, 69, '69', '2022-07-07 16:49:08.559+07', NULL, '2022-07-07 16:49:08.559+07'),(3732, 'Report', 205, 69, '69', '2022-07-07 16:49:09.254+07', NULL, '2022-07-07 16:49:09.254+07'),(3733, 'Oracle', 205, 69, '69', '2022-07-07 16:49:09.254+07', NULL, '2022-07-07 16:49:09.254+07'),(3734, 'OBIEE', 205, 69, '69', '2022-07-07 16:49:09.254+07', NULL, '2022-07-07 16:49:09.254+07'),(3735, 'Report', 205, 69, '69', '2022-07-07 16:49:12.123+07', NULL, '2022-07-07 16:49:12.123+07'),(3736, 'Oracle', 205, 69, '69', '2022-07-07 16:49:12.123+07', NULL, '2022-07-07 16:49:12.123+07'),(3737, 'OBIEE', 205, 69, '69', '2022-07-07 16:49:12.123+07', NULL, '2022-07-07 16:49:12.123+07'),(3738, 'Report', 205, 69, '69', '2022-07-07 16:49:14.029+07', NULL, '2022-07-07 16:49:14.029+07'),(3739, 'Oracle', 205, 69, '69', '2022-07-07 16:49:14.029+07', NULL, '2022-07-07 16:49:14.029+07'),(3740, 'OBIEE', 205, 69, '69', '2022-07-07 16:49:14.029+07', NULL, '2022-07-07 16:49:14.029+07'),(3741, 'Report', 205, 69, '69', '2022-07-07 16:49:14.921+07', NULL, '2022-07-07 16:49:14.921+07'),(3742, 'Oracle', 205, 69, '69', '2022-07-07 16:49:14.921+07', NULL, '2022-07-07 16:49:14.921+07'),(3743, 'OBIEE', 205, 69, '69', '2022-07-07 16:49:14.921+07', NULL, '2022-07-07 16:49:14.921+07'),(3744, 'Report', 205, 69, '69', '2022-07-07 16:49:16.247+07', NULL, '2022-07-07 16:49:16.247+07'),(3745, 'Oracle', 205, 69, '69', '2022-07-07 16:49:16.247+07', NULL, '2022-07-07 16:49:16.247+07'),(3746, 'OBIEE', 205, 69, '69', '2022-07-07 16:49:16.247+07', NULL, '2022-07-07 16:49:16.247+07'),(3747, 'Oracle', 205, 69, '69', '2022-07-07 16:49:17.886+07', NULL, '2022-07-07 16:49:17.886+07'),(3749, 'Report', 205, 69, '69', '2022-07-07 16:49:17.886+07', NULL, '2022-07-07 16:49:17.886+07'),(3748, 'OBIEE', 205, 69, '69', '2022-07-07 16:49:17.886+07', NULL, '2022-07-07 16:49:17.886+07');
COMMIT;
ALTER TABLE "auth_provider" ADD CONSTRAINT "auth_provider_key" PRIMARY KEY ("auth_provider_id");
ALTER TABLE "auth_user_provider" ADD CONSTRAINT "auth_user_provider_pkey" PRIMARY KEY ("auth_user_provider_id");
ALTER TABLE "device" ADD CONSTRAINT "device_pkey" PRIMARY KEY ("device_id");
ALTER TABLE "feedbacks" ADD CONSTRAINT "feedbacks_pkey" PRIMARY KEY ("id");
ALTER TABLE "log_users" ADD CONSTRAINT "log_users_pkey" PRIMARY KEY ("id");
ALTER TABLE "migrations" ADD CONSTRAINT "migrations_pkey" PRIMARY KEY ("id");
ALTER TABLE "migrations_lock" ADD CONSTRAINT "migrations_lock_pkey" PRIMARY KEY ("index");
ALTER TABLE "mt_lov" ADD CONSTRAINT "mt_lov_pkey" PRIMARY KEY ("id");
ALTER TABLE "register_verify" ADD CONSTRAINT "register_verify_pkey" PRIMARY KEY ("id");
ALTER TABLE "users" ADD CONSTRAINT "users_pkey" PRIMARY KEY ("id");
ALTER TABLE "videos" ADD CONSTRAINT "videos_pkey" PRIMARY KEY ("id");
CREATE INDEX "index_cate_title" ON "videos" USING btree (
  "category" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST,
  "title" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);
CREATE INDEX "index_userId" ON "videos" USING btree (
  "userid" "pg_catalog"."int8_ops" ASC NULLS LAST
);
ALTER TABLE "videos_action" ADD CONSTRAINT "videos_action_pkey" PRIMARY KEY ("id");
ALTER TABLE "videos_channel" ADD CONSTRAINT "videos_channel_pkey" PRIMARY KEY ("id");
ALTER TABLE "videos_history" ADD CONSTRAINT "videos_history_pkey" PRIMARY KEY ("id");
ALTER TABLE "videos_playlist" ADD CONSTRAINT "video_playlist_pkey" PRIMARY KEY ("id");
ALTER TABLE "videos_playlist_add" ADD CONSTRAINT "videos_playlist_add_pkey" PRIMARY KEY ("id");
ALTER TABLE "videos_tags" ADD CONSTRAINT "videos_tags_pkey" PRIMARY KEY ("id");
ALTER TABLE "videos_views" ADD CONSTRAINT "videos_views_pkey" PRIMARY KEY ("id");
ALTER TABLE "videos_watch_tags" ADD CONSTRAINT "videos_watch_tags_pkey" PRIMARY KEY ("id");
ALTER TABLE "users" ADD CONSTRAINT "users_email_unique" UNIQUE ("email_address");
ALTER SEQUENCE "feedbacks_id_seq"
OWNED BY "feedbacks"."id";
SELECT setval('"feedbacks_id_seq"', 9, true);
ALTER SEQUENCE "feedbacks_id_seq" OWNER TO "postgres";
ALTER SEQUENCE "log_users_id_seq"
OWNED BY "log_users"."id";
SELECT setval('"log_users_id_seq"', 286, true);
ALTER SEQUENCE "log_users_id_seq" OWNER TO "postgres";
ALTER SEQUENCE "migrations_id_seq"
OWNED BY "migrations"."id";
SELECT setval('"migrations_id_seq"', 2, true);
ALTER SEQUENCE "migrations_id_seq" OWNER TO "postgres";
ALTER SEQUENCE "migrations_lock_index_seq"
OWNED BY "migrations_lock"."index";
SELECT setval('"migrations_lock_index_seq"', 2, true);
ALTER SEQUENCE "migrations_lock_index_seq" OWNER TO "postgres";
ALTER SEQUENCE "mt_lov_id_seq"
OWNED BY "mt_lov"."id";
SELECT setval('"mt_lov_id_seq"', 23, true);
ALTER SEQUENCE "mt_lov_id_seq" OWNER TO "postgres";
ALTER SEQUENCE "register_verify_id_seq"
OWNED BY "register_verify"."id";
SELECT setval('"register_verify_id_seq"', 45, true);
ALTER SEQUENCE "register_verify_id_seq" OWNER TO "postgres";
ALTER SEQUENCE "users_id_seq"
OWNED BY "users"."id";
SELECT setval('"users_id_seq"', 71, true);
ALTER SEQUENCE "users_id_seq" OWNER TO "postgres";
ALTER SEQUENCE "video_playlist_id_seq"
OWNED BY "videos_playlist"."id";
SELECT setval('"video_playlist_id_seq"', 44, true);
ALTER SEQUENCE "video_playlist_id_seq" OWNER TO "postgres";
ALTER SEQUENCE "videos_action_id_seq"
OWNED BY "videos_action"."id";
SELECT setval('"videos_action_id_seq"', 126, true);
ALTER SEQUENCE "videos_action_id_seq" OWNER TO "postgres";
ALTER SEQUENCE "videos_channel_id_seq"
OWNED BY "videos_channel"."id";
SELECT setval('"videos_channel_id_seq"', 34, true);
ALTER SEQUENCE "videos_channel_id_seq" OWNER TO "postgres";
ALTER SEQUENCE "videos_history_id_seq"
OWNED BY "videos_history"."id";
SELECT setval('"videos_history_id_seq"', 573, true);
ALTER SEQUENCE "videos_history_id_seq" OWNER TO "postgres";
ALTER SEQUENCE "videos_id_seq"
OWNED BY "videos"."id";
SELECT setval('"videos_id_seq"', 214, true);
ALTER SEQUENCE "videos_id_seq" OWNER TO "postgres";
ALTER SEQUENCE "videos_playlist_add_id_seq"
OWNED BY "videos_playlist_add"."id";
SELECT setval('"videos_playlist_add_id_seq"', 71, true);
ALTER SEQUENCE "videos_playlist_add_id_seq" OWNER TO "postgres";
ALTER SEQUENCE "videos_tags_id_seq"
OWNED BY "videos_tags"."id";
SELECT setval('"videos_tags_id_seq"', 119, true);
ALTER SEQUENCE "videos_tags_id_seq" OWNER TO "postgres";
ALTER SEQUENCE "videos_views_id_seq"
OWNED BY "videos_views"."id";
SELECT setval('"videos_views_id_seq"', 44, true);
ALTER SEQUENCE "videos_views_id_seq" OWNER TO "postgres";
ALTER SEQUENCE "videos_watch_tags_id_seq"
OWNED BY "videos_watch_tags"."id";
SELECT setval('"videos_watch_tags_id_seq"', 3750, true);
ALTER SEQUENCE "videos_watch_tags_id_seq" OWNER TO "postgres";
