const secretKey = {
  secret: 'bezkoder-secret-key',
  oauthId:'AUTH_ID',
  oauthSecret:'OAUTH_SECRET'
};
module.exports = {
  secretKey,
};
