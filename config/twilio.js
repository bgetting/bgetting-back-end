require('dotenv').config();

const accountSid = 'AC6c8ab91e7643b8f10b11952301543219';
const authToken = 'e47ff849c2159f0abb969856bf71c360';

const sendSms = (phone, message) => {
  const client = require('twilio')(accountSid, authToken);
  client.messages
    .create({
       body: message,
       from: '+15672986125',
       to: phone
     })
    .then(message => console.log(message.sid));
}

module.exports = sendSms;