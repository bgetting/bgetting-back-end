PGDMP         -                y            creativeTim    13beta3    13beta3 4    }           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            ~           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false                       0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            �           1262    16394    creativeTim    DATABASE     q   CREATE DATABASE "creativeTim" WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'English_United States.1252';
    DROP DATABASE "creativeTim";
                postgres    false                        3079    16384 	   adminpack 	   EXTENSION     A   CREATE EXTENSION IF NOT EXISTS adminpack WITH SCHEMA pg_catalog;
    DROP EXTENSION adminpack;
                   false            �           0    0    EXTENSION adminpack    COMMENT     M   COMMENT ON EXTENSION adminpack IS 'administrative functions for PostgreSQL';
                        false    2            �            1259    16469    auth_provider    TABLE     k  CREATE TABLE public.auth_provider (
    auth_provider_id bigint NOT NULL,
    provider_type character varying(255),
    created_by character varying(150),
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_by character varying(150),
    updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    status boolean DEFAULT true
);
 !   DROP TABLE public.auth_provider;
       public         heap    postgres    false            �            1259    16413    users    TABLE     g  CREATE TABLE public.users (
    id bigint NOT NULL,
    email_address character varying(150),
    username character varying(150) NOT NULL,
    password character varying(150) NOT NULL,
    email_verified_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    created_by character varying(150) COLLATE pg_catalog."C",
    updated_by character varying(150) COLLATE pg_catalog."POSIX",
    phone_number character varying(30),
    status boolean DEFAULT true
);
    DROP TABLE public.users;
       public         heap    postgres    false            �            1259    16411    users_id_seq    SEQUENCE     �   CREATE SEQUENCE public.users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.users_id_seq;
       public          postgres    false    206            �           0    0    users_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;
          public          postgres    false    205            �            1259    16481    auth_user_provider    TABLE     c  CREATE TABLE public.auth_user_provider (
    auth_user_provider_id bigint DEFAULT nextval('public.users_id_seq'::regclass) NOT NULL,
    created_by character varying(150),
    created_at timestamp without time zone,
    updated_by character varying(150),
    updated_at timestamp without time zone,
    auth_provider_id bigint,
    auth_user_id bigint
);
 &   DROP TABLE public.auth_user_provider;
       public         heap    postgres    false    205            �            1259    16486    device    TABLE     �  CREATE TABLE public.device (
    device_id bigint NOT NULL,
    browser character varying(150),
    ip_address character varying(150),
    model character varying(150),
    name character varying(150),
    version character varying(150),
    created_by character varying(150),
    created_at timestamp with time zone,
    updated_by character varying(150),
    updated_at timestamp with time zone
);
    DROP TABLE public.device;
       public         heap    postgres    false            �            1259    16397 
   migrations    TABLE     �   CREATE TABLE public.migrations (
    id integer NOT NULL,
    name character varying(255),
    batch integer,
    migration_time timestamp with time zone
);
    DROP TABLE public.migrations;
       public         heap    postgres    false            �            1259    16395    migrations_id_seq    SEQUENCE     �   CREATE SEQUENCE public.migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.migrations_id_seq;
       public          postgres    false    202            �           0    0    migrations_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.migrations_id_seq OWNED BY public.migrations.id;
          public          postgres    false    201            �            1259    16405    migrations_lock    TABLE     [   CREATE TABLE public.migrations_lock (
    index integer NOT NULL,
    is_locked integer
);
 #   DROP TABLE public.migrations_lock;
       public         heap    postgres    false            �            1259    16403    migrations_lock_index_seq    SEQUENCE     �   CREATE SEQUENCE public.migrations_lock_index_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 0   DROP SEQUENCE public.migrations_lock_index_seq;
       public          postgres    false    204            �           0    0    migrations_lock_index_seq    SEQUENCE OWNED BY     W   ALTER SEQUENCE public.migrations_lock_index_seq OWNED BY public.migrations_lock.index;
          public          postgres    false    203            �            1259    41014    videos    TABLE     (  CREATE TABLE public.videos (
    id integer NOT NULL,
    userid bigint NOT NULL,
    patch text,
    status boolean DEFAULT true,
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    created_by character varying(255),
    updated_by character varying(255),
    category character varying(255) NOT NULL,
    title character varying(255) NOT NULL,
    filename text,
    destination text,
    originalname text,
    size character varying(255),
    mimetype character varying(150),
    encoding character varying(50)
);
    DROP TABLE public.videos;
       public         heap    postgres    false            �            1259    41077    videos_action    TABLE     }  CREATE TABLE public.videos_action (
    id integer NOT NULL,
    userid bigint,
    views bigint DEFAULT 0,
    likes bigint DEFAULT 0,
    unlikes bigint DEFAULT 0,
    status boolean DEFAULT true,
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    created_by character varying(255),
    updated_by character varying(255),
    videoid bigint
);
 !   DROP TABLE public.videos_action;
       public         heap    postgres    false            �            1259    41075    videos_action_id_seq    SEQUENCE     �   CREATE SEQUENCE public.videos_action_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.videos_action_id_seq;
       public          postgres    false    213            �           0    0    videos_action_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.videos_action_id_seq OWNED BY public.videos_action.id;
          public          postgres    false    212            �            1259    41012    videos_id_seq    SEQUENCE     �   CREATE SEQUENCE public.videos_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.videos_id_seq;
       public          postgres    false    211            �           0    0    videos_id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE public.videos_id_seq OWNED BY public.videos.id;
          public          postgres    false    210            �            1259    41100    vw_videos_master    VIEW     {  CREATE VIEW public.vw_videos_master AS
 SELECT a.id,
    a.email_address,
    a.username,
    a.phone_number,
    a.email_verified_at,
    a.status AS user_status,
    a.created_at AS user_created_at,
    b.id AS video_id,
    b.patch,
    b.status AS video_status,
    b.category,
    b.title,
    b.filename,
    b.destination,
    b.originalname,
    b.size,
    b.created_at AS video_created_at,
    c.videoid,
    c.views,
    c.likes,
    c.unlikes,
    c.status AS video_action_status
   FROM ((public.users a
     LEFT JOIN public.videos b ON ((a.id = b.userid)))
     LEFT JOIN public.videos_action c ON ((a.id = c.userid)));
 #   DROP VIEW public.vw_videos_master;
       public          postgres    false    211    206    213    213    213    213    213    213    211    211    211    211    211    211    211    211    211    211    206    206    206    206    206    206            �
           2604    16400    migrations id    DEFAULT     n   ALTER TABLE ONLY public.migrations ALTER COLUMN id SET DEFAULT nextval('public.migrations_id_seq'::regclass);
 <   ALTER TABLE public.migrations ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    201    202    202            �
           2604    16408    migrations_lock index    DEFAULT     ~   ALTER TABLE ONLY public.migrations_lock ALTER COLUMN index SET DEFAULT nextval('public.migrations_lock_index_seq'::regclass);
 D   ALTER TABLE public.migrations_lock ALTER COLUMN index DROP DEFAULT;
       public          postgres    false    204    203    204            �
           2604    16430    users id    DEFAULT     d   ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);
 7   ALTER TABLE public.users ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    205    206    206            �
           2604    41017 	   videos id    DEFAULT     f   ALTER TABLE ONLY public.videos ALTER COLUMN id SET DEFAULT nextval('public.videos_id_seq'::regclass);
 8   ALTER TABLE public.videos ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    211    210    211            �
           2604    41080    videos_action id    DEFAULT     t   ALTER TABLE ONLY public.videos_action ALTER COLUMN id SET DEFAULT nextval('public.videos_action_id_seq'::regclass);
 ?   ALTER TABLE public.videos_action ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    212    213    213            t          0    16469    auth_provider 
   TABLE DATA           �   COPY public.auth_provider (auth_provider_id, provider_type, created_by, created_at, updated_by, updated_at, status) FROM stdin;
    public          postgres    false    207   �B       u          0    16481    auth_user_provider 
   TABLE DATA           �   COPY public.auth_user_provider (auth_user_provider_id, created_by, created_at, updated_by, updated_at, auth_provider_id, auth_user_id) FROM stdin;
    public          postgres    false    208   C       v          0    16486    device 
   TABLE DATA           �   COPY public.device (device_id, browser, ip_address, model, name, version, created_by, created_at, updated_by, updated_at) FROM stdin;
    public          postgres    false    209   �C       o          0    16397 
   migrations 
   TABLE DATA           E   COPY public.migrations (id, name, batch, migration_time) FROM stdin;
    public          postgres    false    202   �C       q          0    16405    migrations_lock 
   TABLE DATA           ;   COPY public.migrations_lock (index, is_locked) FROM stdin;
    public          postgres    false    204   D       s          0    16413    users 
   TABLE DATA           �   COPY public.users (id, email_address, username, password, email_verified_at, created_at, updated_at, created_by, updated_by, phone_number, status) FROM stdin;
    public          postgres    false    206   ?D       x          0    41014    videos 
   TABLE DATA           �   COPY public.videos (id, userid, patch, status, created_at, updated_at, created_by, updated_by, category, title, filename, destination, originalname, size, mimetype, encoding) FROM stdin;
    public          postgres    false    211   <F       z          0    41077    videos_action 
   TABLE DATA           �   COPY public.videos_action (id, userid, views, likes, unlikes, status, created_at, updated_at, created_by, updated_by, videoid) FROM stdin;
    public          postgres    false    213   �G       �           0    0    migrations_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.migrations_id_seq', 1, true);
          public          postgres    false    201            �           0    0    migrations_lock_index_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('public.migrations_lock_index_seq', 1, true);
          public          postgres    false    203            �           0    0    users_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.users_id_seq', 12, true);
          public          postgres    false    205            �           0    0    videos_action_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.videos_action_id_seq', 1, false);
          public          postgres    false    212            �           0    0    videos_id_seq    SEQUENCE SET     <   SELECT pg_catalog.setval('public.videos_id_seq', 28, true);
          public          postgres    false    210            �
           2606    16476    auth_provider auth_provider_key 
   CONSTRAINT     k   ALTER TABLE ONLY public.auth_provider
    ADD CONSTRAINT auth_provider_key PRIMARY KEY (auth_provider_id);
 I   ALTER TABLE ONLY public.auth_provider DROP CONSTRAINT auth_provider_key;
       public            postgres    false    207            �
           2606    16485 *   auth_user_provider auth_user_provider_pkey 
   CONSTRAINT     {   ALTER TABLE ONLY public.auth_user_provider
    ADD CONSTRAINT auth_user_provider_pkey PRIMARY KEY (auth_user_provider_id);
 T   ALTER TABLE ONLY public.auth_user_provider DROP CONSTRAINT auth_user_provider_pkey;
       public            postgres    false    208            �
           2606    16493    device device_pkey 
   CONSTRAINT     W   ALTER TABLE ONLY public.device
    ADD CONSTRAINT device_pkey PRIMARY KEY (device_id);
 <   ALTER TABLE ONLY public.device DROP CONSTRAINT device_pkey;
       public            postgres    false    209            �
           2606    16410 $   migrations_lock migrations_lock_pkey 
   CONSTRAINT     e   ALTER TABLE ONLY public.migrations_lock
    ADD CONSTRAINT migrations_lock_pkey PRIMARY KEY (index);
 N   ALTER TABLE ONLY public.migrations_lock DROP CONSTRAINT migrations_lock_pkey;
       public            postgres    false    204            �
           2606    16402    migrations migrations_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.migrations
    ADD CONSTRAINT migrations_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.migrations DROP CONSTRAINT migrations_pkey;
       public            postgres    false    202            �
           2606    16429    users users_email_unique 
   CONSTRAINT     \   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_email_unique UNIQUE (email_address);
 B   ALTER TABLE ONLY public.users DROP CONSTRAINT users_email_unique;
       public            postgres    false    206            �
           2606    16432    users users_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.users DROP CONSTRAINT users_pkey;
       public            postgres    false    206            �
           2606    41086     videos_action videos_action_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.videos_action
    ADD CONSTRAINT videos_action_pkey PRIMARY KEY (id);
 J   ALTER TABLE ONLY public.videos_action DROP CONSTRAINT videos_action_pkey;
       public            postgres    false    213            �
           2606    41023    videos videos_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public.videos
    ADD CONSTRAINT videos_pkey PRIMARY KEY (id);
 <   ALTER TABLE ONLY public.videos DROP CONSTRAINT videos_pkey;
       public            postgres    false    211            t   <   x�3����s���ur�LL�����.#NW_GOL	cN7GgW'oL�=... 4]�      u   �   x�u�;�@���>�����!8A$
�I* a�)}����x��aQ_l\����2���d.���TH�r�d�\'�di-��]�	��^�R[?��j���a⊉�$�`��ҚD��o�^�����L�      v      x������ � �      o   M   x��1
�0�:y��\ؽC�y� Q��$�_�7(��z\��py��.������M)P�:Ŵ����b���a      q      x�3�4������ V      s   �  x���[��@�k����q� WUweeQ9��î}�m����i2	ɼ$�3_�NS��s���.q�l����q��a���6ړr[��ތG�����[���:f�K�
<F���	+VdVڸ���=@��/�	�#_!����0��*�St����釙Bg���,�[�a]wV���Y�}n�1�E8b�	v���/'u�
�������Nȳ,F����K����у���ljk��"�Ne_�x�����+y�c����X�fʨz[]53�yH� �e�7ic�3�oc�h�Z*ve��\Z��=UQN�w�p�Cِ�.�S`���-����tW�	K?&�P[����AC�<wn�znl��K5��xy�`���H�[����[�S|ӌ��)`X���7j����s�������蛬��cȉ:�cŐ���u_E�Ϛ���/k1�z�t&�^=�vra��?"D�	���O�t���6�      x   �  x���]O�0���W���zZ���%݅���A�5�����_/�pљ�M/�r����r�.UjX�&x)Uio�� �n6�u�za�j���Mf�R+��Z�墬u�>SB�D��
�X">.2֔�z+�������(\W+�����DQ�)O����� �!�A��љ�C����A���u���έ7�eM]�xm�lc&��k�Q�.;$�?����?���L}����D����r����Xߣ���H%1�OIgu;�"�7p�B�����,�'����ТS��k!��"�vH��Sf	i�lW��-d%)��~jqR:Z�K-�t����,�ނ�X�v�n�6����l������[H��]D����Y�Y<-��.����8hg�z�j�      z      x������ � �     