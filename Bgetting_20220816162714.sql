/*
PostgreSQL Backup
Database: bgetting/public
Backup Time: 2022-08-16 16:27:19
*/

DROP SEQUENCE IF EXISTS "public"."feedbacks_id_seq";
DROP SEQUENCE IF EXISTS "public"."log_users_id_seq";
DROP SEQUENCE IF EXISTS "public"."migrations_id_seq";
DROP SEQUENCE IF EXISTS "public"."migrations_lock_index_seq";
DROP SEQUENCE IF EXISTS "public"."register_verify_id_seq";
DROP SEQUENCE IF EXISTS "public"."users_id_seq";
DROP SEQUENCE IF EXISTS "public"."videos_action_id_seq";
DROP SEQUENCE IF EXISTS "public"."videos_channel_id_seq";
DROP SEQUENCE IF EXISTS "public"."videos_history_id_seq";
DROP SEQUENCE IF EXISTS "public"."videos_id_seq";
DROP SEQUENCE IF EXISTS "public"."videos_playlist_add_id_seq";
DROP SEQUENCE IF EXISTS "public"."videos_playlist_id_seq";
DROP SEQUENCE IF EXISTS "public"."videos_tags_id_seq";
DROP SEQUENCE IF EXISTS "public"."videos_views_id_seq";
DROP SEQUENCE IF EXISTS "public"."videos_watch_tags_id_seq";
DROP TABLE IF EXISTS "public"."auth_provider";
DROP TABLE IF EXISTS "public"."auth_user_provider";
DROP TABLE IF EXISTS "public"."device";
DROP TABLE IF EXISTS "public"."feedbacks";
DROP TABLE IF EXISTS "public"."log_users";
DROP TABLE IF EXISTS "public"."migrations";
DROP TABLE IF EXISTS "public"."migrations_lock";
DROP TABLE IF EXISTS "public"."mt_lov";
DROP TABLE IF EXISTS "public"."register_verify";
DROP TABLE IF EXISTS "public"."users";
DROP TABLE IF EXISTS "public"."videos";
DROP TABLE IF EXISTS "public"."videos_action";
DROP TABLE IF EXISTS "public"."videos_channel";
DROP TABLE IF EXISTS "public"."videos_history";
DROP TABLE IF EXISTS "public"."videos_playlist";
DROP TABLE IF EXISTS "public"."videos_playlist_add";
DROP TABLE IF EXISTS "public"."videos_tags";
DROP TABLE IF EXISTS "public"."videos_views";
DROP TABLE IF EXISTS "public"."videos_watch_tags";
DROP VIEW IF EXISTS "public"."vw_channel_videos_info";
DROP VIEW IF EXISTS "public"."vw_like_unlike";
DROP VIEW IF EXISTS "public"."vw_like_unlike_not_login";
DROP VIEW IF EXISTS "public"."vw_search_title";
DROP VIEW IF EXISTS "public"."vw_users";
DROP VIEW IF EXISTS "public"."vw_videos_history";
DROP VIEW IF EXISTS "public"."vw_videos_master";
DROP VIEW IF EXISTS "public"."vw_videos_master_list";
DROP VIEW IF EXISTS "public"."vw_videos_master_playlist";
DROP VIEW IF EXISTS "public"."vw_videos_no_watch_tags";
DROP VIEW IF EXISTS "public"."vw_videos_playlist";
DROP VIEW IF EXISTS "public"."vw_videos_playlist_watch";
DROP VIEW IF EXISTS "public"."vw_videos_watch_tags";
DROP FUNCTION IF EXISTS "public"."fn_decimal_million(value_number int4)";
DROP FUNCTION IF EXISTS "public"."fn_decimal_million_subscribe(value_number int4)";
DROP FUNCTION IF EXISTS "public"."fn_time_ago(value_time timestamp)";
CREATE SEQUENCE "feedbacks_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE SEQUENCE "log_users_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE SEQUENCE "migrations_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE SEQUENCE "migrations_lock_index_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE SEQUENCE "register_verify_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE SEQUENCE "users_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE SEQUENCE "videos_action_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE SEQUENCE "videos_channel_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE SEQUENCE "videos_history_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE SEQUENCE "videos_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE SEQUENCE "videos_playlist_add_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE SEQUENCE "videos_playlist_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE SEQUENCE "videos_tags_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE SEQUENCE "videos_views_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE SEQUENCE "videos_watch_tags_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE TABLE "auth_provider" (
  "auth_provider_id" int8 NOT NULL,
  "provider_type" varchar(255) COLLATE "pg_catalog"."default",
  "created_by" varchar(150) COLLATE "pg_catalog"."default",
  "created_at" timestamptz(6) DEFAULT CURRENT_TIMESTAMP,
  "updated_by" varchar(150) COLLATE "pg_catalog"."default",
  "updated_at" timestamptz(6) DEFAULT CURRENT_TIMESTAMP,
  "status" bool DEFAULT true
)
;
ALTER TABLE "auth_provider" OWNER TO "postgres";
CREATE TABLE "auth_user_provider" (
  "auth_user_provider_id" int8 NOT NULL DEFAULT nextval('users_id_seq'::regclass),
  "created_by" varchar(150) COLLATE "pg_catalog"."default",
  "created_at" timestamp(6),
  "updated_by" varchar(150) COLLATE "pg_catalog"."default",
  "updated_at" timestamp(6),
  "auth_provider_id" int8,
  "auth_user_id" int8
)
;
ALTER TABLE "auth_user_provider" OWNER TO "postgres";
CREATE TABLE "device" (
  "device_id" int8 NOT NULL,
  "browser" varchar(150) COLLATE "pg_catalog"."default",
  "ip_address" varchar(150) COLLATE "pg_catalog"."default",
  "model" varchar(150) COLLATE "pg_catalog"."default",
  "name" varchar(150) COLLATE "pg_catalog"."default",
  "version" varchar(150) COLLATE "pg_catalog"."default",
  "created_by" varchar(150) COLLATE "pg_catalog"."default",
  "created_at" timestamptz(6),
  "updated_by" varchar(150) COLLATE "pg_catalog"."default",
  "updated_at" timestamptz(6)
)
;
ALTER TABLE "device" OWNER TO "postgres";
CREATE TABLE "feedbacks" (
  "id" int4 NOT NULL GENERATED ALWAYS AS IDENTITY (
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
),
  "feedback" varchar(255) COLLATE "pg_catalog"."default",
  "image" text COLLATE "pg_catalog"."default",
  "created_at" timestamptz(6),
  "updated_at" timestamptz(6)
)
;
ALTER TABLE "feedbacks" OWNER TO "postgres";
CREATE TABLE "log_users" (
  "id" int4 NOT NULL GENERATED ALWAYS AS IDENTITY (
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
),
  "users_id" int4,
  "access_token" text COLLATE "pg_catalog"."default",
  "expires" varchar(255) COLLATE "pg_catalog"."default",
  "created_at" timestamptz(6),
  "updated_at" timestamptz(6)
)
;
ALTER TABLE "log_users" OWNER TO "postgres";
CREATE TABLE "migrations" (
  "id" int4 NOT NULL DEFAULT nextval('migrations_id_seq'::regclass),
  "name" varchar(255) COLLATE "pg_catalog"."default",
  "batch" int4,
  "migration_time" timestamptz(6)
)
;
ALTER TABLE "migrations" OWNER TO "postgres";
CREATE TABLE "migrations_lock" (
  "index" int4 NOT NULL DEFAULT nextval('migrations_lock_index_seq'::regclass),
  "is_locked" int4
)
;
ALTER TABLE "migrations_lock" OWNER TO "postgres";
CREATE TABLE "mt_lov" (
  "id" int4 NOT NULL,
  "lov_code" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "name_kh" varchar(255) COLLATE "pg_catalog"."default",
  "name_en" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "type" varchar(255) COLLATE "pg_catalog"."default",
  "status" bool,
  "created_at" timestamptz(6),
  "updated_at" timestamptz(6),
  "icon" varchar(255) COLLATE "pg_catalog"."default",
  "image" varchar(500) COLLATE "pg_catalog"."default",
  "tags" varchar(500) COLLATE "pg_catalog"."default",
  "description" varchar(500) COLLATE "pg_catalog"."default",
  "order_by" varchar(255) COLLATE "pg_catalog"."default"
)
;
ALTER TABLE "mt_lov" OWNER TO "postgres";
CREATE TABLE "register_verify" (
  "id" int4 NOT NULL GENERATED ALWAYS AS IDENTITY (
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
),
  "phone_number" varchar(50) COLLATE "pg_catalog"."default",
  "code" varchar(255) COLLATE "pg_catalog"."default",
  "resend" int4 DEFAULT 1,
  "status" bool,
  "created_at" timestamptz(6),
  "updated_at" timestamptz(6)
)
;
ALTER TABLE "register_verify" OWNER TO "postgres";
CREATE TABLE "users" (
  "id" int8 NOT NULL DEFAULT nextval('users_id_seq'::regclass),
  "email_address" varchar(150) COLLATE "pg_catalog"."default",
  "username" varchar(150) COLLATE "pg_catalog"."default",
  "password" varchar(150) COLLATE "pg_catalog"."default" NOT NULL,
  "email_verified_at" timestamptz(6) DEFAULT CURRENT_TIMESTAMP,
  "created_at" timestamptz(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "updated_at" timestamptz(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "created_by" varchar(150) COLLATE "pg_catalog"."C",
  "updated_by" varchar(150) COLLATE "pg_catalog"."POSIX",
  "phone_number" varchar(30) COLLATE "pg_catalog"."default",
  "status" bool DEFAULT true,
  "image" text COLLATE "pg_catalog"."default",
  "verify_code" varchar(255) COLLATE "pg_catalog"."default",
  "verify_status" bool,
  "image_cover" text COLLATE "pg_catalog"."default",
  "image_path" varchar(255) COLLATE "pg_catalog"."default",
  "image_cover_path" varchar(255) COLLATE "pg_catalog"."default"
)
;
ALTER TABLE "users" OWNER TO "postgres";
CREATE TABLE "videos" (
  "id" int4 NOT NULL DEFAULT nextval('videos_id_seq'::regclass),
  "userid" int8 NOT NULL,
  "patch" text COLLATE "pg_catalog"."default",
  "status" bool DEFAULT true,
  "created_at" timestamptz(6),
  "updated_at" timestamptz(6),
  "created_by" varchar(255) COLLATE "pg_catalog"."default",
  "updated_by" varchar(255) COLLATE "pg_catalog"."default",
  "category" varchar(255) COLLATE "pg_catalog"."default",
  "title" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "filename" text COLLATE "pg_catalog"."default",
  "destination" text COLLATE "pg_catalog"."default",
  "originalname" text COLLATE "pg_catalog"."default",
  "size" varchar(255) COLLATE "pg_catalog"."default",
  "mimetype" varchar(150) COLLATE "pg_catalog"."default",
  "encoding" varchar(50) COLLATE "pg_catalog"."default",
  "duration" varchar(150) COLLATE "pg_catalog"."default",
  "video_info" json,
  "provider_name" varchar(255) COLLATE "pg_catalog"."default",
  "thumbnail_url" text COLLATE "pg_catalog"."default",
  "description" text COLLATE "pg_catalog"."default"
)
;
ALTER TABLE "videos" OWNER TO "postgres";
CREATE TABLE "videos_action" (
  "id" int4 NOT NULL DEFAULT nextval('videos_action_id_seq'::regclass),
  "userid" int8,
  "likes" int8 DEFAULT 0,
  "unlikes" int8 DEFAULT 0,
  "status" bool DEFAULT true,
  "created_at" timestamptz(6),
  "updated_at" timestamptz(6),
  "created_by" varchar(255) COLLATE "pg_catalog"."default",
  "updated_by" varchar(255) COLLATE "pg_catalog"."default",
  "videoid" int8
)
;
ALTER TABLE "videos_action" OWNER TO "postgres";
CREATE TABLE "videos_channel" (
  "id" int4 NOT NULL GENERATED ALWAYS AS IDENTITY (
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
),
  "userid_channel" int4,
  "subscribe_id" int4,
  "created_at" timestamptz(6),
  "updated_at" timestamptz(6)
)
;
ALTER TABLE "videos_channel" OWNER TO "postgres";
CREATE TABLE "videos_history" (
  "id" int4 NOT NULL GENERATED ALWAYS AS IDENTITY (
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
),
  "video_id" int4,
  "user_watch_id" int4,
  "created_at" timestamptz(6),
  "updated_at" timestamptz(6)
)
;
ALTER TABLE "videos_history" OWNER TO "postgres";
CREATE TABLE "videos_playlist" (
  "id" int4 NOT NULL GENERATED ALWAYS AS IDENTITY (
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
),
  "user_id" int4 NOT NULL,
  "title" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "description" varchar(255) COLLATE "pg_catalog"."default",
  "created_at" timestamptz(6),
  "updated_at" timestamptz(6)
)
;
ALTER TABLE "videos_playlist" OWNER TO "postgres";
CREATE TABLE "videos_playlist_add" (
  "id" int4 NOT NULL GENERATED ALWAYS AS IDENTITY (
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
),
  "playlist_id" int4 NOT NULL,
  "video_id" int4 NOT NULL,
  "order_by" varchar(255) COLLATE "pg_catalog"."default",
  "created_at" timestamptz(6),
  "updated_at" timestamptz(6)
)
;
ALTER TABLE "videos_playlist_add" OWNER TO "postgres";
CREATE TABLE "videos_tags" (
  "id" int4 NOT NULL GENERATED ALWAYS AS IDENTITY (
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
),
  "title_tags" varchar(255) COLLATE "pg_catalog"."default",
  "video_id" int4,
  "created_by" varchar(255) COLLATE "pg_catalog"."default",
  "created_at" timestamptz(6),
  "updated_by" varchar(255) COLLATE "pg_catalog"."default",
  "updated_at" timestamptz(6)
)
;
ALTER TABLE "videos_tags" OWNER TO "postgres";
CREATE TABLE "videos_views" (
  "id" int4 NOT NULL GENERATED ALWAYS AS IDENTITY (
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
),
  "video_id" int4,
  "views" int4
)
;
ALTER TABLE "videos_views" OWNER TO "postgres";
CREATE TABLE "videos_watch_tags" (
  "id" int4 NOT NULL GENERATED ALWAYS AS IDENTITY (
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
),
  "title_tags" varchar(255) COLLATE "pg_catalog"."default",
  "video_watch_id" int4,
  "user_watch_id" int4,
  "created_by" varchar(255) COLLATE "pg_catalog"."default",
  "created_at" timestamptz(6),
  "updated_by" varchar(255) COLLATE "pg_catalog"."default",
  "updated_at" timestamptz(6)
)
;
ALTER TABLE "videos_watch_tags" OWNER TO "postgres";
CREATE VIEW "vw_channel_videos_info" AS  SELECT a.id AS user_id,
    fn_decimal_million_subscribe(bb.videos_number::integer) AS videos_number,
    fn_decimal_million_subscribe(cc.subscribe_number::integer) AS subscribe_number,
    fn_decimal_million_subscribe(dd.liked::integer) AS liked,
    fn_decimal_million_subscribe(ee.unliked::integer) AS unliked
   FROM users a
     LEFT JOIN ( SELECT b.userid,
            count(b.userid) AS videos_number
           FROM videos b
          GROUP BY b.userid) bb ON a.id = bb.userid
     LEFT JOIN ( SELECT c.userid_channel,
            count(c.userid_channel) AS subscribe_number
           FROM videos_channel c
          GROUP BY c.userid_channel) cc ON a.id = cc.userid_channel
     LEFT JOIN ( SELECT d.userid,
            count(d.userid) AS liked
           FROM videos_action d
          WHERE d.likes = 1
          GROUP BY d.userid) dd ON a.id = dd.userid
     LEFT JOIN ( SELECT e.userid,
            count(e.userid) AS unliked
           FROM videos_action e
          WHERE e.unlikes = 1
          GROUP BY e.userid) ee ON a.id = ee.userid;
ALTER TABLE "vw_channel_videos_info" OWNER TO "postgres";
CREATE VIEW "vw_like_unlike" AS  SELECT a.id AS video_id,
    b.userid,
    sum(b.likes) AS likes,
    COALESCE(c.likes_view::bigint, 0::bigint) AS likes_view,
    sum(b.unlikes) AS unlikes,
    COALESCE(d.unlikes_view::bigint, 0::bigint) AS unlikes_view
   FROM videos a
     LEFT JOIN videos_action b ON a.id = b.videoid
     LEFT JOIN ( SELECT aa.videoid,
            sum(aa.likes) AS likes_view
           FROM videos_action aa
          WHERE aa.likes = 1
          GROUP BY aa.videoid) c ON a.id = c.videoid
     LEFT JOIN ( SELECT bb.videoid,
            sum(bb.unlikes) AS unlikes_view
           FROM videos_action bb
          WHERE bb.unlikes = 1
          GROUP BY bb.videoid) d ON a.id = d.videoid
  GROUP BY a.id, b.userid, c.likes_view, d.unlikes_view;
ALTER TABLE "vw_like_unlike" OWNER TO "postgres";
CREATE VIEW "vw_like_unlike_not_login" AS  SELECT a.id AS video_id,
    COALESCE(c.likes_view::bigint, 0::bigint) AS likes_view,
    COALESCE(d.unlikes_view::bigint, 0::bigint) AS unlikes_view
   FROM videos a
     LEFT JOIN videos_action b ON a.id = b.videoid
     LEFT JOIN ( SELECT aa.videoid,
            sum(aa.likes) AS likes_view
           FROM videos_action aa
          WHERE aa.likes = 1
          GROUP BY aa.videoid) c ON a.id = c.videoid
     LEFT JOIN ( SELECT bb.videoid,
            sum(bb.unlikes) AS unlikes_view
           FROM videos_action bb
          WHERE bb.unlikes = 1
          GROUP BY bb.videoid) d ON a.id = d.videoid
  GROUP BY a.id, c.likes_view, d.unlikes_view;
ALTER TABLE "vw_like_unlike_not_login" OWNER TO "postgres";
CREATE VIEW "vw_search_title" AS  SELECT DISTINCT row_number() OVER () AS id,
    c.video_id,
    c.title,
    c.type
   FROM ( SELECT DISTINCT a.title,
            a.id AS video_id,
            'title'::text AS type
           FROM videos a
        UNION ALL
         SELECT DISTINCT b.title_tags AS title,
            b.video_id,
            'tags'::text AS type
           FROM videos_tags b) c;
ALTER TABLE "vw_search_title" OWNER TO "postgres";
CREATE VIEW "vw_users" AS  SELECT a.id,
    a.username,
    a.phone_number,
    a.status,
    a.image,
    a.created_at,
    a.updated_at,
    b.auth_provider_id,
    c.provider_type
   FROM users a
     LEFT JOIN auth_user_provider b ON a.id = b.auth_user_id
     LEFT JOIN auth_provider c ON b.auth_provider_id = c.auth_provider_id;
ALTER TABLE "vw_users" OWNER TO "postgres";
CREATE VIEW "vw_videos_history" AS  SELECT b.rownumvideo,
    a.rownum,
    a.id,
    a.email_address,
    a.username,
    a.phone_number,
    a.email_verified_at,
    a.user_status,
    a.user_created_at,
    a.img_profile,
    a.image_cover,
    a.image_path,
    a.image_cover_path,
    a.video_id,
    a.video_patch,
    a.thumbnail,
    a.video_status,
    a.title,
    a.filename,
    a.destination,
    a.originalname,
    a.size,
    a.video_created_at,
    a.duration,
    a.time_agos,
    a.provider_name,
    a.views,
    a.subscribe,
    b.user_watch_id,
    a.description,
    b.created_at
   FROM ( SELECT row_number() OVER (ORDER BY bb.video_id) AS rownumvideo,
            bb.video_id,
            bb.user_watch_id,
            bb.created_at
           FROM videos_history bb
          GROUP BY bb.video_id, bb.user_watch_id, bb.created_at) b
     LEFT JOIN vw_videos_master a ON b.video_id = a.video_id
  ORDER BY b.user_watch_id, b.created_at DESC;
ALTER TABLE "vw_videos_history" OWNER TO "postgres";
CREATE VIEW "vw_videos_master" AS  SELECT a.rownum,
    a.id,
    a.email_address,
    a.username,
    a.phone_number,
    a.email_verified_at,
    a.user_status,
    a.user_created_at,
    a.img_profile,
    a.image_cover,
    a.image_path,
    a.image_cover_path,
    a.video_id,
    a.video_patch,
    a.thumbnail,
    a.video_status,
    a.title,
    a.filename,
    a.destination,
    a.originalname,
    a.size,
    a.video_created_at,
    a.duration,
    a.time_agos,
    a.provider_name,
    fn_decimal_million(a.views::integer) AS views,
    a.subscribe,
    a.description
   FROM ( SELECT row_number() OVER (ORDER BY b.id DESC) AS rownum,
            a_1.id,
            a_1.email_address,
            a_1.username,
            a_1.phone_number,
            a_1.email_verified_at,
            a_1.status AS user_status,
            a_1.created_at AS user_created_at,
            a_1.image AS img_profile,
            a_1.image_cover,
            a_1.image_path,
            a_1.image_cover_path,
            b.provider_name,
            b.id AS video_id,
            (b.destination || '/'::text) || b.filename AS video_patch,
            ((b.destination || '/thumbnail/'::text) || b.filename) || '_thumbnail.png'::text AS thumbnail,
            b.status AS video_status,
            b.title,
            b.filename,
            b.destination,
            b.originalname,
            b.size,
            b.created_at AS video_created_at,
            b.duration,
            fn_time_ago(b.created_at::timestamp without time zone) AS time_agos,
            COALESCE(d.views::bigint, 0::bigint) AS views,
            fn_decimal_million_subscribe(ab.subscribe::integer) AS subscribe,
            b.description
           FROM users a_1
             JOIN videos b ON a_1.id = b.userid
             LEFT JOIN videos_views d ON b.id = d.video_id
             LEFT JOIN ( SELECT aa.userid_channel,
                    count(aa.userid_channel) AS subscribe
                   FROM videos_channel aa
                  GROUP BY aa.userid_channel) ab ON a_1.id = ab.userid_channel) a;
ALTER TABLE "vw_videos_master" OWNER TO "postgres";
CREATE VIEW "vw_videos_master_list" AS  SELECT a.id AS playlist_id,
    a.title AS title_playlist,
    a.description AS desc_playlist,
    bb.video_number,
    v.rownum,
    v.id,
    v.email_address,
    v.username,
    v.phone_number,
    v.email_verified_at,
    v.user_status,
    v.user_created_at,
    v.img_profile,
    v.image_cover,
    v.image_path,
    v.image_cover_path,
    v.video_id,
    v.video_patch,
    v.thumbnail,
    v.video_status,
    v.title,
    v.filename,
    v.destination,
    v.originalname,
    v.size,
    v.video_created_at,
    v.duration,
    v.time_agos,
    v.provider_name,
    v.views,
    v.subscribe,
    v.description
   FROM videos_playlist a
     LEFT JOIN ( SELECT count(b.playlist_id) AS video_number,
            b.playlist_id
           FROM videos_playlist_add b
          GROUP BY b.playlist_id) bb ON a.id = bb.playlist_id
     LEFT JOIN ( SELECT h.playlist_id,
            h.video_id
           FROM ( SELECT y.playlist_id,
                    y.video_id,
                    rank() OVER (PARTITION BY y.playlist_id ORDER BY y.video_id DESC) AS rank_number
                   FROM videos_playlist_add y) h
          WHERE h.rank_number = 1) f ON a.id = f.playlist_id
     RIGHT JOIN vw_videos_master v ON f.video_id = v.video_id;
ALTER TABLE "vw_videos_master_list" OWNER TO "postgres";
CREATE VIEW "vw_videos_master_playlist" AS  SELECT l.playlist_id,
    y.title_playlist,
    y.desc_playlist,
    y.video_number,
    y.rownum,
    y.id,
    y.email_address,
    y.username,
    y.phone_number,
    y.email_verified_at,
    y.user_status,
    y.user_created_at,
    y.img_profile,
    y.image_cover,
    y.image_path,
    y.image_cover_path,
    y.video_id,
    y.video_patch,
    y.thumbnail,
    y.video_status,
    y.title,
    y.filename,
    y.destination,
    y.originalname,
    y.size,
    y.video_created_at,
    y.duration,
    y.time_agos,
    y.provider_name,
    y.views,
    y.subscribe,
    y.description
   FROM videos_playlist_add l
     LEFT JOIN ( SELECT a.title AS title_playlist,
            a.description AS desc_playlist,
            ''::text AS video_number,
            v.rownum,
            v.id,
            v.email_address,
            v.username,
            v.phone_number,
            v.email_verified_at,
            v.user_status,
            v.user_created_at,
            v.img_profile,
            v.image_cover,
            v.image_path,
            v.image_cover_path,
            v.video_id,
            v.video_patch,
            v.thumbnail,
            v.video_status,
            v.title,
            v.filename,
            v.destination,
            v.originalname,
            v.size,
            v.video_created_at,
            v.duration,
            v.time_agos,
            v.provider_name,
            v.views,
            v.subscribe,
            v.description
           FROM videos_playlist a
             LEFT JOIN ( SELECT h.playlist_id,
                    h.video_id
                   FROM ( SELECT y_1.playlist_id,
                            y_1.video_id,
                            rank() OVER (PARTITION BY y_1.playlist_id ORDER BY y_1.video_id DESC) AS rank_number
                           FROM videos_playlist_add y_1) h
                  WHERE h.rank_number = 1) f ON a.id = f.playlist_id
             RIGHT JOIN vw_videos_master v ON f.video_id = v.video_id) y ON l.video_id = y.video_id;
ALTER TABLE "vw_videos_master_playlist" OWNER TO "postgres";
CREATE VIEW "vw_videos_no_watch_tags" AS  SELECT row_number() OVER () AS rownum,
    c.title_tags
   FROM ( SELECT b.title_tags
           FROM ( SELECT a.title_tags,
                    count(a.title_tags) AS count_tags
                   FROM videos_watch_tags a
                  GROUP BY a.title_tags, a.video_watch_id) b
          ORDER BY b.count_tags DESC) c
  GROUP BY c.title_tags;
ALTER TABLE "vw_videos_no_watch_tags" OWNER TO "postgres";
CREATE VIEW "vw_videos_playlist" AS  SELECT a.id AS playlist_id,
    a.title AS title_playlist,
    a.description AS desc_playlist,
    bb.video_number,
    v.rownum,
    v.id,
    v.email_address,
    v.username,
    v.phone_number,
    v.email_verified_at,
    v.user_status,
    v.user_created_at,
    v.img_profile,
    v.image_cover,
    v.image_path,
    v.image_cover_path,
    v.video_id,
    v.video_patch,
    v.thumbnail,
    v.video_status,
    v.title,
    v.filename,
    v.destination,
    v.originalname,
    v.size,
    v.video_created_at,
    v.duration,
    v.time_agos,
    v.provider_name,
    v.views,
    v.subscribe,
    v.description
   FROM videos_playlist a
     LEFT JOIN ( SELECT count(b.playlist_id) AS video_number,
            b.playlist_id
           FROM videos_playlist_add b
          GROUP BY b.playlist_id) bb ON a.id = bb.playlist_id
     LEFT JOIN ( SELECT h.playlist_id,
            h.video_id
           FROM ( SELECT y.playlist_id,
                    y.video_id,
                    rank() OVER (PARTITION BY y.playlist_id ORDER BY y.video_id DESC) AS rank_number
                   FROM videos_playlist_add y) h
          WHERE h.rank_number = 1) f ON a.id = f.playlist_id
     LEFT JOIN vw_videos_master v ON f.video_id = v.video_id;
ALTER TABLE "vw_videos_playlist" OWNER TO "postgres";
CREATE VIEW "vw_videos_playlist_watch" AS  SELECT a.id AS playlist_id,
    a.title AS title_playlist,
    a.description AS desc_playlist,
    v.rownum,
    v.id,
    v.email_address,
    v.username,
    v.phone_number,
    v.email_verified_at,
    v.user_status,
    v.user_created_at,
    v.img_profile,
    v.image_cover,
    v.image_path,
    v.image_cover_path,
    v.video_id,
    v.video_patch,
    v.thumbnail,
    v.video_status,
    v.title,
    v.filename,
    v.destination,
    v.originalname,
    v.size,
    v.video_created_at,
    v.duration,
    v.time_agos,
    v.provider_name,
    v.views,
    v.subscribe,
    v.description
   FROM videos_playlist a
     LEFT JOIN videos_playlist_add b ON a.id = b.playlist_id
     LEFT JOIN vw_videos_master v ON b.video_id = v.video_id;
ALTER TABLE "vw_videos_playlist_watch" OWNER TO "postgres";
CREATE VIEW "vw_videos_watch_tags" AS  SELECT row_number() OVER () AS rownum,
    c.title_tags,
    c.user_watch_id
   FROM ( SELECT b.title_tags,
            b.user_watch_id,
            b.count_tags
           FROM ( SELECT a.title_tags,
                    a.user_watch_id,
                    count(a.title_tags) AS count_tags
                   FROM videos_watch_tags a
                  GROUP BY a.title_tags, a.user_watch_id, a.video_watch_id) b
          ORDER BY b.count_tags DESC) c
  GROUP BY c.title_tags, c.user_watch_id;
ALTER TABLE "vw_videos_watch_tags" OWNER TO "postgres";
CREATE OR REPLACE FUNCTION "fn_decimal_million"("value_number" int4)
  RETURNS "pg_catalog"."bpchar" AS $BODY$
DECLARE decimal_number varchar;
BEGIN
decimal_number := (CASE
	WHEN value_number < 1000 THEN CONCAT(value_number::FLOAT, ' ', 'Views')
	WHEN 1000 <= value_number AND value_number < 1000000 THEN CONCAT(value_number::FLOAT / 1000 , 'K')
	WHEN 1000000 <= value_number THEN CONCAT(value_number::FLOAT / 1000000, 'M')
	END);
RETURN decimal_number;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION "fn_decimal_million"("value_number" int4) OWNER TO "postgres";
CREATE OR REPLACE FUNCTION "fn_decimal_million_subscribe"("value_number" int4)
  RETURNS "pg_catalog"."bpchar" AS $BODY$
DECLARE decimal_number varchar;
BEGIN
decimal_number := (CASE
	WHEN value_number < 1000 THEN CONCAT(value_number::FLOAT)
	WHEN 1000 <= value_number AND value_number < 1000000 THEN CONCAT(value_number::FLOAT / 1000 , 'K')
	WHEN 1000000 <= value_number THEN CONCAT(value_number::FLOAT / 1000000, 'M')
	END);
RETURN decimal_number;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION "fn_decimal_million_subscribe"("value_number" int4) OWNER TO "postgres";
CREATE OR REPLACE FUNCTION "fn_time_ago"("value_time" timestamp)
  RETURNS "pg_catalog"."bpchar" AS $BODY$
	DECLARE ago varchar;
BEGIN	

ago := (CASE
					WHEN ROUND(EXTRACT(EPOCH FROM (now() - value_time))) <= 60 THEN '0 just now'
					WHEN ROUND(EXTRACT(EPOCH FROM (now() - value_time) / 60)) <= 60 THEN (CASE WHEN ROUND(EXTRACT(EPOCH FROM (now() - value_time) / 60)) = 1 THEN '1 minute ago' ELSE (CONCAT(ROUND(EXTRACT(EPOCH FROM (now() - value_time) / 60)),' ','minutes ago')) END)
					WHEN ROUND(EXTRACT(EPOCH FROM (now() - value_time) / 3600)) <= 24 THEN (CASE WHEN ROUND(EXTRACT(EPOCH FROM (now() - value_time) / 3600)) = 1 THEN '1 hour ago' ELSE (CONCAT(ROUND(EXTRACT(EPOCH FROM (now() - value_time) / 3600)),' ','hours ago')) END)
					WHEN ROUND(EXTRACT(EPOCH FROM (now() - value_time) / 86400)) <= 7 THEN (CASE WHEN ROUND(EXTRACT(EPOCH FROM (now() - value_time) / 86400)) = 1 THEN 'Yesterday' ELSE (CONCAT(ROUND(EXTRACT(EPOCH FROM (now() - value_time) / 86400)),' ','days ago')) END)
					WHEN ROUND(EXTRACT(EPOCH FROM (now() - value_time) / 604800)) <= 4.3 THEN (CASE WHEN ROUND(EXTRACT(EPOCH FROM (now() - value_time) / 604800)) = 1 THEN '1 week ago' ELSE (CONCAT(ROUND(EXTRACT(EPOCH FROM (now() - value_time) / 604800)),' ','weeks ago')) END)
					WHEN ROUND(EXTRACT(EPOCH FROM (now() - value_time) / 2600640)) <= 12 THEN (CASE WHEN ROUND(EXTRACT(EPOCH FROM (now() - value_time) / 2600640)) = 1 THEN '1 month ago' ELSE (CONCAT(ROUND(EXTRACT(EPOCH FROM (now() - value_time) / 2600640)),' ','months ago')) END)
					ELSE (CASE WHEN ROUND(EXTRACT(EPOCH FROM (now() - value_time) / 31207680)) = 1 THEN '1 year ago' ELSE (CONCAT(ROUND(EXTRACT(EPOCH FROM (now() - value_time) / 31207680)),' ','years ago')) END) END 
				  );
RETURN ago;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION "fn_time_ago"("value_time" timestamp) OWNER TO "postgres";
BEGIN;
LOCK TABLE "public"."auth_provider" IN SHARE MODE;
DELETE FROM "public"."auth_provider";
INSERT INTO "public"."auth_provider" ("auth_provider_id","provider_type","created_by","created_at","updated_by","updated_at","status") VALUES (1, 'PHONE_NUMBER', 'admin', NULL, NULL, NULL, 't'),(2, 'EMAIL', 'admin', NULL, NULL, NULL, 't'),(3, 'FACEBOOK', 'admin', NULL, NULL, NULL, 't'),(4, 'GoogleSignin', 'admin', '2022-08-11 14:11:05.66404+07', NULL, '2022-08-11 14:11:05.66404+07', 't');
COMMIT;
BEGIN;
LOCK TABLE "public"."auth_user_provider" IN SHARE MODE;
DELETE FROM "public"."auth_user_provider";
INSERT INTO "public"."auth_user_provider" ("auth_user_provider_id","created_by","created_at","updated_by","updated_at","auth_provider_id","auth_user_id") VALUES (70, 'yunsariem@gmail.com', '2022-07-07 12:00:42.732', NULL, '2022-07-07 12:00:42.732', 2, 69),(76, 'yunsariem096@gmail.com', '2022-08-12 11:53:59.218', NULL, '2022-08-12 11:53:59.218', 4, 75);
COMMIT;
BEGIN;
LOCK TABLE "public"."device" IN SHARE MODE;
DELETE FROM "public"."device";
COMMIT;
BEGIN;
LOCK TABLE "public"."feedbacks" IN SHARE MODE;
DELETE FROM "public"."feedbacks";
INSERT INTO "public"."feedbacks" ("id","feedback","image","created_at","updated_at") VALUES (9, 'test feadback', NULL, '2022-07-15 11:43:19.494+07', '2022-07-15 11:43:19.494+07');
COMMIT;
BEGIN;
LOCK TABLE "public"."log_users" IN SHARE MODE;
DELETE FROM "public"."log_users";
INSERT INTO "public"."log_users" ("id","users_id","access_token","expires","created_at","updated_at") VALUES (1, 69, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmVzIjoxODE3ODgzNDU3LCJ1c2VyIjp7ImlkIjoiNjkiLCJ1c2VybmFtZSI6Inl1bnNhcmllbUBnbWFpbC5jb20iLCJjcmVhdGVkX2F0IjoiMjAyMi0wNy0wN1QwNTowMDo0Mi43MjFaIn19.aQ1ViJr5LWeXXJ267RoJAM9q93tf6grrW-G9HoLTYD8', '1817883457', '2022-08-11 14:37:37.151+07', '2022-08-11 14:37:37.151+07'),(2, 69, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmVzIjoxODE3ODg1OTQ2LCJ1c2VyIjp7ImlkIjoiNjkiLCJ1c2VybmFtZSI6Inl1bnNhcmllbUBnbWFpbC5jb20iLCJjcmVhdGVkX2F0IjoiMjAyMi0wNy0wN1QwNTowMDo0Mi43MjFaIn19.yKdFcHOwkpCGjVfwftyL9SHurm7Z_nt1E2xm_CpPYYw', '1817885946', '2022-08-11 15:19:06.108+07', '2022-08-11 15:19:06.108+07'),(3, 69, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmVzIjoxODE3ODg2MDgxLCJ1c2VyIjp7ImlkIjoiNjkiLCJ1c2VybmFtZSI6Inl1bnNhcmllbUBnbWFpbC5jb20iLCJjcmVhdGVkX2F0IjoiMjAyMi0wNy0wN1QwNTowMDo0Mi43MjFaIn19.VRzFBOEUr-96lJJmkB9RZp97jX7JBl2AbEJjzr0FmSE', '1817886081', '2022-08-11 15:21:21.039+07', '2022-08-11 15:21:21.039+07'),(4, NULL, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmVzIjoxODE3ODg2MzAzfQ.kyCl-XiiS5ZummipAlf6skoLJZJpi6aFu89omYFqhB8', '1817886303', '2022-08-11 15:25:03.326+07', '2022-08-11 15:25:03.326+07'),(5, NULL, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmVzIjoxODE3ODg2MzA1fQ.jhrBHRZ81ikGQd8bA8U4wflmot6WnLWFqrE8TSEeEis', '1817886305', '2022-08-11 15:25:05.233+07', '2022-08-11 15:25:05.233+07'),(6, NULL, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmVzIjoxODE3ODg2MzA2fQ.AK3-6qCe21x7o-muv4xSbdwAZgxYSdijoLOzROx_vaE', '1817886306', '2022-08-11 15:25:06.869+07', '2022-08-11 15:25:06.869+07'),(7, NULL, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmVzIjoxODE3ODg2NDExfQ.CnIfYpNoObRnsKSp2FVOAhcbMUtz8C5RU4fsLrCIPm0', '1817886411', '2022-08-11 15:26:51.096+07', '2022-08-11 15:26:51.096+07'),(8, NULL, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmVzIjoxODE3ODg2NDEyfQ.rsAD3_kor26_g7nNMFKclZrH4CKQzugVnAHtWeE1mXU', '1817886412', '2022-08-11 15:26:52.776+07', '2022-08-11 15:26:52.776+07'),(9, NULL, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmVzIjoxODE3ODg2NDE2fQ.-__3Ylc45qzhL7oT5q8cRQiu7GFQScT7mfzbxvBSZ4Q', '1817886416', '2022-08-11 15:26:56.969+07', '2022-08-11 15:26:56.969+07'),(10, NULL, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmVzIjoxODE3ODg2NTY3fQ.8XY0F6ia8UHhiERsAK62mgAsp_hs47SbM5cvLkLKNhM', '1817886567', '2022-08-11 15:29:27.274+07', '2022-08-11 15:29:27.274+07'),(11, NULL, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmVzIjoxODE3ODg2NTcxfQ.zkolXglGC8cZH_FVHdTFas66nyntrat2-typh0Z1rAE', '1817886571', '2022-08-11 15:29:31.657+07', '2022-08-11 15:29:31.657+07'),(12, 69, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmVzIjoxODE3ODg2NjA1LCJ1c2VyIjp7ImlkIjoiNjkiLCJ1c2VybmFtZSI6Inl1bnNhcmllbUBnbWFpbC5jb20iLCJjcmVhdGVkX2F0IjoiMjAyMi0wNy0wN1QwNTowMDo0Mi43MjFaIn19.tKrKhhmvgvC3X7ujz5woJj0rEjlaDejut3b8sj-LMXo', '1817886605', '2022-08-11 15:30:05.282+07', '2022-08-11 15:30:05.282+07'),(13, NULL, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmVzIjoxODE3ODg2NzM4fQ.CxsBH18MJkHxAvYPRA2IEuHypIKjNk4h-6Zq6d5CkpQ', '1817886738', '2022-08-11 15:32:18.022+07', '2022-08-11 15:32:18.022+07'),(14, NULL, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmVzIjoxODE3ODg2Nzk2fQ.UMU9AbkLTVHkSbUXo4siNz_je8pmWpzFYzAa63NECeY', '1817886796', '2022-08-11 15:33:16.628+07', '2022-08-11 15:33:16.628+07'),(15, 69, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmVzIjoxODE3ODg2OTQwLCJ1c2VyIjp7ImlkIjoiNjkiLCJ1c2VybmFtZSI6Inl1bnNhcmllbUBnbWFpbC5jb20iLCJjcmVhdGVkX2F0IjoiMjAyMi0wNy0wN1QwNTowMDo0Mi43MjFaIn19.ns0yyUeaZ8A432yFfhz8q_6QUTJ6exyiRJy93InWAJs', '1817886940', '2022-08-11 15:35:40.016+07', '2022-08-11 15:35:40.016+07'),(16, NULL, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmVzIjoxODE3ODg2OTc0fQ.m37KHK3APplUCr_P71HovJgUNuRpDQUaGIOxyMxbYBA', '1817886974', '2022-08-11 15:36:14.141+07', '2022-08-11 15:36:14.141+07'),(17, NULL, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmVzIjoxODE3ODg2OTc2fQ.oONTkIhkGv_OA8_sf2S3lqwlfMuJQwtZl_S9UU5dj9s', '1817886976', '2022-08-11 15:36:16.118+07', '2022-08-11 15:36:16.118+07'),(18, NULL, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmVzIjoxODE3ODg2OTg2fQ.P5xzy74tb-wXSaM5zT2ui7PiNyfONhzqfC2hwwulyTE', '1817886986', '2022-08-11 15:36:26.186+07', '2022-08-11 15:36:26.186+07'),(19, 69, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmVzIjoxODE3ODg3Mjg2LCJ1c2VyIjp7ImlkIjoiNjkiLCJ1c2VybmFtZSI6Inl1bnNhcmllbUBnbWFpbC5jb20iLCJjcmVhdGVkX2F0IjoiMjAyMi0wNy0wN1QwNTowMDo0Mi43MjFaIn19.-vrFmWIiyShn6kwnWoEo4BPRDr4TyHsQgAjrqkKdwno', '1817887286', '2022-08-11 15:41:26.834+07', '2022-08-11 15:41:26.834+07'),(20, 69, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmVzIjoxODE3ODg3NDYyLCJ1c2VyIjp7ImlkIjoiNjkiLCJ1c2VybmFtZSI6Inl1bnNhcmllbUBnbWFpbC5jb20iLCJjcmVhdGVkX2F0IjoiMjAyMi0wNy0wN1QwNTowMDo0Mi43MjFaIn19.CKWvSJDX_Au_JX5gT9elmAPb_UdhAWTrmx9v63A8HRs', '1817887462', '2022-08-11 15:44:22.349+07', '2022-08-11 15:44:22.349+07'),(21, 69, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmVzIjoxODE3ODg3NDY0LCJ1c2VyIjp7ImlkIjoiNjkiLCJ1c2VybmFtZSI6Inl1bnNhcmllbUBnbWFpbC5jb20iLCJjcmVhdGVkX2F0IjoiMjAyMi0wNy0wN1QwNTowMDo0Mi43MjFaIn19.t0kO3oqSDCrCiJO4x_pxG0K_RLNZWRJOdcFGXpEITec', '1817887464', '2022-08-11 15:44:24.117+07', '2022-08-11 15:44:24.117+07'),(22, 69, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmVzIjoxODE3ODg3NTQ3LCJ1c2VyIjp7ImlkIjoiNjkiLCJ1c2VybmFtZSI6Inl1bnNhcmllbUBnbWFpbC5jb20iLCJjcmVhdGVkX2F0IjoiMjAyMi0wNy0wN1QwNTowMDo0Mi43MjFaIn19.fNjUQaHjcXT_1jcyPnF4DS_D5PfP95GNusy16srAeec', '1817887547', '2022-08-11 15:45:47.198+07', '2022-08-11 15:45:47.198+07'),(23, 69, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmVzIjoxODE3ODg3NjA2LCJ1c2VyIjp7ImlkIjoiNjkiLCJ1c2VybmFtZSI6Inl1bnNhcmllbUBnbWFpbC5jb20iLCJjcmVhdGVkX2F0IjoiMjAyMi0wNy0wN1QwNTowMDo0Mi43MjFaIn19.fB9v8ifGISuZZ7Dm7q40yQs1SKpfWHG3ZCiWg0VKqxg', '1817887606', '2022-08-11 15:46:46.641+07', '2022-08-11 15:46:46.641+07'),(24, NULL, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmVzIjoxODE3ODg3NzkzfQ.b0zPTw-ZBnp6aUxl4zoijsoMdrhqSNoZ7waKd83jlzU', '1817887793', '2022-08-11 15:49:53.718+07', '2022-08-11 15:49:53.718+07'),(25, NULL, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmVzIjoxODE3ODg3ODAyfQ.Jb7RSePkG0d9T_VNrCfjmYtAdu7ZwrQ8gSuuFJfIlys', '1817887802', '2022-08-11 15:50:02.086+07', '2022-08-11 15:50:02.086+07'),(26, 69, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmVzIjoxODE3ODg3ODg1LCJ1c2VyIjp7ImlkIjoiNjkiLCJ1c2VybmFtZSI6Inl1bnNhcmllbUBnbWFpbC5jb20iLCJjcmVhdGVkX2F0IjoiMjAyMi0wNy0wN1QwNTowMDo0Mi43MjFaIn19.wygG0qfAGS2udVQWxl-0dAS2I8H2FVpGaShQsp015es', '1817887885', '2022-08-11 15:51:25.311+07', '2022-08-11 15:51:25.311+07'),(27, NULL, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmVzIjoxODE3ODkxNDQyLCJ1c2VyIjoieXVuc2FyaWVtQGdtYWlsLmNvbSJ9.EGbgHVeMVy5vhlPyb3gnIpqv0GOjFJEKeQykpWC-njk', '1817891442', '2022-08-11 16:50:42.097+07', '2022-08-11 16:50:42.097+07'),(28, NULL, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmVzIjoxODE3ODkxNDUxLCJ1c2VyIjoieXVuc2FyaWVtQGdtYWlsLmNvbSJ9.mBH4AXW_RxSmyAPNADgYtKJ41myUDv3F7GBfeKVY5aE', '1817891451', '2022-08-11 16:50:51.765+07', '2022-08-11 16:50:51.765+07'),(29, 69, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmVzIjoxODE3ODkxOTgwLCJ1c2VyIjoieXVuc2FyaWVtQGdtYWlsLmNvbSJ9.HgTHJHs-bCnEsUfEx35pPQ6Y9drFa0_A23A6qBrINSg', '1817891980', '2022-08-11 16:59:40.862+07', '2022-08-11 16:59:40.862+07'),(30, 69, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmVzIjoxODE3ODkxOTg1LCJ1c2VyIjoieXVuc2FyaWVtQGdtYWlsLmNvbSJ9.4kehmdqdYD1xKNp1m-aWHxcHyOEx3l7hvoyNlr_kzx0', '1817891985', '2022-08-11 16:59:45.922+07', '2022-08-11 16:59:45.922+07'),(31, 69, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmVzIjoxODE3ODkyMDE1LCJ1c2VyIjoieXVuc2FyaWVtQGdtYWlsLmNvbSJ9.oppDRMSo-oTahE8hZJB5r38mMgx2qZvJgnZYm4YUS1Y', '1817892015', '2022-08-11 17:00:15.904+07', '2022-08-11 17:00:15.904+07'),(32, 69, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmVzIjoxODE3ODkyMjAxLCJ1c2VyIjoieXVuc2FyaWVtQGdtYWlsLmNvbSJ9.YkeZEjN9HDVVDT5gH-LhiFoAXRk1Z1FsyVIqzbGvdyg', '1817892201', '2022-08-11 17:03:21.272+07', '2022-08-11 17:03:21.272+07'),(33, 69, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmVzIjoxODE3ODkyMjA0LCJ1c2VyIjoieXVuc2FyaWVtQGdtYWlsLmNvbSJ9.qIgHnY_B3NUOOkzMAZTT8oY7-17z8pfPPTQlfq9FFyM', '1817892204', '2022-08-11 17:03:24.939+07', '2022-08-11 17:03:24.939+07'),(34, 69, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmVzIjoxODE3OTUyMTUwLCJ1c2VyIjoieXVuc2FyaWVtQGdtYWlsLmNvbSJ9.ylxR2EJ1B8mEKmKAnDzJ7ZynY55Y8FGn3oAGGuG2oBE', '1817952150', '2022-08-12 09:42:30.946+07', '2022-08-12 09:42:30.946+07'),(35, 69, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmVzIjoxODE3OTUyMjY5LCJ1c2VyIjoieXVuc2FyaWVtQGdtYWlsLmNvbSJ9.ycPBZ5Nrsl4o-eQkgc4lfSLj0epB6zTJh9h_V6PzgTA', '1817952269', '2022-08-12 09:44:29.426+07', '2022-08-12 09:44:29.426+07'),(36, 71, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmVzIjoxODE3OTUzMzI5LCJ1c2VyIjoieXVuc2FyaWVtMDk2QGdtYWlsLmNvbSJ9.-9fYBzs3IWUOkI2LTpuF6boVwQ--SVe5sz6ZmRI0xus', '1817953329', '2022-08-12 10:02:09.944+07', '2022-08-12 10:02:09.944+07'),(37, 71, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmVzIjoxODE3OTUzMzYwLCJ1c2VyIjoieXVuc2FyaWVtMDk2QGdtYWlsLmNvbSJ9.dnK8t__m3FPMejVQovcAwMgFcgFVWOk1CN0887nQ1a4', '1817953360', '2022-08-12 10:02:40.24+07', '2022-08-12 10:02:40.24+07'),(38, 71, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmVzIjoxODE3OTUzMzYxLCJ1c2VyIjoieXVuc2FyaWVtMDk2QGdtYWlsLmNvbSJ9.jQjqqMFSVztppQXztRyhaDinDxBlJ6Jw3Uap2nchr0k', '1817953361', '2022-08-12 10:02:41.814+07', '2022-08-12 10:02:41.814+07'),(39, 71, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmVzIjoxODE3OTUzMzcyLCJ1c2VyIjoieXVuc2FyaWVtMDk2QGdtYWlsLmNvbSJ9.75ap2aVQfdwpsqq4sh51f7KQIC8akQdWabQDJY_myyY', '1817953372', '2022-08-12 10:02:52.497+07', '2022-08-12 10:02:52.497+07'),(40, 71, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmVzIjoxODE3OTUzMzg3LCJ1c2VyIjoieXVuc2FyaWVtMDk2QGdtYWlsLmNvbSJ9.N_YlxHLAZpnWFuRQAXaJvDo6AJkExoZqxuq-RHRxYiA', '1817953387', '2022-08-12 10:03:07.615+07', '2022-08-12 10:03:07.615+07'),(41, 71, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmVzIjoxODE3OTUzNDcwLCJ1c2VyIjoieXVuc2FyaWVtMDk2QGdtYWlsLmNvbSJ9.j1ktnLws0r8RP1LoqCl8bBD0diLfyOtYzcWfshzgAns', '1817953470', '2022-08-12 10:04:30.377+07', '2022-08-12 10:04:30.377+07'),(42, 71, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmVzIjoxODE3OTUzNjg2LCJ1c2VyIjoieXVuc2FyaWVtMDk2QGdtYWlsLmNvbSJ9.ifj3UydxPHvFWWlMLn3sGW6kJZR5fmXwU4y6-GYxY-E', '1817953686', '2022-08-12 10:08:06.283+07', '2022-08-12 10:08:06.283+07'),(43, 71, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmVzIjoxODE3OTUzNzk4LCJ1c2VyIjoieXVuc2FyaWVtMDk2QGdtYWlsLmNvbSJ9.J8U3etkCfH0o70qknyVcbcXllpibe1IJRWu-0A7XlME', '1817953798', '2022-08-12 10:09:58.855+07', '2022-08-12 10:09:58.855+07'),(44, 71, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmVzIjoxODE3OTUzODEwLCJ1c2VyIjoieXVuc2FyaWVtMDk2QGdtYWlsLmNvbSJ9.DY0h8Bam1HVdnuTtFn7bSJVpQ6pw_HFX4iceUOIPgs8', '1817953810', '2022-08-12 10:10:10.458+07', '2022-08-12 10:10:10.458+07'),(45, 71, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmVzIjoxODE3OTUzODgxLCJ1c2VyIjoieXVuc2FyaWVtMDk2QGdtYWlsLmNvbSJ9.WULEKyPB-mpfJjS5yM4Nm3jW9ERpfrdoA-SFmiKOGjU', '1817953881', '2022-08-12 10:11:21.806+07', '2022-08-12 10:11:21.806+07'),(46, 71, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmVzIjoxODE3OTUzODg5LCJ1c2VyIjoieXVuc2FyaWVtMDk2QGdtYWlsLmNvbSJ9.PLhkEV3K5XSNaorCCUYdnh0Sk-6PaJ20bRQGQL3mVhs', '1817953889', '2022-08-12 10:11:29.136+07', '2022-08-12 10:11:29.136+07'),(47, 71, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmVzIjoxODE3OTUzOTE2LCJ1c2VyIjoieXVuc2FyaWVtMDk2QGdtYWlsLmNvbSJ9.bWw76zMqjpwRi1_etsq3X7JMN5HuCViL3vJmBRpxaT0', '1817953916', '2022-08-12 10:11:56.319+07', '2022-08-12 10:11:56.319+07'),(48, 71, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmVzIjoxODE3OTUzOTcxLCJ1c2VyIjoieXVuc2FyaWVtMDk2QGdtYWlsLmNvbSJ9.dEozuDBnrsf_cbof-GZHG_YYIf58OsEOls79Uhwj7Og', '1817953971', '2022-08-12 10:12:51.768+07', '2022-08-12 10:12:51.768+07'),(49, 71, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmVzIjoxODE3OTU0MDI0LCJ1c2VyIjoieXVuc2FyaWVtMDk2QGdtYWlsLmNvbSJ9.DXJVVBzAgWLNDZHE-vrDjy7pKWLJK4YIBa0NO-SSoJ8', '1817954024', '2022-08-12 10:13:44.288+07', '2022-08-12 10:13:44.288+07'),(50, 71, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmVzIjoxODE3OTU0MDQ0LCJ1c2VyIjoieXVuc2FyaWVtMDk2QGdtYWlsLmNvbSJ9.o8KaPbr6I5S39B429060cWksQ-JK9m1XTW8SbAgFmrY', '1817954044', '2022-08-12 10:14:04.37+07', '2022-08-12 10:14:04.37+07'),(51, 71, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmVzIjoxODE3OTU0MDU3LCJ1c2VyIjoieXVuc2FyaWVtMDk2QGdtYWlsLmNvbSJ9.8DS7H7Mqvmv3ajGdiQP4b1JKLRoUj6vcE8X3nb6V6Ws', '1817954057', '2022-08-12 10:14:17.245+07', '2022-08-12 10:14:17.245+07'),(52, 71, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmVzIjoxODE3OTU0MTUyLCJ1c2VyIjoieXVuc2FyaWVtMDk2QGdtYWlsLmNvbSJ9.CT49njtzDB_BRrveHuKP0UmrANaXBtzgvAvJ2Iwgm-U', '1817954152', '2022-08-12 10:15:52.469+07', '2022-08-12 10:15:52.469+07'),(53, 71, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmVzIjoxODE3OTU0MTg2LCJ1c2VyIjp7ImlkIjoiNzEiLCJ1c2VybmFtZSI6Inl1bnNhcmllbTA5NkBnbWFpbC5jb20iLCJjcmVhdGVkX2F0IjoiMjAyMi0wOC0xMlQwMjo1ODowNy44MjlaIn19.cYbu1NGQD4xAPmqxTrrW-7WGoU5cEMApw_HrV3CYvKg', '1817954186', '2022-08-12 10:16:26.529+07', '2022-08-12 10:16:26.529+07'),(54, 71, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmVzIjoxODE3OTU0Mjg2LCJ1c2VyIjoieXVuc2FyaWVtMDk2QGdtYWlsLmNvbSJ9.lX-fZ82IEGr8s2R_J5HJagTxdazfd_O9js2dT6mDfEs', '1817954286', '2022-08-12 10:18:06.681+07', '2022-08-12 10:18:06.681+07'),(55, 71, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmVzIjoxODE3OTU0MzA4LCJ1c2VyIjoieXVuc2FyaWVtMDk2QGdtYWlsLmNvbSJ9.4Dn3EumY83UnYUw-JxgrSJ3J6Rnoy5XKz9wq4CyewqY', '1817954308', '2022-08-12 10:18:28.703+07', '2022-08-12 10:18:28.703+07'),(56, 71, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmVzIjoxODE3OTU0MzI0LCJ1c2VyIjoieXVuc2FyaWVtMDk2QGdtYWlsLmNvbSJ9.1djOqM-5T8KuR9HWXRBS-3zyxB2ErMn4leNdtYF3lRE', '1817954324', '2022-08-12 10:18:44.442+07', '2022-08-12 10:18:44.442+07'),(57, 71, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmVzIjoxODE3OTU0MzgwLCJ1c2VyIjoieXVuc2FyaWVtMDk2QGdtYWlsLmNvbSJ9.MbWmWpmxFmVhQZSSdtbJ7l6AEU5-kiicZC7HrXgDWXM', '1817954380', '2022-08-12 10:19:41.051+07', '2022-08-12 10:19:41.051+07'),(58, 71, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmVzIjoxODE3OTU0MzgxLCJ1c2VyIjp7ImlkIjoiNzEiLCJ1c2VybmFtZSI6Inl1bnNhcmllbTA5NkBnbWFpbC5jb20iLCJjcmVhdGVkX2F0IjoiMjAyMi0wOC0xMlQwMjo1ODowNy44MjlaIn19.89lGx-v-po1j3hnQDkTJB25yxz1T7wFLJlPXule0is4', '1817954381', '2022-08-12 10:19:41.06+07', '2022-08-12 10:19:41.06+07'),(59, 71, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmVzIjoxODE3OTU0Mzg0LCJ1c2VyIjoieXVuc2FyaWVtMDk2QGdtYWlsLmNvbSJ9.5p63n78LEkyRSfXXo4-2sHbHS03Bn2rB-R8SGCaj8zo', '1817954384', '2022-08-12 10:19:44.5+07', '2022-08-12 10:19:44.5+07'),(60, 71, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmVzIjoxODE3OTU0Mzg0LCJ1c2VyIjp7ImlkIjoiNzEiLCJ1c2VybmFtZSI6Inl1bnNhcmllbTA5NkBnbWFpbC5jb20iLCJjcmVhdGVkX2F0IjoiMjAyMi0wOC0xMlQwMjo1ODowNy44MjlaIn19.t_OzUoQ0ByPKNBrGkARdGiKmnDyGwcTYCgSMy_kwmvk', '1817954384', '2022-08-12 10:19:44.514+07', '2022-08-12 10:19:44.514+07'),(61, 73, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmVzIjoxODE3OTU0NDE1LCJ1c2VyIjp7ImlkIjoiNzMiLCJ1c2VybmFtZSI6Inl1bnNhcmllbTA5NjFAZ21haWwuY29tIiwiY3JlYXRlZF9hdCI6IjIwMjItMDgtMTJUMDM6MjA6MTUuMzkxWiJ9fQ.MbcGz4Sk4YYF2j7qaNJMw9QQ4r5DVYurUPwmqMDhbyw', '1817954415', '2022-08-12 10:20:15.404+07', '2022-08-12 10:20:15.404+07'),(62, 73, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmVzIjoxODE3OTU0NTgzLCJ1c2VyIjoieXVuc2FyaWVtMDk2MUBnbWFpbC5jb20ifQ.i-11kNrHsWYUTNTUk6gU0lMWWnbhPks3F9-8Au-GZn8', '1817954583', '2022-08-12 10:23:03.673+07', '2022-08-12 10:23:03.673+07'),(63, 73, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmVzIjoxODE3OTU0NTgzLCJ1c2VyIjp7ImlkIjoiNzMiLCJ1c2VybmFtZSI6Inl1bnNhcmllbTA5NjFAZ21haWwuY29tIiwiY3JlYXRlZF9hdCI6IjIwMjItMDgtMTJUMDM6MjA6MTUuMzkxWiJ9fQ.M3vz7hjg4-L_8_DNNNOe-oTB_6Xw84qblf_InoK2LVI', '1817954583', '2022-08-12 10:23:03.685+07', '2022-08-12 10:23:03.685+07'),(64, 75, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmVzIjoxODE3OTYwMDM5LCJ1c2VyIjp7ImlkIjoiNzUiLCJ1c2VybmFtZSI6Inl1bnNhcmllbTA5NkBnbWFpbC5jb20iLCJjcmVhdGVkX2F0IjoiMjAyMi0wOC0xMlQwNDo1Mzo1OS4yMDlaIn19.aUEfYCNb4aI3iU3IR6exRpdmFxqr_72FHdVsaA8KglI', '1817960039', '2022-08-12 11:53:59.224+07', '2022-08-12 11:53:59.224+07'),(65, 75, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmVzIjoxODE3OTYwMTQ2LCJ1c2VyIjoieXVuc2FyaWVtMDk2QGdtYWlsLmNvbSJ9.6gewggVcDJ0YGeoVEQMSq3gLdBuv5MwxlPWNgBbv5Lo', '1817960146', '2022-08-12 11:55:46.358+07', '2022-08-12 11:55:46.358+07'),(66, 75, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmVzIjoxODE3OTYwMTQ2LCJ1c2VyIjp7ImlkIjoiNzUiLCJ1c2VybmFtZSI6Inl1bnNhcmllbTA5NkBnbWFpbC5jb20iLCJjcmVhdGVkX2F0IjoiMjAyMi0wOC0xMlQwNDo1Mzo1OS4yMDlaIn19.cvxaeCoYALa2e8o7deB6AUcj_btD-gEkd27TtR9SEj8', '1817960146', '2022-08-12 11:55:46.37+07', '2022-08-12 11:55:46.37+07'),(67, 69, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmVzIjoxODE3OTY3NTg2LCJ1c2VyIjp7ImlkIjoiNjkiLCJ1c2VybmFtZSI6Inl1bnNhcmllbUBnbWFpbC5jb20iLCJjcmVhdGVkX2F0IjoiMjAyMi0wNy0wN1QwNTowMDo0Mi43MjFaIn19.yM9aX4DgzwgygBSqPajMVEAZu9HjU0zBQ4cEqFlr7mg', '1817967586', '2022-08-12 13:59:46.407+07', '2022-08-12 13:59:46.407+07');
COMMIT;
BEGIN;
LOCK TABLE "public"."migrations" IN SHARE MODE;
DELETE FROM "public"."migrations";
COMMIT;
BEGIN;
LOCK TABLE "public"."migrations_lock" IN SHARE MODE;
DELETE FROM "public"."migrations_lock";
COMMIT;
BEGIN;
LOCK TABLE "public"."mt_lov" IN SHARE MODE;
DELETE FROM "public"."mt_lov";
INSERT INTO "public"."mt_lov" ("id","lov_code","name_kh","name_en","type","status","created_at","updated_at","icon","image","tags","description","order_by") VALUES (20, 'PHP', NULL, 'PHP', 'Topic', 't', '2021-09-02 15:45:54+07', '2021-09-02 15:45:54+07', NULL, NULL, 'Music', NULL, NULL),(3, 'Web', NULL, 'Web Development', 'Explore', 't', '2021-09-02 15:45:54+07', '2021-09-02 15:45:54+07', 'web.png', 'cover2.png', 'Music', NULL, NULL),(5, 'MOBILE', NULL, 'Mobile Development', 'Explore', 't', '2021-09-02 15:45:54+07', '2021-09-02 15:45:54+07', 'mobile.png', 'cover4.png', 'Mobile Development', NULL, NULL),(1, 'LANGUAGE', NULL, 'Language', 'Explore', 't', '2021-09-02 15:45:54+07', '2021-09-02 15:45:54+07', 'language.png', 'cover.png', 'Language', NULL, NULL),(6, 'GENERAL_KNOWLEDGE', NULL, 'General Knowledge', 'Explore', 't', '2021-09-02 15:45:54+07', '2021-09-02 15:45:54+07', 'general.png', 'cover5.png', 'General Knowledge', NULL, NULL),(4, 'NETWORK', NULL, 'Network & Security', 'Explore', 't', '2021-09-02 15:45:54+07', '2021-09-02 15:45:54+07', 'network.png', 'cover3.png', 'Network & Security', NULL, NULL),(9, 'SLIDE03', NULL, 'Slide 03', 'HomeSlide', 't', '2021-09-02 15:45:54+07', '2021-09-02 15:45:54+07', NULL, 'slide3.png', NULL, 'slide description 3', NULL),(7, 'SLIDE01', NULL, 'Slide 01', 'HomeSlide', 't', '2021-09-02 15:45:54+07', '2021-09-02 15:45:54+07', NULL, 'slide1.png', NULL, 'Slide description 1', NULL),(11, 'SLIDE05', NULL, 'Slide 05', 'HomeSlide', 't', '2021-09-02 15:45:54+07', '2021-09-02 15:45:54+07', NULL, 'slide5.png', NULL, 'slide description 5', NULL),(8, 'SLIDE02', NULL, 'Slide 02', 'HomeSlide', 't', '2021-09-02 15:45:54+07', '2021-09-02 15:45:54+07', NULL, 'slide2.png', NULL, 'slide description 2', NULL),(10, 'SLIDE04', NULL, 'Slide 04', 'HomeSlide', 't', '2021-09-02 15:45:54+07', '2021-09-02 15:45:54+07', NULL, 'slide4.png', NULL, 'slide description 4', NULL),(12, 'Web', NULL, 'Web Development', 'Topic', 't', '2021-09-02 15:45:54+07', '2021-09-02 15:45:54+07', NULL, NULL, 'Web Development', NULL, NULL),(17, 'ReactJS', NULL, 'React JS', 'Topic', 't', '2021-09-02 15:45:54+07', '2021-09-02 15:45:54+07', NULL, NULL, 'React JS', NULL, NULL),(19, 'NodeJS', NULL, 'Node JS', 'Topic', 't', '2021-09-02 15:45:54+07', '2021-09-02 15:45:54+07', NULL, NULL, 'Node JS', NULL, NULL),(21, 'React Native', NULL, 'React Native', 'Topic', 't', '2021-09-02 15:45:54+07', '2021-09-02 15:45:54+07', NULL, NULL, 'React Native', NULL, NULL),(2, 'BUSINESS', NULL, 'Business', 'Explore', 't', '2021-09-02 15:45:54+07', '2021-09-02 15:45:54+07', 'business.png', 'cover1.png', 'Business', NULL, NULL),(22, 'All', NULL, 'All', 'Topic', 't', '2021-09-02 15:45:54+07', '2021-09-02 15:45:54+07', NULL, NULL, NULL, NULL, '1');
COMMIT;
BEGIN;
LOCK TABLE "public"."register_verify" IN SHARE MODE;
DELETE FROM "public"."register_verify";
INSERT INTO "public"."register_verify" ("id","phone_number","code","resend","status","created_at","updated_at") VALUES (1, 'yunsariem097@gmail.com', '5802', 2, 'f', '2022-07-19 13:33:51.723+07', '2022-07-19 13:35:31.028+07'),(2, 'yunsariem@gmail.com', '1221', 1, 'f', '2022-07-19 13:36:00.526+07', '2022-07-19 13:36:00.526+07'),(3, 'khmeridea02@gmail.com', '8932', 1, 'f', '2022-07-19 13:39:05.052+07', '2022-07-19 13:39:05.052+07');
COMMIT;
BEGIN;
LOCK TABLE "public"."users" IN SHARE MODE;
DELETE FROM "public"."users";
INSERT INTO "public"."users" ("id","email_address","username","password","email_verified_at","created_at","updated_at","created_by","updated_by","phone_number","status","image","verify_code","verify_status","image_cover","image_path","image_cover_path") VALUES (69, 'yunsariem@gmail.com', 'Yun Sariem', '$2b$05$A4NB9H8bV7QyFzKMie2Rn.RtkvX3d/N9XADe7eJxw/R3M9/S1SxVW', '2022-07-07 12:00:42.721+07', '2022-07-07 12:00:42.721+07', '2022-07-15 11:37:24.837+07', 'yunsariem@gmail.com', NULL, NULL, 't', NULL, NULL, NULL, NULL, NULL, NULL),(75, 'yunsariem096@gmail.com', NULL, '$2b$05$SVax94XSh28i1O4hR/x8JeMHJJvQST0rNBW7sgdBagEbfEvKojg8K', '2022-08-12 11:53:59.209+07', '2022-08-12 11:53:59.209+07', '2022-08-12 11:53:59.209+07', 'yunsariem096@gmail.com', NULL, NULL, 't', NULL, NULL, NULL, NULL, NULL, NULL);
COMMIT;
BEGIN;
LOCK TABLE "public"."videos" IN SHARE MODE;
DELETE FROM "public"."videos";
INSERT INTO "public"."videos" ("id","userid","patch","status","created_at","updated_at","created_by","updated_by","category","title","filename","destination","originalname","size","mimetype","encoding","duration","video_info","provider_name","thumbnail_url","description") VALUES (204, 69, '..\resources\video\uploads\20220707\1657174004039_xe15ebkl5amm53c-videoplayback.mp4', 't', '2022-07-07 13:06:49.07+07', '2022-07-07 13:06:49.07+07', '69', NULL, NULL, '10 - WSO2 EI Tutorial - Configure HTTP Endpoint', '1657174004039_xe15ebkl5amm53c-videoplayback.mp4', '20220707', 'videoplayback.mp4', '26856715', 'video/mp4', '7bit', '12:15', '{"streams":[{"index":0,"codec_name":"h264","codec_long_name":"H.264 / AVC / MPEG-4 AVC / MPEG-4 part 10","profile":"High","codec_type":"video","codec_time_base":"1/60","codec_tag_string":"avc1","codec_tag":"0x31637661","width":1920,"height":1000,"coded_width":1920,"coded_height":1008,"has_b_frames":1,"sample_aspect_ratio":"1:1","display_aspect_ratio":"48:25","pix_fmt":"yuv420p","level":40,"color_range":"tv","color_space":"smpte170m","color_transfer":"bt709","color_primaries":"bt709","chroma_location":"left","refs":3,"is_avc":"1","nal_length_size":"4","r_frame_rate":"30/1","avg_frame_rate":"30/1","time_base":"1/15360","start_pts":0,"start_time":"0.000000","duration_ts":11295232,"duration":"735.366667","bit_rate":"289115","bits_per_raw_sample":"8","disposition":{"default":1,"dub":0,"original":0,"comment":0,"lyrics":0,"karaoke":0,"forced":0,"hearing_impaired":0,"visual_impaired":0,"clean_effects":0,"attached_pic":0},"tags":{"creation_time":"2020-05-19 13:52:50","language":"und","handler_name":"ISO Media file produced by Google Inc."}}],"format":{"filename":"..\\resources\\video\\uploads\\20220707\\1657174004039_xe15ebkl5amm53c-videoplayback.mp4","nb_streams":1,"nb_programs":0,"format_name":"mov,mp4,m4a,3gp,3g2,mj2","format_long_name":"QuickTime / MOV","start_time":"0.000000","duration":"735.366667","size":"26856715","bit_rate":"292172","probe_score":100,"tags":{"major_brand":"dash","minor_version":"0","compatible_brands":"iso6avc1mp41","creation_time":"2020-05-19 13:52:50"}}}', 'BGetting', NULL, NULL),(205, 69, '..\resources\video\uploads\20220707\1657174243869_xe15ebkl5amra59-1.OBIEE12CTutorialPart01DWConcepts.mp4', 't', '2022-07-07 13:11:15.43+07', '2022-07-07 13:11:15.43+07', '69', NULL, NULL, '1. OBIEE 12C Tutorial Part 01 DW Concepts', '1657174243869_xe15ebkl5amra59-1.OBIEE12CTutorialPart01DWConcepts.mp4', '20220707', '1. OBIEE 12C Tutorial Part 01 DW Concepts.mp4', '173082910', 'video/mp4', '7bit', '1:08:33', '{"streams":[{"index":0,"codec_name":"h264","codec_long_name":"H.264 / AVC / MPEG-4 AVC / MPEG-4 part 10","profile":"High","codec_type":"video","codec_time_base":"1/60","codec_tag_string":"avc1","codec_tag":"0x31637661","width":1280,"height":720,"coded_width":1280,"coded_height":720,"has_b_frames":2,"sample_aspect_ratio":"1:1","display_aspect_ratio":"16:9","pix_fmt":"yuv420p","level":30,"chroma_location":"left","refs":4,"is_avc":"1","nal_length_size":"4","r_frame_rate":"30/1","avg_frame_rate":"30/1","time_base":"1/30","start_pts":0,"start_time":"0.000000","duration_ts":123396,"duration":"4113.200000","bit_rate":"143994","bits_per_raw_sample":"8","nb_frames":"123396","disposition":{"default":1,"dub":0,"original":0,"comment":0,"lyrics":0,"karaoke":0,"forced":0,"hearing_impaired":0,"visual_impaired":0,"clean_effects":0,"attached_pic":0},"tags":{"language":"und","handler_name":"VideoHandler"}},{"index":1,"codec_name":"aac","codec_long_name":"AAC (Advanced Audio Coding)","profile":"LC","codec_type":"audio","codec_time_base":"1/48000","codec_tag_string":"mp4a","codec_tag":"0x6134706d","sample_fmt":"fltp","sample_rate":"48000","channels":2,"channel_layout":"stereo","bits_per_sample":0,"r_frame_rate":"0/0","avg_frame_rate":"0/0","time_base":"1/48000","start_pts":0,"start_time":"0.000000","duration_ts":197450752,"duration":"4113.557333","bit_rate":"189375","max_bit_rate":"192000","nb_frames":"192823","disposition":{"default":1,"dub":0,"original":0,"comment":0,"lyrics":0,"karaoke":0,"forced":0,"hearing_impaired":0,"visual_impaired":0,"clean_effects":0,"attached_pic":0},"tags":{"language":"und","handler_name":"SoundHandler"}}],"format":{"filename":"..\\resources\\video\\uploads\\20220707\\1657174243869_xe15ebkl5amra59-1.OBIEE12CTutorialPart01DWConcepts.mp4","nb_streams":2,"nb_programs":0,"format_name":"mov,mp4,m4a,3gp,3g2,mj2","format_long_name":"QuickTime / MOV","start_time":"0.000000","duration":"4113.558000","size":"173082910","bit_rate":"336609","probe_score":100,"tags":{"major_brand":"mp42","minor_version":"0","compatible_brands":"isomiso2avc1mp41mp42","encoder":"Lavf54.29.104"}}}', 'BGetting', NULL, NULL),(206, 69, '..\resources\video\uploads\20220707\1657174293157_xe15ebkl5amsc6d-2.OBIEE12CTutorialPart02OBIEE12CInstallation.mp4', 't', '2022-07-07 13:12:10.529+07', '2022-07-07 13:12:10.529+07', '69', NULL, NULL, '2. OBIEE 12C Tutorial Part 02 OBIEE 12C Installation', '1657174293157_xe15ebkl5amsc6d-2.OBIEE12CTutorialPart02OBIEE12CInstallation.mp4', '20220707', '2. OBIEE 12C Tutorial Part 02 OBIEE 12C Installation.mp4', '155152085', 'video/mp4', '7bit', '1:11:41', '{"streams":[{"index":0,"codec_name":"h264","codec_long_name":"H.264 / AVC / MPEG-4 AVC / MPEG-4 part 10","profile":"High","codec_type":"video","codec_time_base":"1/20","codec_tag_string":"avc1","codec_tag":"0x31637661","width":1280,"height":720,"coded_width":1280,"coded_height":720,"has_b_frames":2,"sample_aspect_ratio":"1:1","display_aspect_ratio":"16:9","pix_fmt":"yuv420p","level":30,"chroma_location":"left","refs":4,"is_avc":"1","nal_length_size":"4","r_frame_rate":"10/1","avg_frame_rate":"10/1","time_base":"1/10","start_pts":0,"start_time":"0.000000","duration_ts":43011,"duration":"4301.100000","bit_rate":"97006","bits_per_raw_sample":"8","nb_frames":"43011","disposition":{"default":1,"dub":0,"original":0,"comment":0,"lyrics":0,"karaoke":0,"forced":0,"hearing_impaired":0,"visual_impaired":0,"clean_effects":0,"attached_pic":0},"tags":{"language":"und","handler_name":"VideoHandler"}},{"index":1,"codec_name":"aac","codec_long_name":"AAC (Advanced Audio Coding)","profile":"LC","codec_type":"audio","codec_time_base":"1/48000","codec_tag_string":"mp4a","codec_tag":"0x6134706d","sample_fmt":"fltp","sample_rate":"48000","channels":2,"channel_layout":"stereo","bits_per_sample":0,"r_frame_rate":"0/0","avg_frame_rate":"0/0","time_base":"1/48000","start_pts":0,"start_time":"0.000000","duration_ts":206467072,"duration":"4301.397333","bit_rate":"189375","max_bit_rate":"192000","nb_frames":"201628","disposition":{"default":1,"dub":0,"original":0,"comment":0,"lyrics":0,"karaoke":0,"forced":0,"hearing_impaired":0,"visual_impaired":0,"clean_effects":0,"attached_pic":0},"tags":{"language":"und","handler_name":"SoundHandler"}}],"format":{"filename":"..\\resources\\video\\uploads\\20220707\\1657174293157_xe15ebkl5amsc6d-2.OBIEE12CTutorialPart02OBIEE12CInstallation.mp4","nb_streams":2,"nb_programs":0,"format_name":"mov,mp4,m4a,3gp,3g2,mj2","format_long_name":"QuickTime / MOV","start_time":"0.000000","duration":"4301.398000","size":"155152085","bit_rate":"288561","probe_score":100,"tags":{"major_brand":"mp42","minor_version":"0","compatible_brands":"isomiso2avc1mp41mp42","encoder":"Lavf54.29.104"}}}', 'BGetting', NULL, NULL),(207, 69, '..\resources\video\uploads\20220707\1657174341373_xe15ebkl5amtddp-3.OBIEE12CTutorialPart03StarSchemaVsSnowFlakeSchema.mp4', 't', '2022-07-07 13:12:51.02+07', '2022-07-07 13:12:51.02+07', '69', NULL, NULL, '3. OBIEE 12C Tutorial Part 03 Star Schema Vs Snow Flake Schema', '1657174341373_xe15ebkl5amtddp-3.OBIEE12CTutorialPart03StarSchemaVsSnowFlakeSchema.mp4', '20220707', '3. OBIEE 12C Tutorial Part 03 Star Schema Vs Snow Flake Schema.mp4', '147763402', 'video/mp4', '7bit', '1:04:58', '{"streams":[{"index":0,"codec_name":"h264","codec_long_name":"H.264 / AVC / MPEG-4 AVC / MPEG-4 part 10","profile":"High","codec_type":"video","codec_time_base":"1/40","codec_tag_string":"avc1","codec_tag":"0x31637661","width":1280,"height":720,"coded_width":1280,"coded_height":720,"has_b_frames":2,"sample_aspect_ratio":"1:1","display_aspect_ratio":"16:9","pix_fmt":"yuv420p","level":30,"chroma_location":"left","refs":4,"is_avc":"1","nal_length_size":"4","r_frame_rate":"20/1","avg_frame_rate":"20/1","time_base":"1/20","start_pts":0,"start_time":"0.000000","duration_ts":77971,"duration":"3898.550000","bit_rate":"111095","bits_per_raw_sample":"8","nb_frames":"77971","disposition":{"default":1,"dub":0,"original":0,"comment":0,"lyrics":0,"karaoke":0,"forced":0,"hearing_impaired":0,"visual_impaired":0,"clean_effects":0,"attached_pic":0},"tags":{"language":"und","handler_name":"VideoHandler"}},{"index":1,"codec_name":"aac","codec_long_name":"AAC (Advanced Audio Coding)","profile":"LC","codec_type":"audio","codec_time_base":"1/48000","codec_tag_string":"mp4a","codec_tag":"0x6134706d","sample_fmt":"fltp","sample_rate":"48000","channels":2,"channel_layout":"stereo","bits_per_sample":0,"r_frame_rate":"0/0","avg_frame_rate":"0/0","time_base":"1/48000","start_pts":0,"start_time":"0.000000","duration_ts":187147264,"duration":"3898.901333","bit_rate":"189375","max_bit_rate":"192000","nb_frames":"182761","disposition":{"default":1,"dub":0,"original":0,"comment":0,"lyrics":0,"karaoke":0,"forced":0,"hearing_impaired":0,"visual_impaired":0,"clean_effects":0,"attached_pic":0},"tags":{"language":"und","handler_name":"SoundHandler"}}],"format":{"filename":"..\\resources\\video\\uploads\\20220707\\1657174341373_xe15ebkl5amtddp-3.OBIEE12CTutorialPart03StarSchemaVsSnowFlakeSchema.mp4","nb_streams":2,"nb_programs":0,"format_name":"mov,mp4,m4a,3gp,3g2,mj2","format_long_name":"QuickTime / MOV","start_time":"0.000000","duration":"3898.902000","size":"147763402","bit_rate":"303189","probe_score":100,"tags":{"major_brand":"mp42","minor_version":"0","compatible_brands":"isomiso2avc1mp41mp42","encoder":"Lavf54.29.104"}}}', 'BGetting', NULL, NULL),(208, 69, '..\resources\video\uploads\20220707\1657174383220_xe15ebkl5amu9o4-4.OBIEE12CTutorialPart04PhysicalLayerPart01.mp4', 't', '2022-07-07 13:13:32.424+07', '2022-07-07 13:13:32.424+07', '69', NULL, NULL, '4. OBIEE 12C Tutorial Part 04 Physical Layer Part 01', '1657174383220_xe15ebkl5amu9o4-4.OBIEE12CTutorialPart04PhysicalLayerPart01.mp4', '20220707', '4. OBIEE 12C Tutorial Part 04 Physical Layer Part 01.mp4', '141874848', 'video/mp4', '7bit', '1:06:41', '{"streams":[{"index":0,"codec_name":"h264","codec_long_name":"H.264 / AVC / MPEG-4 AVC / MPEG-4 part 10","profile":"High","codec_type":"video","codec_time_base":"1/60","codec_tag_string":"avc1","codec_tag":"0x31637661","width":1280,"height":720,"coded_width":1280,"coded_height":720,"has_b_frames":2,"sample_aspect_ratio":"1:1","display_aspect_ratio":"16:9","pix_fmt":"yuv420p","level":30,"chroma_location":"left","refs":4,"is_avc":"1","nal_length_size":"4","r_frame_rate":"30/1","avg_frame_rate":"30/1","time_base":"1/30","start_pts":0,"start_time":"0.000000","duration_ts":120043,"duration":"4001.433333","bit_rate":"91029","bits_per_raw_sample":"8","nb_frames":"120043","disposition":{"default":1,"dub":0,"original":0,"comment":0,"lyrics":0,"karaoke":0,"forced":0,"hearing_impaired":0,"visual_impaired":0,"clean_effects":0,"attached_pic":0},"tags":{"language":"und","handler_name":"VideoHandler"}},{"index":1,"codec_name":"aac","codec_long_name":"AAC (Advanced Audio Coding)","profile":"LC","codec_type":"audio","codec_time_base":"1/48000","codec_tag_string":"mp4a","codec_tag":"0x6134706d","sample_fmt":"fltp","sample_rate":"48000","channels":2,"channel_layout":"stereo","bits_per_sample":0,"r_frame_rate":"0/0","avg_frame_rate":"0/0","time_base":"1/48000","start_pts":0,"start_time":"0.000000","duration_ts":192084992,"duration":"4001.770667","bit_rate":"189375","max_bit_rate":"192000","nb_frames":"187583","disposition":{"default":1,"dub":0,"original":0,"comment":0,"lyrics":0,"karaoke":0,"forced":0,"hearing_impaired":0,"visual_impaired":0,"clean_effects":0,"attached_pic":0},"tags":{"language":"und","handler_name":"SoundHandler"}}],"format":{"filename":"..\\resources\\video\\uploads\\20220707\\1657174383220_xe15ebkl5amu9o4-4.OBIEE12CTutorialPart04PhysicalLayerPart01.mp4","nb_streams":2,"nb_programs":0,"format_name":"mov,mp4,m4a,3gp,3g2,mj2","format_long_name":"QuickTime / MOV","start_time":"0.000000","duration":"4001.771000","size":"141874848","bit_rate":"283624","probe_score":100,"tags":{"major_brand":"mp42","minor_version":"0","compatible_brands":"isomiso2avc1mp41mp42","encoder":"Lavf54.29.104"}}}', 'BGetting', NULL, NULL),(209, 69, '..\resources\video\uploads\20220707\1657174423870_xe15ebkl5amv51a-5.OBIEE12CTutorialPart05PhysicalLayerPart02.mp4', 't', '2022-07-07 13:14:06.74+07', '2022-07-07 13:14:06.74+07', '69', NULL, NULL, '5. OBIEE 12C Tutorial Part 05 Physical Layer Part 02', '1657174423870_xe15ebkl5amv51a-5.OBIEE12CTutorialPart05PhysicalLayerPart02.mp4', '20220707', '5. OBIEE 12C Tutorial Part 05 Physical Layer Part 02.mp4', '113257984', 'video/mp4', '7bit', '56:44', '{"streams":[{"index":0,"codec_name":"h264","codec_long_name":"H.264 / AVC / MPEG-4 AVC / MPEG-4 part 10","profile":"High","codec_type":"video","codec_time_base":"1/40","codec_tag_string":"avc1","codec_tag":"0x31637661","width":1280,"height":720,"coded_width":1280,"coded_height":720,"has_b_frames":2,"sample_aspect_ratio":"1:1","display_aspect_ratio":"16:9","pix_fmt":"yuv420p","level":30,"chroma_location":"left","refs":4,"is_avc":"1","nal_length_size":"4","r_frame_rate":"20/1","avg_frame_rate":"20/1","time_base":"1/20","start_pts":0,"start_time":"0.000000","duration_ts":68087,"duration":"3404.350000","bit_rate":"74025","bits_per_raw_sample":"8","nb_frames":"68087","disposition":{"default":1,"dub":0,"original":0,"comment":0,"lyrics":0,"karaoke":0,"forced":0,"hearing_impaired":0,"visual_impaired":0,"clean_effects":0,"attached_pic":0},"tags":{"language":"und","handler_name":"VideoHandler"}},{"index":1,"codec_name":"aac","codec_long_name":"AAC (Advanced Audio Coding)","profile":"LC","codec_type":"audio","codec_time_base":"1/48000","codec_tag_string":"mp4a","codec_tag":"0x6134706d","sample_fmt":"fltp","sample_rate":"48000","channels":2,"channel_layout":"stereo","bits_per_sample":0,"r_frame_rate":"0/0","avg_frame_rate":"0/0","time_base":"1/48000","start_pts":0,"start_time":"0.000000","duration_ts":163425280,"duration":"3404.693333","bit_rate":"189375","max_bit_rate":"192000","nb_frames":"159595","disposition":{"default":1,"dub":0,"original":0,"comment":0,"lyrics":0,"karaoke":0,"forced":0,"hearing_impaired":0,"visual_impaired":0,"clean_effects":0,"attached_pic":0},"tags":{"language":"und","handler_name":"SoundHandler"}}],"format":{"filename":"..\\resources\\video\\uploads\\20220707\\1657174423870_xe15ebkl5amv51a-5.OBIEE12CTutorialPart05PhysicalLayerPart02.mp4","nb_streams":2,"nb_programs":0,"format_name":"mov,mp4,m4a,3gp,3g2,mj2","format_long_name":"QuickTime / MOV","start_time":"0.000000","duration":"3404.694000","size":"113257984","bit_rate":"266121","probe_score":100,"tags":{"major_brand":"mp42","minor_version":"0","compatible_brands":"isomiso2avc1mp41mp42","encoder":"Lavf54.29.104"}}}', 'BGetting', NULL, NULL),(210, 69, '..\resources\video\uploads\20220707\1657174456006_xe15ebkl5amvtty-6.OBIEE12CTutorialPart06PhysicalLayerPart03.mp4', 't', '2022-07-07 13:14:41.641+07', '2022-07-07 13:14:41.641+07', '69', NULL, NULL, '6. OBIEE 12C Tutorial Part 06 Physical Layer Part 03', '1657174456006_xe15ebkl5amvtty-6.OBIEE12CTutorialPart06PhysicalLayerPart03.mp4', '20220707', '6. OBIEE 12C Tutorial Part 06 Physical Layer Part 03.mp4', '126566718', 'video/mp4', '7bit', '1:00:59', '{"streams":[{"index":0,"codec_name":"h264","codec_long_name":"H.264 / AVC / MPEG-4 AVC / MPEG-4 part 10","profile":"High","codec_type":"video","codec_time_base":"1/20","codec_tag_string":"avc1","codec_tag":"0x31637661","width":1280,"height":720,"coded_width":1280,"coded_height":720,"has_b_frames":2,"sample_aspect_ratio":"1:1","display_aspect_ratio":"16:9","pix_fmt":"yuv420p","level":30,"chroma_location":"left","refs":4,"is_avc":"1","nal_length_size":"4","r_frame_rate":"10/1","avg_frame_rate":"10/1","time_base":"1/10","start_pts":0,"start_time":"0.000000","duration_ts":36592,"duration":"3659.200000","bit_rate":"85114","bits_per_raw_sample":"8","nb_frames":"36592","disposition":{"default":1,"dub":0,"original":0,"comment":0,"lyrics":0,"karaoke":0,"forced":0,"hearing_impaired":0,"visual_impaired":0,"clean_effects":0,"attached_pic":0},"tags":{"language":"und","handler_name":"VideoHandler"}},{"index":1,"codec_name":"aac","codec_long_name":"AAC (Advanced Audio Coding)","profile":"LC","codec_type":"audio","codec_time_base":"1/48000","codec_tag_string":"mp4a","codec_tag":"0x6134706d","sample_fmt":"fltp","sample_rate":"48000","channels":2,"channel_layout":"stereo","bits_per_sample":0,"r_frame_rate":"0/0","avg_frame_rate":"0/0","time_base":"1/48000","start_pts":0,"start_time":"0.000000","duration_ts":175655936,"duration":"3659.498667","bit_rate":"189375","max_bit_rate":"192000","nb_frames":"171539","disposition":{"default":1,"dub":0,"original":0,"comment":0,"lyrics":0,"karaoke":0,"forced":0,"hearing_impaired":0,"visual_impaired":0,"clean_effects":0,"attached_pic":0},"tags":{"language":"und","handler_name":"SoundHandler"}}],"format":{"filename":"..\\resources\\video\\uploads\\20220707\\1657174456006_xe15ebkl5amvtty-6.OBIEE12CTutorialPart06PhysicalLayerPart03.mp4","nb_streams":2,"nb_programs":0,"format_name":"mov,mp4,m4a,3gp,3g2,mj2","format_long_name":"QuickTime / MOV","start_time":"0.000000","duration":"3659.499000","size":"126566718","bit_rate":"276686","probe_score":100,"tags":{"major_brand":"mp42","minor_version":"0","compatible_brands":"isomiso2avc1mp41mp42","encoder":"Lavf54.29.104"}}}', 'BGetting', NULL, NULL),(211, 69, '..\resources\video\uploads\20220707\1657174491195_xe15ebkl5amwkzf-7.OBIEE12CTutorialPart07PhysicalLayerPart04.mp4', 't', '2022-07-07 13:15:13.048+07', '2022-07-07 13:15:13.048+07', '69', NULL, NULL, '7. OBIEE 12C Tutorial Part 07 Physical Layer Part 04', '1657174491195_xe15ebkl5amwkzf-7.OBIEE12CTutorialPart07PhysicalLayerPart04.mp4', '20220707', '7. OBIEE 12C Tutorial Part 07 Physical Layer Part 04.mp4', '107784241', 'video/mp4', '7bit', '53:42', '{"streams":[{"index":0,"codec_name":"h264","codec_long_name":"H.264 / AVC / MPEG-4 AVC / MPEG-4 part 10","profile":"High","codec_type":"video","codec_time_base":"1/20","codec_tag_string":"avc1","codec_tag":"0x31637661","width":1280,"height":720,"coded_width":1280,"coded_height":720,"has_b_frames":2,"sample_aspect_ratio":"1:1","display_aspect_ratio":"16:9","pix_fmt":"yuv420p","level":30,"chroma_location":"left","refs":4,"is_avc":"1","nal_length_size":"4","r_frame_rate":"10/1","avg_frame_rate":"10/1","time_base":"1/10","start_pts":0,"start_time":"0.000000","duration_ts":32217,"duration":"3221.700000","bit_rate":"76068","bits_per_raw_sample":"8","nb_frames":"32217","disposition":{"default":1,"dub":0,"original":0,"comment":0,"lyrics":0,"karaoke":0,"forced":0,"hearing_impaired":0,"visual_impaired":0,"clean_effects":0,"attached_pic":0},"tags":{"language":"und","handler_name":"VideoHandler"}},{"index":1,"codec_name":"aac","codec_long_name":"AAC (Advanced Audio Coding)","profile":"LC","codec_type":"audio","codec_time_base":"1/48000","codec_tag_string":"mp4a","codec_tag":"0x6134706d","sample_fmt":"fltp","sample_rate":"48000","channels":2,"channel_layout":"stereo","bits_per_sample":0,"r_frame_rate":"0/0","avg_frame_rate":"0/0","time_base":"1/48000","start_pts":0,"start_time":"0.000000","duration_ts":154657792,"duration":"3222.037333","bit_rate":"189375","max_bit_rate":"192000","nb_frames":"151033","disposition":{"default":1,"dub":0,"original":0,"comment":0,"lyrics":0,"karaoke":0,"forced":0,"hearing_impaired":0,"visual_impaired":0,"clean_effects":0,"attached_pic":0},"tags":{"language":"und","handler_name":"SoundHandler"}}],"format":{"filename":"..\\resources\\video\\uploads\\20220707\\1657174491195_xe15ebkl5amwkzf-7.OBIEE12CTutorialPart07PhysicalLayerPart04.mp4","nb_streams":2,"nb_programs":0,"format_name":"mov,mp4,m4a,3gp,3g2,mj2","format_long_name":"QuickTime / MOV","start_time":"0.000000","duration":"3222.038000","size":"107784241","bit_rate":"267617","probe_score":100,"tags":{"major_brand":"mp42","minor_version":"0","compatible_brands":"isomiso2avc1mp41mp42","encoder":"Lavf54.29.104"}}}', 'BGetting', NULL, NULL),(212, 69, '..\resources\video\uploads\20220707\1657174523250_xe15ebkl5amx9pu-8.OBIEE12CTutorialPart08BMMLayer.mp4', 't', '2022-07-07 13:15:48.534+07', '2022-07-07 13:15:48.534+07', '69', NULL, NULL, '8. OBIEE 12C Tutorial Part 08 BMM Layer', '1657174523250_xe15ebkl5amx9pu-8.OBIEE12CTutorialPart08BMMLayer.mp4', '20220707', '8. OBIEE 12C Tutorial Part 08 BMM Layer.mp4', '141453787', 'video/mp4', '7bit', '1:07:55', '{"streams":[{"index":0,"codec_name":"h264","codec_long_name":"H.264 / AVC / MPEG-4 AVC / MPEG-4 part 10","profile":"High","codec_type":"video","codec_time_base":"1/20","codec_tag_string":"avc1","codec_tag":"0x31637661","width":1280,"height":720,"coded_width":1280,"coded_height":720,"has_b_frames":2,"sample_aspect_ratio":"1:1","display_aspect_ratio":"16:9","pix_fmt":"yuv420p","level":30,"chroma_location":"left","refs":4,"is_avc":"1","nal_length_size":"4","r_frame_rate":"10/1","avg_frame_rate":"10/1","time_base":"1/10","start_pts":0,"start_time":"0.000000","duration_ts":40755,"duration":"4075.500000","bit_rate":"86096","bits_per_raw_sample":"8","nb_frames":"40755","disposition":{"default":1,"dub":0,"original":0,"comment":0,"lyrics":0,"karaoke":0,"forced":0,"hearing_impaired":0,"visual_impaired":0,"clean_effects":0,"attached_pic":0},"tags":{"language":"und","handler_name":"VideoHandler"}},{"index":1,"codec_name":"aac","codec_long_name":"AAC (Advanced Audio Coding)","profile":"LC","codec_type":"audio","codec_time_base":"1/48000","codec_tag_string":"mp4a","codec_tag":"0x6134706d","sample_fmt":"fltp","sample_rate":"48000","channels":2,"channel_layout":"stereo","bits_per_sample":0,"r_frame_rate":"0/0","avg_frame_rate":"0/0","time_base":"1/48000","start_pts":0,"start_time":"0.000000","duration_ts":195638272,"duration":"4075.797333","bit_rate":"189375","max_bit_rate":"192000","nb_frames":"191053","disposition":{"default":1,"dub":0,"original":0,"comment":0,"lyrics":0,"karaoke":0,"forced":0,"hearing_impaired":0,"visual_impaired":0,"clean_effects":0,"attached_pic":0},"tags":{"language":"und","handler_name":"SoundHandler"}}],"format":{"filename":"..\\resources\\video\\uploads\\20220707\\1657174523250_xe15ebkl5amx9pu-8.OBIEE12CTutorialPart08BMMLayer.mp4","nb_streams":2,"nb_programs":0,"format_name":"mov,mp4,m4a,3gp,3g2,mj2","format_long_name":"QuickTime / MOV","start_time":"0.000000","duration":"4075.798000","size":"141453787","bit_rate":"277646","probe_score":100,"tags":{"major_brand":"mp42","minor_version":"0","compatible_brands":"isomiso2avc1mp41mp42","encoder":"Lavf54.29.104"}}}', 'BGetting', NULL, NULL),(213, 69, '..\resources\video\uploads\20220707\1657174556560_xe15ebkl5amxzf4-9.OBIEE12CTutorialPart09PresentationLayer.mp4', 't', '2022-07-07 13:16:15.264+07', '2022-07-07 13:16:15.264+07', '69', NULL, NULL, '9. OBIEE 12C Tutorial Part 09 Presentation Layer', '1657174556560_xe15ebkl5amxzf4-9.OBIEE12CTutorialPart09PresentationLayer.mp4', '20220707', '9. OBIEE 12C Tutorial Part 09 Presentation Layer.mp4', '117877074', 'video/mp4', '7bit', '57:45', '{"streams":[{"index":0,"codec_name":"h264","codec_long_name":"H.264 / AVC / MPEG-4 AVC / MPEG-4 part 10","profile":"High","codec_type":"video","codec_time_base":"1/40","codec_tag_string":"avc1","codec_tag":"0x31637661","width":1280,"height":720,"coded_width":1280,"coded_height":720,"has_b_frames":2,"sample_aspect_ratio":"1:1","display_aspect_ratio":"16:9","pix_fmt":"yuv420p","level":30,"chroma_location":"left","refs":4,"is_avc":"1","nal_length_size":"4","r_frame_rate":"20/1","avg_frame_rate":"20/1","time_base":"1/20","start_pts":0,"start_time":"0.000000","duration_ts":69297,"duration":"3464.850000","bit_rate":"80045","bits_per_raw_sample":"8","nb_frames":"69297","disposition":{"default":1,"dub":0,"original":0,"comment":0,"lyrics":0,"karaoke":0,"forced":0,"hearing_impaired":0,"visual_impaired":0,"clean_effects":0,"attached_pic":0},"tags":{"language":"und","handler_name":"VideoHandler"}},{"index":1,"codec_name":"aac","codec_long_name":"AAC (Advanced Audio Coding)","profile":"LC","codec_type":"audio","codec_time_base":"1/48000","codec_tag_string":"mp4a","codec_tag":"0x6134706d","sample_fmt":"fltp","sample_rate":"48000","channels":2,"channel_layout":"stereo","bits_per_sample":0,"r_frame_rate":"0/0","avg_frame_rate":"0/0","time_base":"1/48000","start_pts":0,"start_time":"0.000000","duration_ts":166327296,"duration":"3465.152000","bit_rate":"189375","max_bit_rate":"192000","nb_frames":"162429","disposition":{"default":1,"dub":0,"original":0,"comment":0,"lyrics":0,"karaoke":0,"forced":0,"hearing_impaired":0,"visual_impaired":0,"clean_effects":0,"attached_pic":0},"tags":{"language":"und","handler_name":"SoundHandler"}}],"format":{"filename":"..\\resources\\video\\uploads\\20220707\\1657174556560_xe15ebkl5amxzf4-9.OBIEE12CTutorialPart09PresentationLayer.mp4","nb_streams":2,"nb_programs":0,"format_name":"mov,mp4,m4a,3gp,3g2,mj2","format_long_name":"QuickTime / MOV","start_time":"0.000000","duration":"3465.152000","size":"117877074","bit_rate":"272142","probe_score":100,"tags":{"major_brand":"mp42","minor_version":"0","compatible_brands":"isomiso2avc1mp41mp42","encoder":"Lavf54.29.104"}}}', 'BGetting', NULL, NULL),(214, 69, '..\resources\video\uploads\20220708\1657250853705_xe152pwl5bwdaoa-1.QuickExportasPNG.mp4', 't', '2022-07-08 10:27:37.021+07', '2022-07-08 10:27:37.021+07', '69', NULL, NULL, '1. Quick Export as PNG', '1657250853705_xe152pwl5bwdaoa-1.QuickExportasPNG.mp4', '20220708', '1. Quick Export as PNG.mp4', '10176662', 'video/mp4', '7bit', '1:05', '{"streams":[{"index":0,"codec_name":"h264","codec_long_name":"H.264 / AVC / MPEG-4 AVC / MPEG-4 part 10","profile":"Main","codec_type":"video","codec_time_base":"1/60","codec_tag_string":"avc1","codec_tag":"0x31637661","width":1280,"height":720,"coded_width":1280,"coded_height":720,"has_b_frames":2,"sample_aspect_ratio":"1:1","display_aspect_ratio":"16:9","pix_fmt":"yuv420p","level":31,"chroma_location":"left","refs":4,"is_avc":"1","nal_length_size":"4","r_frame_rate":"30/1","avg_frame_rate":"30/1","time_base":"1/15360","start_pts":0,"start_time":"0.000000","duration_ts":1002496,"duration":"65.266667","bit_rate":"1110373","bits_per_raw_sample":"8","nb_frames":"1958","disposition":{"default":1,"dub":0,"original":0,"comment":0,"lyrics":0,"karaoke":0,"forced":0,"hearing_impaired":0,"visual_impaired":0,"clean_effects":0,"attached_pic":0},"tags":{"language":"und","handler_name":"VideoHandler"}},{"index":1,"codec_name":"aac","codec_long_name":"AAC (Advanced Audio Coding)","profile":"LC","codec_type":"audio","codec_time_base":"1/44100","codec_tag_string":"mp4a","codec_tag":"0x6134706d","sample_fmt":"fltp","sample_rate":"44100","channels":2,"channel_layout":"stereo","bits_per_sample":0,"r_frame_rate":"0/0","avg_frame_rate":"0/0","time_base":"1/44100","start_pts":-2048,"start_time":"-0.046440","duration_ts":2883584,"duration":"65.387392","bit_rate":"128000","max_bit_rate":"128000","nb_frames":"2816","disposition":{"default":1,"dub":0,"original":0,"comment":0,"lyrics":0,"karaoke":0,"forced":0,"hearing_impaired":0,"visual_impaired":0,"clean_effects":0,"attached_pic":0},"tags":{"language":"und","handler_name":"SoundHandler"}}],"format":{"filename":"..\\resources\\video\\uploads\\20220708\\1657250853705_xe152pwl5bwdaoa-1.QuickExportasPNG.mp4","nb_streams":2,"nb_programs":0,"format_name":"mov,mp4,m4a,3gp,3g2,mj2","format_long_name":"QuickTime / MOV","start_time":"-0.046440","duration":"65.388000","size":"10176662","bit_rate":"1245080","probe_score":100,"tags":{"major_brand":"isom","minor_version":"512","compatible_brands":"isomiso2avc1mp41","encoder":"Lavf58.12.100"}}}', 'BGetting', NULL, NULL),(215, 69, '..\resources\video\uploads\20220708\1657250871175_xe152pwl5bwdo5j-2.ExportAs.mp4', 't', '2022-07-08 10:27:55.384+07', '2022-07-08 10:27:55.384+07', '69', NULL, NULL, '2. Export As', '1657250871175_xe152pwl5bwdo5j-2.ExportAs.mp4', '20220708', '2. Export As.mp4', '32345121', 'video/mp4', '7bit', '4:54', '{"streams":[{"index":0,"codec_name":"h264","codec_long_name":"H.264 / AVC / MPEG-4 AVC / MPEG-4 part 10","profile":"Main","codec_type":"video","codec_time_base":"1/60","codec_tag_string":"avc1","codec_tag":"0x31637661","width":1280,"height":720,"coded_width":1280,"coded_height":720,"has_b_frames":2,"sample_aspect_ratio":"1:1","display_aspect_ratio":"16:9","pix_fmt":"yuv420p","level":31,"chroma_location":"left","refs":4,"is_avc":"1","nal_length_size":"4","r_frame_rate":"30/1","avg_frame_rate":"30/1","time_base":"1/15360","start_pts":0,"start_time":"0.000000","duration_ts":4530688,"duration":"294.966667","bit_rate":"740567","bits_per_raw_sample":"8","nb_frames":"8849","disposition":{"default":1,"dub":0,"original":0,"comment":0,"lyrics":0,"karaoke":0,"forced":0,"hearing_impaired":0,"visual_impaired":0,"clean_effects":0,"attached_pic":0},"tags":{"language":"und","handler_name":"VideoHandler"}},{"index":1,"codec_name":"aac","codec_long_name":"AAC (Advanced Audio Coding)","profile":"LC","codec_type":"audio","codec_time_base":"1/44100","codec_tag_string":"mp4a","codec_tag":"0x6134706d","sample_fmt":"fltp","sample_rate":"44100","channels":2,"channel_layout":"stereo","bits_per_sample":0,"r_frame_rate":"0/0","avg_frame_rate":"0/0","time_base":"1/44100","start_pts":-2048,"start_time":"-0.046440","duration_ts":13008896,"duration":"294.986304","bit_rate":"128000","max_bit_rate":"128000","nb_frames":"12704","disposition":{"default":1,"dub":0,"original":0,"comment":0,"lyrics":0,"karaoke":0,"forced":0,"hearing_impaired":0,"visual_impaired":0,"clean_effects":0,"attached_pic":0},"tags":{"language":"und","handler_name":"SoundHandler"}}],"format":{"filename":"..\\resources\\video\\uploads\\20220708\\1657250871175_xe152pwl5bwdo5j-2.ExportAs.mp4","nb_streams":2,"nb_programs":0,"format_name":"mov,mp4,m4a,3gp,3g2,mj2","format_long_name":"QuickTime / MOV","start_time":"-0.046440","duration":"294.987000","size":"32345121","bit_rate":"877194","probe_score":100,"tags":{"major_brand":"isom","minor_version":"512","compatible_brands":"isomiso2avc1mp41","encoder":"Lavf58.12.100"}}}', 'BGetting', NULL, NULL),(216, 69, '..\resources\video\uploads\20220708\1657250887606_xe152pwl5bwe0ty-3.ExportPreferences.mp4', 't', '2022-07-08 10:28:08.179+07', '2022-07-08 10:28:08.179+07', '69', NULL, NULL, '3. Export Preferences', '1657250887606_xe152pwl5bwe0ty-3.ExportPreferences.mp4', '20220708', '3. Export Preferences.mp4', '3540315', 'video/mp4', '7bit', '39s', '{"streams":[{"index":0,"codec_name":"h264","codec_long_name":"H.264 / AVC / MPEG-4 AVC / MPEG-4 part 10","profile":"Main","codec_type":"video","codec_time_base":"1/60","codec_tag_string":"avc1","codec_tag":"0x31637661","width":1280,"height":720,"coded_width":1280,"coded_height":720,"has_b_frames":2,"sample_aspect_ratio":"1:1","display_aspect_ratio":"16:9","pix_fmt":"yuv420p","level":31,"chroma_location":"left","refs":4,"is_avc":"1","nal_length_size":"4","r_frame_rate":"30/1","avg_frame_rate":"30/1","time_base":"1/15360","start_pts":0,"start_time":"0.000000","duration_ts":604672,"duration":"39.366667","bit_rate":"582392","bits_per_raw_sample":"8","nb_frames":"1181","disposition":{"default":1,"dub":0,"original":0,"comment":0,"lyrics":0,"karaoke":0,"forced":0,"hearing_impaired":0,"visual_impaired":0,"clean_effects":0,"attached_pic":0},"tags":{"language":"und","handler_name":"VideoHandler"}},{"index":1,"codec_name":"aac","codec_long_name":"AAC (Advanced Audio Coding)","profile":"LC","codec_type":"audio","codec_time_base":"1/44100","codec_tag_string":"mp4a","codec_tag":"0x6134706d","sample_fmt":"fltp","sample_rate":"44100","channels":2,"channel_layout":"stereo","bits_per_sample":0,"r_frame_rate":"0/0","avg_frame_rate":"0/0","time_base":"1/44100","start_pts":-2048,"start_time":"-0.046440","duration_ts":1736704,"duration":"39.381043","bit_rate":"128103","max_bit_rate":"128103","nb_frames":"1696","disposition":{"default":1,"dub":0,"original":0,"comment":0,"lyrics":0,"karaoke":0,"forced":0,"hearing_impaired":0,"visual_impaired":0,"clean_effects":0,"attached_pic":0},"tags":{"language":"und","handler_name":"SoundHandler"}}],"format":{"filename":"..\\resources\\video\\uploads\\20220708\\1657250887606_xe152pwl5bwe0ty-3.ExportPreferences.mp4","nb_streams":2,"nb_programs":0,"format_name":"mov,mp4,m4a,3gp,3g2,mj2","format_long_name":"QuickTime / MOV","start_time":"-0.046440","duration":"39.382000","size":"3540315","bit_rate":"719174","probe_score":100,"tags":{"major_brand":"isom","minor_version":"512","compatible_brands":"isomiso2avc1mp41","encoder":"Lavf58.12.100"}}}', 'BGetting', NULL, NULL),(217, 69, '..\resources\video\uploads\20220708\1657250898648_xe152pwl5bwe9co-4.SaveforWeb(Legacy).mp4', 't', '2022-07-08 10:28:24.193+07', '2022-07-08 10:28:24.193+07', '69', NULL, NULL, '4. Save for Web (Legacy)', '1657250898648_xe152pwl5bwe9co-4.SaveforWeb(Legacy).mp4', '20220708', '4. Save for Web (Legacy).mp4', '49443023', 'video/mp4', '7bit', '6:53', '{"streams":[{"index":0,"codec_name":"h264","codec_long_name":"H.264 / AVC / MPEG-4 AVC / MPEG-4 part 10","profile":"Main","codec_type":"video","codec_time_base":"1/60","codec_tag_string":"avc1","codec_tag":"0x31637661","width":1280,"height":720,"coded_width":1280,"coded_height":720,"has_b_frames":2,"sample_aspect_ratio":"1:1","display_aspect_ratio":"16:9","pix_fmt":"yuv420p","level":31,"chroma_location":"left","refs":4,"is_avc":"1","nal_length_size":"4","r_frame_rate":"30/1","avg_frame_rate":"30/1","time_base":"1/15360","start_pts":0,"start_time":"0.000000","duration_ts":6358528,"duration":"413.966667","bit_rate":"818839","bits_per_raw_sample":"8","nb_frames":"12419","disposition":{"default":1,"dub":0,"original":0,"comment":0,"lyrics":0,"karaoke":0,"forced":0,"hearing_impaired":0,"visual_impaired":0,"clean_effects":0,"attached_pic":0},"tags":{"language":"und","handler_name":"VideoHandler"}},{"index":1,"codec_name":"aac","codec_long_name":"AAC (Advanced Audio Coding)","profile":"LC","codec_type":"audio","codec_time_base":"1/44100","codec_tag_string":"mp4a","codec_tag":"0x6134706d","sample_fmt":"fltp","sample_rate":"44100","channels":2,"channel_layout":"stereo","bits_per_sample":0,"r_frame_rate":"0/0","avg_frame_rate":"0/0","time_base":"1/44100","start_pts":-2048,"start_time":"-0.046440","duration_ts":18256896,"duration":"413.988571","bit_rate":"128000","max_bit_rate":"128000","nb_frames":"17829","disposition":{"default":1,"dub":0,"original":0,"comment":0,"lyrics":0,"karaoke":0,"forced":0,"hearing_impaired":0,"visual_impaired":0,"clean_effects":0,"attached_pic":0},"tags":{"language":"und","handler_name":"SoundHandler"}}],"format":{"filename":"..\\resources\\video\\uploads\\20220708\\1657250898648_xe152pwl5bwe9co-4.SaveforWeb(Legacy).mp4","nb_streams":2,"nb_programs":0,"format_name":"mov,mp4,m4a,3gp,3g2,mj2","format_long_name":"QuickTime / MOV","start_time":"-0.046440","duration":"413.989000","size":"49443023","bit_rate":"955446","probe_score":100,"tags":{"major_brand":"isom","minor_version":"512","compatible_brands":"isomiso2avc1mp41","encoder":"Lavf58.12.100"}}}', 'BGetting', NULL, NULL),(220, 69, '..\resources\video\uploads\20220708\1657250932821_xe152pwl5bwezpx-7.LayerCompstoFiles.mp4', 't', '2022-07-08 10:28:54.152+07', '2022-07-08 10:28:54.152+07', '69', NULL, NULL, '7. Layer Comps to Files', '1657250932821_xe152pwl5bwezpx-7.LayerCompstoFiles.mp4', '20220708', '7. Layer Comps to Files.mp4', '5442155', 'video/mp4', '7bit', '56s', '{"streams":[{"index":0,"codec_name":"h264","codec_long_name":"H.264 / AVC / MPEG-4 AVC / MPEG-4 part 10","profile":"Main","codec_type":"video","codec_time_base":"1/60","codec_tag_string":"avc1","codec_tag":"0x31637661","width":1280,"height":720,"coded_width":1280,"coded_height":720,"has_b_frames":2,"sample_aspect_ratio":"1:1","display_aspect_ratio":"16:9","pix_fmt":"yuv420p","level":31,"chroma_location":"left","refs":4,"is_avc":"1","nal_length_size":"4","r_frame_rate":"30/1","avg_frame_rate":"30/1","time_base":"1/15360","start_pts":0,"start_time":"0.000000","duration_ts":868864,"duration":"56.566667","bit_rate":"632556","bits_per_raw_sample":"8","nb_frames":"1697","disposition":{"default":1,"dub":0,"original":0,"comment":0,"lyrics":0,"karaoke":0,"forced":0,"hearing_impaired":0,"visual_impaired":0,"clean_effects":0,"attached_pic":0},"tags":{"language":"und","handler_name":"VideoHandler"}},{"index":1,"codec_name":"aac","codec_long_name":"AAC (Advanced Audio Coding)","profile":"LC","codec_type":"audio","codec_time_base":"1/44100","codec_tag_string":"mp4a","codec_tag":"0x6134706d","sample_fmt":"fltp","sample_rate":"44100","channels":2,"channel_layout":"stereo","bits_per_sample":0,"r_frame_rate":"0/0","avg_frame_rate":"0/0","time_base":"1/44100","start_pts":-2048,"start_time":"-0.046440","duration_ts":2499584,"duration":"56.679909","bit_rate":"128000","max_bit_rate":"128000","nb_frames":"2441","disposition":{"default":1,"dub":0,"original":0,"comment":0,"lyrics":0,"karaoke":0,"forced":0,"hearing_impaired":0,"visual_impaired":0,"clean_effects":0,"attached_pic":0},"tags":{"language":"und","handler_name":"SoundHandler"}}],"format":{"filename":"..\\resources\\video\\uploads\\20220708\\1657250932821_xe152pwl5bwezpx-7.LayerCompstoFiles.mp4","nb_streams":2,"nb_programs":0,"format_name":"mov,mp4,m4a,3gp,3g2,mj2","format_long_name":"QuickTime / MOV","start_time":"-0.046440","duration":"56.680000","size":"5442155","bit_rate":"768123","probe_score":100,"tags":{"major_brand":"isom","minor_version":"512","compatible_brands":"isomiso2avc1mp41","encoder":"Lavf58.12.100"}}}', 'BGetting', NULL, NULL),(226, 69, '..\resources\video\uploads\20220708\1657250998604_xe152pwl5bwgeh8-13.RenderVideo.mp4', 't', '2022-07-08 10:29:59.849+07', '2022-07-08 10:29:59.849+07', '69', NULL, NULL, '13. Render Video', '1657250998604_xe152pwl5bwgeh8-13.RenderVideo.mp4', '20220708', '13. Render Video.mp4', '7154466', 'video/mp4', '7bit', '57s', '{"streams":[{"index":0,"codec_name":"h264","codec_long_name":"H.264 / AVC / MPEG-4 AVC / MPEG-4 part 10","profile":"Main","codec_type":"video","codec_time_base":"1/60","codec_tag_string":"avc1","codec_tag":"0x31637661","width":1280,"height":720,"coded_width":1280,"coded_height":720,"has_b_frames":2,"sample_aspect_ratio":"1:1","display_aspect_ratio":"16:9","pix_fmt":"yuv420p","level":31,"chroma_location":"left","refs":4,"is_avc":"1","nal_length_size":"4","r_frame_rate":"30/1","avg_frame_rate":"30/1","time_base":"1/15360","start_pts":0,"start_time":"0.000000","duration_ts":882688,"duration":"57.466667","bit_rate":"858895","bits_per_raw_sample":"8","nb_frames":"1724","disposition":{"default":1,"dub":0,"original":0,"comment":0,"lyrics":0,"karaoke":0,"forced":0,"hearing_impaired":0,"visual_impaired":0,"clean_effects":0,"attached_pic":0},"tags":{"language":"und","handler_name":"VideoHandler"}},{"index":1,"codec_name":"aac","codec_long_name":"AAC (Advanced Audio Coding)","profile":"LC","codec_type":"audio","codec_time_base":"1/44100","codec_tag_string":"mp4a","codec_tag":"0x6134706d","sample_fmt":"fltp","sample_rate":"44100","channels":2,"channel_layout":"stereo","bits_per_sample":0,"r_frame_rate":"0/0","avg_frame_rate":"0/0","time_base":"1/44100","start_pts":-2048,"start_time":"-0.046440","duration_ts":2539520,"duration":"57.585488","bit_rate":"128000","max_bit_rate":"128000","nb_frames":"2480","disposition":{"default":1,"dub":0,"original":0,"comment":0,"lyrics":0,"karaoke":0,"forced":0,"hearing_impaired":0,"visual_impaired":0,"clean_effects":0,"attached_pic":0},"tags":{"language":"und","handler_name":"SoundHandler"}}],"format":{"filename":"..\\resources\\video\\uploads\\20220708\\1657250998604_xe152pwl5bwgeh8-13.RenderVideo.mp4","nb_streams":2,"nb_programs":0,"format_name":"mov,mp4,m4a,3gp,3g2,mj2","format_long_name":"QuickTime / MOV","start_time":"-0.046440","duration":"57.586000","size":"7154466","bit_rate":"993917","probe_score":100,"tags":{"major_brand":"isom","minor_version":"512","compatible_brands":"isomiso2avc1mp41","encoder":"Lavf58.12.100"}}}', 'BGetting', NULL, NULL),(218, 69, '..\resources\video\uploads\20220708\1657250911045_xe152pwl5bweix1-5.ArtboardstoFiles.mp4', 't', '2022-07-08 10:28:32.234+07', '2022-07-08 10:28:32.234+07', '69', NULL, NULL, '5. Artboards to Files', '1657250911045_xe152pwl5bweix1-5.ArtboardstoFiles.mp4', '20220708', '5. Artboards to Files.mp4', '8446313', 'video/mp4', '7bit', '1:20', '{"streams":[{"index":0,"codec_name":"h264","codec_long_name":"H.264 / AVC / MPEG-4 AVC / MPEG-4 part 10","profile":"Main","codec_type":"video","codec_time_base":"1/60","codec_tag_string":"avc1","codec_tag":"0x31637661","width":1280,"height":720,"coded_width":1280,"coded_height":720,"has_b_frames":2,"sample_aspect_ratio":"1:1","display_aspect_ratio":"16:9","pix_fmt":"yuv420p","level":31,"chroma_location":"left","refs":4,"is_avc":"1","nal_length_size":"4","r_frame_rate":"30/1","avg_frame_rate":"30/1","time_base":"1/15360","start_pts":0,"start_time":"0.000000","duration_ts":1228288,"duration":"79.966667","bit_rate":"708040","bits_per_raw_sample":"8","nb_frames":"2399","disposition":{"default":1,"dub":0,"original":0,"comment":0,"lyrics":0,"karaoke":0,"forced":0,"hearing_impaired":0,"visual_impaired":0,"clean_effects":0,"attached_pic":0},"tags":{"language":"und","handler_name":"VideoHandler"}},{"index":1,"codec_name":"aac","codec_long_name":"AAC (Advanced Audio Coding)","profile":"LC","codec_type":"audio","codec_time_base":"1/44100","codec_tag_string":"mp4a","codec_tag":"0x6134706d","sample_fmt":"fltp","sample_rate":"44100","channels":2,"channel_layout":"stereo","bits_per_sample":0,"r_frame_rate":"0/0","avg_frame_rate":"0/0","time_base":"1/44100","start_pts":-2048,"start_time":"-0.046440","duration_ts":3531776,"duration":"80.085624","bit_rate":"128000","max_bit_rate":"128000","nb_frames":"3449","disposition":{"default":1,"dub":0,"original":0,"comment":0,"lyrics":0,"karaoke":0,"forced":0,"hearing_impaired":0,"visual_impaired":0,"clean_effects":0,"attached_pic":0},"tags":{"language":"und","handler_name":"SoundHandler"}}],"format":{"filename":"..\\resources\\video\\uploads\\20220708\\1657250911045_xe152pwl5bweix1-5.ArtboardstoFiles.mp4","nb_streams":2,"nb_programs":0,"format_name":"mov,mp4,m4a,3gp,3g2,mj2","format_long_name":"QuickTime / MOV","start_time":"-0.046440","duration":"80.086000","size":"8446313","bit_rate":"843724","probe_score":100,"tags":{"major_brand":"isom","minor_version":"512","compatible_brands":"isomiso2avc1mp41","encoder":"Lavf58.12.100"}}}', 'BGetting', NULL, NULL),(221, 69, '..\resources\video\uploads\20220708\1657250944916_xe152pwl5bwf91w-8.LayerCompstoPDF.mp4', 't', '2022-07-08 10:29:06.444+07', '2022-07-08 10:29:06.444+07', '69', NULL, NULL, '8. Layer Comps to PDF', '1657250944916_xe152pwl5bwf91w-8.LayerCompstoPDF.mp4', '20220708', '8. Layer Comps to PDF.mp4', '8247832', 'video/mp4', '7bit', '1:10', '{"streams":[{"index":0,"codec_name":"h264","codec_long_name":"H.264 / AVC / MPEG-4 AVC / MPEG-4 part 10","profile":"Main","codec_type":"video","codec_time_base":"1/60","codec_tag_string":"avc1","codec_tag":"0x31637661","width":1280,"height":720,"coded_width":1280,"coded_height":720,"has_b_frames":2,"sample_aspect_ratio":"1:1","display_aspect_ratio":"16:9","pix_fmt":"yuv420p","level":31,"chroma_location":"left","refs":4,"is_avc":"1","nal_length_size":"4","r_frame_rate":"30/1","avg_frame_rate":"30/1","time_base":"1/15360","start_pts":0,"start_time":"0.000000","duration_ts":1076224,"duration":"70.066667","bit_rate":"804696","bits_per_raw_sample":"8","nb_frames":"2102","disposition":{"default":1,"dub":0,"original":0,"comment":0,"lyrics":0,"karaoke":0,"forced":0,"hearing_impaired":0,"visual_impaired":0,"clean_effects":0,"attached_pic":0},"tags":{"language":"und","handler_name":"VideoHandler"}},{"index":1,"codec_name":"aac","codec_long_name":"AAC (Advanced Audio Coding)","profile":"LC","codec_type":"audio","codec_time_base":"1/44100","codec_tag_string":"mp4a","codec_tag":"0x6134706d","sample_fmt":"fltp","sample_rate":"44100","channels":2,"channel_layout":"stereo","bits_per_sample":0,"r_frame_rate":"0/0","avg_frame_rate":"0/0","time_base":"1/44100","start_pts":-2048,"start_time":"-0.046440","duration_ts":3095552,"duration":"70.193923","bit_rate":"128000","max_bit_rate":"128000","nb_frames":"3023","disposition":{"default":1,"dub":0,"original":0,"comment":0,"lyrics":0,"karaoke":0,"forced":0,"hearing_impaired":0,"visual_impaired":0,"clean_effects":0,"attached_pic":0},"tags":{"language":"und","handler_name":"SoundHandler"}}],"format":{"filename":"..\\resources\\video\\uploads\\20220708\\1657250944916_xe152pwl5bwf91w-8.LayerCompstoPDF.mp4","nb_streams":2,"nb_programs":0,"format_name":"mov,mp4,m4a,3gp,3g2,mj2","format_long_name":"QuickTime / MOV","start_time":"-0.046440","duration":"70.194000","size":"8247832","bit_rate":"940004","probe_score":100,"tags":{"major_brand":"isom","minor_version":"512","compatible_brands":"isomiso2avc1mp41","encoder":"Lavf58.12.100"}}}', 'BGetting', NULL, NULL),(223, 69, '..\resources\video\uploads\20220708\1657250963369_xe152pwl5bwfnah-10.ColorLookupTables.mp4', 't', '2022-07-08 10:29:23.817+07', '2022-07-08 10:29:23.817+07', '69', NULL, NULL, '10. Color Lookup Tables', '1657250963369_xe152pwl5bwfnah-10.ColorLookupTables.mp4', '20220708', '10. Color Lookup Tables.mp4', '2396800', 'video/mp4', '7bit', '25s', '{"streams":[{"index":0,"codec_name":"h264","codec_long_name":"H.264 / AVC / MPEG-4 AVC / MPEG-4 part 10","profile":"Main","codec_type":"video","codec_time_base":"1/60","codec_tag_string":"avc1","codec_tag":"0x31637661","width":1280,"height":720,"coded_width":1280,"coded_height":720,"has_b_frames":2,"sample_aspect_ratio":"1:1","display_aspect_ratio":"16:9","pix_fmt":"yuv420p","level":31,"chroma_location":"left","refs":4,"is_avc":"1","nal_length_size":"4","r_frame_rate":"30/1","avg_frame_rate":"30/1","time_base":"1/15360","start_pts":0,"start_time":"0.000000","duration_ts":385024,"duration":"25.066667","bit_rate":"624538","bits_per_raw_sample":"8","nb_frames":"752","disposition":{"default":1,"dub":0,"original":0,"comment":0,"lyrics":0,"karaoke":0,"forced":0,"hearing_impaired":0,"visual_impaired":0,"clean_effects":0,"attached_pic":0},"tags":{"language":"und","handler_name":"VideoHandler"}},{"index":1,"codec_name":"aac","codec_long_name":"AAC (Advanced Audio Coding)","profile":"LC","codec_type":"audio","codec_time_base":"1/44100","codec_tag_string":"mp4a","codec_tag":"0x6134706d","sample_fmt":"fltp","sample_rate":"44100","channels":2,"channel_layout":"stereo","bits_per_sample":0,"r_frame_rate":"0/0","avg_frame_rate":"0/0","time_base":"1/44100","start_pts":-2048,"start_time":"-0.046440","duration_ts":1105920,"duration":"25.077551","bit_rate":"128000","max_bit_rate":"128000","nb_frames":"1080","disposition":{"default":1,"dub":0,"original":0,"comment":0,"lyrics":0,"karaoke":0,"forced":0,"hearing_impaired":0,"visual_impaired":0,"clean_effects":0,"attached_pic":0},"tags":{"language":"und","handler_name":"SoundHandler"}}],"format":{"filename":"..\\resources\\video\\uploads\\20220708\\1657250963369_xe152pwl5bwfnah-10.ColorLookupTables.mp4","nb_streams":2,"nb_programs":0,"format_name":"mov,mp4,m4a,3gp,3g2,mj2","format_long_name":"QuickTime / MOV","start_time":"-0.046440","duration":"25.078000","size":"2396800","bit_rate":"764590","probe_score":100,"tags":{"major_brand":"isom","minor_version":"512","compatible_brands":"isomiso2avc1mp41","encoder":"Lavf58.12.100"}}}', 'BGetting', NULL, NULL),(225, 69, '..\resources\video\uploads\20220708\1657250987509_xe152pwl5bwg5x1-12.PathstoIllustrator.mp4', 't', '2022-07-08 10:29:48.728+07', '2022-07-08 10:29:48.728+07', '69', NULL, NULL, '12. Paths to Illustrator', '1657250987509_xe152pwl5bwg5x1-12.PathstoIllustrator.mp4', '20220708', '12. Paths to Illustrator.mp4', '6130021', 'video/mp4', '7bit', '49s', '{"streams":[{"index":0,"codec_name":"h264","codec_long_name":"H.264 / AVC / MPEG-4 AVC / MPEG-4 part 10","profile":"Main","codec_type":"video","codec_time_base":"1/60","codec_tag_string":"avc1","codec_tag":"0x31637661","width":1280,"height":720,"coded_width":1280,"coded_height":720,"has_b_frames":2,"sample_aspect_ratio":"1:1","display_aspect_ratio":"16:9","pix_fmt":"yuv420p","level":31,"chroma_location":"left","refs":4,"is_avc":"1","nal_length_size":"4","r_frame_rate":"30/1","avg_frame_rate":"30/1","time_base":"1/15360","start_pts":0,"start_time":"0.000000","duration_ts":764416,"duration":"49.766667","bit_rate":"848199","bits_per_raw_sample":"8","nb_frames":"1493","disposition":{"default":1,"dub":0,"original":0,"comment":0,"lyrics":0,"karaoke":0,"forced":0,"hearing_impaired":0,"visual_impaired":0,"clean_effects":0,"attached_pic":0},"tags":{"language":"und","handler_name":"VideoHandler"}},{"index":1,"codec_name":"aac","codec_long_name":"AAC (Advanced Audio Coding)","profile":"LC","codec_type":"audio","codec_time_base":"1/44100","codec_tag_string":"mp4a","codec_tag":"0x6134706d","sample_fmt":"fltp","sample_rate":"44100","channels":2,"channel_layout":"stereo","bits_per_sample":0,"r_frame_rate":"0/0","avg_frame_rate":"0/0","time_base":"1/44100","start_pts":-2048,"start_time":"-0.046440","duration_ts":2200576,"duration":"49.899683","bit_rate":"128000","max_bit_rate":"128000","nb_frames":"2149","disposition":{"default":1,"dub":0,"original":0,"comment":0,"lyrics":0,"karaoke":0,"forced":0,"hearing_impaired":0,"visual_impaired":0,"clean_effects":0,"attached_pic":0},"tags":{"language":"und","handler_name":"SoundHandler"}}],"format":{"filename":"..\\resources\\video\\uploads\\20220708\\1657250987509_xe152pwl5bwg5x1-12.PathstoIllustrator.mp4","nb_streams":2,"nb_programs":0,"format_name":"mov,mp4,m4a,3gp,3g2,mj2","format_long_name":"QuickTime / MOV","start_time":"-0.046440","duration":"49.900000","size":"6130021","bit_rate":"982768","probe_score":100,"tags":{"major_brand":"isom","minor_version":"512","compatible_brands":"isomiso2avc1mp41","encoder":"Lavf58.12.100"}}}', 'BGetting', NULL, NULL),(227, 69, '..\resources\video\uploads\20220708\1657251008415_xe152pwl5bwgm1r-14.Zoomify.mp4', 't', '2022-07-08 10:30:09.488+07', '2022-07-08 10:30:09.488+07', '69', NULL, NULL, '14. Zoomify', '1657251008415_xe152pwl5bwgm1r-14.Zoomify.mp4', '20220708', '14. Zoomify.mp4', '5392692', 'video/mp4', '7bit', '52s', '{"streams":[{"index":0,"codec_name":"h264","codec_long_name":"H.264 / AVC / MPEG-4 AVC / MPEG-4 part 10","profile":"Main","codec_type":"video","codec_time_base":"1/60","codec_tag_string":"avc1","codec_tag":"0x31637661","width":1280,"height":720,"coded_width":1280,"coded_height":720,"has_b_frames":2,"sample_aspect_ratio":"1:1","display_aspect_ratio":"16:9","pix_fmt":"yuv420p","level":31,"chroma_location":"left","refs":4,"is_avc":"1","nal_length_size":"4","r_frame_rate":"30/1","avg_frame_rate":"30/1","time_base":"1/15360","start_pts":0,"start_time":"0.000000","duration_ts":798208,"duration":"51.966667","bit_rate":"691494","bits_per_raw_sample":"8","nb_frames":"1559","disposition":{"default":1,"dub":0,"original":0,"comment":0,"lyrics":0,"karaoke":0,"forced":0,"hearing_impaired":0,"visual_impaired":0,"clean_effects":0,"attached_pic":0},"tags":{"language":"und","handler_name":"VideoHandler"}},{"index":1,"codec_name":"aac","codec_long_name":"AAC (Advanced Audio Coding)","profile":"LC","codec_type":"audio","codec_time_base":"1/44100","codec_tag_string":"mp4a","codec_tag":"0x6134706d","sample_fmt":"fltp","sample_rate":"44100","channels":2,"channel_layout":"stereo","bits_per_sample":0,"r_frame_rate":"0/0","avg_frame_rate":"0/0","time_base":"1/44100","start_pts":-2048,"start_time":"-0.046440","duration_ts":2296832,"duration":"52.082358","bit_rate":"128000","max_bit_rate":"128000","nb_frames":"2243","disposition":{"default":1,"dub":0,"original":0,"comment":0,"lyrics":0,"karaoke":0,"forced":0,"hearing_impaired":0,"visual_impaired":0,"clean_effects":0,"attached_pic":0},"tags":{"language":"und","handler_name":"SoundHandler"}}],"format":{"filename":"..\\resources\\video\\uploads\\20220708\\1657251008415_xe152pwl5bwgm1r-14.Zoomify.mp4","nb_streams":2,"nb_programs":0,"format_name":"mov,mp4,m4a,3gp,3g2,mj2","format_long_name":"QuickTime / MOV","start_time":"-0.046440","duration":"52.083000","size":"5392692","bit_rate":"828322","probe_score":100,"tags":{"major_brand":"isom","minor_version":"512","compatible_brands":"isomiso2avc1mp41","encoder":"Lavf58.12.100"}}}', 'BGetting', NULL, NULL),(219, 69, '..\resources\video\uploads\20220708\1657250923028_xe152pwl5bwes5w-6.ArtboardstoPDF.mp4', 't', '2022-07-08 10:28:43.67+07', '2022-07-08 10:28:43.67+07', '69', NULL, NULL, '6. Artboards to PDF', '1657250923028_xe152pwl5bwes5w-6.ArtboardstoPDF.mp4', '20220708', '6. Artboards to PDF.mp4', '5049389', 'video/mp4', '7bit', '42s', '{"streams":[{"index":0,"codec_name":"h264","codec_long_name":"H.264 / AVC / MPEG-4 AVC / MPEG-4 part 10","profile":"Main","codec_type":"video","codec_time_base":"1/60","codec_tag_string":"avc1","codec_tag":"0x31637661","width":1280,"height":720,"coded_width":1280,"coded_height":720,"has_b_frames":2,"sample_aspect_ratio":"1:1","display_aspect_ratio":"16:9","pix_fmt":"yuv420p","level":31,"chroma_location":"left","refs":4,"is_avc":"1","nal_length_size":"4","r_frame_rate":"30/1","avg_frame_rate":"30/1","time_base":"1/15360","start_pts":0,"start_time":"0.000000","duration_ts":650752,"duration":"42.366667","bit_rate":"816511","bits_per_raw_sample":"8","nb_frames":"1271","disposition":{"default":1,"dub":0,"original":0,"comment":0,"lyrics":0,"karaoke":0,"forced":0,"hearing_impaired":0,"visual_impaired":0,"clean_effects":0,"attached_pic":0},"tags":{"language":"und","handler_name":"VideoHandler"}},{"index":1,"codec_name":"aac","codec_long_name":"AAC (Advanced Audio Coding)","profile":"LC","codec_type":"audio","codec_time_base":"1/44100","codec_tag_string":"mp4a","codec_tag":"0x6134706d","sample_fmt":"fltp","sample_rate":"44100","channels":2,"channel_layout":"stereo","bits_per_sample":0,"r_frame_rate":"0/0","avg_frame_rate":"0/0","time_base":"1/44100","start_pts":-2048,"start_time":"-0.046440","duration_ts":1869824,"duration":"42.399637","bit_rate":"128000","max_bit_rate":"128000","nb_frames":"1826","disposition":{"default":1,"dub":0,"original":0,"comment":0,"lyrics":0,"karaoke":0,"forced":0,"hearing_impaired":0,"visual_impaired":0,"clean_effects":0,"attached_pic":0},"tags":{"language":"und","handler_name":"SoundHandler"}}],"format":{"filename":"..\\resources\\video\\uploads\\20220708\\1657250923028_xe152pwl5bwes5w-6.ArtboardstoPDF.mp4","nb_streams":2,"nb_programs":0,"format_name":"mov,mp4,m4a,3gp,3g2,mj2","format_long_name":"QuickTime / MOV","start_time":"-0.046440","duration":"42.400000","size":"5049389","bit_rate":"952714","probe_score":100,"tags":{"major_brand":"isom","minor_version":"512","compatible_brands":"isomiso2avc1mp41","encoder":"Lavf58.12.100"}}}', 'BGetting', NULL, NULL),(222, 69, '..\resources\video\uploads\20220708\1657250954009_xe152pwl5bwfg2h-9.LayerstoFiles.mp4', 't', '2022-07-08 10:29:16.118+07', '2022-07-08 10:29:16.118+07', '69', NULL, NULL, '9. Layers to Files', '1657250954009_xe152pwl5bwfg2h-9.LayerstoFiles.mp4', '20220708', '9. Layers to Files.mp4', '12523586', 'video/mp4', '7bit', '1:37', '{"streams":[{"index":0,"codec_name":"h264","codec_long_name":"H.264 / AVC / MPEG-4 AVC / MPEG-4 part 10","profile":"Main","codec_type":"video","codec_time_base":"1/60","codec_tag_string":"avc1","codec_tag":"0x31637661","width":1280,"height":720,"coded_width":1280,"coded_height":720,"has_b_frames":2,"sample_aspect_ratio":"1:1","display_aspect_ratio":"16:9","pix_fmt":"yuv420p","level":31,"chroma_location":"left","refs":4,"is_avc":"1","nal_length_size":"4","r_frame_rate":"30/1","avg_frame_rate":"30/1","time_base":"1/15360","start_pts":0,"start_time":"0.000000","duration_ts":1497088,"duration":"97.466667","bit_rate":"891035","bits_per_raw_sample":"8","nb_frames":"2924","disposition":{"default":1,"dub":0,"original":0,"comment":0,"lyrics":0,"karaoke":0,"forced":0,"hearing_impaired":0,"visual_impaired":0,"clean_effects":0,"attached_pic":0},"tags":{"language":"und","handler_name":"VideoHandler"}},{"index":1,"codec_name":"aac","codec_long_name":"AAC (Advanced Audio Coding)","profile":"LC","codec_type":"audio","codec_time_base":"1/44100","codec_tag_string":"mp4a","codec_tag":"0x6134706d","sample_fmt":"fltp","sample_rate":"44100","channels":2,"channel_layout":"stereo","bits_per_sample":0,"r_frame_rate":"0/0","avg_frame_rate":"0/0","time_base":"1/44100","start_pts":-2048,"start_time":"-0.046440","duration_ts":4303872,"duration":"97.593469","bit_rate":"128000","max_bit_rate":"128000","nb_frames":"4203","disposition":{"default":1,"dub":0,"original":0,"comment":0,"lyrics":0,"karaoke":0,"forced":0,"hearing_impaired":0,"visual_impaired":0,"clean_effects":0,"attached_pic":0},"tags":{"language":"und","handler_name":"SoundHandler"}}],"format":{"filename":"..\\resources\\video\\uploads\\20220708\\1657250954009_xe152pwl5bwfg2h-9.LayerstoFiles.mp4","nb_streams":2,"nb_programs":0,"format_name":"mov,mp4,m4a,3gp,3g2,mj2","format_long_name":"QuickTime / MOV","start_time":"-0.046440","duration":"97.594000","size":"12523586","bit_rate":"1026586","probe_score":100,"tags":{"major_brand":"isom","minor_version":"512","compatible_brands":"isomiso2avc1mp41","encoder":"Lavf58.12.100"}}}', 'BGetting', NULL, NULL),(224, 69, '..\resources\video\uploads\20220708\1657250974759_xe152pwl5bwfw2v-11.DataSetsasFiles.mp4', 't', '2022-07-08 10:29:36.796+07', '2022-07-08 10:29:36.796+07', '69', NULL, NULL, '11. Data Sets as Files', '1657250974759_xe152pwl5bwfw2v-11.DataSetsasFiles.mp4', '20220708', '11. Data Sets as Files.mp4', '13166135', 'video/mp4', '7bit', '1:07', '{"streams":[{"index":0,"codec_name":"h264","codec_long_name":"H.264 / AVC / MPEG-4 AVC / MPEG-4 part 10","profile":"Main","codec_type":"video","codec_time_base":"1/60","codec_tag_string":"avc1","codec_tag":"0x31637661","width":1280,"height":720,"coded_width":1280,"coded_height":720,"has_b_frames":2,"sample_aspect_ratio":"1:1","display_aspect_ratio":"16:9","pix_fmt":"yuv420p","level":31,"chroma_location":"left","refs":4,"is_avc":"1","nal_length_size":"4","r_frame_rate":"30/1","avg_frame_rate":"30/1","time_base":"1/15360","start_pts":0,"start_time":"0.000000","duration_ts":1042432,"duration":"67.866667","bit_rate":"1414990","bits_per_raw_sample":"8","nb_frames":"2036","disposition":{"default":1,"dub":0,"original":0,"comment":0,"lyrics":0,"karaoke":0,"forced":0,"hearing_impaired":0,"visual_impaired":0,"clean_effects":0,"attached_pic":0},"tags":{"language":"und","handler_name":"VideoHandler"}},{"index":1,"codec_name":"aac","codec_long_name":"AAC (Advanced Audio Coding)","profile":"LC","codec_type":"audio","codec_time_base":"1/44100","codec_tag_string":"mp4a","codec_tag":"0x6134706d","sample_fmt":"fltp","sample_rate":"44100","channels":2,"channel_layout":"stereo","bits_per_sample":0,"r_frame_rate":"0/0","avg_frame_rate":"0/0","time_base":"1/44100","start_pts":-2048,"start_time":"-0.046440","duration_ts":2998272,"duration":"67.988027","bit_rate":"128000","max_bit_rate":"128000","nb_frames":"2928","disposition":{"default":1,"dub":0,"original":0,"comment":0,"lyrics":0,"karaoke":0,"forced":0,"hearing_impaired":0,"visual_impaired":0,"clean_effects":0,"attached_pic":0},"tags":{"language":"und","handler_name":"SoundHandler"}}],"format":{"filename":"..\\resources\\video\\uploads\\20220708\\1657250974759_xe152pwl5bwfw2v-11.DataSetsasFiles.mp4","nb_streams":2,"nb_programs":0,"format_name":"mov,mp4,m4a,3gp,3g2,mj2","format_long_name":"QuickTime / MOV","start_time":"-0.046440","duration":"67.989000","size":"13166135","bit_rate":"1549207","probe_score":100,"tags":{"major_brand":"isom","minor_version":"512","compatible_brands":"isomiso2avc1mp41","encoder":"Lavf58.12.100"}}}', 'BGetting', NULL, NULL);
COMMIT;
BEGIN;
LOCK TABLE "public"."videos_action" IN SHARE MODE;
DELETE FROM "public"."videos_action";
INSERT INTO "public"."videos_action" ("id","userid","likes","unlikes","status","created_at","updated_at","created_by","updated_by","videoid") VALUES (127, 69, 1, 0, 't', '2022-07-15 11:29:06.752+07', '2022-07-15 11:29:06.752+07', '69', NULL, 227),(129, 69, 1, 0, 't', '2022-07-15 11:34:48.27+07', '2022-07-15 11:34:48.27+07', '69', NULL, 213);
COMMIT;
BEGIN;
LOCK TABLE "public"."videos_channel" IN SHARE MODE;
DELETE FROM "public"."videos_channel";
COMMIT;
BEGIN;
LOCK TABLE "public"."videos_history" IN SHARE MODE;
DELETE FROM "public"."videos_history";
INSERT INTO "public"."videos_history" ("id","video_id","user_watch_id","created_at","updated_at") VALUES (564, 208, 69, '2022-07-07 13:19:01.257+07', '2022-07-07 13:19:01.257+07'),(565, 213, 69, '2022-07-07 13:19:53.399+07', '2022-07-07 13:19:53.399+07'),(566, 204, 69, '2022-07-07 14:53:45.355+07', '2022-07-07 14:53:45.355+07'),(567, 213, 69, '2022-07-07 16:34:39.392+07', '2022-07-07 16:34:39.392+07'),(568, 213, 69, '2022-07-07 16:38:49.095+07', '2022-07-07 16:38:49.095+07'),(569, 213, 69, '2022-07-07 16:45:34.331+07', '2022-07-07 16:45:34.331+07'),(570, 213, 69, '2022-07-07 16:45:42.419+07', '2022-07-07 16:45:42.419+07'),(571, 204, 69, '2022-07-07 16:48:31.599+07', '2022-07-07 16:48:31.599+07'),(572, 205, 69, '2022-07-07 16:48:43.948+07', '2022-07-07 16:48:43.948+07'),(573, 204, 69, '2022-07-08 10:17:40.51+07', '2022-07-08 10:17:40.51+07'),(574, 213, 69, '2022-07-08 10:17:53.289+07', '2022-07-08 10:17:53.289+07'),(575, 206, 69, '2022-07-08 11:00:31.124+07', '2022-07-08 11:00:31.124+07'),(576, 207, 69, '2022-07-08 11:10:32.369+07', '2022-07-08 11:10:32.369+07'),(577, 218, 69, '2022-07-08 13:06:22.597+07', '2022-07-08 13:06:22.597+07'),(578, 213, 69, '2022-07-08 13:44:36.489+07', '2022-07-08 13:44:36.489+07'),(579, 213, 69, '2022-07-08 14:35:47.442+07', '2022-07-08 14:35:47.442+07'),(580, 221, 69, '2022-07-08 14:53:23.815+07', '2022-07-08 14:53:23.815+07'),(1, 227, 75, '2022-08-12 11:54:13.53+07', '2022-08-12 11:54:13.53+07'),(2, 227, 69, '2022-08-12 14:34:37.467+07', '2022-08-12 14:34:37.467+07'),(3, 227, 69, '2022-08-12 14:35:50.891+07', '2022-08-12 14:35:50.891+07'),(4, 227, 69, '2022-08-12 14:37:19.746+07', '2022-08-12 14:37:19.746+07'),(5, 227, 69, '2022-08-12 14:39:16.156+07', '2022-08-12 14:39:16.156+07'),(6, 227, 69, '2022-08-16 16:24:50.251+07', '2022-08-16 16:24:50.251+07'),(7, 204, 69, '2022-08-16 16:25:11.798+07', '2022-08-16 16:25:11.798+07'),(8, 227, 69, '2022-08-16 16:26:16.331+07', '2022-08-16 16:26:16.331+07');
COMMIT;
BEGIN;
LOCK TABLE "public"."videos_playlist" IN SHARE MODE;
DELETE FROM "public"."videos_playlist";
INSERT INTO "public"."videos_playlist" ("id","user_id","title","description","created_at","updated_at") VALUES (43, 69, 'OBIEE 12C', NULL, '2022-07-07 13:18:17.545+07', '2022-07-07 13:18:17.545+07'),(1, 69, 'test playlist', NULL, '2022-07-15 11:32:37.582+07', '2022-07-15 11:32:37.582+07');
COMMIT;
BEGIN;
LOCK TABLE "public"."videos_playlist_add" IN SHARE MODE;
DELETE FROM "public"."videos_playlist_add";
INSERT INTO "public"."videos_playlist_add" ("id","playlist_id","video_id","order_by","created_at","updated_at") VALUES (62, 43, 213, NULL, '2022-07-07 13:18:17.553+07', '2022-07-07 13:18:17.553+07'),(63, 43, 212, NULL, '2022-07-07 13:18:17.553+07', '2022-07-07 13:18:17.553+07'),(64, 43, 211, NULL, '2022-07-07 13:18:17.553+07', '2022-07-07 13:18:17.553+07'),(65, 43, 210, NULL, '2022-07-07 13:18:17.553+07', '2022-07-07 13:18:17.553+07'),(66, 43, 209, NULL, '2022-07-07 13:18:17.553+07', '2022-07-07 13:18:17.553+07'),(67, 43, 208, NULL, '2022-07-07 13:18:17.553+07', '2022-07-07 13:18:17.553+07'),(68, 43, 207, NULL, '2022-07-07 13:18:17.553+07', '2022-07-07 13:18:17.553+07'),(69, 43, 206, NULL, '2022-07-07 13:18:17.553+07', '2022-07-07 13:18:17.553+07'),(70, 43, 205, NULL, '2022-07-07 13:18:17.553+07', '2022-07-07 13:18:17.553+07');
COMMIT;
BEGIN;
LOCK TABLE "public"."videos_tags" IN SHARE MODE;
DELETE FROM "public"."videos_tags";
INSERT INTO "public"."videos_tags" ("id","title_tags","video_id","created_by","created_at","updated_by","updated_at") VALUES (89, 'WSO2', 204, '69', '2022-07-07 13:06:49.975+07', NULL, '2022-07-07 13:06:49.975+07'),(90, 'Programe', 204, '69', '2022-07-07 13:06:49.975+07', NULL, '2022-07-07 13:06:49.975+07'),(91, 'API', 204, '69', '2022-07-07 13:06:49.975+07', NULL, '2022-07-07 13:06:49.975+07'),(92, 'OBIEE', 205, '69', '2022-07-07 13:11:15.45+07', NULL, '2022-07-07 13:11:15.45+07'),(93, 'Oracle', 205, '69', '2022-07-07 13:11:15.45+07', NULL, '2022-07-07 13:11:15.45+07'),(94, 'Report', 205, '69', '2022-07-07 13:11:15.45+07', NULL, '2022-07-07 13:11:15.45+07'),(95, 'OBIEE', 206, '69', '2022-07-07 13:12:10.549+07', NULL, '2022-07-07 13:12:10.549+07'),(96, 'Oracle', 206, '69', '2022-07-07 13:12:10.549+07', NULL, '2022-07-07 13:12:10.549+07'),(97, 'Report', 206, '69', '2022-07-07 13:12:10.549+07', NULL, '2022-07-07 13:12:10.549+07'),(98, 'OBIEE', 207, '69', '2022-07-07 13:12:51.039+07', NULL, '2022-07-07 13:12:51.039+07'),(99, 'Oracle', 207, '69', '2022-07-07 13:12:51.039+07', NULL, '2022-07-07 13:12:51.039+07'),(100, 'Report', 207, '69', '2022-07-07 13:12:51.039+07', NULL, '2022-07-07 13:12:51.039+07'),(101, 'OBIEE', 208, '69', '2022-07-07 13:13:32.445+07', NULL, '2022-07-07 13:13:32.445+07'),(102, 'Oracle', 208, '69', '2022-07-07 13:13:32.445+07', NULL, '2022-07-07 13:13:32.445+07'),(103, 'Report', 208, '69', '2022-07-07 13:13:32.445+07', NULL, '2022-07-07 13:13:32.445+07'),(104, 'OBIEE', 209, '69', '2022-07-07 13:14:06.76+07', NULL, '2022-07-07 13:14:06.76+07'),(105, 'Oracle', 209, '69', '2022-07-07 13:14:06.76+07', NULL, '2022-07-07 13:14:06.76+07'),(106, 'Report', 209, '69', '2022-07-07 13:14:06.76+07', NULL, '2022-07-07 13:14:06.76+07'),(108, 'Oracle', 210, '69', '2022-07-07 13:14:41.661+07', NULL, '2022-07-07 13:14:41.661+07'),(107, 'OBIEE', 210, '69', '2022-07-07 13:14:41.661+07', NULL, '2022-07-07 13:14:41.661+07'),(109, 'Report', 210, '69', '2022-07-07 13:14:41.661+07', NULL, '2022-07-07 13:14:41.661+07'),(110, 'OBIEE', 211, '69', '2022-07-07 13:15:13.065+07', NULL, '2022-07-07 13:15:13.065+07'),(111, 'Oracle', 211, '69', '2022-07-07 13:15:13.065+07', NULL, '2022-07-07 13:15:13.065+07'),(112, 'Report', 211, '69', '2022-07-07 13:15:13.065+07', NULL, '2022-07-07 13:15:13.065+07'),(114, 'Oracle', 212, '69', '2022-07-07 13:15:48.554+07', NULL, '2022-07-07 13:15:48.554+07'),(113, 'OBIEE', 212, '69', '2022-07-07 13:15:48.554+07', NULL, '2022-07-07 13:15:48.554+07'),(115, 'Report', 212, '69', '2022-07-07 13:15:48.554+07', NULL, '2022-07-07 13:15:48.554+07'),(116, 'OBIEE', 213, '69', '2022-07-07 13:16:15.284+07', NULL, '2022-07-07 13:16:15.284+07'),(117, 'Oracle', 213, '69', '2022-07-07 13:16:15.284+07', NULL, '2022-07-07 13:16:15.284+07'),(118, 'Report', 213, '69', '2022-07-07 13:16:15.284+07', NULL, '2022-07-07 13:16:15.284+07'),(119, 'Photoshop', 214, '69', '2022-07-08 10:27:38.213+07', NULL, '2022-07-08 10:27:38.213+07'),(120, 'Design', 214, '69', '2022-07-08 10:27:38.214+07', NULL, '2022-07-08 10:27:38.214+07'),(121, 'Image', 214, '69', '2022-07-08 10:27:38.214+07', NULL, '2022-07-08 10:27:38.214+07'),(122, 'Photoshop', 215, '69', '2022-07-08 10:27:55.402+07', NULL, '2022-07-08 10:27:55.402+07'),(123, 'Design', 215, '69', '2022-07-08 10:27:55.402+07', NULL, '2022-07-08 10:27:55.402+07'),(124, 'Image', 215, '69', '2022-07-08 10:27:55.402+07', NULL, '2022-07-08 10:27:55.402+07'),(125, 'Photoshop', 216, '69', '2022-07-08 10:28:08.197+07', NULL, '2022-07-08 10:28:08.197+07'),(126, 'Design', 216, '69', '2022-07-08 10:28:08.197+07', NULL, '2022-07-08 10:28:08.197+07'),(127, 'Image', 216, '69', '2022-07-08 10:28:08.197+07', NULL, '2022-07-08 10:28:08.197+07'),(128, 'Photoshop', 217, '69', '2022-07-08 10:28:24.21+07', NULL, '2022-07-08 10:28:24.21+07'),(129, 'Design', 217, '69', '2022-07-08 10:28:24.21+07', NULL, '2022-07-08 10:28:24.21+07'),(130, 'Image', 217, '69', '2022-07-08 10:28:24.21+07', NULL, '2022-07-08 10:28:24.21+07'),(131, 'Photoshop', 218, '69', '2022-07-08 10:28:32.251+07', NULL, '2022-07-08 10:28:32.251+07'),(132, 'Design', 218, '69', '2022-07-08 10:28:32.251+07', NULL, '2022-07-08 10:28:32.251+07'),(133, 'Image', 218, '69', '2022-07-08 10:28:32.251+07', NULL, '2022-07-08 10:28:32.251+07'),(134, 'Photoshop', 219, '69', '2022-07-08 10:28:43.686+07', NULL, '2022-07-08 10:28:43.686+07'),(135, 'Design', 219, '69', '2022-07-08 10:28:43.686+07', NULL, '2022-07-08 10:28:43.686+07'),(136, 'Image', 219, '69', '2022-07-08 10:28:43.686+07', NULL, '2022-07-08 10:28:43.686+07'),(137, 'Photoshop', 220, '69', '2022-07-08 10:28:54.17+07', NULL, '2022-07-08 10:28:54.17+07'),(138, 'Design', 220, '69', '2022-07-08 10:28:54.17+07', NULL, '2022-07-08 10:28:54.17+07'),(139, 'Image', 220, '69', '2022-07-08 10:28:54.17+07', NULL, '2022-07-08 10:28:54.17+07'),(140, 'Photoshop', 221, '69', '2022-07-08 10:29:06.461+07', NULL, '2022-07-08 10:29:06.461+07'),(141, 'Design', 221, '69', '2022-07-08 10:29:06.461+07', NULL, '2022-07-08 10:29:06.461+07'),(142, 'Image', 221, '69', '2022-07-08 10:29:06.461+07', NULL, '2022-07-08 10:29:06.461+07'),(143, 'Photoshop', 222, '69', '2022-07-08 10:29:16.134+07', NULL, '2022-07-08 10:29:16.134+07'),(144, 'Design', 222, '69', '2022-07-08 10:29:16.134+07', NULL, '2022-07-08 10:29:16.134+07'),(145, 'Image', 222, '69', '2022-07-08 10:29:16.134+07', NULL, '2022-07-08 10:29:16.134+07'),(146, 'Photoshop', 223, '69', '2022-07-08 10:29:23.833+07', NULL, '2022-07-08 10:29:23.833+07'),(147, 'Design', 223, '69', '2022-07-08 10:29:23.833+07', NULL, '2022-07-08 10:29:23.833+07'),(148, 'Image', 223, '69', '2022-07-08 10:29:23.833+07', NULL, '2022-07-08 10:29:23.833+07'),(149, 'Photoshop', 224, '69', '2022-07-08 10:29:36.812+07', NULL, '2022-07-08 10:29:36.812+07'),(150, 'Design', 224, '69', '2022-07-08 10:29:36.812+07', NULL, '2022-07-08 10:29:36.812+07'),(151, 'Image', 224, '69', '2022-07-08 10:29:36.812+07', NULL, '2022-07-08 10:29:36.812+07'),(152, 'Photoshop', 225, '69', '2022-07-08 10:29:48.746+07', NULL, '2022-07-08 10:29:48.746+07'),(153, 'Design', 225, '69', '2022-07-08 10:29:48.746+07', NULL, '2022-07-08 10:29:48.746+07'),(154, 'Image', 225, '69', '2022-07-08 10:29:48.746+07', NULL, '2022-07-08 10:29:48.746+07'),(155, 'Photoshop', 226, '69', '2022-07-08 10:29:59.865+07', NULL, '2022-07-08 10:29:59.865+07'),(156, 'Design', 226, '69', '2022-07-08 10:29:59.865+07', NULL, '2022-07-08 10:29:59.865+07'),(157, 'Image', 226, '69', '2022-07-08 10:29:59.865+07', NULL, '2022-07-08 10:29:59.865+07'),(158, 'Photoshop', 227, '69', '2022-07-08 10:30:09.505+07', NULL, '2022-07-08 10:30:09.505+07'),(159, 'Design', 227, '69', '2022-07-08 10:30:09.505+07', NULL, '2022-07-08 10:30:09.505+07'),(160, 'Image', 227, '69', '2022-07-08 10:30:09.505+07', NULL, '2022-07-08 10:30:09.505+07');
COMMIT;
BEGIN;
LOCK TABLE "public"."videos_views" IN SHARE MODE;
DELETE FROM "public"."videos_views";
INSERT INTO "public"."videos_views" ("id","video_id","views") VALUES (40, 208, 1),(43, 205, 1),(44, 206, 1),(45, 207, 1),(46, 218, 1),(47, 221, 2),(2, 226, 1),(3, 212, 1),(41, 213, 10),(4, 209, 2),(42, 204, 4),(1, 227, 12);
COMMIT;
BEGIN;
LOCK TABLE "public"."videos_watch_tags" IN SHARE MODE;
DELETE FROM "public"."videos_watch_tags";
INSERT INTO "public"."videos_watch_tags" ("id","title_tags","video_watch_id","user_watch_id","created_by","created_at","updated_by","updated_at") VALUES (3589, 'Oracle', 208, 69, '69', '2022-07-07 13:19:01.3+07', NULL, '2022-07-07 13:19:01.3+07'),(3588, 'Report', 208, 69, '69', '2022-07-07 13:19:01.3+07', NULL, '2022-07-07 13:19:01.3+07'),(3590, 'OBIEE', 208, 69, '69', '2022-07-07 13:19:01.3+07', NULL, '2022-07-07 13:19:01.3+07'),(3591, 'API', 204, 69, '69', '2022-07-07 14:53:45.274+07', NULL, '2022-07-07 14:53:45.274+07'),(3592, 'Programe', 204, 69, '69', '2022-07-07 14:53:45.274+07', NULL, '2022-07-07 14:53:45.274+07'),(3593, 'WSO2', 204, 69, '69', '2022-07-07 14:53:45.274+07', NULL, '2022-07-07 14:53:45.274+07'),(3594, 'Report', 213, 69, '69', '2022-07-07 16:34:39.325+07', NULL, '2022-07-07 16:34:39.325+07'),(3595, 'Oracle', 213, 69, '69', '2022-07-07 16:34:39.325+07', NULL, '2022-07-07 16:34:39.325+07'),(3596, 'OBIEE', 213, 69, '69', '2022-07-07 16:34:39.325+07', NULL, '2022-07-07 16:34:39.325+07'),(3597, 'Report', 213, 69, '69', '2022-07-07 16:38:49.087+07', NULL, '2022-07-07 16:38:49.087+07'),(3598, 'Oracle', 213, 69, '69', '2022-07-07 16:38:49.087+07', NULL, '2022-07-07 16:38:49.087+07'),(3599, 'OBIEE', 213, 69, '69', '2022-07-07 16:38:49.087+07', NULL, '2022-07-07 16:38:49.087+07'),(3600, 'Report', 213, 69, '69', '2022-07-07 16:39:00.361+07', NULL, '2022-07-07 16:39:00.361+07'),(3601, 'Oracle', 213, 69, '69', '2022-07-07 16:39:00.361+07', NULL, '2022-07-07 16:39:00.361+07'),(3602, 'OBIEE', 213, 69, '69', '2022-07-07 16:39:00.361+07', NULL, '2022-07-07 16:39:00.361+07'),(3603, 'Report', 213, 69, '69', '2022-07-07 16:39:01.835+07', NULL, '2022-07-07 16:39:01.835+07'),(3604, 'Oracle', 213, 69, '69', '2022-07-07 16:39:01.835+07', NULL, '2022-07-07 16:39:01.835+07'),(3605, 'OBIEE', 213, 69, '69', '2022-07-07 16:39:01.835+07', NULL, '2022-07-07 16:39:01.835+07'),(3606, 'Report', 213, 69, '69', '2022-07-07 16:39:04.204+07', NULL, '2022-07-07 16:39:04.204+07'),(3607, 'Oracle', 213, 69, '69', '2022-07-07 16:39:04.204+07', NULL, '2022-07-07 16:39:04.204+07'),(3608, 'OBIEE', 213, 69, '69', '2022-07-07 16:39:04.204+07', NULL, '2022-07-07 16:39:04.204+07'),(3609, 'Report', 213, 69, '69', '2022-07-07 16:39:05.921+07', NULL, '2022-07-07 16:39:05.921+07'),(3610, 'Oracle', 213, 69, '69', '2022-07-07 16:39:05.921+07', NULL, '2022-07-07 16:39:05.921+07'),(3611, 'OBIEE', 213, 69, '69', '2022-07-07 16:39:05.921+07', NULL, '2022-07-07 16:39:05.921+07'),(3612, 'Report', 213, 69, '69', '2022-07-07 16:39:19.851+07', NULL, '2022-07-07 16:39:19.851+07'),(3613, 'Oracle', 213, 69, '69', '2022-07-07 16:39:19.851+07', NULL, '2022-07-07 16:39:19.851+07'),(3614, 'OBIEE', 213, 69, '69', '2022-07-07 16:39:19.851+07', NULL, '2022-07-07 16:39:19.851+07'),(3615, 'Report', 213, 69, '69', '2022-07-07 16:39:23.033+07', NULL, '2022-07-07 16:39:23.033+07'),(3616, 'Oracle', 213, 69, '69', '2022-07-07 16:39:23.033+07', NULL, '2022-07-07 16:39:23.033+07'),(3617, 'OBIEE', 213, 69, '69', '2022-07-07 16:39:23.033+07', NULL, '2022-07-07 16:39:23.033+07'),(3618, 'Report', 213, 69, '69', '2022-07-07 16:39:24.034+07', NULL, '2022-07-07 16:39:24.034+07'),(3619, 'Oracle', 213, 69, '69', '2022-07-07 16:39:24.035+07', NULL, '2022-07-07 16:39:24.035+07'),(3620, 'OBIEE', 213, 69, '69', '2022-07-07 16:39:24.035+07', NULL, '2022-07-07 16:39:24.035+07'),(3621, 'Report', 213, 69, '69', '2022-07-07 16:39:25.617+07', NULL, '2022-07-07 16:39:25.617+07'),(3622, 'Oracle', 213, 69, '69', '2022-07-07 16:39:25.617+07', NULL, '2022-07-07 16:39:25.617+07'),(3623, 'OBIEE', 213, 69, '69', '2022-07-07 16:39:25.617+07', NULL, '2022-07-07 16:39:25.617+07'),(3624, 'Report', 213, 69, '69', '2022-07-07 16:39:27.519+07', NULL, '2022-07-07 16:39:27.519+07'),(3625, 'Oracle', 213, 69, '69', '2022-07-07 16:39:27.519+07', NULL, '2022-07-07 16:39:27.519+07'),(3626, 'OBIEE', 213, 69, '69', '2022-07-07 16:39:27.519+07', NULL, '2022-07-07 16:39:27.519+07'),(3627, 'Report', 213, 69, '69', '2022-07-07 16:39:36.703+07', NULL, '2022-07-07 16:39:36.703+07'),(3628, 'Oracle', 213, 69, '69', '2022-07-07 16:39:36.703+07', NULL, '2022-07-07 16:39:36.703+07'),(3629, 'OBIEE', 213, 69, '69', '2022-07-07 16:39:36.703+07', NULL, '2022-07-07 16:39:36.703+07'),(3630, 'Report', 213, 69, '69', '2022-07-07 16:39:37.529+07', NULL, '2022-07-07 16:39:37.529+07'),(3631, 'Oracle', 213, 69, '69', '2022-07-07 16:39:37.529+07', NULL, '2022-07-07 16:39:37.529+07'),(3632, 'OBIEE', 213, 69, '69', '2022-07-07 16:39:37.529+07', NULL, '2022-07-07 16:39:37.529+07'),(3633, 'Report', 213, 69, '69', '2022-07-07 16:39:39.912+07', NULL, '2022-07-07 16:39:39.912+07'),(3634, 'OBIEE', 213, 69, '69', '2022-07-07 16:39:39.912+07', NULL, '2022-07-07 16:39:39.912+07'),(3635, 'Oracle', 213, 69, '69', '2022-07-07 16:39:39.912+07', NULL, '2022-07-07 16:39:39.912+07'),(3636, 'Report', 213, 69, '69', '2022-07-07 16:39:42.083+07', NULL, '2022-07-07 16:39:42.083+07'),(3637, 'Oracle', 213, 69, '69', '2022-07-07 16:39:42.083+07', NULL, '2022-07-07 16:39:42.083+07'),(3638, 'OBIEE', 213, 69, '69', '2022-07-07 16:39:42.083+07', NULL, '2022-07-07 16:39:42.083+07'),(3639, 'Report', 213, 69, '69', '2022-07-07 16:39:43.175+07', NULL, '2022-07-07 16:39:43.175+07'),(3640, 'Oracle', 213, 69, '69', '2022-07-07 16:39:43.175+07', NULL, '2022-07-07 16:39:43.175+07'),(3641, 'OBIEE', 213, 69, '69', '2022-07-07 16:39:43.175+07', NULL, '2022-07-07 16:39:43.175+07'),(3642, 'Report', 213, 69, '69', '2022-07-07 16:39:44.618+07', NULL, '2022-07-07 16:39:44.618+07'),(3643, 'Oracle', 213, 69, '69', '2022-07-07 16:39:44.618+07', NULL, '2022-07-07 16:39:44.618+07'),(3644, 'OBIEE', 213, 69, '69', '2022-07-07 16:39:44.618+07', NULL, '2022-07-07 16:39:44.618+07'),(3645, 'Oracle', 213, 69, '69', '2022-07-07 16:39:46.572+07', NULL, '2022-07-07 16:39:46.572+07'),(3646, 'Report', 213, 69, '69', '2022-07-07 16:39:46.572+07', NULL, '2022-07-07 16:39:46.572+07'),(3647, 'OBIEE', 213, 69, '69', '2022-07-07 16:39:46.572+07', NULL, '2022-07-07 16:39:46.572+07'),(3648, 'Report', 213, 69, '69', '2022-07-07 16:45:42.424+07', NULL, '2022-07-07 16:45:42.424+07'),(3649, 'Oracle', 213, 69, '69', '2022-07-07 16:45:42.424+07', NULL, '2022-07-07 16:45:42.424+07'),(3650, 'OBIEE', 213, 69, '69', '2022-07-07 16:45:42.424+07', NULL, '2022-07-07 16:45:42.424+07'),(3651, 'Report', 213, 69, '69', '2022-07-07 16:45:59.253+07', NULL, '2022-07-07 16:45:59.253+07'),(3652, 'Oracle', 213, 69, '69', '2022-07-07 16:45:59.253+07', NULL, '2022-07-07 16:45:59.253+07'),(3653, 'OBIEE', 213, 69, '69', '2022-07-07 16:45:59.253+07', NULL, '2022-07-07 16:45:59.253+07'),(3654, 'Report', 213, 69, '69', '2022-07-07 16:46:01.14+07', NULL, '2022-07-07 16:46:01.14+07'),(3655, 'Oracle', 213, 69, '69', '2022-07-07 16:46:01.14+07', NULL, '2022-07-07 16:46:01.14+07'),(3656, 'OBIEE', 213, 69, '69', '2022-07-07 16:46:01.14+07', NULL, '2022-07-07 16:46:01.14+07'),(3657, 'Report', 213, 69, '69', '2022-07-07 16:46:06.353+07', NULL, '2022-07-07 16:46:06.353+07'),(3658, 'Oracle', 213, 69, '69', '2022-07-07 16:46:06.353+07', NULL, '2022-07-07 16:46:06.353+07'),(3659, 'OBIEE', 213, 69, '69', '2022-07-07 16:46:06.353+07', NULL, '2022-07-07 16:46:06.353+07'),(3661, 'Oracle', 213, 69, '69', '2022-07-07 16:46:08.563+07', NULL, '2022-07-07 16:46:08.563+07'),(3660, 'Report', 213, 69, '69', '2022-07-07 16:46:08.563+07', NULL, '2022-07-07 16:46:08.563+07'),(3662, 'OBIEE', 213, 69, '69', '2022-07-07 16:46:08.563+07', NULL, '2022-07-07 16:46:08.563+07'),(3663, 'Report', 213, 69, '69', '2022-07-07 16:46:09.256+07', NULL, '2022-07-07 16:46:09.256+07'),(3664, 'Oracle', 213, 69, '69', '2022-07-07 16:46:09.256+07', NULL, '2022-07-07 16:46:09.256+07'),(3665, 'OBIEE', 213, 69, '69', '2022-07-07 16:46:09.256+07', NULL, '2022-07-07 16:46:09.256+07'),(3666, 'Report', 213, 69, '69', '2022-07-07 16:46:12.551+07', NULL, '2022-07-07 16:46:12.551+07'),(3667, 'Oracle', 213, 69, '69', '2022-07-07 16:46:12.551+07', NULL, '2022-07-07 16:46:12.551+07'),(3668, 'OBIEE', 213, 69, '69', '2022-07-07 16:46:12.551+07', NULL, '2022-07-07 16:46:12.551+07'),(3669, 'Report', 213, 69, '69', '2022-07-07 16:46:14.459+07', NULL, '2022-07-07 16:46:14.459+07'),(3670, 'Oracle', 213, 69, '69', '2022-07-07 16:46:14.459+07', NULL, '2022-07-07 16:46:14.459+07'),(3671, 'OBIEE', 213, 69, '69', '2022-07-07 16:46:14.459+07', NULL, '2022-07-07 16:46:14.459+07'),(3672, 'Report', 213, 69, '69', '2022-07-07 16:46:15.454+07', NULL, '2022-07-07 16:46:15.454+07'),(3673, 'Oracle', 213, 69, '69', '2022-07-07 16:46:15.454+07', NULL, '2022-07-07 16:46:15.454+07'),(3674, 'OBIEE', 213, 69, '69', '2022-07-07 16:46:15.454+07', NULL, '2022-07-07 16:46:15.454+07'),(3675, 'Report', 213, 69, '69', '2022-07-07 16:46:16.877+07', NULL, '2022-07-07 16:46:16.877+07'),(3676, 'Oracle', 213, 69, '69', '2022-07-07 16:46:16.877+07', NULL, '2022-07-07 16:46:16.877+07'),(3677, 'OBIEE', 213, 69, '69', '2022-07-07 16:46:16.877+07', NULL, '2022-07-07 16:46:16.877+07'),(3678, 'Report', 213, 69, '69', '2022-07-07 16:46:18.707+07', NULL, '2022-07-07 16:46:18.707+07'),(3679, 'Oracle', 213, 69, '69', '2022-07-07 16:46:18.707+07', NULL, '2022-07-07 16:46:18.707+07'),(3680, 'OBIEE', 213, 69, '69', '2022-07-07 16:46:18.707+07', NULL, '2022-07-07 16:46:18.707+07'),(3681, 'Report', 213, 69, '69', '2022-07-07 16:46:27.411+07', NULL, '2022-07-07 16:46:27.411+07'),(3682, 'Oracle', 213, 69, '69', '2022-07-07 16:46:27.411+07', NULL, '2022-07-07 16:46:27.411+07'),(3683, 'OBIEE', 213, 69, '69', '2022-07-07 16:46:27.411+07', NULL, '2022-07-07 16:46:27.411+07'),(3684, 'Report', 213, 69, '69', '2022-07-07 16:46:29.084+07', NULL, '2022-07-07 16:46:29.084+07'),(3685, 'Oracle', 213, 69, '69', '2022-07-07 16:46:29.084+07', NULL, '2022-07-07 16:46:29.084+07'),(3686, 'OBIEE', 213, 69, '69', '2022-07-07 16:46:29.084+07', NULL, '2022-07-07 16:46:29.084+07'),(3687, 'Report', 213, 69, '69', '2022-07-07 16:46:33.43+07', NULL, '2022-07-07 16:46:33.43+07'),(3688, 'Oracle', 213, 69, '69', '2022-07-07 16:46:33.43+07', NULL, '2022-07-07 16:46:33.43+07'),(3689, 'OBIEE', 213, 69, '69', '2022-07-07 16:46:33.43+07', NULL, '2022-07-07 16:46:33.43+07'),(3690, 'Report', 213, 69, '69', '2022-07-07 16:46:34.402+07', NULL, '2022-07-07 16:46:34.402+07'),(3691, 'Oracle', 213, 69, '69', '2022-07-07 16:46:34.402+07', NULL, '2022-07-07 16:46:34.402+07'),(3692, 'OBIEE', 213, 69, '69', '2022-07-07 16:46:34.402+07', NULL, '2022-07-07 16:46:34.402+07'),(3693, 'Report', 213, 69, '69', '2022-07-07 16:46:36.683+07', NULL, '2022-07-07 16:46:36.683+07'),(3694, 'Oracle', 213, 69, '69', '2022-07-07 16:46:36.683+07', NULL, '2022-07-07 16:46:36.683+07'),(3695, 'OBIEE', 213, 69, '69', '2022-07-07 16:46:36.683+07', NULL, '2022-07-07 16:46:36.683+07'),(3696, 'Report', 213, 69, '69', '2022-07-07 16:46:38.815+07', NULL, '2022-07-07 16:46:38.815+07'),(3697, 'Oracle', 213, 69, '69', '2022-07-07 16:46:38.815+07', NULL, '2022-07-07 16:46:38.815+07'),(3698, 'OBIEE', 213, 69, '69', '2022-07-07 16:46:38.815+07', NULL, '2022-07-07 16:46:38.815+07'),(3699, 'Report', 213, 69, '69', '2022-07-07 16:46:39.782+07', NULL, '2022-07-07 16:46:39.782+07'),(3700, 'Oracle', 213, 69, '69', '2022-07-07 16:46:39.782+07', NULL, '2022-07-07 16:46:39.782+07'),(3701, 'OBIEE', 213, 69, '69', '2022-07-07 16:46:39.782+07', NULL, '2022-07-07 16:46:39.782+07'),(3702, 'Report', 213, 69, '69', '2022-07-07 16:46:41.259+07', NULL, '2022-07-07 16:46:41.259+07'),(3703, 'Oracle', 213, 69, '69', '2022-07-07 16:46:41.259+07', NULL, '2022-07-07 16:46:41.259+07'),(3704, 'OBIEE', 213, 69, '69', '2022-07-07 16:46:41.259+07', NULL, '2022-07-07 16:46:41.259+07'),(3705, 'Report', 213, 69, '69', '2022-07-07 16:46:43.056+07', NULL, '2022-07-07 16:46:43.056+07'),(3706, 'Oracle', 213, 69, '69', '2022-07-07 16:46:43.056+07', NULL, '2022-07-07 16:46:43.056+07'),(3707, 'OBIEE', 213, 69, '69', '2022-07-07 16:46:43.056+07', NULL, '2022-07-07 16:46:43.056+07'),(3708, 'API', 204, 69, '69', '2022-07-07 16:48:31.601+07', NULL, '2022-07-07 16:48:31.601+07'),(3710, 'WSO2', 204, 69, '69', '2022-07-07 16:48:31.601+07', NULL, '2022-07-07 16:48:31.601+07'),(3709, 'Programe', 204, 69, '69', '2022-07-07 16:48:31.601+07', NULL, '2022-07-07 16:48:31.601+07'),(3711, 'Report', 205, 69, '69', '2022-07-07 16:48:43.939+07', NULL, '2022-07-07 16:48:43.939+07'),(3712, 'Oracle', 205, 69, '69', '2022-07-07 16:48:43.939+07', NULL, '2022-07-07 16:48:43.939+07'),(3713, 'OBIEE', 205, 69, '69', '2022-07-07 16:48:43.939+07', NULL, '2022-07-07 16:48:43.939+07'),(3714, 'Report', 205, 69, '69', '2022-07-07 16:48:50.188+07', NULL, '2022-07-07 16:48:50.188+07'),(3715, 'Oracle', 205, 69, '69', '2022-07-07 16:48:50.188+07', NULL, '2022-07-07 16:48:50.188+07'),(3716, 'OBIEE', 205, 69, '69', '2022-07-07 16:48:50.188+07', NULL, '2022-07-07 16:48:50.188+07'),(3717, 'Oracle', 205, 69, '69', '2022-07-07 16:48:51.771+07', NULL, '2022-07-07 16:48:51.771+07'),(3718, 'Report', 205, 69, '69', '2022-07-07 16:48:51.771+07', NULL, '2022-07-07 16:48:51.771+07'),(3719, 'OBIEE', 205, 69, '69', '2022-07-07 16:48:51.771+07', NULL, '2022-07-07 16:48:51.771+07'),(3720, 'Report', 205, 69, '69', '2022-07-07 16:48:52.674+07', NULL, '2022-07-07 16:48:52.674+07'),(3721, 'Oracle', 205, 69, '69', '2022-07-07 16:48:52.674+07', NULL, '2022-07-07 16:48:52.674+07'),(3722, 'OBIEE', 205, 69, '69', '2022-07-07 16:48:52.674+07', NULL, '2022-07-07 16:48:52.674+07'),(3723, 'Report', 205, 69, '69', '2022-07-07 16:48:53.999+07', NULL, '2022-07-07 16:48:53.999+07'),(3724, 'Oracle', 205, 69, '69', '2022-07-07 16:48:53.999+07', NULL, '2022-07-07 16:48:53.999+07'),(3725, 'OBIEE', 205, 69, '69', '2022-07-07 16:48:53.999+07', NULL, '2022-07-07 16:48:53.999+07'),(3726, 'Report', 205, 69, '69', '2022-07-07 16:48:55.667+07', NULL, '2022-07-07 16:48:55.667+07'),(3727, 'Oracle', 205, 69, '69', '2022-07-07 16:48:55.667+07', NULL, '2022-07-07 16:48:55.667+07'),(3728, 'OBIEE', 205, 69, '69', '2022-07-07 16:48:55.667+07', NULL, '2022-07-07 16:48:55.667+07'),(3729, 'Report', 205, 69, '69', '2022-07-07 16:49:08.559+07', NULL, '2022-07-07 16:49:08.559+07'),(3730, 'Oracle', 205, 69, '69', '2022-07-07 16:49:08.559+07', NULL, '2022-07-07 16:49:08.559+07'),(3731, 'OBIEE', 205, 69, '69', '2022-07-07 16:49:08.559+07', NULL, '2022-07-07 16:49:08.559+07'),(3732, 'Report', 205, 69, '69', '2022-07-07 16:49:09.254+07', NULL, '2022-07-07 16:49:09.254+07'),(3733, 'Oracle', 205, 69, '69', '2022-07-07 16:49:09.254+07', NULL, '2022-07-07 16:49:09.254+07'),(3734, 'OBIEE', 205, 69, '69', '2022-07-07 16:49:09.254+07', NULL, '2022-07-07 16:49:09.254+07'),(3735, 'Report', 205, 69, '69', '2022-07-07 16:49:12.123+07', NULL, '2022-07-07 16:49:12.123+07'),(3736, 'Oracle', 205, 69, '69', '2022-07-07 16:49:12.123+07', NULL, '2022-07-07 16:49:12.123+07'),(3737, 'OBIEE', 205, 69, '69', '2022-07-07 16:49:12.123+07', NULL, '2022-07-07 16:49:12.123+07'),(3738, 'Report', 205, 69, '69', '2022-07-07 16:49:14.029+07', NULL, '2022-07-07 16:49:14.029+07'),(3739, 'Oracle', 205, 69, '69', '2022-07-07 16:49:14.029+07', NULL, '2022-07-07 16:49:14.029+07'),(3740, 'OBIEE', 205, 69, '69', '2022-07-07 16:49:14.029+07', NULL, '2022-07-07 16:49:14.029+07'),(3741, 'Report', 205, 69, '69', '2022-07-07 16:49:14.921+07', NULL, '2022-07-07 16:49:14.921+07'),(3742, 'Oracle', 205, 69, '69', '2022-07-07 16:49:14.921+07', NULL, '2022-07-07 16:49:14.921+07'),(3743, 'OBIEE', 205, 69, '69', '2022-07-07 16:49:14.921+07', NULL, '2022-07-07 16:49:14.921+07'),(3744, 'Report', 205, 69, '69', '2022-07-07 16:49:16.247+07', NULL, '2022-07-07 16:49:16.247+07'),(3745, 'Oracle', 205, 69, '69', '2022-07-07 16:49:16.247+07', NULL, '2022-07-07 16:49:16.247+07'),(3746, 'OBIEE', 205, 69, '69', '2022-07-07 16:49:16.247+07', NULL, '2022-07-07 16:49:16.247+07'),(3747, 'Oracle', 205, 69, '69', '2022-07-07 16:49:17.886+07', NULL, '2022-07-07 16:49:17.886+07'),(3749, 'Report', 205, 69, '69', '2022-07-07 16:49:17.886+07', NULL, '2022-07-07 16:49:17.886+07'),(3748, 'OBIEE', 205, 69, '69', '2022-07-07 16:49:17.886+07', NULL, '2022-07-07 16:49:17.886+07'),(3750, 'WSO2', 204, 69, '69', '2022-07-08 10:17:40.65+07', NULL, '2022-07-08 10:17:40.65+07'),(3751, 'Programe', 204, 69, '69', '2022-07-08 10:17:40.65+07', NULL, '2022-07-08 10:17:40.65+07'),(3752, 'API', 204, 69, '69', '2022-07-08 10:17:40.65+07', NULL, '2022-07-08 10:17:40.65+07'),(3753, 'Report', 213, 69, '69', '2022-07-08 10:17:53.487+07', NULL, '2022-07-08 10:17:53.488+07'),(3754, 'Oracle', 213, 69, '69', '2022-07-08 10:17:53.488+07', NULL, '2022-07-08 10:17:53.488+07'),(3755, 'OBIEE', 213, 69, '69', '2022-07-08 10:17:53.488+07', NULL, '2022-07-08 10:17:53.488+07'),(3756, 'Report', 213, 69, '69', '2022-07-08 10:18:02.745+07', NULL, '2022-07-08 10:18:02.745+07'),(3757, 'Oracle', 213, 69, '69', '2022-07-08 10:18:02.745+07', NULL, '2022-07-08 10:18:02.745+07'),(3758, 'OBIEE', 213, 69, '69', '2022-07-08 10:18:02.745+07', NULL, '2022-07-08 10:18:02.745+07'),(3759, 'Report', 213, 69, '69', '2022-07-08 10:18:06.074+07', NULL, '2022-07-08 10:18:06.074+07'),(3760, 'Oracle', 213, 69, '69', '2022-07-08 10:18:06.074+07', NULL, '2022-07-08 10:18:06.074+07'),(3761, 'OBIEE', 213, 69, '69', '2022-07-08 10:18:06.074+07', NULL, '2022-07-08 10:18:06.074+07'),(3762, 'Report', 213, 69, '69', '2022-07-08 10:18:07.297+07', NULL, '2022-07-08 10:18:07.297+07'),(3763, 'Oracle', 213, 69, '69', '2022-07-08 10:18:07.297+07', NULL, '2022-07-08 10:18:07.297+07'),(3764, 'OBIEE', 213, 69, '69', '2022-07-08 10:18:07.297+07', NULL, '2022-07-08 10:18:07.297+07'),(3765, 'Report', 213, 69, '69', '2022-07-08 10:18:08.754+07', NULL, '2022-07-08 10:18:08.754+07'),(3766, 'Oracle', 213, 69, '69', '2022-07-08 10:18:08.754+07', NULL, '2022-07-08 10:18:08.754+07'),(3767, 'OBIEE', 213, 69, '69', '2022-07-08 10:18:08.754+07', NULL, '2022-07-08 10:18:08.754+07'),(3768, 'Report', 213, 69, '69', '2022-07-08 10:18:10.765+07', NULL, '2022-07-08 10:18:10.765+07'),(3769, 'Oracle', 213, 69, '69', '2022-07-08 10:18:10.765+07', NULL, '2022-07-08 10:18:10.765+07'),(3770, 'OBIEE', 213, 69, '69', '2022-07-08 10:18:10.765+07', NULL, '2022-07-08 10:18:10.765+07'),(3771, 'Report', 213, 69, '69', '2022-07-08 10:18:21.554+07', NULL, '2022-07-08 10:18:21.554+07'),(3772, 'Oracle', 213, 69, '69', '2022-07-08 10:18:21.554+07', NULL, '2022-07-08 10:18:21.554+07'),(3773, 'OBIEE', 213, 69, '69', '2022-07-08 10:18:21.554+07', NULL, '2022-07-08 10:18:21.554+07'),(3774, 'Report', 213, 69, '69', '2022-07-08 10:18:22.312+07', NULL, '2022-07-08 10:18:22.312+07'),(3775, 'Oracle', 213, 69, '69', '2022-07-08 10:18:22.312+07', NULL, '2022-07-08 10:18:22.312+07'),(3776, 'OBIEE', 213, 69, '69', '2022-07-08 10:18:22.312+07', NULL, '2022-07-08 10:18:22.312+07'),(3777, 'Report', 213, 69, '69', '2022-07-08 10:18:25.497+07', NULL, '2022-07-08 10:18:25.497+07'),(3778, 'Oracle', 213, 69, '69', '2022-07-08 10:18:25.497+07', NULL, '2022-07-08 10:18:25.497+07'),(3779, 'OBIEE', 213, 69, '69', '2022-07-08 10:18:25.497+07', NULL, '2022-07-08 10:18:25.497+07'),(3781, 'Oracle', 213, 69, '69', '2022-07-08 10:18:28.082+07', NULL, '2022-07-08 10:18:28.082+07'),(3782, 'OBIEE', 213, 69, '69', '2022-07-08 10:18:28.082+07', NULL, '2022-07-08 10:18:28.082+07'),(3780, 'Report', 213, 69, '69', '2022-07-08 10:18:28.082+07', NULL, '2022-07-08 10:18:28.082+07'),(3783, 'Report', 213, 69, '69', '2022-07-08 10:18:29.074+07', NULL, '2022-07-08 10:18:29.074+07'),(3784, 'Oracle', 213, 69, '69', '2022-07-08 10:18:29.074+07', NULL, '2022-07-08 10:18:29.074+07'),(3785, 'OBIEE', 213, 69, '69', '2022-07-08 10:18:29.074+07', NULL, '2022-07-08 10:18:29.074+07'),(3786, 'Report', 213, 69, '69', '2022-07-08 10:18:30.516+07', NULL, '2022-07-08 10:18:30.516+07'),(3787, 'Oracle', 213, 69, '69', '2022-07-08 10:18:30.516+07', NULL, '2022-07-08 10:18:30.516+07'),(3788, 'OBIEE', 213, 69, '69', '2022-07-08 10:18:30.516+07', NULL, '2022-07-08 10:18:30.516+07'),(3789, 'Report', 213, 69, '69', '2022-07-08 10:18:32.365+07', NULL, '2022-07-08 10:18:32.365+07'),(3790, 'Oracle', 213, 69, '69', '2022-07-08 10:18:32.365+07', NULL, '2022-07-08 10:18:32.365+07'),(3791, 'OBIEE', 213, 69, '69', '2022-07-08 10:18:32.365+07', NULL, '2022-07-08 10:18:32.365+07'),(3792, 'Report', 206, 69, '69', '2022-07-08 11:00:31.3+07', NULL, '2022-07-08 11:00:31.3+07'),(3793, 'Oracle', 206, 69, '69', '2022-07-08 11:00:31.3+07', NULL, '2022-07-08 11:00:31.3+07'),(3794, 'OBIEE', 206, 69, '69', '2022-07-08 11:00:31.3+07', NULL, '2022-07-08 11:00:31.3+07'),(3795, 'OBIEE', 206, 69, '69', '2022-07-08 11:00:42.909+07', NULL, '2022-07-08 11:00:42.909+07'),(3796, 'Report', 206, 69, '69', '2022-07-08 11:00:42.909+07', NULL, '2022-07-08 11:00:42.909+07'),(3797, 'Oracle', 206, 69, '69', '2022-07-08 11:00:42.909+07', NULL, '2022-07-08 11:00:42.909+07'),(3798, 'Report', 206, 69, '69', '2022-07-08 11:00:44.591+07', NULL, '2022-07-08 11:00:44.591+07'),(3799, 'Oracle', 206, 69, '69', '2022-07-08 11:00:44.591+07', NULL, '2022-07-08 11:00:44.591+07'),(3800, 'OBIEE', 206, 69, '69', '2022-07-08 11:00:44.591+07', NULL, '2022-07-08 11:00:44.591+07'),(3801, 'OBIEE', 207, 69, '69', '2022-07-08 11:10:32.486+07', NULL, '2022-07-08 11:10:32.486+07'),(3802, 'Oracle', 207, 69, '69', '2022-07-08 11:10:32.487+07', NULL, '2022-07-08 11:10:32.487+07'),(3803, 'Report', 207, 69, '69', '2022-07-08 11:10:32.487+07', NULL, '2022-07-08 11:10:32.487+07'),(3804, 'Photoshop', 218, 69, '69', '2022-07-08 13:06:22.775+07', NULL, '2022-07-08 13:06:22.775+07'),(3805, 'Design', 218, 69, '69', '2022-07-08 13:06:22.775+07', NULL, '2022-07-08 13:06:22.775+07'),(3806, 'Image', 218, 69, '69', '2022-07-08 13:06:22.775+07', NULL, '2022-07-08 13:06:22.775+07'),(3807, 'Image', 221, 69, '69', '2022-07-08 14:53:23.66+07', NULL, '2022-07-08 14:53:23.66+07'),(3808, 'Design', 221, 69, '69', '2022-07-08 14:53:23.66+07', NULL, '2022-07-08 14:53:23.66+07'),(3809, 'Photoshop', 221, 69, '69', '2022-07-08 14:53:23.66+07', NULL, '2022-07-08 14:53:23.66+07'),(1, 'Image', 227, 69, '69', '2022-07-15 11:25:31.417+07', NULL, '2022-07-15 11:25:31.417+07'),(2, 'Design', 227, 69, '69', '2022-07-15 11:25:31.417+07', NULL, '2022-07-15 11:25:31.417+07'),(3, 'Photoshop', 227, 69, '69', '2022-07-15 11:25:31.417+07', NULL, '2022-07-15 11:25:31.417+07'),(4, 'Image', 226, 69, '69', '2022-07-15 11:25:38.78+07', NULL, '2022-07-15 11:25:38.78+07'),(5, 'Design', 226, 69, '69', '2022-07-15 11:25:38.78+07', NULL, '2022-07-15 11:25:38.78+07'),(6, 'Photoshop', 226, 69, '69', '2022-07-15 11:25:38.78+07', NULL, '2022-07-15 11:25:38.78+07'),(7, 'Image', 226, 69, '69', '2022-07-15 11:26:52.081+07', NULL, '2022-07-15 11:26:52.081+07'),(8, 'Design', 226, 69, '69', '2022-07-15 11:26:52.081+07', NULL, '2022-07-15 11:26:52.081+07'),(9, 'Photoshop', 226, 69, '69', '2022-07-15 11:26:52.081+07', NULL, '2022-07-15 11:26:52.081+07'),(10, 'Image', 226, 69, '69', '2022-07-15 11:26:52.826+07', NULL, '2022-07-15 11:26:52.826+07'),(11, 'Photoshop', 226, 69, '69', '2022-07-15 11:26:52.826+07', NULL, '2022-07-15 11:26:52.826+07'),(12, 'Design', 226, 69, '69', '2022-07-15 11:26:52.826+07', NULL, '2022-07-15 11:26:52.826+07'),(13, 'Image', 217, 69, '69', '2022-07-15 11:26:54.832+07', NULL, '2022-07-15 11:26:54.832+07'),(14, 'Design', 217, 69, '69', '2022-07-15 11:26:54.832+07', NULL, '2022-07-15 11:26:54.832+07'),(15, 'Photoshop', 217, 69, '69', '2022-07-15 11:26:54.832+07', NULL, '2022-07-15 11:26:54.832+07'),(16, 'Image', 217, 69, '69', '2022-07-15 11:26:57.537+07', NULL, '2022-07-15 11:26:57.537+07'),(17, 'Design', 217, 69, '69', '2022-07-15 11:26:57.537+07', NULL, '2022-07-15 11:26:57.537+07'),(18, 'Photoshop', 217, 69, '69', '2022-07-15 11:26:57.537+07', NULL, '2022-07-15 11:26:57.537+07'),(19, 'Image', 227, 69, '69', '2022-07-15 11:28:14.118+07', NULL, '2022-07-15 11:28:14.118+07'),(21, 'Photoshop', 227, 69, '69', '2022-07-15 11:28:14.118+07', NULL, '2022-07-15 11:28:14.118+07'),(20, 'Design', 227, 69, '69', '2022-07-15 11:28:14.118+07', NULL, '2022-07-15 11:28:14.118+07'),(22, 'Image', 226, 69, '69', '2022-07-15 11:29:38.987+07', NULL, '2022-07-15 11:29:38.987+07'),(23, 'Design', 226, 69, '69', '2022-07-15 11:29:38.987+07', NULL, '2022-07-15 11:29:38.987+07'),(24, 'Photoshop', 226, 69, '69', '2022-07-15 11:29:38.987+07', NULL, '2022-07-15 11:29:38.987+07'),(25, 'OBIEE', 213, 69, '69', '2022-07-15 11:36:27.601+07', NULL, '2022-07-15 11:36:27.601+07'),(26, 'Report', 213, 69, '69', '2022-07-15 11:36:27.601+07', NULL, '2022-07-15 11:36:27.601+07'),(27, 'Oracle', 213, 69, '69', '2022-07-15 11:36:27.601+07', NULL, '2022-07-15 11:36:27.601+07'),(28, 'Image', 227, 75, '75', '2022-08-12 11:54:13.539+07', NULL, '2022-08-12 11:54:13.539+07'),(29, 'Design', 227, 75, '75', '2022-08-12 11:54:13.539+07', NULL, '2022-08-12 11:54:13.539+07'),(30, 'Photoshop', 227, 75, '75', '2022-08-12 11:54:13.539+07', NULL, '2022-08-12 11:54:13.539+07'),(31, 'Image', 227, 69, '69', '2022-08-12 14:34:37.48+07', NULL, '2022-08-12 14:34:37.48+07'),(32, 'Design', 227, 69, '69', '2022-08-12 14:34:37.48+07', NULL, '2022-08-12 14:34:37.48+07'),(33, 'Photoshop', 227, 69, '69', '2022-08-12 14:34:37.48+07', NULL, '2022-08-12 14:34:37.48+07'),(34, 'Image', 227, 69, '69', '2022-08-12 14:35:50.894+07', NULL, '2022-08-12 14:35:50.894+07'),(35, 'Design', 227, 69, '69', '2022-08-12 14:35:50.894+07', NULL, '2022-08-12 14:35:50.894+07'),(36, 'Photoshop', 227, 69, '69', '2022-08-12 14:35:50.894+07', NULL, '2022-08-12 14:35:50.894+07'),(37, 'Image', 227, 69, '69', '2022-08-12 14:37:19.941+07', NULL, '2022-08-12 14:37:19.941+07'),(38, 'Design', 227, 69, '69', '2022-08-12 14:37:19.941+07', NULL, '2022-08-12 14:37:19.941+07'),(39, 'Photoshop', 227, 69, '69', '2022-08-12 14:37:19.941+07', NULL, '2022-08-12 14:37:19.941+07'),(40, 'Image', 227, 69, '69', '2022-08-12 14:39:16.164+07', NULL, '2022-08-12 14:39:16.164+07'),(41, 'Design', 227, 69, '69', '2022-08-12 14:39:16.164+07', NULL, '2022-08-12 14:39:16.164+07'),(42, 'Photoshop', 227, 69, '69', '2022-08-12 14:39:16.164+07', NULL, '2022-08-12 14:39:16.164+07'),(43, 'Image', 227, 69, '69', '2022-08-16 16:24:50.192+07', NULL, '2022-08-16 16:24:50.192+07'),(44, 'Design', 227, 69, '69', '2022-08-16 16:24:50.192+07', NULL, '2022-08-16 16:24:50.192+07'),(45, 'Photoshop', 227, 69, '69', '2022-08-16 16:24:50.192+07', NULL, '2022-08-16 16:24:50.192+07'),(46, 'WSO2', 204, 69, '69', '2022-08-16 16:25:11.912+07', NULL, '2022-08-16 16:25:11.912+07'),(47, 'API', 204, 69, '69', '2022-08-16 16:25:11.912+07', NULL, '2022-08-16 16:25:11.912+07'),(48, 'Programe', 204, 69, '69', '2022-08-16 16:25:11.912+07', NULL, '2022-08-16 16:25:11.912+07'),(49, 'Image', 227, 69, '69', '2022-08-16 16:26:16.442+07', NULL, '2022-08-16 16:26:16.442+07'),(50, 'Design', 227, 69, '69', '2022-08-16 16:26:16.442+07', NULL, '2022-08-16 16:26:16.442+07'),(51, 'Photoshop', 227, 69, '69', '2022-08-16 16:26:16.442+07', NULL, '2022-08-16 16:26:16.442+07');
COMMIT;
ALTER TABLE "auth_provider" ADD CONSTRAINT "auth_provider_key" PRIMARY KEY ("auth_provider_id");
ALTER TABLE "auth_user_provider" ADD CONSTRAINT "auth_user_provider_pkey" PRIMARY KEY ("auth_user_provider_id");
ALTER TABLE "device" ADD CONSTRAINT "device_pkey" PRIMARY KEY ("device_id");
ALTER TABLE "feedbacks" ADD CONSTRAINT "feedbacks_pkey" PRIMARY KEY ("id");
ALTER TABLE "log_users" ADD CONSTRAINT "log_users_pkey" PRIMARY KEY ("id");
ALTER TABLE "migrations" ADD CONSTRAINT "migrations_pkey" PRIMARY KEY ("id");
ALTER TABLE "migrations_lock" ADD CONSTRAINT "migrations_lock_pkey" PRIMARY KEY ("index");
ALTER TABLE "mt_lov" ADD CONSTRAINT "mt_lov_pkey" PRIMARY KEY ("id");
ALTER TABLE "register_verify" ADD CONSTRAINT "register_verify_pkey" PRIMARY KEY ("id");
ALTER TABLE "users" ADD CONSTRAINT "users_pkey" PRIMARY KEY ("id");
ALTER TABLE "videos" ADD CONSTRAINT "videos_pkey" PRIMARY KEY ("id");
CREATE INDEX "index_cate_title" ON "videos" USING btree (
  "category" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST,
  "title" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);
CREATE INDEX "index_userId" ON "videos" USING btree (
  "userid" "pg_catalog"."int8_ops" ASC NULLS LAST
);
ALTER TABLE "videos_action" ADD CONSTRAINT "videos_action_pkey" PRIMARY KEY ("id");
ALTER TABLE "videos_channel" ADD CONSTRAINT "videos_channel_pkey" PRIMARY KEY ("id");
ALTER TABLE "videos_history" ADD CONSTRAINT "videos_history_pkey" PRIMARY KEY ("id");
ALTER TABLE "videos_playlist" ADD CONSTRAINT "video_playlist_pkey" PRIMARY KEY ("id");
ALTER TABLE "videos_playlist_add" ADD CONSTRAINT "videos_playlist_add_pkey" PRIMARY KEY ("id");
ALTER TABLE "videos_tags" ADD CONSTRAINT "videos_tags_pkey" PRIMARY KEY ("id");
ALTER TABLE "videos_views" ADD CONSTRAINT "videos_views_pkey" PRIMARY KEY ("id");
ALTER TABLE "videos_watch_tags" ADD CONSTRAINT "videos_watch_tags_pkey" PRIMARY KEY ("id");
ALTER TABLE "users" ADD CONSTRAINT "users_email_unique" UNIQUE ("email_address");
ALTER SEQUENCE "feedbacks_id_seq"
OWNED BY "feedbacks"."id";
SELECT setval('"feedbacks_id_seq"', 10, true);
ALTER SEQUENCE "feedbacks_id_seq" OWNER TO "postgres";
ALTER SEQUENCE "log_users_id_seq"
OWNED BY "log_users"."id";
SELECT setval('"log_users_id_seq"', 68, true);
ALTER SEQUENCE "log_users_id_seq" OWNER TO "postgres";
ALTER SEQUENCE "migrations_id_seq"
OWNED BY "migrations"."id";
SELECT setval('"migrations_id_seq"', 2, true);
ALTER SEQUENCE "migrations_id_seq" OWNER TO "postgres";
ALTER SEQUENCE "migrations_lock_index_seq"
OWNED BY "migrations_lock"."index";
SELECT setval('"migrations_lock_index_seq"', 2, true);
ALTER SEQUENCE "migrations_lock_index_seq" OWNER TO "postgres";
ALTER SEQUENCE "register_verify_id_seq"
OWNED BY "register_verify"."id";
SELECT setval('"register_verify_id_seq"', 4, true);
ALTER SEQUENCE "register_verify_id_seq" OWNER TO "postgres";
ALTER SEQUENCE "users_id_seq"
OWNED BY "users"."id";
SELECT setval('"users_id_seq"', 77, true);
ALTER SEQUENCE "users_id_seq" OWNER TO "postgres";
ALTER SEQUENCE "videos_action_id_seq"
OWNED BY "videos_action"."id";
SELECT setval('"videos_action_id_seq"', 130, true);
ALTER SEQUENCE "videos_action_id_seq" OWNER TO "postgres";
ALTER SEQUENCE "videos_channel_id_seq"
OWNED BY "videos_channel"."id";
SELECT setval('"videos_channel_id_seq"', 2, false);
ALTER SEQUENCE "videos_channel_id_seq" OWNER TO "postgres";
ALTER SEQUENCE "videos_history_id_seq"
OWNED BY "videos_history"."id";
SELECT setval('"videos_history_id_seq"', 9, true);
ALTER SEQUENCE "videos_history_id_seq" OWNER TO "postgres";
ALTER SEQUENCE "videos_id_seq"
OWNED BY "videos"."id";
SELECT setval('"videos_id_seq"', 228, true);
ALTER SEQUENCE "videos_id_seq" OWNER TO "postgres";
ALTER SEQUENCE "videos_playlist_add_id_seq"
OWNED BY "videos_playlist_add"."id";
SELECT setval('"videos_playlist_add_id_seq"', 4, true);
ALTER SEQUENCE "videos_playlist_add_id_seq" OWNER TO "postgres";
ALTER SEQUENCE "videos_playlist_id_seq"
OWNED BY "videos_playlist"."id";
SELECT setval('"videos_playlist_id_seq"', 3, true);
ALTER SEQUENCE "videos_playlist_id_seq" OWNER TO "postgres";
ALTER SEQUENCE "videos_tags_id_seq"
OWNED BY "videos_tags"."id";
SELECT setval('"videos_tags_id_seq"', 2, false);
ALTER SEQUENCE "videos_tags_id_seq" OWNER TO "postgres";
ALTER SEQUENCE "videos_views_id_seq"
OWNED BY "videos_views"."id";
SELECT setval('"videos_views_id_seq"', 5, true);
ALTER SEQUENCE "videos_views_id_seq" OWNER TO "postgres";
ALTER SEQUENCE "videos_watch_tags_id_seq"
OWNED BY "videos_watch_tags"."id";
SELECT setval('"videos_watch_tags_id_seq"', 52, true);
ALTER SEQUENCE "videos_watch_tags_id_seq" OWNER TO "postgres";
