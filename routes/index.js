const express = require('express');
const swaggerUi = require('swagger-ui-express');
const path = require('path');
const router = express.Router();
const debug = require('debug')('express:login');
// const swaggerDocumentUser = require('./swagger-user.json');
// const swaggerDocumentService = require('./swagger-service.json');

const registerRoutes = require('../features/register/routes');
const registerVerifyRoutes = require('../features/registerVerify/routes');
const loginRoutes = require('../features/login/routes');
const userRoutes = require('../features/user/routes');
const logoutRoutes = require('../features/logout/routes');
const profileRoutes = require('../features/profile/routes');
const videoRoutes = require('../features/video/routes');
const youTubevideoRoutes = require('../features/youTubeVideo/routes')
const smsRoutes = require('../features/sms/routes');//test
// videoRoutes
const options = {
  explorer: true,
  swaggerOptions: {
    urls: [
      {
        url: 'http://localhost:8001/swaggers/swagger-user.json',
        name: 'auth-service'
      },
      {
        url: 'http://localhost:8001/swaggers/swagger-service.json',
        name: 'video-service'
      }
    ]
  }
}

router.use('/api-docs', swaggerUi.serve, swaggerUi.setup(null, options));

// var options = {}

// router.use('/api-docs-user', swaggerUi.serveFiles(swaggerDocumentUser, options), swaggerUi.setup(swaggerDocumentUser));
// router.use('/api-docs-service', swaggerUi.serveFiles(swaggerDocumentService, options), swaggerUi.setup(swaggerDocumentService));

// router.get('/swaggerData', (req, res) => {
//   return res.json(options);
// });

/**User Management */
router.use('/api/user-service/v1/auth/login', loginRoutes);
router.use('/api/user-service/v1/auth/logout', logoutRoutes);
router.use('/api/user-service/v1/users', userRoutes);
router.use('/profile', profileRoutes);
router.use('/api/user-service/v1/register', registerRoutes);
router.use('/api/user-service/v1/register-verify', registerVerifyRoutes);
router.use('/api/user-service/v1/sms', smsRoutes);//test

/**Upload Video */
router.use('/api/auth-service/v1/video', videoRoutes);
router.use('/api/auth-service/v1/youTubeVideo', youTubevideoRoutes);

router.use("/resources", express.static(path.join(__dirname, '../../resources/video/uploads/')));
router.use("/resources-profile", express.static(path.join(__dirname, '../../resources/image/uploads/')));
router.use("/resources/images", express.static(path.join(__dirname, '../../resources/assets/')));
router.use("/swaggers", express.static(path.join(__dirname, './swagger/')));

module.exports = router;
