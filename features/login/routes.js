const { wrap } = require('async-middleware');
const router = require('express').Router();
const authJwt = require('../../middlewares/authJwt');
const verifyRequestBody = require('./commands/verify-request-body');
const login = require('./commands/login');
const redirect = require('./commands/redirect-to-dashboard');

router.post(
  '/',
  wrap(verifyRequestBody),
  wrap(authJwt.verifySecret),
  wrap(login),
  wrap(redirect.redirectToDashboard)
);
router.post(
  '/googlesignin',
  wrap(verifyRequestBody),
  wrap(authJwt.verifySecret),
  wrap(redirect.checkGoogleSignin),
  wrap(login),
  wrap(redirect.redirectToDashboardGoogleSignin)
);
module.exports = router;
