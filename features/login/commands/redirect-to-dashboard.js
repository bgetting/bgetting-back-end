const debug = require('debug')('express:login');

const { secretKey } = require('../../../config/auth.config');
const jwt = require('jwt-simple');
const moment = require('moment');

const { FETCH_INFO_ERROR_MESSAGE } = require('../constants');
const { getUserById, getUserByIdGooglesinginCheck, insertUserByIdGooglesingin } = require('../repository');

async function redirectToDashboard(req, res) {
  let userInfo;
  const { user, body } = req;
  //debug(user)
  var expires = parseInt(
    moment()
      .add(43800, 'hours')
      .format('X')
  );
  var token = jwt.encode(
    {
      expires: expires,
      user: user,
    },
    secretKey.secret
  );
  try {
    userInfo = await getUserById(user && user.id, expires, token);
  } catch (getUserError) {
    const messages = {
      errors: {
        databaseError: FETCH_INFO_ERROR_MESSAGE,
      },
    };

    return res.status(500).send({ success: false, messages });
  }

  debug('login:redirectToDashboard');
  return res.send({
    tokenType: 'Bearer ',
    success: true,
    accessToken: token,
    accessExpiresIn: expires,
    user: userInfo,
  });
}

async function redirectToDashboardGoogleSignin(req, res) {
  let userInfo;
  const { user } = req;
  //debug(user)
  var expires = parseInt(
    moment()
      .add(43800, 'hours')
      .format('X')
  );
  var token = jwt.encode(
    {
      expires: expires,
      user: user,
    },
    secretKey.secret
  );

  try {
    userInfo = await getUserById(user && user.id, expires, token);
  } catch (getUserError) {
    const messages = {
      errors: {
        databaseError: FETCH_INFO_ERROR_MESSAGE,
      },
    };
    return res.status(500).send({ success: false, messages });
  }
  return res.send({
    tokenType: 'Bearer ',
    success: true,
    accessToken: token,
    accessExpiresIn: expires,
    user: userInfo,
  });
}

async function checkGoogleSignin(req, res, next) {
  let userInfo;
  const { body } = req;
  var expires = parseInt(
    moment()
      .add(43800, 'hours')
      .format('X')
  );
  var token = jwt.encode(
    {
      expires: expires,
      user: body.username,
    },
    secretKey.secret
  );
  try {
    userInfo = await getUserByIdGooglesinginCheck(body, expires, token);
    if (userInfo) {
      next();
      return;
    } else {
      userInsert = await insertUserByIdGooglesingin(body);
      if (userInsert) {
        next();
        return;
      } else {
        const messages = {
          errors: {
            databaseError: FETCH_INFO_ERROR_MESSAGE,
          },
        };
        return res.status(500).send({ success: false, messages });
      }
    }
  } catch (getUserError) {
    const messages = {
      errors: {
        databaseError: FETCH_INFO_ERROR_MESSAGE,
      },
    };
    return res.status(500).send({ success: false, messages });
  }
}

const redirect = {
  redirectToDashboard: redirectToDashboard,
  redirectToDashboardGoogleSignin: redirectToDashboardGoogleSignin,
  checkGoogleSignin: checkGoogleSignin,
};
module.exports = redirect;
