const Joi = require('joi');

const constants = require('../constants');

const {
  PASSWORD_MAX,
  PASSWORD_MIN,
  PHONE_NUMBER_MIN,
  PHONE_NUMBER_MAX,
  PHONE_NUMBER,
  EMAIL,
  EMAIL_GOOGLE_SIGNIN,
} = constants;

const schema = Joi.object().keys({
  oauthId: Joi.string().required(),
  oauthSecret: Joi.string().required(),
  provider: Joi.number().required(),
  username: Joi.string()
    .when('provider', {
      is: PHONE_NUMBER,
      then: Joi.string()
        // .regex(/^[0-9]{10}$/)
        .min(PHONE_NUMBER_MIN)
        .max(PHONE_NUMBER_MAX)
        .required(),
    })
    .when('provider', {
      is: EMAIL,
      then: Joi.string()
        .email({ minDomainAtoms: 2 })
        .required(),
    })
    .when('provider', {
      is: EMAIL_GOOGLE_SIGNIN,
      then: Joi.string()
        .email({ minDomainAtoms: 2 })
        .required(),
    }),

  password: Joi.string()
    .min(PASSWORD_MIN)
    .max(PASSWORD_MAX),
});

module.exports = async function validate(req, res, next) {
  let payloadValidation = {};
  try {
    payloadValidation = await Joi.validate(req.body, schema, { abortEarly: false });
  } catch (validateRegisterError) {
    payloadValidation = validateRegisterError;
  }
  const { details } = payloadValidation;
  let errors;

  if (details) {
    errors = {};
    details.forEach(errorDetail => {    
      errors = errorDetail.message;
    });
  }

  if (errors) {
    return res.status(400).send({ success: false, messages: errors });
  }
  return next();
};
