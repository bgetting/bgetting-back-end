const debug = require('debug')('express:login');
const Knex = require('knex');
const bcrypt = require('bcrypt');

const knexConfig = require('../../db/knexfile');

const knex = Knex(knexConfig[process.env.NODE_ENV]);

async function getUserForLoginData(email_address, password) {
  if (isNaN(email_address)) {
    where = { email_address: email_address };
  } else {
    where = { phone_number: email_address };
  }

  const [user] = await knex('users')
    .select()
    .where(where)
    .limit(1);

  if (!user) {
    return null;
  }

  const isPasswordValid = await bcrypt.compare(password, user.password);

  if (!isPasswordValid) {
    return null;
  }

  return {
    id: user.id,
    username: user.email_address,
    created_at: user.created_at,
  };
}

async function getUser(query) {
  const [user] = await knex('users')
    .select('id', 'username', 'email_address', 'phone_number', 'image', 'image_cover', 'status', 'created_at')
    .where(query)
    .limit(1);

  return user;
}

async function getUserById(id, accessExpiresIn, token) {
  await knex('log_users')
    .insert({
      users_id: id,
      access_token: token,
      expires: accessExpiresIn,
      created_at: new Date(),
      updated_at: new Date(),
    });

  return getUser({ id });
}

async function getUserByIdGooglesinginCheck(bordy, accessExpiresIn, token) {
  const username = { email_address: bordy.username }
  const data = await getUser(username);
  if (data) {
    await knex('log_users')
      .insert({
        users_id: data.id,
        access_token: token,
        expires: accessExpiresIn,
        created_at: new Date(),
        updated_at: new Date(),
      });
  }

  return getUser(username);
}

async function insertUserByIdGooglesingin(bordy) {
  const { provider, username, password } = bordy;
  const hashedPass = await bcrypt.hash(password, 5);
  if (isNaN(username)) {
    value = {
      password: hashedPass,
      email_address: username.toLowerCase(),
      created_by: username.toLowerCase(),
      created_at: new Date(),
      updated_at: new Date(),
      email_verified_at: new Date(),
    };
    mailPhone = {
      email_address: username.toLowerCase(),
    };
  } 

  const checkMailPhone = await knex('users')
    .select('id', 'email_address', 'phone_number')
    .where(mailPhone)
    .limit(1);
  if (checkMailPhone.length === 0) {
    const user = await knex('users')
      .insert(value)
      .returning('id')
      .then(row => {
        return knex('auth_user_provider')
          .insert({
            auth_provider_id: provider,
            auth_user_id: row[0],
            created_by: username.toLowerCase(),
            created_at: new Date(),
            updated_at: new Date(),
          })
          .returning(['auth_provider_id'])
          .then(data => {
            return data;
          });
      });
    return { user };
  } else {
    return { code: '23505' };
  }
}

module.exports = {
  getUserForLoginData,
  getUserById,
  getUserByIdGooglesinginCheck,
  insertUserByIdGooglesingin
};
