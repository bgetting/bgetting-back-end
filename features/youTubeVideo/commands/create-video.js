const debug = require('debug')('express:login');
const registerRepo = require('../repository');

async function createYoutubeVideo(req, res) {
  let video = {};
  const registerSuccessMessage = 'You have successfully upload video.';
  try {
    video = await registerRepo.createYoutubeVideo(req.body);  
  } catch (error) {
    video = error;
  }
  if (video.checkUserId === 1) {
    return res.send({ success: true, messages: registerSuccessMessage });
  }
  const databaseError =
  video.code === '23505' ? 'No userId.' : 'Something went wrong.';
  const checkError = video.code === '23404' ? 'Video upload already.' : databaseError;
  return res.status(500).send({ success: false, messages:  checkError  });
}

module.exports = createYoutubeVideo;
