const Joi = require('joi');
const debug = require('debug')('express:login');

const schema = Joi.object().keys({
  youtube_videoId: Joi.string().required(),
  title: Joi.string().required(),
  userId: Joi.string().required(),
  tags: Joi.required(),
  provider_name: Joi.string().required(),
  thumbnail_url: Joi.string().required(),
});

async function validateRegisterPayload(req, res, next) {
  
  let payloadValidation;
  try {
    const body = {
      youtube_videoId: req.body.youtube_videoId,
      title: req.body.title,
      userId: req.body.userId,
      tags: req.body.tags,
      provider_name: req.body.provider_name,
      thumbnail_url: req.body.thumbnail_url
  }
    payloadValidation = await Joi.validate(body, schema, { abortEarly: false });
  // debug(payloadValidation)
  } catch (validateRegisterError) {
    payloadValidation = validateRegisterError;
  }
  const { details } = payloadValidation;
  let errors;
  // debug(details)
  if (details) {
    errors = {};
    details.forEach(errorDetail => {
      errors = errorDetail.message;
    });
  }

  if (errors) {
    return res.status(400).send({ success: false, messages: errors });
  }
  return next();
}

module.exports = validateRegisterPayload;
