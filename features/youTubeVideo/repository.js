const debug = require('debug')('express:login');
const knex = require('../../db');
const download = require('image-downloader');
const path = require('path');
const fs = require('fs');
const __basedir = path.resolve();
let date_ob = new Date();
let date = ('0' + date_ob.getDate()).slice(-2);
let month = ('0' + (date_ob.getMonth() + 1)).slice(-2);
let year = date_ob.getFullYear();
// const mainfolderName = __basedir + '/resources/video/uploads/' + year + month + date
// const folderName = __basedir + '/resources/video/uploads/' + year + month + date + '/thumbnail';
const mainfolderName = '../resources/video/uploads/' + year + month + date
const folderName = '../resources/video/uploads/' + year + month + date + '/thumbnail';

async function createYoutubeVideo(req, res) {
  if (!fs.existsSync(mainfolderName)) {
    fs.mkdirSync(mainfolderName);
    if (!fs.existsSync(folderName)) {
      fs.mkdirSync(folderName);
    }
  }
  const filename = req.youtube_videoId + '_thumbnail.png';

  const finduserId = {
    id: req.userId,
  };
  const findVideoId = {
    filename: req.youtube_videoId,
  };

  const checkUserId = await knex('users')
    .select('*')
    .where(finduserId)
    .limit(1);
  // debug(checkUserId.length);
  if (checkUserId.length === 1) {
    const checkVideoId = await knex('videos')
      .select('*')
      .where(findVideoId)
      .limit(1);

    if (checkVideoId.length === 0) {
      const options = {
        url: req.thumbnail_url,
        dest: folderName + '/' + filename
      }

      const youtubeinfo = await download.image(options);
      if (youtubeinfo) {
        const value = {
          patch: filename,
          title: req.title,
          userid: req.userId,
          thumbnail_url: req.thumbnail_url,
          provider_name: "YouTube",
          destination: year + month + date,
          filename: req.youtube_videoId,
          created_by: req.userId,
          description: req.description,
          created_at: new Date(),
          updated_at: new Date(),
        };
        const [video] = await knex('videos')
          .insert(value)
          .returning('id');
        if (Array.isArray(req.tags)) {
          req.tags.map(row => {
            return createVideoTage({
              title_tags: row,
              video_id: video,
              created_by: req.userId,
              created_at: new Date(),
              updated_at: new Date(),
            })
          });
        } else {
          return createVideoTage({
            title_tags: req.tags,
            video_id: video,
            created_by: req.userId,
            created_at: new Date(),
            updated_at: new Date(),
          });
        }
        return { video: video, checkUserId: checkUserId.length };
      }else{
        return { code: '23505' };
      }
    } else {
      return { code: '23404' };
    }
  } else {
    return { code: '23505' };
  }
}

async function createVideoTage(tags) {
  const videoTag = await knex('videos_tags').insert(tags);
  return videoTag;
};

module.exports = {
  createYoutubeVideo,
};
