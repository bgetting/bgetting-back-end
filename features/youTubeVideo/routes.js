const { wrap } = require('async-middleware');
const router = require('express').Router();

const requestBodyValidation = require('./commands/verify-request-body');
const createYoutubeVideo = require('./commands/create-video');
const  authJwt  = require("../../middlewares/authJwt");

router.post('/', wrap(authJwt.verifyToken), wrap(authJwt.verifySecretHeader), wrap(requestBodyValidation), wrap(createYoutubeVideo));

module.exports = router;
