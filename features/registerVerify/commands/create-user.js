const debug = require('debug')('express:login');
const registerRepo = require('../repository');

async function createUser(req, res) {
  let user = {};
  const SuccessMessage = 'You have successfully send';
  try {
    user = await registerRepo.createUser(req.body);
  } catch (error) {
    user = error;
  }
  if (user.checkPhone) {
    return res.send({ success: true, messages: { success: SuccessMessage } });
  }
  const databaseError =
  user.code === '23505' ? 'The user has already create.' : 'Something went wrong.';
  return res.status(500).send({ success: false, messages:  databaseError });
}

async function createCodeForReset(req, res) {
  let user = {};
  const SuccessMessage = 'You have successfully send';
  try {
    user = await registerRepo.createCodeForReset(req.body);
  } catch (error) {
    user = error;
  }
  if (user.checkPhone) {
    return res.send({ success: true, messages: { success: SuccessMessage } });
  }
  const databaseError =
  user.code === '23505' ? 'This email has not been registered' : 'Something went wrong.';
  return res.status(500).send({ success: false, messages:  databaseError });
}

async function createUserVerify(req, res) {
  let user = {};
  const SuccessMessage = 'You have successfully verify';
  try {
    user = await registerRepo.createUserVerify(req.body);
  } catch (error) {
    user = error;
  }
  debug(user);
  if (user.checkPhone) {
    return res.send({ success: true, messages: { success: SuccessMessage } });
  }
  const error =
  user.code === '23505' ? 'That code is wrong' : 'Something went wrong.';
  return res.status(500).send({ success: false, messages: error });
}

module.exports = {
  createUser,
  createUserVerify,
  createCodeForReset,
};

