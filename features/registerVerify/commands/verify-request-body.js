const Joi = require('joi');
const debug = require('debug')('express:login');
const constants = require('../constants');

const {
  PHONE_NUMBER_MIN,
  PHONE_NUMBER_MAX,
} = constants;

const schema = Joi.object().keys({
  phoneNumber: Joi.string()
    // .regex(/^[0-9]{10}$/)
    .min(PHONE_NUMBER_MIN)
    .max(PHONE_NUMBER_MAX)
    .required(),
  code: Joi.string()
    .min(4)
    .max(4)
    .required()
});

async function requestVerifyBodyValidation(req, res, next) {

  let payloadValidation;
  try {
    payloadValidation = await Joi.validate({ phoneNumber: req.body.phoneNumber, code: req.body.code }, schema, { abortEarly: false });
  } catch (validateRegisterError) {
    payloadValidation = validateRegisterError;
  }
  const { details } = payloadValidation;
  let errors;
  // debug(details)
  if (details) {
    errors = {};
    details.forEach(errorDetail => {
      errors = errorDetail.message;
    });
  }

  if (errors) {
    return res.status(400).send({ success: false, messages:  errors  });
  }
  return next();
}

module.exports = requestVerifyBodyValidation;
