const Joi = require('joi');
const debug = require('debug')('express:login');
const constants = require('../constants');

const {
  PHONE_NUMBER_MIN,
  PHONE_NUMBER_MAX,
} = constants;

const schema = Joi.object().keys({
  provider: Joi.number().required() ,
  phoneNumber: Joi.string()
    // .regex(/^[0-9]{10}$/)
    .min(PHONE_NUMBER_MIN)
    .max(PHONE_NUMBER_MAX)
    .required()  
});

async function requestSendBodyValidation(req, res, next) {

  let payloadValidation;
  try {
    payloadValidation = await Joi.validate({ provider: req.body.provider, phoneNumber: req.body.phoneNumber }, schema, { abortEarly: false });
    // debug(payloadValidation)
  } catch (validateRegisterError) {
    payloadValidation = validateRegisterError;
  }
  const { details } = payloadValidation;
  let errors;
  // debug(details)
  if (details) {
    errors = {};
    details.forEach(errorDetail => {
      errors = errorDetail.message;
    });
  }

  if (errors) {
    return res.status(400).send({ success: false, messages: errors });
  }
  return next();
}

module.exports = requestSendBodyValidation;
