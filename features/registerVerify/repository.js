const debug = require('debug')('express:login');
// const bcrypt = require('bcrypt');
const knex = require('../../db');
const sendSms = require('../../config/twilio');
const nodemailer = require('nodemailer');

function generate(min, max) {
  return Math.floor(
    Math.random() * (max - min + 1) + min
  )
}

async function createUser(req, res) {
  const { provider, phoneNumber } = req;
  const generateCode = generate(1111, 9999);
  const value = {
    phone_number: phoneNumber.toLowerCase(),
    code: generateCode,
    status: false,
    created_at: new Date(),
    updated_at: new Date(),
  };
  const Phone = {
    phone_number: phoneNumber.toLowerCase(),
  };

  if (provider === 1) {
    phoneEmail = {
      phone_number: phoneNumber.toLowerCase(),
    };
  } else if (provider === 2) {
    phoneEmail = {
      email_address: phoneNumber.toLowerCase(),
    };
  }

  const checkPhone = await knex('users')
    .select('id', 'email_address', 'phone_number')
    .where(phoneEmail)
    .limit(1);
  if (checkPhone.length === 0) {
    // debug( value.code);
    const welcomeMessage = `BGetting app verification code: ${value.code}`;
    const welcomeMessageEgmail = `BGetting app verification code: <a href="#">${value.code}</a>`;
    /*auth: {
      user: 'bgettingkh@gmail.com',
      pass: 'P@ssw0rd097'
    }*/
    if (provider === 1) {
      sendSms(phoneNumber, welcomeMessage);
    } else if (provider === 2) {
      var transporter = nodemailer.createTransport({
        service: 'gmail',
        port: 465,
        secure: true,
        auth: {
          user: 'yunsariem097@gmail.com',
          pass: 'gbxkxpocatjxihdn'
        },
        tls: {
          rejectUnauthorized: false
        }
      });
      var mailOptions = {
        from: 'yunsariem097@gmail.com',
        to: phoneNumber.toLowerCase(),
        subject: welcomeMessage,
        html: welcomeMessageEgmail
      };
      transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
          console.log(error);
        } else {
          console.log('Email sent: ' + info.response);
        }
      });
    }
    const checkPhoneVeriry = await knex('register_verify').select('*').where(Phone).limit(1);
    if (checkPhoneVeriry && checkPhoneVeriry.length === 0) {
      const user = await knex('register_verify')
        .insert(value)
        .returning('id');
      return { user, checkPhone };
    } else {
      const valueUpdate = {
        code: generateCode,
        resend: checkPhoneVeriry[0].resend + 1,
        status: false,
        updated_at: new Date(),
      };
      const user = await knex('register_verify')
        .update(valueUpdate)
        .where(Phone)
      return { user, checkPhone };
    }
  } else {
    return { code: '23505' };
  }
}

async function createCodeForReset(req, res) {
  const { phoneNumber, provider } = req;
  const generateCode = generate(1111, 9999);
  const value = {
    phone_number: phoneNumber.toLowerCase(),
    code: generateCode,
    status: false,
    created_at: new Date(),
    updated_at: new Date(),
  };
  if (provider === 1) {
    phoneEmail = {
      phone_number: phoneNumber.toLowerCase(),
    };
  } else if (provider === 2) {
    phoneEmail = {
      email_address: phoneNumber.toLowerCase(),
    };
  }

  const checkPhone = await knex('users')
    .select('id', 'email_address', 'phone_number')
    .where(phoneEmail)
    .limit(1);
  if (checkPhone.length === 1) {
    // debug( value.code);
    const welcomeMessage = `BGetting app verification code: ${value.code}`;
    const welcomeMessageEgmail = `BGetting app verification code: <a href="#">${value.code}</a>`;    

    if (provider === 1) {
      sendSms(phoneNumber, welcomeMessage);
    } else if (provider === 2) {
      var transporter = nodemailer.createTransport({
        service: 'gmail',
        port: 465,
        secure: true,
        auth: {
          user: 'yunsariem097@gmail.com',
          pass: 'gbxkxpocatjxihdn'
        },
        tls: {
          rejectUnauthorized: false
        }
      });
      var mailOptions = {
        from: 'yunsariem097@gmail.com',
        to: phoneNumber.toLowerCase(),
        subject: welcomeMessage,
        html: welcomeMessageEgmail
      };
      transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
          console.log(error);
        } else {
          console.log('Email sent: ' + info.response);
        }
      });
    }

    const checkPhoneVeriry = await knex('register_verify').select('*').where('phone_number', phoneNumber.toLowerCase()).limit(1);  
    if (checkPhoneVeriry && checkPhoneVeriry.length === 0) {
      const user = await knex('register_verify')
        .insert(value)
        .returning('id');
      return { user, checkPhone };
    } else {
      const valueUpdate = {
        code: generateCode,
        resend: checkPhoneVeriry[0].resend + 1,
        status: false,
        updated_at: new Date(),
      };
      const user = await knex('register_verify')
        .update(valueUpdate)
        .where('phone_number', phoneNumber.toLowerCase())
      return { user, checkPhone };
    }
  } else {
    return { code: '23505' };
  }
}

async function createUserVerify(req, res) {
  const { phoneNumber, code } = req;
  const value = {
    status: true,
    updated_at: new Date(),
  };
  const Phone = {
    phone_number: phoneNumber.toLowerCase(),
    code: code
  };
  const checkPhone = await knex('register_verify')
    .select('*')
    .where(Phone)
    .limit(1);
  if (checkPhone.length === 1) {
    const user = await knex('register_verify')
      .update(value)
      .where(Phone)
    return { user, checkPhone };
  } else {
    return { code: '23505' };
  }
}

module.exports = {
  createUser,
  createUserVerify,
  createCodeForReset,
};
