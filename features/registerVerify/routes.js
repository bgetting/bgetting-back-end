const { wrap } = require('async-middleware');
const router = require('express').Router();

const requestSendBodyValidation = require('./commands/send-request-body');
const requestVerifyBodyValidation = require('./commands/verify-request-body');
const {createUser, createUserVerify, createCodeForReset} = require('./commands/create-user');
const  authJwt  = require("../../middlewares/authJwt");

router.post('/send', wrap(authJwt.verifySecretHeader) ,wrap(requestSendBodyValidation), wrap(createUser));
router.post('/send-reset', wrap(authJwt.verifySecretHeader) ,wrap(requestSendBodyValidation), wrap(createCodeForReset));
router.post('/verify', wrap(authJwt.verifySecretHeader) ,wrap(requestVerifyBodyValidation), wrap(createUserVerify));
module.exports = router;
