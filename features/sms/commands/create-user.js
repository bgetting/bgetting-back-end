const debug = require('debug')('express:login');
const registerRepo = require('../repository');
const sendSms = require('../../../config/twilio');

async function createUser(req, res) {
  // let user = {};
  // const registerSuccessMessage = 'You have successfully registered, you can now log in.';
  // try {
  //   user = await registerRepo.createUser(req.body);  
  // } catch (error) {
  //   user = error;
  // }
  // if (user.checkMailPhone) {
  //   return res.send({ success: true, messages: registerSuccessMessage });
  // }
  // const databaseError =
  // user.code === '23505' ? 'The user has already create.' : 'Something went wrong.';
  // return res.status(500).send({ success: false, messages:  databaseError  });
  const { phone } = req.body;
  const welcomeMessage = 'Welcome to Chillz! Your verification code is 54875';

  sendSms(phone, welcomeMessage);

  res.status(201).send({
    message: 'Account created successfully, kindly check your phone to activate your account!'
  })
}

async function test_url(req, res) {
  return res.send({
    message: 'Account created successfully, kindly check your phone to activate your account!'
  })
}

module.exports = test_url;
