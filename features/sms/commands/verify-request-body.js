const Joi = require('joi');
const debug = require('debug')('express:login');
const constants = require('../constants');

const {
  NAME_MIN,
  NAME_MAX,
  PASSWORD_MAX,
  PASSWORD_MIN,
  EMAIL,
  PHONE_NUMBER,
  PHONE_NUMBER_MIN,
  PHONE_NUMBER_MAX,
} = constants;

const schema = Joi.object().keys({
  provider: Joi.number().required(),
  // username: Joi.string()
  //   .min(NAME_MIN)
  //   .max(NAME_MAX)
  //   .required(),
  password: Joi.string()
    .min(PASSWORD_MIN)
    .max(PASSWORD_MAX)
    .required(),
  userNameProvider: Joi.string()
    .when('provider', {
      is: PHONE_NUMBER,
      then: Joi.string()
        // .regex(/^[0-9]{10}$/)
        .min(PHONE_NUMBER_MIN)
        .max(PHONE_NUMBER_MAX)
        .required(),
    })
    .when('provider', {
      is: EMAIL,
      then: Joi.string()
        .email({ minDomainAtoms: 2 })
        .required(),
    })
});

async function validateRegisterPayload(req, res, next) {
  
  let payloadValidation;
  try {
    payloadValidation = await Joi.validate({ provider: req.body.provider, userNameProvider: req.body.userNameProvider, password: req.body.password }, schema, { abortEarly: false });
  // debug(payloadValidation)
  } catch (validateRegisterError) {
    payloadValidation = validateRegisterError;
  }
  const { details } = payloadValidation;
  let errors;
  // debug(details)
  if (details) {
    errors = {};
    details.forEach(errorDetail => {
      errors = errorDetail.message;
    });
  }

  if (errors) {
    return res.status(400).send({ success: false, messages: errors });
  }
  return next();
}

module.exports = validateRegisterPayload;
