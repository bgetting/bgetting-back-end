const { wrap } = require('async-middleware');
const router = require('express').Router();

const requestBodyValidation = require('./commands/verify-request-body');
const test_url = require('./commands/create-user');
const  authJwt  = require("../../middlewares/authJwt");

// router.post('/'/*, wrap(authJwt.verifySecret) ,wrap(requestBodyValidation), wrap(createUser)*/);
router.get('/', wrap(test_url));
module.exports = router;
