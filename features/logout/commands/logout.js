const debug = require('debug')('express:login');
const knex = require('../../../db');
const {
  NO_TOKEN_PRIVIDED,
} = require('../.././../constants/authJwtConstants');

async function logout(req, res) {
  const { accessToken } = req.body;
  if (!accessToken) {
    return res.status(403).send({
      message: NO_TOKEN_PRIVIDED,
    });
  }
  const [user] = await knex('log_users')
    .where('access_token', accessToken)
    .select('*');
  if (user) {
    await knex('log_users')
      .where('id', user.id)
      .del()
    req.logout();
    if (req.session) {
      req.session.destroy(() => {
        res.clearCookie(process.env.SESSION_COOKIE_NAME);
        res.status(301).send({ message: false });
      });
    }
    res.status(301).send({ message: true });
  } else {
    res.status(301).send({ message: NO_TOKEN_PRIVIDED });
  }

}

module.exports = {
  logout,
};
