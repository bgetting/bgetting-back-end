const debug = require('debug')('express:login');
const util = require('util');
const fs = require('fs');
const uniqid = require('uniqid');
const multer = require('multer');
const path = require('path');
// const ffmpeg = require('fluent-ffmpeg');
// const ffmpeg_static = require('ffmpeg-static');

const maxSize = 1024 * 1024 * 1024;
const __basedir = path.resolve();
let date_ob = new Date();
let date = ('0' + date_ob.getDate()).slice(-2);
let month = ('0' + (date_ob.getMonth() + 1)).slice(-2);
let year = date_ob.getFullYear();
// const folderName = __basedir + '/resources/video/uploads/' + year + month + date;
const folderName = '../resources/video/uploads/' + year + month + date;

let storage = multer.diskStorage({
  destination: (req, file, cb) => {
    if (!fs.existsSync(folderName) && fs.mkdirSync(folderName)) {
      fs.mkdirSync(folderName);
    } else {
      if(file.mimetype == 'video/x-ms-wmv' || file.mimetype == 'video/mp4' || file.mimetype == 'video/x-msvideo'){
        cb(null, folderName);
      }else{
        cb('Only .wmv, .mp4 and .avi format allowed!');
      }
    }
  },
  filename: (req, file, cb) => {

    cb(null, Date.now() + '_' + uniqid() + '-' + file.originalname.replace(/\s/g, ''));
  }

});

let uploadFile = multer({
  storage: storage,
  limits: { fileSize: maxSize },
}).single('file');
debug(uploadFile)
let uploadFileMiddleware = util.promisify(uploadFile);
module.exports = uploadFileMiddleware;
