const debug = require('debug')('express:login');
const uploadFile = require('../middleware/upload');
const ffmpeg = require('fluent-ffmpeg');

const { DATA_NOT_FOUND } = require('../constants');
const {
  createVideo,
  downloadFiles,
  selectFilter,
  VideoHottesTopic,
  selectWatchFilter,
  selectChannelFilter,
  selectFilterPopular,
  selectFilterRecommendTags,
  selectFilterLikeUnlike,
  selectFilterLikeUnlikeView,
  selectVideosHistoryFilter,
  selectSearchTitleFilter,
  selectDeleteVideo,
  selectSearchVideoFilter,
  selectSubscribe,
  selectSubscribeCheck,
  selectChannelVideoInfo,
  selectFeedback,
  selectSlideExplore,
  selectExplore,
  selectVideoHistoryRecored,
  selectVideoPlaylist,
  selectFilterVidePlaylist,
  selectFilterVidePlaylistWatch,
  selectFilterVodepPlayList,
  selectDeleteVideoPlaylist,
} = require('../repository');

const message = (req, res) => {
  return res.send({
    status: '404',
    error: 'NOT_FOUND',
    message: DATA_NOT_FOUND,
    path: req.url,
  });
};

function seconds2time(seconds) {
  seconds = Math.floor(seconds, -1);
  var hours = Math.floor(seconds / 3600);
  var minutes = Math.floor((seconds - (hours * 3600)) / 60);
  var seconds = seconds - (hours * 3600) - (minutes * 60);
  var time = "";

  if (hours != 0) {
    time = hours + ":";
  }
  if (minutes != 0 || time !== "") {
    minutes = (minutes < 10 && time !== "") ? "0" + minutes : String(minutes);
    time += minutes + ":";
  }
  if (time === "") {
    time = seconds + "s";
  }
  else {
    time += (seconds < 10) ? "0" + seconds : String(seconds);
  }
  return time;
}



const findAll = async (req, res) => {  
  const data = await selectFilter(req);
  if (data) {
    if (data != 0) {
      return res.send(data);
    } else {
      return message(req, res);
    }
  } else {
    return message(req, res);
  }
};

const findVideoPlayList = async (req, res) => {  
  const data = await selectFilterVodepPlayList(req);
  if (data) {
    if (data != 0) {
      return res.send(data);
    } else {
      return message(req, res);
    }
  } else {
    return message(req, res);
  }
};

const findVideoPlaylist = async (req, res) => {  
  const data = await selectFilterVidePlaylist(req);
  if (data) {
    if (data != 0) {
      return res.send(data);
    } else {
      return message(req, res);
    }
  } else {
    return message(req, res);
  }
};

const findVideoPlaylistWatch = async (req, res) => {  
  const data = await selectFilterVidePlaylistWatch(req);
  if (data) {
    if (data != 0) {
      return res.send(data);
    } else {
      return message(req, res);
    }
  } else {
    return message(req, res);
  }
};

const findVideoHottesTopic = async (req, res) => {  
  const data = await VideoHottesTopic(req);
  if (data) {
    if (data != 0) {
      return res.send(data);
    } else {
      return message(req, res);
    }
  } else {
    return message(req, res);
  }
};

const findPopular = async (req, res) => {  
  const data = await selectFilterPopular(req);
  if (data) {
    if (data != 0) {
      return res.send(data);
    } else {
      return message(req, res);
    }
  } else {
    return message(req, res);
  }
};

const findRecommendTags = async (req, res) => {  
  const data = await selectFilterRecommendTags(req);
  if (data) {
    if (data != 0) {
      return res.send(data);
    } else {
      return message(req, res);
    }
  } else {
    return message(req, res);
  }
};

const findLikeUnlike = async (req, res) => {   
  const data = await selectFilterLikeUnlike(req);
  if (data) {
    if (data != 0) {
      return res.send(data);
    } else {
      return message(req, res);
    }
  } else {
    return message(req, res);
  }
};

const findVideoHistoryRecored = async (req, res) => {   
  const data = await selectVideoHistoryRecored(req);
  if (data) {
    if (data != 0) {
      return res.send(data);
    } else {
      return message(req, res);
    }
  } else {
    return message(req, res);
  }
};

const findLikeUnlikeView = async (req, res) => {   
  const data = await selectFilterLikeUnlikeView(req);
  if (data) {
    if (data != 0) {
      return res.send(data);
    } else {
      return message(req, res);
    }
  } else {
    return message(req, res);
  }
};

const findWatch = async (req, res) => {
  // debug(req);
  const data = await selectWatchFilter(req);
  if (data) {
    if (data != 0) {
      return res.send(data);
    } else {
      return message(req, res);
    }
  } else {
    return message(req, res);
  }
};

const findChannel = async (req, res) => {
  // debug(req);
  const data = await selectChannelFilter(req);
  if (data) {
    if (data != 0) {
      return res.send(data);
    } else {
      return message(req, res);
    }
  } else {
    return message(req, res);
  }
};

const findVideoHistory = async (req, res) => {
  // debug(req);
  const data = await selectVideosHistoryFilter(req);
  if (data) {
    if (data != 0) {
      return res.send(data);
    } else {
      return message(req, res);
    }
  } else {
    return message(req, res);
  }
};

const uploadVideo = async (req, res) => {  
  try {
    await uploadFile(req, res);
    if (req.file == undefined) {
      return res.status(400).send({ message: 'Please upload a file!' });
    }

    // await ffmpeg(req.file.path)
    //   .audioCodec('libmp3lame')
    //   .videoCodec('libx264')
    //   .output(req.file.destination + '/' + req.file.filename + '_quality.mp4')
    //   .on('end', () => {
    //     debug('convert is done');
    //   })
    //   .on('error', err => {
    //     debug('Error', err);
    //   })
    //   .run();

    if (req.file.originalname) {
      await createVideo(req);
    }

    res.status(200).send({
      message: 'Uploaded the file successfully: ' + req.file.originalname,
    });
    debug(JSON.stringify(req.file));
  } catch (err) {
    if (err.code == 'LIMIT_FILE_SIZE') {
      return res.status(500).send({
        message: 'File size cannot be larger than 1GB!',
      });
    }

    res.status(500).send({
      message: `Could not upload the file. ${err}`,
    });
  }
};

const downloadFile = async (req, res) => {
  const fileName = req.params.name;
  const data = await downloadFiles(fileName);
  if (data) {
    res.download(data.patch, fileName, err => {
      if (err) {
        return message(req, res);
      }
    });
  } else {
    return message(req, res);
  }
};

const findSearchTitle = async (req, res) => {  
  const data = await selectSearchTitleFilter(req);
  if (data) {
    if (data != 0) {
      return res.send(data);
    } else {
      return message(req, res);
    }
  } else {
    return message(req, res);
  }
};

const findDeleteVideo = async (req, res) => {  
  const data = await selectDeleteVideo(req);
  if (data) {
    if (data != 0) {
      return res.send(data);
    } else {
      return message(req, res);
    }
  } else {
    return message(req, res);
  }
};

const findDeleteVideoPlaylist = async (req, res) => {  
  const data = await selectDeleteVideoPlaylist(req);
  if (data) {
    if (data != 0) {
      return res.send(data);
    } else {
      return message(req, res);
    }
  } else {
    return message(req, res);
  }
};

const findSearchVideo = async (req, res) => {  
  const data = await selectSearchVideoFilter(req);
  if (data) {
    if (data != 0) {
      return res.send(data);
    } else {
      return message(req, res);
    }
  } else {
    return message(req, res);
  }
};

const findSubscribe = async (req, res) => {
  const data = await selectSubscribe(req);
  if (data) {
    if (data != 0) {
      return res.send(data);
    } else {
      return message(req, res);
    }
  } else {
    return message(req, res);
  }
};

const findSubscribeCheck = async (req, res) => {
  const data = await selectSubscribeCheck(req);
  if (data) {
    if (data != 0) {
      return res.send(data);
    } else {
      return message(req, res);
    }
  } else {
    return message(req, res);
  }
};

const findChannelVideoInfo = async (req, res) => {
  const data = await selectChannelVideoInfo(req);
  if (data) {
    if (data != 0) {
      return res.send(data);
    } else {
      return message(req, res);
    }
  } else {
    return message(req, res);
  }
};

const findFeedback = async (req, res) => {
  const data = await selectFeedback(req);
  if (data) {
    if (data != 0) {
      return res.send(data);
    } else {
      return message(req, res);
    }
  } else {
    return message(req, res);
  }
};

const findSlideExplore = async (req, res) => {
  const data = await selectSlideExplore(req);
  if (data) {
    if (data != 0) {
      return res.send(data);
    } else {
      return message(req, res);
    }
  } else {
    return message(req, res);
  }
};

const findExplore = async (req, res) => {  
  const data = await selectExplore(req);
  if (data) {
    if (data != 0) {
      return res.send(data);
    } else {
      return message(req, res);
    }
  } else {
    return message(req, res);
  }
};

const findAddPlaylist = async (req, res) => {  
  const data = await selectVideoPlaylist(req);
  if (data) {
    if (data != 0) {
      return res.send(data);
    } else {
      return message(req, res);
    }
  } else {
    return message(req, res);
  }
};

const redirect = {
  findAll: findAll,
  findVideoHottesTopic: findVideoHottesTopic,
  uploadVideo: uploadVideo,
  downloadFile: downloadFile,
  findWatch: findWatch,
  findChannel: findChannel,
  findPopular: findPopular,
  findRecommendTags: findRecommendTags,
  findLikeUnlike:findLikeUnlike,
  findVideoHistoryRecored:findVideoHistoryRecored,
  findLikeUnlikeView: findLikeUnlikeView,
  findVideoHistory: findVideoHistory,
  findSearchTitle: findSearchTitle,
  findDeleteVideo: findDeleteVideo,
  findSearchVideo: findSearchVideo,
  findSubscribe: findSubscribe,
  findSubscribeCheck: findSubscribeCheck,
  findChannelVideoInfo: findChannelVideoInfo,
  findFeedback: findFeedback,
  findSlideExplore: findSlideExplore,
  findExplore: findExplore,
  findAddPlaylist: findAddPlaylist,
  findVideoPlaylist: findVideoPlaylist,
  findVideoPlaylistWatch: findVideoPlaylistWatch,
  findVideoPlayList: findVideoPlayList,
  findDeleteVideoPlaylist: findDeleteVideoPlaylist,
};
module.exports = redirect;
