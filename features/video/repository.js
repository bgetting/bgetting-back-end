const debug = require('debug')('express:login');
const Knex = require('knex');
const bcrypt = require('bcrypt');
const getVideoInfo = require('get-video-info-url');
let date_ob = new Date();
let date = ('0' + date_ob.getDate()).slice(-2);
let month = ('0' + (date_ob.getMonth() + 1)).slice(-2);
let year = date_ob.getFullYear();
const knexConfig = require('../../db/knexfile');
const ffmpeg = require('fluent-ffmpeg');
const ffmpeg_static = require('ffmpeg-static');
const {
  INTERNAL_SERVER_ERROR,
  SUCCESSFULLY_CREATE,
  FETCH_INFO_ERROR_MESSAGE,
} = require('./constants');

const knex = Knex(knexConfig[process.env.NODE_ENV]);
const getPagination = (page, size) => {
  const limit = size ? +size : 10;
  const offset = page ? (page - 1) * limit : 0;

  return { limit, offset };
};

const getPagingData = (data, page, limit, count, recommended = {}) => {
  const currentPage = page ? +page : 0;
  const totalOffset = Math.ceil(currentPage / limit - 1);
  return {
    hasNext: true,
    limit: limit,
    offset: totalOffset,
    page: currentPage,
    total: count,
    data,
    recommended
  };
};

buildFilter = filters => {
  const query = {};
  for (let keys in filters) {
    if (filters[keys].length > 0) {
      query[keys] = filters[keys];
    }
  }
  return query;
};

filterData = (data, query) => {
  const filteredData = data.filter(item => {
    for (let key in query) {
      if (item[key] === undefined || !query[key].includes(item[key])) {
        return false;
      }
    }
    return true;
  });
  return filteredData;
};
/** end pagination---------*/
function seconds2time(seconds) {
  seconds = Math.floor(seconds, -1);
  var hours = Math.floor(seconds / 3600);
  var minutes = Math.floor((seconds - (hours * 3600)) / 60);
  var seconds = seconds - (hours * 3600) - (minutes * 60);
  var time = "";

  if (hours != 0) {
    time = hours + ":";
  }
  if (minutes != 0 || time !== "") {
    minutes = (minutes < 10 && time !== "") ? "0" + minutes : String(minutes);
    time += minutes + ":";
  }
  if (time === "") {
    time = seconds + "s";
  }
  else {
    time += (seconds < 10) ? "0" + seconds : String(seconds);
  }
  return time;
}

async function createVideo(req, res) {
  // const urlVideo = req.protocol + '://' + req.headers.host + '/resources/' + year + month + date + '/' + req.file.filename;
  const getVideo = await getVideoInfo(req.file.path);
  // debug('Test',getVideo.format.duration);
  // debug(seconds2time(getVideo));
  await ffmpeg(req.file.path)
    .setFfmpegPath(ffmpeg_static)
    .screenshots({
      timestamps: [getVideo.format.duration / 2],
      size: '480x360',
      filename: req.file.filename + '_thumbnail.png',
      folder: req.file.destination + '/thumbnail',
    })
    .on('end', function () {
      debug('done');
    });

  const value = {
    title: req.body.title,
    userid: req.body.userId,
    patch: req.file.path,
    originalname: req.file.originalname,
    encoding: req.file.encoding,
    mimetype: req.file.mimetype,
    destination: year + month + date,
    filename: req.file.filename,
    size: req.file.size,
    created_by: req.body.userId,
    created_at: new Date(),
    updated_at: new Date(),
    duration: seconds2time(getVideo.format.duration),
    video_info: getVideo,
    provider_name: 'BGetting',
    description: req.body.description
  };
  const [video] = await knex('videos')
    .insert(value)
    .returning('id');
  // debug(Array.isArray(req.body.tags));
  if (Array.isArray(req.body.tags)) {
    req.body.tags.map(row => {
      return createVideoTage({
        title_tags: row,
        video_id: video,
        created_by: req.body.userId,
        created_at: new Date(),
        updated_at: new Date(),
      })
    });
  } else {
    return createVideoTage({
      title_tags: req.body.tags,
      video_id: video,
      created_by: req.body.userId,
      created_at: new Date(),
      updated_at: new Date(),
    });
  }

  return video;
}

async function createVideoTage(tags) {
  const videoTag = await knex('videos_tags').insert(tags);
  return videoTag;
};


async function downloadFiles(query) {
  const [download] = await knex
    .from('videos')
    .select('id', 'patch', 'filename', 'destination', 'status')
    .where('filename', query);
  return download;
}

async function selectFilter(req, res) {
  const page = req.query.page;
  const size = req.query.limit;
  const title = req.query.title ? req.query.title.toLowerCase() : '';
  const video_not_in_id = req.query.video_not_in_id ? req.query.video_not_in_id : null;
  const title_tags = req.query.title_tags ? Array.isArray(req.query.title_tags) ? req.query.title_tags : [req.query.title_tags] : '';
  const provider_name = req.query.provider_name ? req.query.provider_name : '';
  const { rownum, id, username, phone_number, video_id } = req.query;
  const _id = id ? id : '';
  const _video_id = video_id ? video_id : '';
  const _rownum = rownum ? rownum : '';
  const _username = username ? username : '';
  const _phone_number = phone_number ? phone_number : '';

  const filter = {
    id: _id,
    video_id: _video_id,
    rownum: _rownum,
    username: _username,
    phone_number: _phone_number
  };
  const { limit, offset } = getPagination(page, size);
  try {

    if (title_tags) {
      const mydatacount = knex('videos as a')
        .leftJoin('videos_tags as b', 'a.id', 'b.video_id')
        .count('a.id AS count')
        .whereRaw(`LOWER(a.title) LIKE ?`, `%${title}%`)
        .whereIn('b.title_tags', title_tags)
        .where('a.status', true)
        .whereNot('a.id', video_not_in_id)
        .then(data => {
          return data;
        });
      const [restulcount] = await mydatacount;
      const myData = knex('vw_videos_master_list as a')
        .leftJoin('videos_tags as b', 'a.video_id', 'b.video_id')
        .select(['a.rownum'
          , 'a.playlist_id'
          , 'a.title_playlist'
          , 'a.desc_playlist'
          , 'a.video_number'
          , 'a.id'
          , 'a.email_address'
          , 'a.username'
          , 'a.phone_number'
          , 'a.email_verified_at'
          , 'a.user_status'
          , 'a.user_created_at'
          , 'a.image_path'
          , 'a.image_cover_path'
          , 'a.video_id'
          , 'a.video_patch'
          , 'a.thumbnail'
          , 'a.video_status'
          , 'a.title'
          , 'a.filename'
          , 'a.destination'
          , 'a.originalname'
          , 'a.size'
          , 'a.video_created_at'
          , 'a.duration'
          , 'a.time_agos'
          , 'a.provider_name'
          , 'a.views'
          , 'a.subscribe'
          , 'a.description'
          , knex.raw('ARRAY_AGG(b.title_tags)as title_tags')
          , knex.raw(`(select count(id) from videos r where LOWER(title) LIKE '%${title}%' and video_status = true ) as video_total`)
        ])
        .whereRaw(`LOWER(a.title) LIKE ?`, `%${title}%`)
        .whereIn('b.title_tags', title_tags)
        .whereRaw(`a.provider_name LIKE ?`, `%${provider_name}%`)
        .where('a.video_status', true)
        .whereNot('a.video_id', video_not_in_id)
        .orderBy('a.video_id', 'desc')
        .groupBy('a.rownum', 'a.id', 'a.email_address', 'a.username', 'a.phone_number', 'a.email_verified_at', 'a.user_status', 'a.user_created_at'
          , 'a.image_path', 'a.image_cover_path', 'a.video_id', 'a.video_patch', 'a.thumbnail', 'a.video_status', 'a.title', 'a.filename', 'a.destination', 'a.originalname'
          , 'a.size', 'a.video_created_at', 'a.duration', 'a.time_agos', 'a.provider_name', 'a.views', 'a.subscribe', 'a.description', 'a.playlist_id'
          , 'a.title_playlist', 'a.desc_playlist', 'a.video_number')
        .limit(limit)
        .offset(offset)
        .then(data => {
          return data;
        });
      const restul = await myData;

      if (req.query.user_watch_id && req.query.video_watch_id) {
        if (Array.isArray(req.query.title_tags)) {
          req.query.title_tags.map(row => {
            createVideoWatchTage({
              title_tags: row,
              video_watch_id: req.query.video_watch_id,
              user_watch_id: req.query.user_watch_id,
              created_by: req.query.user_watch_id,
              created_at: new Date(),
              updated_at: new Date(),
            })
          });
        } else {
          createVideoWatchTage({
            title_tags: req.query.title_tags,
            video_watch_id: req.query.video_watch_id,
            user_watch_id: req.query.user_watch_id,
            created_by: req.query.user_watch_id,
            created_at: new Date(),
            updated_at: new Date(),
          });
        }

      }
      if (req.query.user_watch_id) {
        video_recommended = knex('vw_videos_watch_tags')
          .select('*')
          .where('user_watch_id', req.query.user_watch_id)
          .limit(10);
      } else {
        video_recommended = knex('vw_videos_no_watch_tags')
          .select('*')
          .limit(10);
      }
      const restul_video_recommended = await video_recommended;

      const keyCount = Number(restulcount.count);
      const query = buildFilter(filter);
      const result = filterData(restul, query);
      const response = getPagingData(result, page, limit, keyCount, restul_video_recommended);
      return response;
    } else {
      const mydatacount = knex('videos as a')
        .count('a.id AS count')
        .whereRaw(`LOWER(a.title) LIKE ?`, `%${title}%`)
        .where('a.status', true)
        .then(data => {
          return data;
        });
      const [restulcount] = await mydatacount;
      const myData = knex('vw_videos_master_list as a')
        .leftJoin('videos_tags as b', 'a.video_id', 'b.video_id')
        .select(['a.rownum'
          , 'a.playlist_id'
          , 'a.title_playlist'
          , 'a.desc_playlist'
          , 'a.video_number'
          , 'a.id'
          , 'a.email_address'
          , 'a.username'
          , 'a.phone_number'
          , 'a.email_verified_at'
          , 'a.user_status'
          , 'a.user_created_at'
          , 'a.image_path'
          , 'a.image_cover_path'
          , 'a.video_id'
          , 'a.video_patch'
          , 'a.thumbnail'
          , 'a.video_status'
          , 'a.title'
          , 'a.filename'
          , 'a.destination'
          , 'a.originalname'
          , 'a.size'
          , 'a.video_created_at'
          , 'a.duration'
          , 'a.time_agos'
          , 'a.provider_name'
          , 'a.views'
          , 'a.subscribe'
          , 'a.description'
          , knex.raw('ARRAY_AGG(b.title_tags)as title_tags')
        ])
        .whereRaw(`LOWER(a.title) LIKE ?`, `%${title}%`)
        .whereRaw(`a.provider_name LIKE ?`, `%${provider_name}%`)
        .where('a.video_status', true)
        .orderBy('a.video_id', 'desc')
        .groupBy('a.rownum', 'a.id', 'a.email_address', 'a.username', 'a.phone_number', 'a.email_verified_at', 'a.user_status', 'a.user_created_at'
          , 'a.image_path', 'a.image_cover_path', 'a.video_id', 'a.video_patch', 'a.thumbnail', 'a.video_status', 'a.title', 'a.filename', 'a.destination', 'a.originalname'
          , 'a.size', 'a.video_created_at', 'a.duration', 'a.time_agos', 'a.provider_name', 'a.views', 'a.subscribe', 'a.description', 'a.playlist_id'
          , 'a.title_playlist', 'a.desc_playlist', 'a.video_number')
        .limit(limit)
        .offset(offset)
        .then(data => {
          return data;
        });

      const restul = await myData;
      if (req.query.user_watch_id) {
        video_recommended = knex('vw_videos_watch_tags')
          .select('*')
          .where('user_watch_id', req.query.user_watch_id)
          .limit(10);
      } else {
        video_recommended = knex('vw_videos_no_watch_tags')
          .select('*')
          .limit(10);
      }
      const restul_video_recommended = await video_recommended;
      const keyCount = Number(restulcount.count);
      debug(keyCount);
      const query = buildFilter(filter);
      const result = filterData(restul, query);

      const response = getPagingData(result, page, limit, keyCount, restul_video_recommended);
      return response;
    }

  } catch (e) {
    debug(e);
  }
}

async function selectFilterVodepPlayList(req, res) {
  const page = req.query.page;
  const size = req.query.limit;
  const title = req.query.title ? req.query.title.toLowerCase() : '';
  const video_not_in_id = req.query.video_not_in_id ? req.query.video_not_in_id : null;
  const playlist_id = req.query.playlist_id ? req.query.playlist_id : null;
  const provider_name = req.query.provider_name ? req.query.provider_name : '';
  const { rownum, id, username, phone_number, video_id } = req.query;
  const _id = id ? id : '';
  const _video_id = video_id ? video_id : '';
  const _rownum = rownum ? rownum : '';
  const _username = username ? username : '';
  const _phone_number = phone_number ? phone_number : '';

  const filter = {
    id: _id,
    video_id: _video_id,
    rownum: _rownum,
    username: _username,
    phone_number: _phone_number
  };
  const { limit, offset } = getPagination(page, size);
  try {
    const mydatacount = knex('vw_videos_master_playlist as a')
      .count('a.playlist_id AS count')
      .whereRaw(`LOWER(a.title) LIKE ?`, `%${title}%`)
      .where('a.playlist_id', playlist_id)
      .where('a.video_status', true)
      .whereNot('a.video_id', video_not_in_id)
      .then(data => {
        return data;
      });
    const [restulcount] = await mydatacount;
    const myData = knex('vw_videos_master_playlist as a')
      .leftJoin('videos_tags as b', 'a.video_id', 'b.video_id')
      .select(['a.rownum'
        , 'a.playlist_id'
        , 'a.title_playlist'
        , 'a.desc_playlist'
        , 'a.video_number'
        , 'a.id'
        , 'a.email_address'
        , 'a.username'
        , 'a.phone_number'
        , 'a.email_verified_at'
        , 'a.user_status'
        , 'a.user_created_at'
        , 'a.image_path'
        , 'a.image_cover_path'
        , 'a.video_id'
        , 'a.video_patch'
        , 'a.thumbnail'
        , 'a.video_status'
        , 'a.title'
        , 'a.filename'
        , 'a.destination'
        , 'a.originalname'
        , 'a.size'
        , 'a.video_created_at'
        , 'a.duration'
        , 'a.time_agos'
        , 'a.provider_name'
        , 'a.views'
        , 'a.subscribe'
        , 'a.description'
        , knex.raw('ARRAY_AGG(b.title_tags)as title_tags')
      ])
      .whereRaw(`LOWER(a.title) LIKE ?`, `%${title}%`)
      .whereRaw(`a.provider_name LIKE ?`, `%${provider_name}%`)
      .where('a.playlist_id', playlist_id)
      .where('a.video_status', true)
      .whereNot('a.video_id', video_not_in_id)
      .orderBy('a.video_id', 'desc')
      .groupBy('a.rownum', 'a.id', 'a.email_address', 'a.username', 'a.phone_number', 'a.email_verified_at', 'a.user_status', 'a.user_created_at'
        , 'a.image_path', 'a.image_cover_path', 'a.video_id', 'a.video_patch', 'a.thumbnail', 'a.video_status', 'a.title', 'a.filename', 'a.destination', 'a.originalname'
        , 'a.size', 'a.video_created_at', 'a.duration', 'a.time_agos', 'a.provider_name', 'a.views', 'a.subscribe', 'a.description', 'a.playlist_id'
        , 'a.title_playlist', 'a.desc_playlist', 'a.video_number')
      .limit(limit)
      .offset(offset)
      .then(data => {
        return data;
      });

    const restul = await myData;
    if (req.query.user_watch_id) {
      video_recommended = knex('vw_videos_watch_tags')
        .select('*')
        .where('user_watch_id', req.query.user_watch_id)
        .limit(10);
    } else {
      video_recommended = knex('vw_videos_no_watch_tags')
        .select('*')
        .limit(10);
    }
    const restul_video_recommended = await video_recommended;
    const keyCount = Number(restulcount.count);
    const query = buildFilter(filter);
    const result = filterData(restul, query);

    const response = getPagingData(result, page, limit, keyCount, restul_video_recommended);
    return response;

  } catch (e) {
    debug(e);
  }
}

async function selectFilterVidePlaylist(req, res) {
  const page = req.query.page;
  const size = req.query.limit;
  const title = req.query.title ? req.query.title.toLowerCase() : '';
  const video_not_in_id = req.query.video_not_in_id ? req.query.video_not_in_id : null;
  const title_tags = req.query.title_tags ? Array.isArray(req.query.title_tags) ? req.query.title_tags : [req.query.title_tags] : '';
  const provider_name = req.query.provider_name ? req.query.provider_name : '';
  const user_id = req.query.user_id ? req.query.user_id : '';
  const { rownum, id, username, phone_number, video_id } = req.query;
  const _video_id = video_id ? video_id : '';
  const _rownum = rownum ? rownum : '';
  const _username = username ? username : '';
  const _phone_number = phone_number ? phone_number : '';

  const filter = {
    video_id: _video_id,
    rownum: _rownum,
    username: _username,
    phone_number: _phone_number
  };
  const { limit, offset } = getPagination(page, size);
  try {

    if (title_tags) {
      const mydatacount = knex('vw_videos_playlist as a')
        .leftJoin('videos_tags as b', 'a.video_id', 'b.video_id')
        .count('a.id AS count')
        .whereRaw(`LOWER(a.title) LIKE ?`, `%${title}%`)
        .whereIn('b.title_tags', title_tags)
        .whereRaw(`a.provider_name LIKE ?`, `%${provider_name}%`)
        .where(`${user_id ? 'a.id' : 'a.video_status'}`, `${user_id ? user_id : true}`)
        .where('a.video_status', true)
        .whereNot('a.video_id', video_not_in_id)
        .then(data => {
          return data;
        });
      const [restulcount] = await mydatacount;
      const myData = knex('vw_videos_playlist as a')
        .leftJoin('videos_tags as b', 'a.video_id', 'b.video_id')
        .select(['a.rownum'
          , 'a.id'
          , 'a.playlist_id'
          , 'a.title_playlist'
          , 'a.desc_playlist'
          , 'a.video_number'
          , 'a.email_address'
          , 'a.username'
          , 'a.phone_number'
          , 'a.email_verified_at'
          , 'a.user_status'
          , 'a.user_created_at'
          , 'a.image_path'
          , 'a.image_cover_path'
          , 'a.video_id'
          , 'a.video_patch'
          , 'a.thumbnail'
          , 'a.video_status'
          , 'a.title'
          , 'a.filename'
          , 'a.destination'
          , 'a.originalname'
          , 'a.size'
          , 'a.video_created_at'
          , 'a.duration'
          , 'a.time_agos'
          , 'a.provider_name'
          , 'a.views'
          , 'a.subscribe'
          , 'a.description'
          , knex.raw('ARRAY_AGG(b.title_tags)as title_tags')
        ])
        .whereRaw(`LOWER(a.title) LIKE ?`, `%${title}%`)
        .whereIn('b.title_tags', title_tags)
        .whereRaw(`a.provider_name LIKE ?`, `%${provider_name}%`)
        .where(`${user_id ? 'a.id' : 'a.video_status'}`, `${user_id ? user_id : true}`)
        .where('a.video_status', true)
        .whereNot('a.video_id', video_not_in_id)
        .orderBy('a.playlist_id', 'desc')
        .groupBy('a.rownum', 'a.id', 'a.email_address', 'a.username', 'a.phone_number', 'a.email_verified_at', 'a.user_status', 'a.user_created_at'
          , 'a.image_path', 'a.image_cover_path', 'a.video_id', 'a.video_patch', 'a.thumbnail', 'a.video_status', 'a.title', 'a.filename', 'a.destination', 'a.originalname'
          , 'a.size', 'a.video_created_at', 'a.duration', 'a.time_agos', 'a.provider_name', 'a.views', 'a.subscribe', 'a.description', 'a.title_playlist'
          , 'a.desc_playlist', 'a.video_number', 'a.playlist_id')
        .limit(limit)
        .offset(offset)
        .then(data => {
          return data;
        });
      const restul = await myData;

      if (req.query.user_watch_id && req.query.video_watch_id) {
        if (Array.isArray(req.query.title_tags)) {
          req.query.title_tags.map(row => {
            createVideoWatchTage({
              title_tags: row,
              video_watch_id: req.query.video_watch_id,
              user_watch_id: req.query.user_watch_id,
              created_by: req.query.user_watch_id,
              created_at: new Date(),
              updated_at: new Date(),
            })
          });
        } else {
          createVideoWatchTage({
            title_tags: req.query.title_tags,
            video_watch_id: req.query.video_watch_id,
            user_watch_id: req.query.user_watch_id,
            created_by: req.query.user_watch_id,
            created_at: new Date(),
            updated_at: new Date(),
          });
        }

      }
      if (req.query.user_watch_id) {
        video_recommended = knex('vw_videos_watch_tags')
          .select('*')
          .where('user_watch_id', req.query.user_watch_id)
          .limit(10);
      } else {
        video_recommended = knex('vw_videos_no_watch_tags')
          .select('*')
          .limit(10);
      }
      const restul_video_recommended = await video_recommended;

      const keyCount = Number(restulcount.count);
      const query = buildFilter(filter);
      const result = filterData(restul, query);
      const response = getPagingData(result, page, limit, keyCount, restul_video_recommended);
      return response;
    } else {
      const mydatacount = knex('vw_videos_playlist as a')
        .leftJoin('videos_tags as b', 'a.video_id', 'b.video_id')
        .count('a.id AS count')
        .whereRaw(`LOWER(a.title) LIKE ?`, `%${title}%`)
        .whereRaw(`a.provider_name LIKE ?`, `%${provider_name}%`)
        .where('a.video_status', true)
        .where(`${user_id ? 'a.id' : 'a.video_status'}`, `${user_id ? user_id : true}`)
        .then(data => {
          return data;
        });
      const [restulcount] = await mydatacount;
      const myData = knex('vw_videos_playlist as a')
        .leftJoin('videos_tags as b', 'a.video_id', 'b.video_id')
        .select(['a.rownum'
          , 'a.id'
          , 'a.playlist_id'
          , 'a.title_playlist'
          , 'a.desc_playlist'
          , 'a.video_number'
          , 'a.email_address'
          , 'a.username'
          , 'a.phone_number'
          , 'a.email_verified_at'
          , 'a.user_status'
          , 'a.user_created_at'
          , 'a.image_path'
          , 'a.image_cover_path'
          , 'a.video_id'
          , 'a.video_patch'
          , 'a.thumbnail'
          , 'a.video_status'
          , 'a.title'
          , 'a.filename'
          , 'a.destination'
          , 'a.originalname'
          , 'a.size'
          , 'a.video_created_at'
          , 'a.duration'
          , 'a.time_agos'
          , 'a.provider_name'
          , 'a.views'
          , 'a.subscribe'
          , 'a.description'
          , knex.raw('ARRAY_AGG(b.title_tags)as title_tags')
        ])
        .whereRaw(`LOWER(a.title) LIKE ?`, `%${title}%`)
        .whereRaw(`a.provider_name LIKE ?`, `%${provider_name}%`)
        .where('a.video_status', true)
        .where(`${user_id ? 'a.id' : 'a.video_status'}`, `${user_id ? user_id : true}`)
        .orderBy('a.playlist_id', 'desc')
        .groupBy('a.rownum', 'a.id', 'a.email_address', 'a.username', 'a.phone_number', 'a.email_verified_at', 'a.user_status', 'a.user_created_at'
          , 'a.image_path', 'a.image_cover_path', 'a.video_id', 'a.video_patch', 'a.thumbnail', 'a.video_status', 'a.title', 'a.filename', 'a.destination', 'a.originalname'
          , 'a.size', 'a.video_created_at', 'a.duration', 'a.time_agos', 'a.provider_name', 'a.views', 'a.subscribe', 'a.description', 'a.title_playlist'
          , 'a.desc_playlist', 'a.video_number', 'a.playlist_id')
        .limit(limit)
        .offset(offset)
        .then(data => {
          return data;
        });

      const restul = await myData;
      if (req.query.user_watch_id) {
        video_recommended = knex('vw_videos_watch_tags')
          .select('*')
          .where('user_watch_id', req.query.user_watch_id)
          .limit(10);
      } else {
        video_recommended = knex('vw_videos_no_watch_tags')
          .select('*')
          .limit(10);
      }
      const restul_video_recommended = await video_recommended;
      const keyCount = Number(restulcount.count);
      const query = buildFilter(filter);
      const result = filterData(restul, query);

      const response = getPagingData(result, page, limit, keyCount, restul_video_recommended);
      return response;
    }

  } catch (e) {
    debug(e);
  }
}

async function selectFilterVidePlaylistWatch(req, res) {
  const page = req.query.page;
  const size = req.query.limit;
  const title = req.query.title ? req.query.title.toLowerCase() : '';
  const video_not_in_id = req.query.video_not_in_id ? req.query.video_not_in_id : null;
  const playlist_id = req.query.playlist_id ? req.query.playlist_id : null;
  const title_tags = req.query.title_tags ? Array.isArray(req.query.title_tags) ? req.query.title_tags : [req.query.title_tags] : '';
  const provider_name = req.query.provider_name ? req.query.provider_name : '';
  const { rownum, id, username, phone_number, video_id } = req.query;
  const _id = id ? id : '';
  const _video_id = video_id ? video_id : '';
  const _rownum = rownum ? rownum : '';
  const _username = username ? username : '';
  const _phone_number = phone_number ? phone_number : '';

  const filter = {
    id: _id,
    video_id: _video_id,
    rownum: _rownum,
    username: _username,
    phone_number: _phone_number
  };
  const { limit, offset } = getPagination(page, size);
  try {

    if (title_tags) {
      const mydatacount = knex('vw_videos_playlist_watch as a')
        .leftJoin('videos_tags as b', 'a.video_id', 'b.video_id')
        .count('a.id AS count')
        .whereRaw(`LOWER(a.title) LIKE ?`, `%${title}%`)
        .where(`a.playlist_id`, playlist_id)
        .whereIn('b.title_tags', title_tags)
        .whereRaw(`a.provider_name LIKE ?`, `%${provider_name}%`)
        .where('a.video_status', true)
        .whereNot('a.video_id', video_not_in_id)
        .then(data => {
          return data;
        });
      const [restulcount] = await mydatacount;
      const myData = knex('vw_videos_playlist_watch as a')
        .leftJoin('videos_tags as b', 'a.video_id', 'b.video_id')
        .select(['a.rownum'
          , 'a.id'
          , 'a.playlist_id'
          , 'a.title_playlist'
          , 'a.desc_playlist'
          , 'a.email_address'
          , 'a.username'
          , 'a.phone_number'
          , 'a.email_verified_at'
          , 'a.user_status'
          , 'a.user_created_at'
          , 'a.image_path'
          , 'a.image_cover_path'
          , 'a.video_id'
          , 'a.video_patch'
          , 'a.thumbnail'
          , 'a.video_status'
          , 'a.title'
          , 'a.filename'
          , 'a.destination'
          , 'a.originalname'
          , 'a.size'
          , 'a.video_created_at'
          , 'a.duration'
          , 'a.time_agos'
          , 'a.provider_name'
          , 'a.views'
          , 'a.subscribe'
          , 'a.description'
          , knex.raw('ARRAY_AGG(b.title_tags)as title_tags')
        ])
        .whereRaw(`LOWER(a.title) LIKE ?`, `%${title}%`)
        .where(`a.playlist_id`, playlist_id)
        .whereIn('b.title_tags', title_tags)
        .whereRaw(`a.provider_name LIKE ?`, `%${provider_name}%`)
        .where('a.video_status', true)
        .whereNot('a.video_id', video_not_in_id)
        .orderBy('a.video_id', 'desc')
        .groupBy('a.rownum', 'a.id', 'a.email_address', 'a.username', 'a.phone_number', 'a.email_verified_at', 'a.user_status', 'a.user_created_at'
          , 'a.image_path', 'a.image_cover_path', 'a.video_id', 'a.video_patch', 'a.thumbnail', 'a.video_status', 'a.title', 'a.filename', 'a.destination', 'a.originalname'
          , 'a.size', 'a.video_created_at', 'a.duration', 'a.time_agos', 'a.provider_name', 'a.views', 'a.subscribe', 'a.description', 'a.title_playlist'
          , 'a.desc_playlist', 'a.playlist_id')
        .limit(limit)
        .offset(offset)
        .then(data => {
          return data;
        });
      const restul = await myData;

      if (req.query.user_watch_id && req.query.video_watch_id) {
        if (Array.isArray(req.query.title_tags)) {
          req.query.title_tags.map(row => {
            createVideoWatchTage({
              title_tags: row,
              video_watch_id: req.query.video_watch_id,
              user_watch_id: req.query.user_watch_id,
              created_by: req.query.user_watch_id,
              created_at: new Date(),
              updated_at: new Date(),
            })
          });
        } else {
          createVideoWatchTage({
            title_tags: req.query.title_tags,
            video_watch_id: req.query.video_watch_id,
            user_watch_id: req.query.user_watch_id,
            created_by: req.query.user_watch_id,
            created_at: new Date(),
            updated_at: new Date(),
          });
        }

      }
      if (req.query.user_watch_id) {
        video_recommended = knex('vw_videos_watch_tags')
          .select('*')
          .where('user_watch_id', req.query.user_watch_id)
          .limit(10);
      } else {
        video_recommended = knex('vw_videos_no_watch_tags')
          .select('*')
          .limit(10);
      }
      const restul_video_recommended = await video_recommended;

      const keyCount = Number(restulcount.count);
      const query = buildFilter(filter);
      const result = filterData(restul, query);
      const response = getPagingData(result, page, limit, keyCount, restul_video_recommended);
      return response;
    } else {
      const mydatacount = knex('vidvw_videos_playlist_watcheos as a')
        .count('a.id AS count')
        .whereRaw(`LOWER(a.title) LIKE ?`, `%${title}%`)
        .where(`a.playlist_id`, playlist_id)
        .whereRaw(`a.provider_name LIKE ?`, `%${provider_name}%`)
        .where('a.video_status', true)
        .then(data => {
          return data;
        });
      const [restulcount] = await mydatacount;
      const myData = knex('vw_videos_playlist_watch as a')
        .leftJoin('videos_tags as b', 'a.video_id', 'b.video_id')
        .select(['a.rownum'
          , 'a.id'
          , 'a.playlist_id'
          , 'a.title_playlist'
          , 'a.desc_playlist'
          , 'a.email_address'
          , 'a.username'
          , 'a.phone_number'
          , 'a.email_verified_at'
          , 'a.user_status'
          , 'a.user_created_at'
          , 'a.image_path'
          , 'a.image_cover_path'
          , 'a.video_id'
          , 'a.video_patch'
          , 'a.thumbnail'
          , 'a.video_status'
          , 'a.title'
          , 'a.filename'
          , 'a.destination'
          , 'a.originalname'
          , 'a.size'
          , 'a.video_created_at'
          , 'a.duration'
          , 'a.time_agos'
          , 'a.provider_name'
          , 'a.views'
          , 'a.subscribe'
          , 'a.description'
          , knex.raw('ARRAY_AGG(b.title_tags)as title_tags')
        ])
        .whereRaw(`LOWER(a.title) LIKE ?`, `%${title}%`)
        .where(`a.playlist_id`, playlist_id)
        .whereRaw(`a.provider_name LIKE ?`, `%${provider_name}%`)
        .where('a.video_status', true)
        .orderBy('a.video_id', 'desc')
        .groupBy('a.rownum', 'a.id', 'a.email_address', 'a.username', 'a.phone_number', 'a.email_verified_at', 'a.user_status', 'a.user_created_at'
          , 'a.image_path', 'a.image_cover_path', 'a.video_id', 'a.video_patch', 'a.thumbnail', 'a.video_status', 'a.title', 'a.filename', 'a.destination', 'a.originalname'
          , 'a.size', 'a.video_created_at', 'a.duration', 'a.time_agos', 'a.provider_name', 'a.views', 'a.subscribe', 'a.description', 'a.title_playlist'
          , 'a.desc_playlist', 'a.playlist_id')
        .limit(limit)
        .offset(offset)
        .then(data => {
          return data;
        });

      const restul = await myData;
      if (req.query.user_watch_id) {
        video_recommended = knex('vw_videos_watch_tags')
          .select('*')
          .where('user_watch_id', req.query.user_watch_id)
          .limit(10);
      } else {
        video_recommended = knex('vw_videos_no_watch_tags')
          .select('*')
          .limit(10);
      }
      const restul_video_recommended = await video_recommended;
      const keyCount = Number(restulcount.count);
      const query = buildFilter(filter);
      const result = filterData(restul, query);

      const response = getPagingData(result, page, limit, keyCount, restul_video_recommended);
      return response;
    }

  } catch (e) {
    debug(e);
  }
}

async function VideoHottesTopic(req, res) {
  const page = req.query.page;
  const size = req.query.limit;
  const title = req.query.title ? req.query.title.toLowerCase() : '';
  const title_tags = req.query.title_tags ? Array.isArray(req.query.title_tags) ? req.query.title_tags : [req.query.title_tags] : '';
  const provider_name = req.query.provider_name ? req.query.provider_name : '';
  const { rownum, id, username, phone_number, video_id } = req.query;
  const _id = id ? id : '';
  const _video_id = video_id ? video_id : '';
  const _rownum = rownum ? rownum : '';
  const _username = username ? username : '';
  const _phone_number = phone_number ? phone_number : '';

  const filter = {
    id: _id,
    video_id: _video_id,
    rownum: _rownum,
    username: _username,
    phone_number: _phone_number
  };
  const { limit, offset } = getPagination(page, size);
  try {

    if (title_tags) {
      const mydatacount = knex('vw_videos_master_list as a')
        .leftJoin('videos_tags as b', 'a.video_id', 'b.video_id')
        .count('a.id AS count')
        .whereRaw(`LOWER(a.title) LIKE ?`, `%${title}%`)
        .whereIn('b.title_tags', title_tags)
        .where('a.video_status', true)
        .then(data => {
          return data;
        });
      const [restulcount] = await mydatacount;
      const myData = knex('vw_videos_master_list as a')
        .leftJoin('videos_tags as b', 'a.video_id', 'b.video_id')
        .select(['a.rownum'
        , 'a.playlist_id'
        , 'a.title_playlist'
        , 'a.desc_playlist'
        , 'a.video_number'
        , 'a.id'
        , 'a.email_address'
        , 'a.username'
        , 'a.phone_number'
        , 'a.email_verified_at'
        , 'a.user_status'
        , 'a.user_created_at'
        , 'a.image_path'
        , 'a.image_cover_path'
        , 'a.video_id'
        , 'a.video_patch'
        , 'a.thumbnail'
        , 'a.video_status'
        , 'a.title'
        , 'a.filename'
        , 'a.destination'
        , 'a.originalname'
        , 'a.size'
        , 'a.video_created_at'
        , 'a.duration'
        , 'a.time_agos'
        , 'a.provider_name'
        , 'a.views'
        , 'a.subscribe'
        , 'a.description'
          , knex.raw('ARRAY_AGG(b.title_tags)as title_tags')
        ])
        .whereRaw(`LOWER(a.title) LIKE ?`, `%${title}%`)
        .whereIn('b.title_tags', title_tags)
        .whereRaw(`a.provider_name LIKE ?`, `%${provider_name}%`)
        .where('a.video_status', true)
        .orderBy('a.video_id', 'desc')
        .groupBy('a.rownum', 'a.id', 'a.email_address', 'a.username', 'a.phone_number', 'a.email_verified_at', 'a.user_status', 'a.user_created_at'
          , 'a.image_path', 'a.image_cover_path', 'a.video_id', 'a.video_patch', 'a.thumbnail', 'a.video_status', 'a.title', 'a.filename', 'a.destination', 'a.originalname'
          , 'a.size', 'a.video_created_at', 'a.duration', 'a.time_agos', 'a.provider_name', 'a.views', 'a.subscribe', 'a.description', 'a.playlist_id'
          , 'a.title_playlist', 'a.desc_playlist', 'a.video_number')
        .limit(limit)
        .offset(offset)
        .then(data => {
          return data;
        });
      const restul = await myData;
      const keyCount = Number(restulcount.count);
      const query = buildFilter(filter);
      const result = filterData(restul, query);
      const response = getPagingData(result, page, limit, keyCount, null);
      return response;
    } else {
      const exploreQL = knex('mt_lov')
        .select(knex.raw('ARRAY_AGG(tags)as tags'))
        .whereNot('tags', null)
        .where('type', 'Topic');
      const restul_explore = await exploreQL;
      const mydatacount = knex('vw_videos_master_list as a')
        .leftJoin('videos_tags as b', 'a.video_id', 'b.video_id')
        .count('a.id AS count')
        .whereRaw(`LOWER(a.title) LIKE ?`, `%${title}%`)
        .whereRaw(`a.provider_name LIKE ?`, `%${provider_name}%`)
        .whereIn('b.title_tags', restul_explore[0].tags)
        .where('a.video_status', true)
        .then(data => {
          return data;
        });
      const [restulcount] = await mydatacount;
      const myData = knex('vw_videos_master_list as a')
        .leftJoin('videos_tags as b', 'a.video_id', 'b.video_id')
        .select(['a.rownum'
        , 'a.playlist_id'
        , 'a.title_playlist'
        , 'a.desc_playlist'
        , 'a.video_number'
        , 'a.id'
        , 'a.email_address'
        , 'a.username'
        , 'a.phone_number'
        , 'a.email_verified_at'
        , 'a.user_status'
        , 'a.user_created_at'
        , 'a.image_path'
        , 'a.image_cover_path'
        , 'a.video_id'
        , 'a.video_patch'
        , 'a.thumbnail'
        , 'a.video_status'
        , 'a.title'
        , 'a.filename'
        , 'a.destination'
        , 'a.originalname'
        , 'a.size'
        , 'a.video_created_at'
        , 'a.duration'
        , 'a.time_agos'
        , 'a.provider_name'
        , 'a.views'
        , 'a.subscribe'
        , 'a.description'
          , knex.raw('ARRAY_AGG(b.title_tags)as title_tags')
        ])
        .whereRaw(`LOWER(a.title) LIKE ?`, `%${title}%`)
        .whereRaw(`a.provider_name LIKE ?`, `%${provider_name}%`)
        .whereIn('b.title_tags', restul_explore[0].tags)
        .where('a.video_status', true)
        .orderBy('a.video_id', 'desc')
        .groupBy('a.rownum', 'a.id', 'a.email_address', 'a.username', 'a.phone_number', 'a.email_verified_at', 'a.user_status', 'a.user_created_at'
          , 'a.image_path', 'a.image_cover_path', 'a.video_id', 'a.video_patch', 'a.thumbnail', 'a.video_status', 'a.title', 'a.filename', 'a.destination', 'a.originalname'
          , 'a.size', 'a.video_created_at', 'a.duration', 'a.time_agos', 'a.provider_name', 'a.views', 'a.subscribe', 'a.description', 'a.playlist_id'
          , 'a.title_playlist', 'a.desc_playlist', 'a.video_number')
        .limit(limit)
        .offset(offset)
        .then(data => {
          return data;
        });

      const restul = await myData;
      const keyCount = Number(restulcount.count);
      const query = buildFilter(filter);
      const result = filterData(restul, query);
      const response = getPagingData(result, page, limit, keyCount, null);
      return response;
    }

  } catch (e) {
    debug(e);
  }
}

async function createVideoWatchTage(tags) {
  const videoTag = await knex('videos_watch_tags').insert(tags);
  return videoTag;
};

async function selectFilterPopular(req, res) {
  const limit = req.query.limit;
  try {
    const myData = knex('vw_videos_master as a')
      .leftJoin('videos_tags as b', 'a.video_id', 'b.video_id')
      .select(['a.rownum'
        , 'a.id'
        , 'a.email_address'
        , 'a.username'
        , 'a.phone_number'
        , 'a.email_verified_at'
        , 'a.user_status'
        , 'a.user_created_at'
        , 'a.image_path'
        , 'a.image_cover_path'
        , 'a.video_id'
        , 'a.video_patch'
        , 'a.thumbnail'
        , 'a.video_status'
        , 'a.title'
        , 'a.filename'
        , 'a.destination'
        , 'a.originalname'
        , 'a.size'
        , 'a.video_created_at'
        , 'a.duration'
        , 'a.time_agos'
        , 'a.provider_name'
        , 'a.views'
        , 'a.subscribe'
        , 'a.description'
        , knex.raw('ARRAY_AGG(b.title_tags)as title_tags')
      ])
      .where('a.video_status', true)
      .orderBy('a.views', 'desc')
      .groupBy('a.rownum', 'a.id', 'a.email_address', 'a.username', 'a.phone_number', 'a.email_verified_at', 'a.user_status', 'a.user_created_at'
        , 'a.image_path', 'a.image_cover_path', 'a.video_id', 'a.video_patch', 'a.thumbnail', 'a.video_status', 'a.title', 'a.filename', 'a.destination', 'a.originalname'
        , 'a.size', 'a.video_created_at', 'a.duration', 'a.time_agos', 'a.provider_name', 'a.views', 'a.subscribe', 'a.description')
      .limit(limit)
      .then(data => {
        return data;
      });

    const restul = await myData;
    const groupData = { popular: restul }
    return groupData;
  } catch (e) {
    debug(e);
  }
}


async function selectFilterRecommendTags(req, res) {
  try {
    if (req.query.user_watch_id) {
      myData = knex('vw_videos_watch_tags')
        .select('*')
        .where('user_watch_id', req.query.user_watch_id)
        .limit(15);
    } else {
      myData = knex('vw_videos_no_watch_tags')
        .select('*')
        .limit(15);
    }

    const restul = await myData;
    const groupData = { data: restul }
    return groupData;
  } catch (e) {
    debug(e);
  }
}

async function selectFilterLikeUnlikeView(req, res) {
  const video_id = req.query.video_id ? req.query.video_id : '';
  const user_id = req.query.user_id ? req.query.user_id : '';
  const data = {
    videoid: video_id,
    userid: user_id
  }
  const datanotlogin = {
    videoid: video_id
  }
  if (video_id && user_id) {
    const mydata = await selectWatchFilterLikeUnlike(data);
    if (mydata.length) {
      // await insertVideoHistory({
      //   video_id: video_id,
      //   user_watch_id: user_id,
      //   created_at: new Date(),
      //   updated_at: new Date()
      // });
      return mydata;
    } else {
      return selectWatchFilterLikeUnlikeNotLogin(datanotlogin);
    }

  } else {
    return selectWatchFilterLikeUnlikeNotLogin(datanotlogin);
  }

}

async function selectFilterLikeUnlike(req, res) {
  const video_id = req.query.video_id ? req.query.video_id : '';
  const user_id = req.query.user_id ? req.query.user_id : '';
  const like = req.query.like ? req.query.like : '';
  const unlike = req.query.unlike ? req.query.unlike : '';
  if (like && unlike) {
    null
  }
  else {
    if (like == 1) {
      const select = await selectVideoLikeUnlike({
        videoid: video_id,
        userid: user_id,
        likes: 1,
      });
      if (select.length == 0) {
        return insertVideoLikeUnlike({
          videoid: video_id,
          userid: user_id,
          likes: 1,
          created_by: user_id,
          created_at: new Date(),
          updated_at: new Date(),
        });
      }
    } else if (like == 2) {
      return deleteVideoLikeUnlike({
        videoid: video_id,
        userid: user_id,
        likes: 1,
      });
    } else if (unlike == 1) {
      const select = await selectVideoLikeUnlike({
        videoid: video_id,
        userid: user_id,
        unlikes: 1,
      });
      if (select.length == 0) {
        return insertVideoLikeUnlike({
          videoid: video_id,
          userid: user_id,
          unlikes: 1,
          created_by: user_id,
          created_at: new Date(),
          updated_at: new Date(),
        });
      }
    } else if (unlike == 2) {
      try {
        return deleteVideoLikeUnlike({
          videoid: video_id,
          userid: user_id,
          unlikes: 1,
        });
      } catch (e) {
        debug(e);
        return e;
      }
    }
  }

}

async function selectVideoHistoryRecored(req, res) {
  const video_id = req.params.video_id ? req.params.video_id : '';
  const user_watch_id = req.params.user_watch_id ? req.params.user_watch_id : '';
  try {
    const myData = await insertVideoHistory({
      video_id: video_id,
      user_watch_id: user_watch_id,
      created_at: new Date(),
      updated_at: new Date()
    });
    if (myData.rowCount === 1) {
      return { status: true, message: "Successfully !" };
    }

  } catch (e) {
    debug(e);
    return e;
  }

}

async function selectWatchFilterLikeUnlikeNotLogin(value) {
  try {
    const myData = knex
      .from('vw_like_unlike_not_login')
      .select('*')
      .where("video_id", value.videoid)
      .then(data => {
        return data;
      });
    const restul = await myData;
    return restul;
  } catch (e) {
    debug(e);
  }
}

async function selectWatchFilterLikeUnlike(value) {
  try {
    const myData = knex
      .from('vw_like_unlike')
      .select('*')
      .where("video_id", value.videoid)
      .where("userid", value.userid)
      .then(data => {
        return data;
      });
    const restul = await myData;
    return restul;
  } catch (e) {
    debug(e);
  }
}

async function selectVideoLikeUnlike(value) {
  const myData = await knex
    .from('videos_action')
    .select('*')
    .where(value)
    .then(data => { return data });
  return myData;
};

async function insertVideoLikeUnlike(value) {
  try {
    const myData = await knex('videos_action').insert(value);
    if (myData.rowCount == 1) {
      return selectWatchFilterLikeUnlike(value);
    } else {
      return null;
    }
  } catch (e) {
    return e;
  }
};

async function insertVideoHistory(value) {
  try {
    const myData = await knex('videos_history').insert(value);
    return myData;
  } catch (e) {
    return e;
  }
};

async function deleteVideoLikeUnlike(value) {
  try {
    const myData = await knex('videos_action')
      .where(value)
      .del();
    if (myData == 1) {
      return [{ status: true, videoid: value.videoid, userid: value.userid }];
    } else {
      return null;
    }
  } catch (e) {
    debug(e);
    return e;
  }
}

async function selectWatchFilter(req, res) {
  let id = req.params.id ? req.params.id : '';
  try {
    const myData = knex
      .from('vw_videos_master')
      .select('*')
      .where('video_id', id)
      .where('video_status', true)
      .then(data => {
        if (data.length == 1) {
          insertVideoView({
            video_id: id
          });
        }
        return data;
      });
    const restul = await myData;
    return restul;
  } catch (e) {
    debug(e);
  }
}

async function insertVideoView(value) {
  const myData = knex
    .from('videos_views')
    .select('*')
    .where('video_id', value.video_id)
    .then(data => {
      return data;
    });
  const restul = await myData;
  const insertValue = {
    video_id: value.video_id,
    views: 1
  }
  if (restul.length == 1) {
    return updateVideoView({
      video_id: value.video_id,
      views: restul[0].views + 1
    });
  } else {
    const videoTag = await knex('videos_views').insert(insertValue);
    return videoTag;
  }

};

async function updateVideoView(value) {
  const myData = await knex('videos_views')
    .where('video_id', value.video_id)
    .update({ views: value.views })
  return myData;
}

async function selectChannelFilter(req, res) {
  const page = req.query.page;
  const size = req.query.limit;
  const id = req.params.id ? req.params.id : '';
  const title = req.query.title ? req.query.title.toLowerCase() : '';
  const { video_created_at } = req.query;
  const _video_created_at = video_created_at ? video_created_at : '';
  const filter = {
    video_created_at: _video_created_at
  };
  const { limit, offset } = getPagination(page, size);
  try {
    const mydatacount = knex('vw_videos_master as a')
      .count('a.id AS count')
      .where('a.id', id)
      .whereRaw(`LOWER(a.title) LIKE ?`, `%${title}%`)
      .where('a.video_status', true)
      .then(data => {
        return data;
      });
    const [restulcount] = await mydatacount;
    const myData = knex('vw_videos_master as a')
      .leftJoin('videos_tags as b', 'a.video_id', 'b.video_id')
      .select(['a.rownum'
        , 'a.id'
        , 'a.email_address'
        , 'a.username'
        , 'a.phone_number'
        , 'a.email_verified_at'
        , 'a.user_status'
        , 'a.user_created_at'
        , 'a.image_path'
        , 'a.image_cover_path'
        , 'a.video_id'
        , 'a.video_patch'
        , 'a.thumbnail'
        , 'a.video_status'
        , 'a.title'
        , 'a.filename'
        , 'a.destination'
        , 'a.originalname'
        , 'a.size'
        , 'a.video_created_at'
        , 'a.duration'
        , 'a.time_agos'
        , 'a.provider_name'
        , 'a.views'
        , 'a.subscribe'
        , 'a.description'
        , knex.raw('ARRAY_AGG(b.title_tags)as title_tags')
      ])
      .where('a.id', id)
      .whereRaw(`LOWER(a.title) LIKE ?`, `%${title}%`)
      .where('a.video_status', true)
      .orderBy('a.video_id', 'desc')
      .groupBy('a.rownum', 'a.id', 'a.email_address', 'a.username', 'a.phone_number', 'a.email_verified_at', 'a.user_status', 'a.user_created_at'
        , 'a.image_path', 'a.image_cover_path', 'a.video_id', 'a.video_patch', 'a.thumbnail', 'a.video_status', 'a.title', 'a.filename', 'a.destination', 'a.originalname'
        , 'a.size', 'a.video_created_at', 'a.duration', 'a.time_agos', 'a.provider_name', 'a.views', 'a.subscribe', 'a.description')
      .limit(limit)
      .offset(offset)
      .then(data => {
        return data;
      });

    const restul = await myData;
    const keyCount = Number(restulcount.count);
    const query = buildFilter(filter);
    const result = filterData(restul, query);

    const response = getPagingData(result, page, limit, keyCount);
    return response;

  } catch (e) {
    debug(e);
  }
}

async function selectVideosHistoryFilter(req, res) {
  const page = req.query.page;
  const size = req.query.limit;
  const title = req.query.title ? req.query.title.toLowerCase() : '';
  const id = req.params.id ? req.params.id : '';
  const { username } = req.query;
  const _username = username ? username : '';
  const filter = {
    username: _username
  };
  const { limit, offset } = getPagination(page, size);
  try {
    const mydatacount = knex('vw_videos_history as a')
      .count('a.id AS count')
      .where('a.user_watch_id', id)
      .whereRaw(`LOWER(a.title) LIKE ?`, `%${title}%`)
      .where('a.video_status', true)
      .then(data => {
        return data;
      });
    const [restulcount] = await mydatacount;
    const myData = knex('vw_videos_history as a')
      .leftJoin('videos_tags as b', 'a.video_id', 'b.video_id')
      .select(['a.rownumvideo'
        , 'a.rownum'
        , 'a.id'
        , 'a.email_address'
        , 'a.username'
        , 'a.phone_number'
        , 'a.email_verified_at'
        , 'a.user_status'
        , 'a.user_created_at'
        , 'a.image_path'
        , 'a.image_cover_path'
        , 'a.video_id'
        , 'a.video_patch'
        , 'a.thumbnail'
        , 'a.video_status'
        , 'a.title'
        , 'a.filename'
        , 'a.destination'
        , 'a.originalname'
        , 'a.size'
        , 'a.video_created_at'
        , 'a.duration'
        , 'a.time_agos'
        , 'a.provider_name'
        , 'a.views'
        , 'a.subscribe'
        , 'a.description'
        , 'a.created_at'
        , knex.raw('ARRAY_AGG(b.title_tags)as title_tags')
      ])
      .where('a.user_watch_id', id)
      .whereRaw(`LOWER(a.title) LIKE ?`, `%${title}%`)
      .where('a.video_status', true)
      .orderBy('a.created_at', 'desc')
      .groupBy('a.rownum', 'a.id', 'a.created_at', 'a.email_address', 'a.username', 'a.phone_number', 'a.email_verified_at', 'a.user_status', 'a.user_created_at'
        , 'a.image_path', 'a.image_cover_path', 'a.video_id', 'a.video_patch', 'a.thumbnail', 'a.video_status', 'a.title', 'a.filename', 'a.destination', 'a.originalname'
        , 'a.size', 'a.video_created_at', 'a.duration', 'a.time_agos', 'a.provider_name', 'a.views', 'a.rownumvideo', 'a.subscribe', 'a.description')
      .limit(limit)
      .offset(offset)
      .then(data => {
        return data;
      });

    const restul = await myData;
    const keyCount = Number(restulcount.count);
    const query = buildFilter(filter);
    const result = filterData(restul, query);

    const response = getPagingData(result, page, limit, keyCount);
    return response;

  } catch (e) {
    debug(e);
  }
}


async function selectSearchTitleFilter(req, res) {
  const page = req.query.page;
  const size = req.query.limit;
  const title = req.query.title ? req.query.title.toLowerCase() : null;
  const { type } = req.query;
  const _type = type ? type : '';
  const filter = {
    type: _type
  };
  const { limit, offset } = getPagination(page, size);
  try {
    const myData = knex('vw_search_title')
      .select('title', 'type')
      .whereRaw(`LOWER(title) LIKE ?`, `%${title}%`)
      .groupBy('title', 'type')
      .limit(limit)
      .offset(offset)
      .then(data => {
        return data;
      });

    const restul = await myData;
    const keyCount = Object.keys(restul).length;
    const query = buildFilter(filter);
    const result = filterData(restul, query);
    const response = getPagingData(result, page, limit, keyCount);
    return response;
  } catch (e) {
    debug(e);
  }
}

async function selectDeleteVideo(req, res) {
  const id = req.params.id ? req.params.id : '';
  try {
    const myData = await knex('videos')
      .innerJoin('videos_action', 'videos.id', 'videos_action.videoid')
      .innerJoin('videos_history', 'videos.id', 'videos_history.video_id')
      .innerJoin('videos_tags', 'videos.id', 'videos_tags.video_id')
      .innerJoin('videos_views', 'videos.id', 'videos_views.video_id')
      .innerJoin('videos_watch_tags', 'videos.id', 'videos_watch_tags.video_watch_id')
      .where('videos.id', id)
      .del();
    if (myData == 1) {
      return [{ status: true, videoid: id, message: "Successfully !" }];
    } else {
      return null;
    }
  } catch (e) {
    debug(e);
    return e;
  }
}

async function selectDeleteVideoPlaylist(req, res) {
  const id = req.params.id ? req.params.id : '';
  try {
    const myData = await knex('videos_playlist')
      .where('id', id)
      .del();
      const myDatalist = await knex('videos_playlist_add')
      .where('playlist_id', id)
      .del();
    if (myData == 1 && myDatalist == 1) {
      return [{ status: true, videoid: id, message: "Successfully !" }];
    } else {
      return null;
    }
  } catch (e) {
    debug(e);
    return e;
  }
}

async function selectSearchVideoFilter(req, res) {
  const page = req.query.page;
  const size = req.query.limit;
  const title = req.query.title ? req.query.title.toLowerCase() : '';
  const { rownum, id, username, phone_number, video_id } = req.query;
  const _id = id ? id : '';
  const _video_id = video_id ? video_id : '';
  const _rownum = rownum ? rownum : '';
  const _username = username ? username : '';
  const _phone_number = phone_number ? phone_number : '';

  const filter = {
    id: _id,
    video_id: _video_id,
    rownum: _rownum,
    username: _username,
    phone_number: _phone_number
  };
  const { limit, offset } = getPagination(page, size);
  try {
    const mydatacount = knex('vw_videos_master as a')
      .rightJoin('vw_search_title as c', 'c.video_id', 'a.video_id')
      .count('a.id AS count')
      .whereRaw(`LOWER(c.title) LIKE ?`, `%${title}%`)
      .where('a.video_status', true)
      .then(data => {
        return data;
      });
    const [restulcount] = await mydatacount;
    debug(`LOWER(a.title) LIKE ?`, `%${title}%`);
    const myData = knex('vw_videos_master as a')
      .leftJoin('videos_tags as b', 'a.video_id', 'b.video_id')
      .rightJoin('vw_search_title as c', 'c.video_id', 'a.video_id')
      .select(['a.rownum'
        , 'a.id'
        , 'a.email_address'
        , 'a.username'
        , 'a.phone_number'
        , 'a.email_verified_at'
        , 'a.user_status'
        , 'a.user_created_at'
        , 'a.image_path'
        , 'a.image_cover_path'
        , 'a.video_id'
        , 'a.video_patch'
        , 'a.thumbnail'
        , 'a.video_status'
        , 'a.title'
        , 'a.filename'
        , 'a.destination'
        , 'a.originalname'
        , 'a.size'
        , 'a.video_created_at'
        , 'a.duration'
        , 'a.time_agos'
        , 'a.provider_name'
        , 'a.views'
        , 'a.subscribe'
        , 'a.description'
        , knex.raw('ARRAY_AGG(b.title_tags)as title_tags')
      ])
      .whereRaw(`LOWER(c.title) LIKE ?`, `%${title}%`)
      .where('a.video_status', true)
      .orderBy('a.views', 'desc')
      .groupBy('a.rownum', 'a.id', 'a.email_address', 'a.username', 'a.phone_number', 'a.email_verified_at', 'a.user_status', 'a.user_created_at'
        , 'a.image_path', 'a.image_cover_path', 'a.video_id', 'a.video_patch', 'a.thumbnail', 'a.video_status', 'a.title', 'a.filename', 'a.destination', 'a.originalname'
        , 'a.size', 'a.video_created_at', 'a.duration', 'a.time_agos', 'a.provider_name', 'a.views', 'a.subscribe', 'a.description')
      .limit(limit)
      .offset(offset)
      .then(data => {
        return data;
      });

    const restul = await myData;
    const keyCount = Number(restulcount.count);
    const query = buildFilter(filter);
    const result = filterData(restul, query);
    const response = getPagingData(result, page, limit, keyCount);
    return response;

  } catch (e) {
    debug(e);
  }
}

async function insertSubscribe(value) {
  const myData = knex
    .from('videos_channel')
    .select('*')
    .where('userid_channel', value.userid_channel)
    .where('subscribe_id', value.subscribe_id)
    .then(data => {
      return data;
    });
  const restul = await myData;
  const insertValue = {
    userid_channel: value.userid_channel,
    subscribe_id: value.subscribe_id,
    created_at: new Date(),
    updated_at: new Date()
  }
  if (restul.length == 1) {
    return deleteSubscribe({
      userid_channel: value.userid_channel,
      subscribe_id: value.subscribe_id
    });
  } else {
    const video = await knex('videos_channel').insert(insertValue);
    const result = video.rowCount == 1 ? { status: true, message: "Subscription added" } : { status: false, message: "Error" }
    return result;
  }

};

async function deleteSubscribe(value) {
  try {
    const myData = await knex('videos_channel')
      .where(value)
      .del();
    if (myData == 1) {
      return { status: false, message: "Unsubscribed" };
    } else {
      return null;
    }
  } catch (e) {
    debug(e);
    return e;
  }
}

async function selectSubscribe(req, res) {
  const userid_channel = req.params.userid_channel ? req.params.userid_channel : '';
  const subscribe_id = req.params.subscribe_id ? req.params.subscribe_id : '';
  try {
    return insertSubscribe({
      userid_channel: userid_channel,
      subscribe_id: subscribe_id
    });
  } catch (e) {
    debug(e);
    return e;
  }
}

async function selectSubscribeCheck(req, res) {
  const userid_channel = req.params.userid_channel ? req.params.userid_channel : '';
  const subscribe_id = req.params.subscribe_id ? req.params.subscribe_id : '';
  try {
    const myData = knex
      .from('videos_channel')
      .select('*')
      .where('userid_channel', userid_channel)
      .where('subscribe_id', subscribe_id)
      .then(data => {
        return data;
      });
    const restul = await myData;
    return restul;
  } catch (e) {
    debug(e);
    return e;
  }
}


async function selectChannelVideoInfo(req, res) {
  const user_id = req.params.user_id ? req.params.user_id : '';
  try {
    const myData = knex
      .from('vw_channel_videos_info')
      .select('*')
      .where('user_id', user_id)
      .then(data => {
        return data;
      });
    const restul = await myData;
    return restul[0];
  } catch (e) {
    debug(e);
    return e;
  }
}

async function selectFeedback(req, res) {
  const { feedback, image } = req.body;
  const video = await knex('feedbacks').insert({
    feedback, image,
    created_at: new Date(),
    updated_at: new Date()
  });
  const result = video.rowCount == 1 ? { status: true, message: "Successfully" } : { status: false, message: "Error" }
  return result;

};

async function selectSlideExplore(req, res) {
  const user_id = req.params.user_id ? req.params.user_id : '';
  try {
    const querySlide = knex
      .from('mt_lov')
      .select('id', 'lov_code', 'name_en', 'type', 'image', 'description')
      .where('status', true)
      .where('type', 'HomeSlide')
      .orderBy('id', 'desc')
      .limit(6)
      .then(data => {
        return data;
      });
    const restulSlide = await querySlide;

    const queryExplore = knex
      .from('mt_lov')
      .select('id', 'lov_code', 'name_en', 'type', 'icon', 'image', 'tags')
      .where('status', true)
      .where('type', 'Explore')
      .orderBy('name_en', 'asc')
      .limit(6)
      .then(data => {
        return data;
      });
    const restulExplore = await queryExplore;
    const queryTopic = knex
      .from('mt_lov')
      .select('id', 'lov_code', 'name_en', 'type', 'tags')
      .where('status', true)
      .where('type', 'Topic')
      .orderBy('order_by', 'asc')
      .limit(20)
      .then(data => {
        return data;
      });
    const restulTopic = await queryTopic;
    const result = {
      slide: restulSlide,
      explore: restulExplore,
      topic: restulTopic
    }
    return result;
  } catch (e) {
    debug(e);
    return e;
  }
}

async function selectExplore(req, res) {
  const page = req.query.page;
  const size = req.query.limit;
  const name_en = req.query.name_en ? req.query.name_en.toLowerCase() : '';
  const { lov_code } = req.query;
  const _lov_code = lov_code ? lov_code : '';
  const filter = {
    lov_code: _lov_code
  };
  const { limit, offset } = getPagination(page, size);
  try {
    const queryLovExplore = knex('mt_lov')
      .select('lov_code', 'name_en', 'type', 'icon', 'image', 'tags')
      .whereRaw(`LOWER(name_en) LIKE ?`, `%${name_en}%`)
      .where('status', true)
      .where('type', 'Explore')
      .orderBy('name_en', 'asc')
      .limit(limit)
      .offset(offset)
      .then(data => {
        return data;
      });

    const restul = await queryLovExplore;
    const keyCount = Object.keys(restul).length;
    const query = buildFilter(filter);
    const result = filterData(restul, query);
    const response = getPagingData(result, page, limit, keyCount);
    return response;
  } catch (e) {
    debug(e);
  }
}

async function selectVideoPlaylist(req, res) {
  const { user_id, video_id, title, description, order_by } = req.body;
  try {
    const playlist = await createVideoPlaylist({
      user_id: user_id,
      title: title,
      description: description,
      created_at: new Date(),
      updated_at: new Date(),
    });
    debug('playlist[0]', playlist[0]);
    if (playlist[0]) {
      if (Array.isArray(video_id)) {
        video_id.map(row => {
          createAddVideoPlaylist({
            playlist_id: playlist[0],
            video_id: row,
            order_by: order_by,
            created_at: new Date(),
            updated_at: new Date(),
          })
        });
      } else {
        createAddVideoPlaylist({
          playlist_id: playlist[0],
          video_id: video_id,
          order_by: order_by,
          created_at: new Date(),
          updated_at: new Date(),
        });
      }
    }
    const result = playlist[0] ? { status: true, message: "Successfully" } : { status: false, message: "Error" }
    return result;
  } catch (e) {
    debug(e);
  }
}

async function createVideoPlaylist(data) {
  try {
    if (data.user_id && data.title) {
      let video = await knex('videos_playlist').insert(data).returning('id');
      return video;
    } else {
      return false;
    }
  } catch (e) {
    debug(e);
  }
};

async function createAddVideoPlaylist(data) {
  try {
    let video = await knex('videos_playlist_add').insert(data);
    return video;
  } catch (e) {
    debug(e);
  }
};

module.exports = {
  createVideo,
  downloadFiles,
  selectFilter,
  selectWatchFilter,
  selectChannelFilter,
  selectFilterPopular,
  selectFilterRecommendTags,
  selectFilterLikeUnlike,
  selectFilterLikeUnlikeView,
  selectVideosHistoryFilter,
  selectSearchTitleFilter,
  selectDeleteVideo,
  selectSearchVideoFilter,
  selectSubscribe,
  selectSubscribeCheck,
  selectChannelVideoInfo,
  selectFeedback,
  selectSlideExplore,
  selectExplore,
  selectVideoHistoryRecored,
  VideoHottesTopic,
  selectVideoPlaylist,
  selectFilterVidePlaylist,
  selectFilterVidePlaylistWatch,
  selectFilterVodepPlayList,
  selectDeleteVideoPlaylist
};
