const debug = require('debug')('express:login');
const Knex = require('knex');
const bcrypt = require('bcrypt');
const knexConfig = require('../../db/knexfile');
const { INTERNAL_SERVER_ERROR, SUCCESSFULLY_CREATE } = require('./constants');
const knex = Knex(knexConfig[process.env.NODE_ENV]);
const uniqid = require('uniqid');
const fs = require('fs');
const path = require('path');
const __basedir = path.resolve();
const date_ob = new Date();
const date = ('0' + date_ob.getDate()).slice(-2);
const month = ('0' + (date_ob.getMonth() + 1)).slice(-2);
const year = date_ob.getFullYear();
// const folderName = __basedir + '/resources/image/uploads/' + year + month + date;
const folderName = '../resources/image/uploads/' + year + month + date;

const getPagination = (page, size) => {
  const limit = size ? +size : 10;
  const offset = page ? (page - 1) * limit : 0;

  return { limit, offset };
};

const getPagingData = (data, page, limit, count) => {
  const currentPage = page ? +page : 0;
  const totalOffset = Math.ceil(currentPage / limit - 1);
  return {
    hasNext: true,
    limit: limit,
    offset: totalOffset,
    page: currentPage,
    total: count,
    data,
  };
};

buildFilter = filters => {
  const query = {};
  for (let keys in filters) {
    if (filters[keys].length > 0) {
      query[keys] = filters[keys];
    }
  }
  return query;
};

filterData = (data, query) => {
  const filteredData = data.filter(item => {
    for (let key in query) {
      if (item[key] === undefined || !query[key].includes(item[key])) {
        return false;
      }
    }
    return true;
  });
  return filteredData;
};
/** end pagination---------*/

async function selectFilter(req, res) {
  const page = req.query.page;
  const size = req.query.limit;
  const { id, username, phone_number, email_address } = req.query;
  const _id = id ? id : '';
  const _username = username ? username : '';
  const _phone_number = phone_number ? phone_number : '';
  const _email_address = req.query.email_address ? req.query.email_address : '';

  const filter = {
    id: _id,
    username: _username,
    phone_number: _phone_number,
    email_address: _email_address
  };
  const { limit, offset } = getPagination(page, size);
  try {
    const myData = knex
      .from('vw_users')
      .select('*')
      .orderBy('id', 'desc')
      .limit(limit)
      .offset(offset)
      .then(data => {
        return data;
      });

    const restul = await myData;
    const keyCount = Object.keys(restul).length;
    const query = buildFilter(filter);
    const result = filterData(restul, query);
    const response = getPagingData(result, page, limit, keyCount);
    return response;
  } catch (e) {
    debug(e);
  }
}

async function selectFilterOne(req, res) {
  const id = req.params.id;
  try {
    const myData = knex
      .from('vw_users')
      .select('*')
      .where('id', id)
      .then(data => {
        return data;
      });
    const restul = await myData;
    return restul;
  } catch (e) {
    debug(e);
  };
}

async function getUserForLoginData(email_address, password) {
  if (isNaN(email_address)) {
    where = { email_address: email_address };
  } else {
    where = { phone_number: email_address };
  }
  const [user] = await knex('users')
    .select()
    .where(where)
    .limit(1);
  if (!user) {
    return null;
  }

  const isPasswordValid = await bcrypt.compare(password, user.password);

  if (!isPasswordValid) {
    return null;
  }

  return {
    id: user.id,
    username: user.email_address,
    created_at: user.created_at,
  };
}

async function getUser(query) {
  const [user] = await knex('users')
    .select('id', 'username', 'email_address', 'phone_number', 'status', 'created_at')
    .where(query)
    .limit(1);
  return user;
}

async function updateUser(folderName, image, image_cover) {

}

async function updateUser(query) {
  const { username, phone_number, status, image, image_cover } = query.body;
  const { id } = query.params;
  // debug({ username, phone_number, status, image, image_cover });

  if (image) {
    createfolderName(folderName);
    var base64Data = image.replace(/^data:image\/png;base64,/, "");
    var nameIMG = `${year + month + date}-${uniqid()}_bgetting_profile.png`;
    require("fs").writeFile(folderName + `/${nameIMG}`, base64Data, 'base64', function (err) {
      console.log(err);
    });
    value = {
      image: image,
      image_path: `${year + month + date}/${nameIMG}`
    }
  } else if (image_cover) {
    createfolderName(folderName);
    var base64Data = image_cover.replace(/^data:image\/png;base64,/, "");
    var nameIMG = `${year + month + date}-${uniqid()}_bgetting_profile.png`;
    require("fs").writeFile(folderName + `/${nameIMG}`, base64Data, 'base64', function (err) {
      console.log(err);
    });
    value = {
      image_cover: image_cover,
      image_cover_path: `${year + month + date}/${nameIMG}`
    }
  } else {
    value = {
      username: username,
      phone_number: phone_number,
      status: status
    }
  }

  // debug(value);
  const user = await knex('users')
    .update(value)
    .update('updated_at', new Date())
    .where({ id })
    .then(row => {
      if (row > 0) {
        return { messages: SUCCESSFULLY_CREATE };
      }
      return { messages: INTERNAL_SERVER_ERROR };
    });

  return user;
}

async function createfolderName(folderName) {
  if (!fs.existsSync(folderName) && fs.mkdirSync(folderName)) {
    fs.mkdirSync(folderName);
  }
}


async function getUserById(id) {
  return getUser({ id });
}

async function updateUserById(value) {
  return updateUser(value);
}

module.exports = {
  getUserForLoginData,
  getUserById,
  updateUserById,
  selectFilter,
  selectFilterOne,
};
