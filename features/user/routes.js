const { wrap } = require('async-middleware');
const router = require('express').Router();
const  authJwt  = require("../../middlewares/authJwt");
const verifyRequestPhone = require('./commands/verify-request-phone');
const redirect = require('./commands/redirect-to-user');

router.get('/:id', wrap(authJwt.verifyToken) , wrap(redirect.findOne));
router.get('/', wrap(authJwt.verifyToken) , wrap(redirect.findAll));
router.post('/:id', wrap(authJwt.verifySecretHeader), wrap(verifyRequestPhone), wrap(authJwt.verifyToken) , wrap(redirect.userUpdate));
module.exports = router;
