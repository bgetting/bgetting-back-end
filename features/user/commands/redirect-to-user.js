const debug = require('debug')('express:login');
const { FETCH_INFO_ERROR_MESSAGE } = require('../constants');
const { 
  updateUserById,
  selectFilter,
  selectFilterOne
} = require('../repository');

const findAll = async (req, res) => {
  const data = await selectFilter(req);
  if (data) {
    if (data != 0) {
      return res.send(data);
    } else {
      return res.status(500).send({ success: false, messages: FETCH_INFO_ERROR_MESSAGE });
    }
  } else {
    return res.status(500).send({ success: false, messages: FETCH_INFO_ERROR_MESSAGE });
  }
};

const findOne = async (req, res) => {
  const data = await selectFilterOne(req);
  if (data) {
    if (data != 0) {
      return res.send(data);
    } else {
      return res.status(500).send({ success: false, messages: FETCH_INFO_ERROR_MESSAGE });
    }
  } else {
    return res.status(500).send({ success: false, messages: FETCH_INFO_ERROR_MESSAGE });
  }
};

async function userUpdate(req, res) {
  let userInfo;
  try {
    userInfo = await updateUserById(req);
  } catch (getUserError) {
    const messages = {
      errors: {
        databaseError: FETCH_INFO_ERROR_MESSAGE,
      },
    };
    return res.status(500).send({ success: false, messages });
  }
  return res.send({
    success: true,
    user: userInfo,
  });
}

const redirect = {
  findAll: findAll,
  findOne: findOne,
  userUpdate: userUpdate,
};
module.exports = redirect;
