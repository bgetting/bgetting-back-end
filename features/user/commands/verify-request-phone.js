const Joi = require('joi');

const constants = require('../constants');

const { PHONE_NUMBER_MIN, PHONE_NUMBER_MAX } = constants;

const schema = Joi.object().keys({
  // id: Joi.string().required(),
  phone_number:Joi.string().regex(/^[0-9]{10}$/).min(PHONE_NUMBER_MIN).max(PHONE_NUMBER_MAX),
  username: Joi.string(),
  image: Joi.string(),
  image_path: Joi.string(),
  image_cover: Joi.string(),
  image_cover_path: Joi.string(),
  status: Joi.boolean(),

});

async function verifyRequestPhone(req, res, next) {
  let payloadValidation;
  try {
    payloadValidation = await Joi.validate(req.body, schema, { abortEarly: false });
  } catch (validateRegisterError) {
    payloadValidation = validateRegisterError;
  }
  const { details } = payloadValidation;
  let errors;
  if (details) {
    errors = {};
    details.forEach(errorDetail => {
      const {
        message,
        path: [key],
        type,
      } = errorDetail;
      const errorType = type.split('.')[1];
      errors[key] = constants[`${key.toUpperCase()}_${errorType.toUpperCase()}_ERROR`] || message;
    });
  }

  if (errors) {
    return res.status(400).send({ success: false, messages: { errors }});
  }
  return next();
}

module.exports = verifyRequestPhone;
