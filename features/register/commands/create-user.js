const debug = require('debug')('express:login');
const registerRepo = require('../repository');

async function createUser(req, res) {
  let user = {};
  const registerSuccessMessage = 'You have successfully registered, you can now log in.';
  try {
    user = await registerRepo.createUser(req.body);  
  } catch (error) {
    user = error;
  }
  if (user.checkMailPhone) {
    return res.send({ success: true, messages: registerSuccessMessage });
  }
  const databaseError =
  user.code === '23505' ? 'The user has already create.' : 'Something went wrong.';
  return res.status(500).send({ success: false, messages:  databaseError  });
}

async function createResetPassword(req, res) {
  let user = {};
  const registerSuccessMessage = 'Your password has been changed successfully';
  try {
    user = await registerRepo.createResetPassword(req.body);  
  } catch (error) {
    user = error;
  }
  if (user.checkMailPhone) {
    return res.send({ success: true, messages: registerSuccessMessage });
  }
  const databaseError =
  user.code === '23505' ? 'This email has not been registered' : 'Something went wrong.';
  return res.status(500).send({ success: false, messages:  databaseError  });
}

module.exports = {
  createUser,
  createResetPassword,
};
