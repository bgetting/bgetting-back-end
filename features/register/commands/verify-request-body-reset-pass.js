const Joi = require('joi');
const debug = require('debug')('express:login');
const constants = require('../constants');

const {
  NAME_MIN,
  NAME_MAX,
  PASSWORD_MAX,
  PASSWORD_MIN,
} = constants;

const schema = Joi.object().keys({
  provider: Joi.number().required(),
  username: Joi.string()
    .min(NAME_MIN)
    .max(NAME_MAX)
    .required(),
  password: Joi.string()
    .min(PASSWORD_MIN)
    .max(PASSWORD_MAX)
    .required(),
  confirmPassword: Joi.any().valid(Joi.ref('password')).required().options({ language: { any: { allowOnly: 'must match password' } } })
});

async function validateResetPassword(req, res, next) {
  
  let payloadValidation;
  try {
    payloadValidation = await Joi.validate({ provider: req.body.provider, username: req.body.username, password: req.body.password, confirmPassword: req.body.confirmPassword }, schema, { abortEarly: false });
  // debug(payloadValidation)
  } catch (validateRegisterError) {
    payloadValidation = validateRegisterError;
  }
  const { details } = payloadValidation;
  let errors;
  // debug(details)
  if (details) {
    errors = {};
    details.forEach(errorDetail => {
      errors = errorDetail.message;
    });
  }

  if (errors) {
    return res.status(400).send({ success: false, messages: errors });
  }
  return next();
}

module.exports = validateResetPassword;
