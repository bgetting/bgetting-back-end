const { wrap } = require('async-middleware');
const router = require('express').Router();

const requestBodyValidation = require('./commands/verify-request-body');
const validateResetPassword = require('./commands/verify-request-body-reset-pass');
const { createUser, createResetPassword } = require('./commands/create-user');
const  authJwt  = require("../../middlewares/authJwt");

router.post('/', wrap(authJwt.verifySecret) ,wrap(requestBodyValidation), wrap(createUser));
router.post('/reset-password', wrap(authJwt.verifySecretHeader) ,wrap(validateResetPassword), wrap(createResetPassword));
module.exports = router;
