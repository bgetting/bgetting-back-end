const debug = require('debug')('express:login');
const bcrypt = require('bcrypt');
const knex = require('../../db');

async function createUser(req, res) {
  const { provider, userNameProvider, password } = req;
  const hashedPass = await bcrypt.hash(password, 5);
  if (isNaN(userNameProvider)) {
    value = {
      password: hashedPass,
      email_address: userNameProvider.toLowerCase(),
      created_by: userNameProvider.toLowerCase(),
      created_at: new Date(),
      updated_at: new Date(),
      email_verified_at: new Date(),
    };
    mailPhone = {
      email_address: userNameProvider.toLowerCase(),
    };
  } else {
    value = {
      password: hashedPass,
      phone_number: userNameProvider.toLowerCase(),
      created_by: userNameProvider.toLowerCase(),
      created_at: new Date(),
      updated_at: new Date(),
      email_verified_at: new Date(),
    };
    mailPhone = {
      phone_number: userNameProvider.toLowerCase(),
    };
  }

  const checkMailPhone = await knex('users')
    .select('id', 'email_address', 'phone_number')
    .where(mailPhone)
    .limit(1);
  if (checkMailPhone.length === 0) {
    const user = await knex('users')
      .insert(value)
      .returning('id')
      .then(row => {
        return knex('auth_user_provider')
          .insert({
            auth_provider_id: provider,
            auth_user_id: row[0],
            created_by: userNameProvider.toLowerCase(),
            created_at: new Date(),
            updated_at: new Date(),
          })
          .returning(['auth_provider_id'])
          .then(data => {
            return data;
          });
      });
    await knex('register_verify')
      .where({ phone_number: userNameProvider.toLowerCase(), status: true })
      .del();
    return { user, checkMailPhone };
  } else {
    return { code: '23505' };
  }
}

async function createResetPassword(req, res) {
  const { provider, password, username } = req;
  const hashedPass = await bcrypt.hash(password, 5);
  if (provider === 1) {
    checkusername = {
      phone_number: username,
    };
  } else if (provider === 2) {
    checkusername = {
      email_address: username,
    };
  }
  const checkMailPhone = await knex('users')
    .select('id', 'email_address', 'phone_number')
    .where(checkusername)
    .limit(1);
  if (checkMailPhone.length === 1) {
    const valueUpdate = {
      password: hashedPass,
      status: true,
      updated_at: new Date(),
    };
    const user = await knex('users')
      .update(valueUpdate)
      .where(checkusername);

    return { user, checkMailPhone };
  } else {
    return { code: '23505' };
  }
}

module.exports = {
  createUser,
  createResetPassword,
};
