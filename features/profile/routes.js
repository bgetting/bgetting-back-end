const { wrap } = require('async-middleware');
const router = require('express').Router();

const requestBodyValidation = require('./commands/verify-request-body');
const updateUserInfo = require('./commands/update-user-info');
const authJwt = require('../../middlewares/authJwt');

router.post('/update-profile-info'/*, wrap(authJwt.verifyToken)*/, wrap(requestBodyValidation), wrap(updateUserInfo));

module.exports = router;
