const INTERNAL_SERVER_ERROR = 'These credentials do not match our records.';
const NO_OAUTHSECRET = 'No oauthSecret.';
const NO_OAUTHID = 'No oauthId.';
const NO_TOKEN_PRIVIDED = 'No token provided.';
const UNAUTHORIZED = 'Unauthorized.';

module.exports = {
  INTERNAL_SERVER_ERROR,
  NO_OAUTHSECRET,
  NO_OAUTHID,
  NO_TOKEN_PRIVIDED,
  UNAUTHORIZED,
};
