module.exports = {
  apps: [
    {
      name: 'bgetting',
      script: './bin/www',

      watch: true,
      ignore_watch: ['public/**/*', 'views/**/*.ejs'],
    },
  ],
};
